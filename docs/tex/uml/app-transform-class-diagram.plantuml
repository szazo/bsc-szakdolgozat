@startuml

namespace TransformServer {

  namespace Domain {
    class TransformCommandHandler {
      + execute(): void
    }

    interface TransformRepository {
      findNextNonSyncedFile(): NonSyncedFileResult
      save(transform: Transform)
    }

    class Transform {
      + create(rawTelemetryFileId: int, rawTelemetryFileRevision: DateTime, targetFlightId: int): void
      + setAsTransformed(): void
      + setAsError(): void
    }

    class TransformState {
      + id: int
      + rawTelemetryFileId: int
      + rawTelemetryFileRevision: DateTime
      + targetFlightId: int
      + phase: TransformPhase
    }

    enum TransformPhase {
      none,
      transforming,
      transformed,
      error
    }

    TransformState --> TransformPhase
    Transform *-- TransformState
    TransformRepository --> Transform

    namespace Operators {
      class NetworkOperator {
        + create(flightId: int): OperatorFunction<RawTelemetry, void>
      }

      TransformServerApi.ConverterModule <------ NetworkOperator
      TransformServerApi.FlightMetaOperator <----- NetworkOperator
    }

    TransformCommandHandler --> TransformServer.Domain.Operators.NetworkOperator
  }

  namespace App {
    class TransformJob {
      + start(): void
      + stop(): void
    }

    TransformJob --> TransformServer.Domain.TransformCommandHandler
  }

  namespace Db {
    class TransformRepository {
    }

    TransformServer.Domain.TransformRepository <|-up- TransformRepository
  }

  namespace Bootstrap {

    class TransformBootstrap {
      +start()
      +stop()
    }

    TransformServer.App.TransformJob <-down- TransformBootstrap
  }
}

namespace TransformServerApi {

  interface ConverterModule {
    + messageBase: number
    + operator: OperatorFunction
  }

  interface FlightMeta {
    + startTime: DateTime
    + lengthUS: int
    + maxAltitudeMeters: float
  }

  interface FlightMetaOperator {
    create(): OperatorFunction
  }

  FlightMeta <-- FlightMetaOperator
}

@enduml

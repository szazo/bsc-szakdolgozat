@startuml

namespace TelemetryApi {

  interface Telemetry {
    + orientation: Orientation
    + gyro: Axes
    + accel: Axes
    + position: GpsPosition
    + altitudeMeters: float
    + heightMeters: float
    + trueTrackDegrees: float
    + magneticTrackDegrees: float
    + groundSpeedKnots: float
  }

  interface Orientation {
    + x: float
    + y: float
    + z: float
    + w: float
  }

  interface Axes {
    + x: float
    + y: float
    + z: float
  }

  interface GpsPosition {
    +  latitude: float
    + longitude: float
  }

  Telemetry --> Orientation
  Telemetry --> Axes
  Telemetry --> GpsPosition
}

package TelemetryServer {

  namespace Domain {

    interface TelemetryRepository {
      + queryTelemetry(flightId: int, startTime: bigint, endTime: bigint): TelemetryEntry[]
      + insertTelemetry(flighId: int, telemetries: TelemetryEntry[]): void
      + saveFlight(flight: Flight): int
      + loadFlight(id: int): Flight
      + flights(phase: FlightPhase): Flight[]
    }

    interface TelemetryEntry {
      + time: bigint
      + data: Telemetry
    }

    enum FlightPhase {
      none
      draft
      valid
      deleted
    }

    class Flight {
      + id: int
      + phase: FlightPhase
      + startTime: int
      + endTime: int
      + length: int
      + maxAltitudeMeters: float
    }

    Flight --> FlightPhase
    TelemetryRepository --> TelemetryEntry
    TelemetryRepository -left-> Flight
    TelemetryEntry --> TelemetryApi.Telemetry
  }

  namespace Db {
    class TelemetryRepository {
    }

    Domain.TelemetryRepository <|-up- TelemetryRepository
  }

}

@enduml

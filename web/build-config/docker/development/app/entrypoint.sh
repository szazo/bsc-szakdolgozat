#!/bin/sh

echo "starting"

yarn global add lerna
lerna bootstrap

export DEBUG=av:*
# export ROARR_LOG=true

yarn run dev:build
yarn run dev:server:monitor

while true; do sleep 3; done

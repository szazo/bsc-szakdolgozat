#!/bin/sh

echo 'Working directory is '$PWD
echo 'waiting for sql server...'
while ! nc -vz db 5432; do sleep 3; done
echo 'sql server found'

./migrate-db.sh
node services/server/server/dist/index.js

#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

eval PRIVATE_KEY="\$$CI_SWARM_DEPLOY_SSH_KEY_VARIABLE"

# ssh-agent
echo "Preparing private key variable $CI_SWARM_DEPLOY_SSH_KEY_VARIABLE for $CI_SWARM_DEPLOY_HOST:$CI_SWARM_DEPLOY_SSH_PORT"
eval $(ssh-agent -s)
echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
mkdir -p ~/.ssh
chmod 700 ~/.ssh
ssh-keyscan -p $CI_SWARM_DEPLOY_SSH_PORT $CI_SWARM_DEPLOY_HOST >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

# deploy

export DOCKER_HOST="ssh://$CI_SWARM_DEPLOY_USER"@"$CI_SWARM_DEPLOY_HOST:$CI_SWARM_DEPLOY_SSH_PORT"
echo "Logging into $CI_REGISTRY from $DOCKER_HOST ..."
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
echo "Deploying to $DOCKER_HOST ..."
docker stack deploy --with-registry-auth --compose-file=$CI_SWARM_DEPLOY_COMPOSE_FILE $CI_PCFG_SERVICE_NAME

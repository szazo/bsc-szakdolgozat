import { inject, injectable } from "inversify";
import { TelemetryRepository, TelemetryRepositorySymbol, FlightPhase } from '@av/telemetry-server'
import { mapFlight } from "./flight.query-handler";

@injectable()
export class FlightsQueryHandler {

  constructor(
    @inject(TelemetryRepositorySymbol)
    private telemetryRepositry: TelemetryRepository) {
  }
  
  async execute() {
    const flights = await this.telemetryRepositry.flights(FlightPhase.active)

    return flights.map(x => mapFlight(x))
  }
}

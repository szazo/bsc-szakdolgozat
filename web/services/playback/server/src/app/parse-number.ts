
export function parseNumber(input: string) {
  return input && !isNaN(parseInt(input)) ? parseInt(input) : 0
}

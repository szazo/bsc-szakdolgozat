import { inject, injectable } from "inversify";
import { TelemetryRepository, TelemetryRepositorySymbol } from '@av/telemetry-server'
import * as Api from '@av/playback-api'
import { parseNumber } from './parse-number'

@injectable()
export class DeleteFlightCommandHandler {

  constructor(
    @inject(TelemetryRepositorySymbol)
    private telemetryRepositry: TelemetryRepository) {
  }

  async execute(flightId: string): Promise<Api.DeleteFlightCommandResult> {

    const flightIdNumber = parseNumber(flightId)
    if (!flightIdNumber) {
      return Api.DeleteFlightCommandResultCode.flightNotFound
    }

    const flight = await this.telemetryRepositry.loadFlight(flightIdNumber)
    if (!flight) {
      return Api.DeleteFlightCommandResultCode.flightNotFound
    }

    flight.setAsDeleted()
    await this.telemetryRepositry.saveFlight(flight)

    return Api.DeleteFlightCommandResultCode.success
  }
}

import { inject, injectable } from "inversify";
import { TelemetryRepository, TelemetryRepositorySymbol, Flight } from '@av/telemetry-server'
import * as Api from '@av/playback-api'
import { parseNumber } from "./parse-number";

export function mapFlight(flight: Flight): Api.Flight {
  return {
    id: flight.id,
    lengthUS: flight.lengthUS,
    startTime: flight.startTime ? flight.startTime.toISOString() : '',
    endTime: flight.endTime ? flight.endTime.toISOString() : '',
    maxAltitudeMeters: flight.maxAltitudeMeters
  }
}

@injectable()
export class FlightQueryHandler {

  constructor(
    @inject(TelemetryRepositorySymbol)
    private telemetryRepositry: TelemetryRepository) {
  }

  async execute(flightId: string): Promise<Api.FlightQueryResult> {

    const flightIdNumber = parseNumber(flightId)

    const flight = await this.telemetryRepositry.loadFlight(flightIdNumber)
    if (!flight || !flight.isActive) {
      return {
        code: Api.FlightQueryResultCode.flightNotFound
      }
    }

    return {
      code: Api.FlightQueryResultCode.success,
      result: mapFlight(flight)
    }
  }
}

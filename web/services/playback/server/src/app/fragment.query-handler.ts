import { inject, injectable } from "inversify";
import { TelemetryRepository, TelemetryRepositorySymbol } from '@av/telemetry-server'
import * as Api from '@av/playback-api'

@injectable()
export class FragmentQueryHandler {

  constructor(
    @inject(TelemetryRepositorySymbol)
    private telemetryRepositry: TelemetryRepository) {
  }

  async execute(flightId: string, startTime: string, endTime: string): Promise<Api.FragmentQueryResult> {

    const flightIdNumber = this.parseNumber(flightId)
    const startTimeNumber = this.parseNumber(startTime)
    const endTimeNumber = this.parseNumber(endTime)

    console.log('flightId', flightId, startTime, endTime)
    if (flightIdNumber === null || startTimeNumber === null || endTimeNumber === null) {
      return {
        code: Api.FragmentQueryResultCode.invalidArguments,
        startTime: -1,
        endTime: -1,
        frames: []
      }
    }

    const entries = await this.telemetryRepositry.queryTelemetry(flightIdNumber,
                                                                 BigInt(startTimeNumber),
                                                                 BigInt(endTimeNumber))
    console.log('Entries', entries.length)
    if (entries.length > 0) {
      return {
        code: Api.FragmentQueryResultCode.success,
        startTime: Number(entries[0].time),
        endTime: Number(entries[entries.length - 1].time),
        frames: entries
      }
    }

    return {
      code: Api.FragmentQueryResultCode.invalidInterval,
      startTime: -1,
      endTime: -1,
      frames: []
    }
  }

  private parseNumber(text: string) {
    return text && !isNaN(parseInt(text)) ? parseInt(text) : null
  }
}

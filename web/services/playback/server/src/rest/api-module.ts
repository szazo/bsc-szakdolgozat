import { injectable } from 'inversify'
import * as express from 'express'
import * as App from '../app'

@injectable()
export class PlaybackApiModule {

  constructor(
    private flightsQuery: App.FlightsQueryHandler,
    private flightQuery: App.FlightQueryHandler,
    private deleteFlightCommand: App.DeleteFlightCommandHandler,
    private playbackFragmentQuery: App.FragmentQueryHandler) {

  }

  initialize(router: express.Router) {

    const tryExecute = async <T>(func: () => Promise<T>,
                        _req: express.Request, res: express.Response, next: express.NextFunction) => {

      try {
        return res.json(await func())
      } catch (err) { return next(err) }
    }
    
    router.get('/flights', async (req, res, next) => {
      return tryExecute(() => this.flightsQuery.execute(), req, res, next)
    })

    router.get('/flight/:flightId', async (req, res, next) => {
      return tryExecute(() => this.flightQuery.execute(req.params.flightId), req, res, next)
    })

    router.delete('/flight/:flightId', async (req, res, next) => {
      return tryExecute(() => this.deleteFlightCommand.execute(req.params.flightId), req, res, next)
    })

    router.get('/playback/:flightId/:startTime/:endTime', async (req, res, next) => {
      return tryExecute(() => this.playbackFragmentQuery.execute(
        req.params.flightId, req.params.startTime, req.params.endTime), req, res, next)
    })
  }
}

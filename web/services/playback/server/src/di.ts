import * as IoC from 'inversify'
import { ExpressApiModuleSymbol, ExpressApiModule } from '@av/server-api'
import * as BootstrapApi from '@av/bootstrap'
import * as App from './app'
import * as Rest from './rest'

export const playbackModule = new IoC.ContainerModule(bind => {

  // App
  bind(App.FlightsQueryHandler).toSelf()
  bind(App.FlightQueryHandler).toSelf()
  bind(App.DeleteFlightCommandHandler).toSelf()
  bind(App.FragmentQueryHandler).toSelf()

  // REST
  bind<ExpressApiModule>(ExpressApiModuleSymbol).to(Rest.PlaybackApiModule).inSingletonScope()
})

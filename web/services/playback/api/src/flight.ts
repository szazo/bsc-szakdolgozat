
export interface Flight {
  id: number,
  lengthUS: number
  startTime: string
  endTime: string,
  maxAltitudeMeters: number
}

import { TelemetryEntry } from '@av/telemetry-api'

export enum FragmentQueryResultCode {
  success,
  invalidArguments,
  invalidInterval
}

export interface FragmentQueryResult {
  code: FragmentQueryResultCode
  startTime: number
  endTime: number
  frames: TelemetryEntry[]
}

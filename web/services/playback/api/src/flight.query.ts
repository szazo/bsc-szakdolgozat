import { Flight } from './flight'

export enum FlightQueryResultCode {
  success,
  flightNotFound
}

export interface FlightQueryResult {
  code: FlightQueryResultCode
  result?: Flight
}


export enum DeleteFlightCommandResultCode {
  success,
  flightNotFound
}

export type DeleteFlightCommandResult = DeleteFlightCommandResultCode

import * as React from 'react'
import * as Router from 'react-router'
import * as RouterDom from 'react-router-dom'
import * as Api from '@av/playback-api'
import { Player } from './player'
import { FlightList, FlightListProps } from './flights'

export const Playback = () => {

  const params = Router.useParams<{ flightId: string }>()
  const flightIdNumber = !isNaN(parseInt(params.flightId)) ? parseInt(params.flightId) : null

  const [loading, setLoading] = React.useState<boolean>(false)
  const [inited, setInited] = React.useState<boolean>(false)
  const [flights, setFlights] = React.useState<Api.Flight[]>([])
  const [redirect, setRedirect] = React.useState<boolean>(false)

  const fetchData = async () => {
    setLoading(true)
    const response = await fetch(`/api/flights`)
    setLoading(false)
    if (!response.ok) {
      console.error(response.statusText)
      return
    }

    if (!inited) {
      setInited(true)
    }
    const items: Api.Flight[] = await response.json()
    setFlights(items)
  }

  const handleDeleteClick = async (id: number) => {

    if (await callDelete(id)) {
      const index = flights.findIndex(x => x.id === id)
      if (index >= 0) {
        const newFlights = [ ...flights ]
        newFlights.splice(index, 1)
        setFlights(newFlights)

        if (flightIdNumber === id) {
          setRedirect(true)
        }
      }
    }
  }

  React.useEffect(() => { fetchData(); setRedirect(false); }, [redirect])
  
  React.useEffect(() => {
    if (!inited) { return }
    const interval = setInterval(() => fetchData(), 4000)

    return () => clearInterval(interval)
  }, [inited])

  if (redirect) {
    return <RouterDom.Redirect push to={{ pathname: '/playback'}} />
  }

  return <div className="flex flex-row h-full">
    <List activeFlightId={flightIdNumber} flights={flights} onDelete={handleDeleteClick} />
    <div className="flex-grow h-full">
      <Content loading={loading} inited={inited} flightId={flightIdNumber} hasFlight={flights.length > 0} />
    </div>
  </div>
}

const List: React.FC<FlightListProps> = (props) => {
  if (props.flights.length === 0) {
    return null
  }

  return <div className="h-full overflow-auto w-64">
    <FlightList {...props} />
  </div>
}

const CenterMessage: React.FC<{ message: string }> = ({ message }) => {
  return <div className="flex justify-center items-center uppercase text-gray-500 h-full">{message}</div>
}

const PleaseSelectFlight = () => {
  return <CenterMessage message="Please select a flight in the left menu" />
}

const ThereIsNoFlight = () => {
  return <CenterMessage message="There is no recorded flight, please record one" />
}

const Loading = () => {
  return <CenterMessage message="Loading..." />
}

const callDelete = async (id: number) => {
  if (!confirm('Are you sure you want to delete this flight?')) {
    return
  }
  
  const response = await fetch(`/api/flight/${id}`, {
    method: 'DELETE'
  })

  if (response.ok) {
    return true
  }

  console.error(response.statusText)
  return false
}

interface ContentProps {
  loading: boolean
  inited: boolean
  flightId: number | null
  hasFlight: boolean
}

const Content: React.FC<ContentProps> = ({ loading, inited, flightId, hasFlight }) => {

  if (!inited && loading) {
    return <Loading />
  }

  if (!hasFlight) {
    return <ThereIsNoFlight />
  }

  if (!flightId) {
    return <PleaseSelectFlight />
  }

  return <Player flightId={flightId} />
}

import * as React from 'react'
import * as Api from '@av/playback-api'
import { FlightItem } from './flight-item'

export interface FlightListProps {
  flights: Api.Flight[]
  activeFlightId: number | null
  onDelete: (id: number) => void
}

export const FlightList: React.FC<FlightListProps> = ({ activeFlightId, flights, onDelete }) => {

  return <>
    { flights.map(x => <FlightItem key={x.id}
                         flight={x}
                         isActive={x.id === activeFlightId}
                         onDelete={() => onDelete(x.id)} />) }
  </>
}

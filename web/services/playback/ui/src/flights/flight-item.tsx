import * as moment from 'moment'
import * as React from 'react'
import * as RouterDom from 'react-router-dom'
import * as Api from '@av/playback-api'

interface FlightItemProps {
  flight: Api.Flight
  onDelete: (id: number) => void
  isActive?: boolean
}

const activeIcon = <div className="absolute right-0 bottom-0">
<span className="w-8 h-8 p-1">
<i className="fas fa-play fa-sm text-blue-600" /></span> 
</div>

export const FlightItem: React.FC<FlightItemProps> = ({ flight, isActive, onDelete }) => {

  const duration = moment.duration(flight.lengthUS / 1000)
  const activeClass = isActive ? 'border-blue-400' : ''

  return <RouterDom.Link to={`/playback/${flight.id}`}>
    <div className={`h-24 w-56 border-gray-400 rounded border flex flex-col relative p-2 my-2 mx-4 ${activeClass}`}>
      <div className="absolute right-0 top-0">
        <span className="w-8 h-8 p-1" onClick={(e) => { e.preventDefault(); onDelete(flight.id) }}>
          <i className="fas fa-trash-alt fa-sm text-gray-600" /></span> 
      </div>
      {isActive ? activeIcon : null}
      <div className="flex-grow">
        <div className="text-gray-900 font-bold text-lg">{moment(flight.startTime).format('L LTS')}</div>
        <div className="text-gray-700 text-sm">{duration.minutes()} minutes {duration.seconds()} seconds</div>
      </div>
      <div>
        <div className="text-gray-600 text-xs">{flight.maxAltitudeMeters}m maximum altitude</div>
      </div>
      {/* <RouterDom.Link to={`/playback/${flight.id}`}>{flight.id}</RouterDom.Link> */}
    </div>
  </RouterDom.Link>
}

declare module 'react-slider' {

  namespace ReactSlider {
    interface ReactSliderProps {
      min?: number;
      max?: number;
      step?: number;
      minDistance?: number;
      defaultValue?: number | number[];
      value?: number | number[];
      orientation?: 'horizontal' | 'vertical';
      className?: string;
      thumbClassName?: string;
      thumbActiveClassName?: string;
      withBars?: boolean;
      trackClassName?: string;
      pearling?: boolean;
      disabled?: boolean;
      snapDragDisabled?: boolean;
      invert?: boolean;
      onBeforeChange?: (value: number | number[] | undefined | null) => void;
      onChange?: (value: number | number[] | undefined | null) => void;
      onAfterChange?: (value: number | number[] | undefined | null) => void;
      onSliderClick?: (value: number) => void;
      renderTrack?: (props: unknown, state: RenderState) => React.ReactNode
      renderThumb?: (props: unknown, state: RenderState) => React.ReactNode
    }

    interface RenderState {
      index: number
      value: number
    }
  }

  const ReactSlider: React.ComponentClass<ReactSlider.ReactSliderProps>;
  export default ReactSlider;
}

import * as React from 'react'
import * as moment from 'moment'
import * as classNames from 'classnames'
import { PlayerPhase } from '../logic'
import styled from 'styled-components';
import ReactSlider from 'react-slider'

interface ButtonProps {
  onClick: () => void
}

const Button: React.FC<ButtonProps & { icon: string }> =
  ({ onClick, icon }) => <button className={`p-2 fas fa-${icon}`} onClick={onClick} />
const PlayButton: React.FC<ButtonProps> = (props) => <Button {...props} icon="play" />
const PauseButton: React.FC<ButtonProps> = (props) => <Button {...props} icon="pause" />

interface PlayerControlsProps {
  phase: PlayerPhase
  positionMs: number
  lengthMs: number
  onPause: () => void
  onPlay: () => void
  onSeek: (timeMs: number) => void
}

const StyledTrack = styled.div.attrs((props: { index: number }) => {

  const classes = classNames('h-3 cursor-pointer', {
    'bg-blue-400': props.index == 0,
    'bg-gray-200 rounded-l-full': props.index == 1
  })

  return {
    className: classes
  }
})``

const Track = (props: any, state: any) => <StyledTrack {...props} disabled={props.disabled} index={state.index} />

const StyledThumb = styled.div.attrs((_props: { value: number }) => {

  const classes = classNames('rounded-full w-4 h-4 bg-blue-600 cursor-pointer', {
  })

  return {
    className: classes,
  }
})`
margin-top: -2px
`

const Thumb = (props: any, state: ReactSlider.RenderState) => <StyledThumb {...props} disabled={props.disabled} value={state.value} />

export const PlayerControls: React.FC<PlayerControlsProps> = ({ phase, onPause, onPlay, onSeek, positionMs, lengthMs, children }) => {

  let button = null;
  let buffering = null
  switch (phase) {
    case PlayerPhase.playing:
    case PlayerPhase.playingWaitingForFragment:
      button = <PauseButton onClick={onPause} />
      let bufferingText = ''
      if (phase === PlayerPhase.playingWaitingForFragment) {
        bufferingText = 'buffering...'
      }
      buffering = <span className="text-sm text-blue-800 w-24">{bufferingText}</span>
      break
      case PlayerPhase.paused:
      case PlayerPhase.none:
      button = <PlayButton onClick={onPlay} />
      break
      }

      const [seekMs, setSeekMs] = React.useState<number | null>(null)

        const handleSeekChange = (value: number | number[] | undefined | null) => {
          if (!value || value instanceof Array) { return }
          const timeMs = value * lengthMs / 100
          setSeekMs(timeMs)
          onSeek(timeMs)

          console.log('TIME', timeMs, lengthMs, value)
        }

        const timeMs = seekMs != null ? seekMs : positionMs
        const sliderPosition = lengthMs !== 0 ? positionMs / lengthMs * 100 : 0
        
        return <div>
        <ReactSlider
          className="w-full h-3 bg-gray-200"
          renderThumb={Thumb}
          renderTrack={Track}
          defaultValue={0}
          onChange={handleSeekChange}
          value={sliderPosition}
        />
        <div className="flex items-center">
          {button}
          <TimeControl positionMs={timeMs} lengthMs={lengthMs} />
          <span className="px-2">
            {children}
          </span>
          {buffering}
        </div>
        </div>
}

interface TimeProps {
  positionMs: number
  lengthMs: number
}

const TimeControl: React.FC<TimeProps> = ({ positionMs, lengthMs }) => {

  const position = moment.utc(positionMs).format('HH:mm:ss')
  const length = moment.utc(lengthMs).format('HH:mm:ss')
  
  return <span className="text-sm text-gray-800">
    {position} / {length}
  </span>
}

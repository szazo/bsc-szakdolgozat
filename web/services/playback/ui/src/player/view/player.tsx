import * as React from 'react'
import * as classnames from 'classnames'
import { Telemetry, MissingData, fillTelemetryWithMissing } from '@av/telemetry-api'
import { FlightQueryResult, FlightQueryResultCode } from '@av/playback-api'
import { Map, MapMode, ObjectMapPosition, Quaternion, InfoPanel, MapModeButtons } from '@av/map-ui'
import { PlayerFSM, Frame, PlayerPhase, PlayerFragmentSource } from '../logic'
import { PlayerControls } from './player-controls'

interface PlayerProps {
  flightId: number
}

export const Player: React.FC<PlayerProps> = ({ flightId }) => {

  const defaultPosition = {
    longitude: 18.986944444444447,
    latitude: 47.4497222,
    altitudeMeters: 0
  }

  /* const { flightId } = Router.useParams<{ flightId: string }>() */
  const [ objectPosition, setObjectPosition ] = React.useState<ObjectMapPosition>(defaultPosition)
  const [ objectOrientation, setObjectOrientation ] = React.useState<Quaternion>({ x:0, y: 0, z:0, w: 0 })
  const [ currentTelemetry, setCurrentTelemetry ] = React.useState<Telemetry>(fillTelemetryWithMissing({}))
  const [ flyTo, setFlyTo ] = React.useState<ObjectMapPosition | undefined>(undefined)
  const [ mapMode, setMapMode ] = React.useState<MapMode>(MapMode.follow)
  const [ phase, setPhase ] = React.useState<PlayerPhase>(PlayerPhase.none)
  const [ timePosition, setTimePosition ] = React.useState<number>(0)
  const [ inited, setInited ] = React.useState<boolean>(false)
  const [ frame, setFrame ] = React.useState<Frame<Telemetry> | undefined>(undefined)

  React.useEffect(() => {
    if (!frame) {
      return
    }

    const telemetry = frame.data
    if (telemetry.position !== MissingData) {
      const newPosition = {
        latitude: telemetry.position.latitude,
        longitude: telemetry.position.longitude,
        altitudeMeters: telemetry.altitudeMeters !== MissingData &&
                        telemetry.altitudeMeters != 0 ? (telemetry.altitudeMeters - 130) / 2 :
                        objectPosition.altitudeMeters
      }

      setCurrentTelemetry(telemetry)
      setObjectPosition(newPosition)
      
      if (telemetry.orientation !== MissingData) {
        setObjectOrientation(telemetry.orientation)
      }

      if (!inited) {
        setFlyTo({
          latitude: telemetry.position.latitude,
          longitude: telemetry.position.longitude,
          altitudeMeters: 0
        })
        setInited(true)
        console.log('FLYTO', flyTo)
      }
    }

  }, [ frame ])

  const emit = (frame: Frame<Telemetry>) => {
    setFrame(frame)
  }
  
  const logic = React.useRef<PlayerFSM<Telemetry> | null>(null)
  
  React.useEffect(() => {
    console.log('Creating player')

    const fragmentRequestInterval = 1000 * 1000 * 10 // 10 seconds
    const preloadTimeThreshold = 1000 * 1000 * 5 // 5 seconds
    logic.current = new PlayerFSM(new PlayerFragmentSource(flightId),
                                  { emit }, fragmentRequestInterval, preloadTimeThreshold)
    
    logic.current.onPhaseChanged = (phase) => {
      setPhase(phase)
    }
    logic.current.onTimeChanged = (time) => {
      const positionMilliseconds = Math.floor(time / 1000)
      setTimePosition(positionMilliseconds)
    }

    fetchData()

    return function cleanup() {
      console.log('Cleanup previous')
      if (logic.current) {
        logic.current.onPhaseChanged = undefined
        logic.current.onTimeChanged = undefined
        logic.current = null
        setTimePosition(0)
      }
    }
  }, [flightId])

  async function fetchData() {
    const response = await fetch(`/api/flight/${flightId}`)
    if (!response.ok) {
      console.error(response.statusText)
      return
    }

    if (!logic.current) {
      return
    }

    const result = await response.json() as FlightQueryResult
    if (result.code === FlightQueryResultCode.success) {
      logic.current.init(Number(result.result!.lengthUS))
    } else {
      console.error(`Error querying flight: ${result.code}`)
      return
    }

    requestAnimationFrame(update)
  }

  function update(time: number) {

    if (!logic.current) {
      return
    }
    
    logic.current.update(time * 1000)
    requestAnimationFrame(update)
  }

  function recenter() {
    setFlyTo({
      latitude: objectPosition.latitude,
      longitude: objectPosition.longitude,
      altitudeMeters: 0
    })
  }

  function switchMode() {
    setMapMode(mapMode === MapMode.follow ? MapMode.default : MapMode.follow)
  }

  const time = logic.current ?
               { length: logic.current.length, position: logic.current.position } :
               { length: 0, position: 0 }

  const lengthMilliseconds= Math.floor(time.length / 1000)

  return <div className="flex flex-col h-full">
    <div className="flex flex-grow">
      <Map className="flex-grow"
        position={objectPosition}
        orientation={objectOrientation}
        flyTo={flyTo}
        mode={mapMode}
      />
      <InfoPanel input={currentTelemetry} />
    </div>
    <PlayerControls
      positionMs={timePosition} lengthMs={lengthMilliseconds}
      phase={phase}
      onPlay={() => { if (logic.current) { logic.current.play() } } }
      onPause={() => { if (logic.current) { logic.current.pause() } } }
      onSeek={(value) => { if (logic.current) { logic.current.seek(value * 1000); logic.current.play() } }}>
      <MapModeButtons mode={mapMode} onFollowClick={switchMode} onLocateClick={recenter} />
    </PlayerControls>
  </div>
}

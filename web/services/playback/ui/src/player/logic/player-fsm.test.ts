import { expect } from 'chai'
import * as td from 'testdouble'
import { PlayerFSM, FragmentSource, Fragment, PlayerPhase, FrameOutput, FragmentResponse } from './player-fsm'

interface SampleData { value: number}

describe('player', () => {

  let fragmentSourceMock: FragmentSource<SampleData>
  let frameOutputMock: FrameOutput<SampleData>

  before(() => {
    fragmentSourceMock = td.object<FragmentSource<SampleData>>()
    frameOutputMock = td.object<FrameOutput<SampleData>>()
  })

  afterEach(() => {
    td.reset()
  })

  const LENGTH = 10000
  const REQUEST_INTERVAL = 10
  const PRELOAD_TIME_THRESHOLD = 3
  const INCREMENT = 3

  class MockPromise<T> {

    private wrappedPromise: Promise<T>
    private wrappedResolve!: (value?: T) => void
    
    constructor() {
      this.wrappedPromise = new Promise<T>(resolve => {
        this.wrappedResolve = resolve
      })
    }

    get promise() {
      return this.wrappedPromise
    }

    resolve(value?: T) {
      this.wrappedResolve(value)
    }
  }
  
  describe('in none phase', () => {

    describe('when init', () => {
      
      it('should start in playing phase', () => {

        // given
        const player = createPlayer()

        // when
        player.init(LENGTH)

        // then
        expect(player.phase).eql(PlayerPhase.playing)
      })

    })
  })

  describe('in playing phase', () => {

    let player!: PlayerFSM<SampleData>

    beforeEach(async () => {
      player = await createPlayerWithFirstFragmentBuffered()
    })

    context('when update at fragment start', () => {
      
      it('should emit frame', async () => {

        // given
        
        // when
        player.update(0)

        // then
        td.verify(frameOutputMock.emit({ time: 0, data: { value: 0 } }))
      })

    })

    context('when update in middle', () => {
      
      it('should emit next with smallest diff', async () => {

        // given
        const captor = td.matchers.captor()
        
        // when
        player.update(0)
        player.update(4)
        player.update(5)

        // then
        td.verify(frameOutputMock.emit(captor.capture()))
        td.verify(frameOutputMock.emit(captor.capture()))

        expect(captor.values![0]).eql({ time: 0, data: { value: 0 } })
        expect(captor.values![1]).eql({ time: 3, data: { value: 3 } })
        expect(captor.values![2]).eql({ time: 6, data: { value: 6 } })
      })
    })

    context('when update in frame end', () => {
      
      it('should emit last', async () => {

        // given
        const lastFrame = REQUEST_INTERVAL - (REQUEST_INTERVAL % INCREMENT)
        
        const promiseMock = createFragmentPromise()
        td.when(fragmentSourceMock.request(
          lastFrame, lastFrame + REQUEST_INTERVAL, td.matchers.anything())).thenReturn(promiseMock.promise)

        // when
        player.update(0)
        player.update(lastFrame)

        // then
        td.verify(frameOutputMock.emit({ time: lastFrame, data: { value: lastFrame } }))
      })
    })

    context('when update and missing frame and not preloaded', () => {

      it('should request fragment', () => {

        // given
        const lastFrame = REQUEST_INTERVAL - (REQUEST_INTERVAL % INCREMENT)
        const nextFrame = lastFrame + 1

        const promiseMock = createFragmentPromise()
        td.when(fragmentSourceMock.request(
          nextFrame, nextFrame + REQUEST_INTERVAL, td.matchers.anything())).thenReturn(promiseMock.promise)

        // when
        player.update(0)
        player.update(nextFrame)

        // then
        expect(player.phase).eql(PlayerPhase.playingWaitingForFragment)
      })
    })

    context('when update at the end of fragment', () => {

      it('should preload next fragment', () => {

        // given
        const lastFrame = REQUEST_INTERVAL - (REQUEST_INTERVAL % INCREMENT)
        const frame = lastFrame - PRELOAD_TIME_THRESHOLD

        const promiseMock = createFragmentPromise()
        td.when(fragmentSourceMock.request(
          lastFrame, lastFrame + REQUEST_INTERVAL, td.matchers.anything())).thenReturn(promiseMock.promise)

        // when
        player.update(0)
        player.update(frame)

        // then
        expect(player.phase).eql(PlayerPhase.playing)
      })
    })

    context('when update with already preloading fragment', () => {

      it('should not preload again', () => {

        // given
        const lastFrame = REQUEST_INTERVAL - (REQUEST_INTERVAL % INCREMENT)
        const frame = lastFrame - PRELOAD_TIME_THRESHOLD

        const promiseMock = createFragmentPromise()
        td.when(fragmentSourceMock.request(
          lastFrame, lastFrame + REQUEST_INTERVAL, td.matchers.anything()))
              .thenReturn(promiseMock.promise, null)
        player.update(0)
        player.update(frame)

        // when
        player.update(frame)

        // then
        expect(player.phase).eql(PlayerPhase.playing)
      })
    })

    context('when update with already preloaded fragment', () => {

      it('should not preload again', async () => {

        // given
        const lastFrame = REQUEST_INTERVAL - (REQUEST_INTERVAL % INCREMENT)
        const frame = lastFrame - PRELOAD_TIME_THRESHOLD

        const promiseMock = createFragmentPromise()
        const tag = 2
        td.when(fragmentSourceMock.request(
          lastFrame, lastFrame + REQUEST_INTERVAL, tag))
              .thenReturn(promiseMock.promise, null)

        player.update(0)
        player.update(frame)

        promiseMock.resolve(createSampleFragmentResponse(lastFrame, lastFrame + REQUEST_INTERVAL, tag))
        await nextTick()

        // when
        player.update(frame)

        // then
        expect(player.phase).eql(PlayerPhase.playing)
      })
    })

    context('when update after length', () => {

      it('it should pause', async () => {

        player.update(0)

        // when
        player.update(LENGTH + 1)

        // then
        expect(player.phase).eql(PlayerPhase.paused)
      })
    })

    context('when pause is called', () => {
      
      it('should not emit on update', async () => {

        // when
        player.pause()

        // then
        player.update(REQUEST_INTERVAL)
        td.verify(frameOutputMock.emit(td.matchers.anything()), { times: 0 })
      })

      it('should go to paused phase', async () => {

        // when
        player.pause()

        // then
        expect(player.phase).eql(PlayerPhase.paused)
      })
    })

    context('when seek is called', () => {
      
      it('should emit next valid frame', async () => {

        // when
        player.seek(4)
        player.update(0)

        // then
        td.verify(frameOutputMock.emit({ time: 3, data: { value: 3 } }))
        expect(player.phase).eql(PlayerPhase.playing)
      })
    })
  })

  context('in paused phase', () => {

    let player!: PlayerFSM<SampleData>

    beforeEach(async () => {
      player = await createPlayerWithFirstFragmentBuffered()
    })

    context('when fragment arrived', () => {

      it('should not resume playing', async () => {
        // given
        const promiseMock = createFragmentPromise()
        const requestTag = 2
        td.when(fragmentSourceMock.request(REQUEST_INTERVAL + 1,
                                           2 * REQUEST_INTERVAL + 1, requestTag)).thenReturn(promiseMock.promise)
        player.update(REQUEST_INTERVAL + 1)
        player.pause()

        // when
        promiseMock.resolve(createSampleFragmentResponse(0, REQUEST_INTERVAL, requestTag))
        await nextTick()
        
        // then
        expect(player.phase).eql(PlayerPhase.paused)
      })
    })

    context('when seek', () => {

      it('should not resume playing, but update position', async () => {

        // given
        player.pause()

        // when
        player.seek(4)

        // then
        expect(player.phase).eql(PlayerPhase.paused)

        player.play()
        player.update(12345)
        td.verify(frameOutputMock.emit({ time: 3, data: { value: 3 } }))
      })
    })

    context('when play', () => {

      it('should continue from last time position', async () => {

        // given
        player.pause()

        // when
        player.play()
        player.update(12345)

        // then
        expect(player.phase).eql(PlayerPhase.playing)
        td.verify(frameOutputMock.emit({ time: 0, data: { value: 0 } }))
      })
    })
  })
  
  describe('in waitingForFragment phase', () => {

    let player!: PlayerFSM<SampleData>
    let promiseMock!: MockPromise<FragmentResponse<SampleData>>
    const requestTag = 1

    beforeEach(() => {
      player = createPlayer()
      player.init(LENGTH)
      player.seek(4)
      promiseMock = createFragmentPromise()
      td.when(fragmentSourceMock.request(4, 4 + REQUEST_INTERVAL, requestTag)).thenReturn(promiseMock.promise)
      player.update(0)
    })

    context('when last requested fragment arrived', () => {
      
      it('should resume playing at fragment start time', async () => {

        // when
        promiseMock.resolve(createSampleFragmentResponse(4, 4 + REQUEST_INTERVAL, requestTag))

        // then
        await nextTick()
        expect(player.phase).eql(PlayerPhase.playing)

        player.update(0)
        td.verify(frameOutputMock.emit({ time: 6, data: { value: 6 } }))
      })

    })

    context('when other requested fragment arrived', () => {
      
      it('should remain in playingWaitingForFragment phase', async () => {

        // when
        const otherTag = 42
        promiseMock.resolve(createSampleFragmentResponse(0, REQUEST_INTERVAL, otherTag))

        // then
        await nextTick()

        expect(player.phase).eql(PlayerPhase.playingWaitingForFragment)
      })
    })
  })


  async function createPlayerWithFirstFragmentBuffered() {

    const requestTag = 1
    const promiseMock = createFragmentPromise()
    td.when(fragmentSourceMock.request(0, REQUEST_INTERVAL, requestTag)).thenReturn(promiseMock.promise)

    const player = createPlayer()
    player.init(LENGTH)
    player.update(0)

    promiseMock.resolve(createSampleFragmentResponse(0, REQUEST_INTERVAL, requestTag))
    await nextTick()

    return player
  }

  function createPlayer() {
    return new PlayerFSM(fragmentSourceMock, frameOutputMock, REQUEST_INTERVAL, PRELOAD_TIME_THRESHOLD)
  }

  function createFragmentPromise() {
    return new MockPromise<FragmentResponse<SampleData>>()
  }

  let createSampleFragmentResponse = (startTime: number, endTime: number, tag: any) => {

    const frames = []
    let startFrameTime = Math.floor(startTime / INCREMENT) * INCREMENT
    if (startTime % INCREMENT > 0) { startFrameTime += INCREMENT }
    for (let time = startFrameTime; time <= endTime; time += INCREMENT) {
      frames.push({
        time: time,
        data: { value: time }
      })
    }

    return {
      tag: tag,
      fragment: {
        startTime: startTime,
        endTime: frames[frames.length - 1].time,
        frames: frames
      }
    }
  }

  function nextTick() {
    return new Promise(resolve => setImmediate(resolve));
  }
})

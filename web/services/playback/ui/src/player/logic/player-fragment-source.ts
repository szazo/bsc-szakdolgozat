import { Telemetry } from '@av/telemetry-api'
import { FragmentSource, Fragment } from './player-fsm'

export class PlayerFragmentSource implements FragmentSource<Telemetry> {

  constructor(private flightId: number) {}

  async request(startTime: number, endTime: number, tag: any) {

    do {
      const response = await fetch(`/api/playback/${this.flightId}/${startTime}/${endTime}`)
      if (response.ok) {
        const fragment: Fragment<Telemetry> = await response.json()
        console.log('FRAGMENT', fragment.startTime, fragment.endTime, 'request: ', startTime, endTime)
        return {
          tag,
          fragment
        }
      } else {
        console.error(response.statusText)
        await this.sleep(3000)
      }
    } while (true)
  }

  private sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}

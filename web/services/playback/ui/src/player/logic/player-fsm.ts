
/*
                           fragmentArrived      play          pause          seek               update
  playing                    ab=x                 x           gotopaused      gotoplaying       if pb 
  paused                     ab=x                gotoplaying    x             gotoplaying         x
  waitingForfragment         ab=x; phase          x           gotopaused      gotoplaying        x 
  
                           request              stop
  streaming                stop and start      stop
  not streaming            start                x

play buffer (pb)
arrived buffer (ab)


*/

enum BufferingPhase {
  none,
  buffering
}

export enum PlayerPhase {
  none = 'none',
  playing = 'playing',
  playingWaitingForFragment = 'playingWaitingForFragment',
  paused = 'paused'
}

export interface Frame<T> {
  time: number
  data: T
}

export interface Fragment<T> {
  startTime: number
  endTime: number
  frames: Frame<T>[]
}

export interface FragmentResponse<T> {
  tag: any,
  fragment: Fragment<T>
}

export interface FragmentSource<T> {
  request(startTime: number, endTime: number, tag: any): Promise<FragmentResponse<T>>
}

export interface FrameOutput<T> {
  emit(frame: Frame<T>): void
}

interface FragmentRequest {
  startTime: number, endTime: number
}

export class PlayerFSM<T> {

  private totalLength: number = 0
  private currentPhase: PlayerPhase = PlayerPhase.none

  private fragmentRequestTag: number = 0
  private bufferingPhase = BufferingPhase.none
  private currentBufferingFragment: FragmentRequest | null = null
  // The last arrived fragment
  private arrivedFragment: Fragment<T> | null = null

  private lastTime: number | null = null

  private timePosition: number = 0
  private playFragmentFrameIndex: number = 0
  private playFragment: Fragment<T> | null = null

  public onPhaseChanged?: (phase: PlayerPhase) => void
  public onTimeChanged?: (timePosition: number) => void
  
  constructor(
    private source: FragmentSource<T>,
    private output: FrameOutput<T>,
    private fragmentRequestInterval: number,
    // while playing before this time interval of the fragment end, it will returned
    private preloadTimeThreshold: number) {
  }

  init(length: number) {
    if (this.currentPhase !== PlayerPhase.none) { throw new Error(`Invalid phase ${this.currentPhase}`) }

    this.totalLength = length
    this.play()
  }

  seek(time: number) {
    switch (this.currentPhase) {
      case PlayerPhase.playing: {

        if (time < 0 || time > this.totalLength) { return }

        this.changeTimePosition(time)
        this.lastTime = null
        this.playFragmentFrameIndex = 0
        break
      }

      case PlayerPhase.paused:
        this.changeTimePosition(time)
        this.playFragmentFrameIndex = 0
        break
    }
  }

  play() {
    switch (this.currentPhase) {
      case PlayerPhase.none:
      case PlayerPhase.paused:
        this.lastTime = null
        this.changePhase(PlayerPhase.playing)
        break
    }
  }

  pause() {
    switch (this.currentPhase) {

      case PlayerPhase.playing:
      case PlayerPhase.playingWaitingForFragment:
        this.changePhase(PlayerPhase.paused)
        break
    }
  }

  update(time: number) {
    time = Math.floor(time)
    switch (this.currentPhase) {
      case PlayerPhase.playing:
        this.updatePlaying(time)
    }
  }

  private updatePlaying(time: number) {
    if (this.lastTime === null) {
      this.lastTime = time
    }

    const elapsed = time - this.lastTime
    this.lastTime = time
    const nextTimePosition = this.timePosition + elapsed

    if (nextTimePosition > this.totalLength) {
      this.changePhase(PlayerPhase.paused)
      return
    }

    let fragmentResult = this.prepareFragment(nextTimePosition)
    if (fragmentResult === false) {
      // we have no fragment, wait
      this.changePhase(PlayerPhase.playingWaitingForFragment)
      console.log('Wait for fragment', nextTimePosition, time, this.lastTime, elapsed)
      this.requestFragment(nextTimePosition)
      return
    }

    const fragment = fragmentResult.fragment
    let index = fragmentResult.index
    while (index < fragment.frames.length - 1) {
      if (Math.abs(fragment.frames[index].time - nextTimePosition)
          < Math.abs(fragment.frames[index + 1].time - nextTimePosition)) {
        break
      }
      index++
    }

    if (nextTimePosition >= fragment.endTime - this.preloadTimeThreshold) {
      // preload next
      this.requestFragment(fragment.endTime)
    }

    const frame = fragment.frames[index]
    this.playFragmentFrameIndex = index
    this.changeTimePosition(nextTimePosition)
    this.output.emit(frame)
  }

  private prepareFragment(time: number) {
    if (this.playFragment && this.inFragment(time, this.playFragment)) {
      return { fragment: this.playFragment, index: this.playFragmentFrameIndex }
    }

    if (this.arrivedFragment && this.inFragment(time, this.arrivedFragment)) {
      this.playFragment = this.arrivedFragment
      this.playFragmentFrameIndex = 0
      this.arrivedFragment = null
      return { fragment: this.playFragment, index: this.playFragmentFrameIndex }
    }

    return false
  }

  private inFragment(time: number, fragment: { startTime: number, endTime: number }) {
    return time >= fragment.startTime && time <= fragment.endTime
  }

  private requestFragment(startTime: number) {

    if (this.bufferingPhase === BufferingPhase.buffering) {
      if (this.currentBufferingFragment && this.inFragment(startTime, this.currentBufferingFragment)) {
        return
      }
    }

    if (this.arrivedFragment && this.inFragment(startTime, this.arrivedFragment)) {
      return
    }

    this.setAsBuffering()
    this.currentBufferingFragment = { startTime: startTime, endTime: startTime + this.fragmentRequestInterval }
    const promise = this.source.request(this.currentBufferingFragment.startTime,
                                        this.currentBufferingFragment.endTime,
                                        this.fragmentRequestTag)
    promise.then((fragmentResponse) => {
      this.fragmentArrived(fragmentResponse)
    })
  }

  private fragmentArrived(response: FragmentResponse<T>) {

    if (this.bufferingPhase != BufferingPhase.buffering) {
      return
    }

    if (response.tag !== this.fragmentRequestTag) {
      return
    }

    if (response.fragment.startTime === -1) {
      // empty fragment
      console.error('Invalid fragment returned')
      return
    }

    this.arrivedFragment = response.fragment
    this.currentBufferingFragment = null
    this.setAsNotBuffering()

    switch (this.currentPhase) {
      case PlayerPhase.playingWaitingForFragment: {
        this.changeTimePosition(this.arrivedFragment.startTime)
        this.lastTime = null
        this.changePhase(PlayerPhase.playing)
        break
      }
    }
  }

  private setAsBuffering() {
    this.fragmentRequestTag++
    this.bufferingPhase = BufferingPhase.buffering
  }

  private setAsNotBuffering() {
    this.bufferingPhase = BufferingPhase.none
  }

  private changePhase(phase: PlayerPhase) {
    if (this.currentPhase === phase) {
      return
    }

    console.log('CHANGEPHASE', phase)
    
    this.currentPhase = phase
    if (this.onPhaseChanged) {
      this.onPhaseChanged(phase)
    }
  }

  private changeTimePosition(time: number) {
    const timeChanged = time !== this.timePosition
    this.timePosition = time

    if (this.onTimeChanged && timeChanged) {
      this.onTimeChanged(time)
    }
  }

  get phase() {
    return this.currentPhase
  }

  get length() {
    return this.totalLength
  }

  get position() {
    return this.timePosition
  }
}

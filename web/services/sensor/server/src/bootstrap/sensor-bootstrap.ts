import { injectable } from 'inversify';
import { BootstrapModule } from '@av/bootstrap'
import { GyroDeserializer } from '../gyro'
import { GpsDeserializer } from '../gps';
import { PressureDeserializer } from '../pressure';
import { AirspeedDeserializer } from '../airspeed';

@injectable()
export class SensorBootstrap implements BootstrapModule {

  constructor(
    private gyroDeserializer: GyroDeserializer,
    private gpsDeserializer: GpsDeserializer,
    private pressureDeserializer: PressureDeserializer,
    private airspeedDeserializer: AirspeedDeserializer) {}

  async start() {
    await this.gyroDeserializer.initialize()
    await this.gpsDeserializer.initialize()
    await this.pressureDeserializer.initialize()
    await this.airspeedDeserializer.initialize()
  }

  async stop() {
  }
}

import { injectable } from "inversify";
import * as TransformApi from '@av/transform-server-api'
import { PressureDeserializer } from "./pressure-deserializer";
import { PressureTelemetry } from './pressure-telemetry';

@injectable()
export class PressureTelemetryConverter {

  constructor(private pressureDeserializer: PressureDeserializer) {
  }

  convert (telemetries: TransformApi.RawTelemetry[]) {

    let output: Partial<PressureTelemetry> = {}
    for (let rawTelemetry of telemetries) {
      output = {
        ...output,
        ...this.convertOne(rawTelemetry)
      }
    }
    return output
  }

  private convertOne(rawTelemetry: TransformApi.RawTelemetry): Partial<PressureTelemetry> {

    const deserialized = this.pressureDeserializer.deserialize(rawTelemetry.content)
    if (!deserialized) {
      return {}
    }

    return {
      pressureHpa: deserialized.pressure / 100,
      temperatureCelsius: deserialized.temperature,
      relativeHumidity: deserialized.humidity
    }
  }
}

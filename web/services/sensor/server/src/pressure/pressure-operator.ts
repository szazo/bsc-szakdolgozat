import * as rxjs from 'rxjs';
import * as op from 'rxjs/operators';
import { injectable } from "inversify";
import * as TransformApi from '@av/transform-server-api'
import { PressureTelemetryConverter } from './pressure-telemetry-converter'
import { pressureTelemetryToTelemetry } from './pressure-telemetry';
import { PressureLinearInterpolate } from './pressure-linear-interpolate';

@injectable()
export class PressureOperator {

  constructor(private converter: PressureTelemetryConverter,
              private interpolate: PressureLinearInterpolate) {
  }

  create(sampleInterval: bigint, input$: rxjs.Observable<TransformApi.RawTelemetriesByType>) {

    return input$.pipe(
      op.map(x => ({ time: x.time, data: this.converter.convert(x.telemetries) })),
      TransformApi.linearInterpolate(sampleInterval, this.interpolate.interpolate.bind(this.interpolate)),
      op.map(x => ({ time: x.time, data: pressureTelemetryToTelemetry(x.data) }))
    )
  }
}

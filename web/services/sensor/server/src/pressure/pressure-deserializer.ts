import { injectable } from 'inversify'
import * as Protobuf from 'protobufjs'
import * as Common from '../common'
const proto = require('./proto/pressure.proto')
import { ProtoPressure } from './proto/pressure-proto-schema'

@injectable()
export class PressureDeserializer {

  private MessageType?: Protobuf.Type

  constructor(private deserializer: Common.ProtoDeserializer) {
  }

  async initialize() {
    const root = await this.deserializer.initialize(proto)
    this.MessageType = root.lookupType('ProtoPressure')
  }

  deserialize(buffer: Buffer): ProtoPressure | false {
    if (!this.MessageType) {
      throw new Error('Not initialized')
    }

    return this.deserializer.deserialize(this.MessageType, buffer)
  }
}

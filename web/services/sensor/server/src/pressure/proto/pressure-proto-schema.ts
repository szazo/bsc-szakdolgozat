export interface ProtoPressure {
  pressure: number
  temperature: number
  humidity: number
}

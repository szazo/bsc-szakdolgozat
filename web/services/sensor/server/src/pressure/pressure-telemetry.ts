import * as Telemetry  from '@av/telemetry-server'

export function pressureTelemetryToTelemetry(input: Partial<PressureTelemetry>): Partial<Telemetry.Telemetry> {
  return {
    pressurehPa: Telemetry.missingIfUndefined(input.pressureHpa),
    temperatureCelsius: Telemetry.missingIfUndefined(input.temperatureCelsius),
    relativeHumidity: Telemetry.missingIfUndefined(input.relativeHumidity)
  }
}

export interface PressureTelemetry {
  pressureHpa: number
  temperatureCelsius: number
  relativeHumidity: number
}

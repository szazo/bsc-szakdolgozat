import { injectable } from 'inversify';
import { linearInterpolateFunc } from '@av/transform-server-api'
import { PressureTelemetry } from './pressure-telemetry'

@injectable()
export class PressureLinearInterpolate {

  interpolate(x0: bigint,
              y0: Partial<PressureTelemetry>,
              x1: bigint,
              y1: Partial<PressureTelemetry>,
              x: bigint): Partial<PressureTelemetry> {

    return {
      pressureHpa: this.interpolateNumber(x0, y0.pressureHpa, x1, y1.pressureHpa, x),
      temperatureCelsius: this.interpolateNumber(x0, y0.temperatureCelsius, x1, y1.temperatureCelsius, x),
      relativeHumidity: this.interpolateNumber(x0, y0.relativeHumidity, x1, y1.relativeHumidity, x)
    }
  }

  private interpolateNumber(x0: bigint,
                                y0: number | undefined,
                                x1: bigint,
                                y1: number | undefined,
                                x: bigint) {

    if (!y0) {
      return undefined
    }

    if (!y1) {
      return y0
    }

    return linearInterpolateFunc(x0, y0, x1, y1, x)
  }
}

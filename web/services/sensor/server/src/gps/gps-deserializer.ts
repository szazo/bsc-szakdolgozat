import { injectable } from 'inversify'
import * as Protobuf from 'protobufjs'
import * as Common from '../common'
const proto = require('./proto/gps.proto')
require('./proto/gps_common.proto')
import { ProtoGps } from './gps-proto-schema'
import { GpsMessageType } from './gps-message-type'

@injectable()
export class GpsDeserializer {

  private types: { [type: number]: Protobuf.Type  } = {}
  
  constructor(private deserializer: Common.ProtoDeserializer) {
  }

  async initialize() {
    const root = await this.deserializer.initialize(proto)
    this.types[GpsMessageType.rmc] = root.lookupType('ProtoGpsRmc')
    this.types[GpsMessageType.gga] = root.lookupType('ProtoGpsGga')
    this.types[GpsMessageType.gsa] = root.lookupType('ProtoGpsGsa')
    this.types[GpsMessageType.gll] = root.lookupType('ProtoGpsGll')
    this.types[GpsMessageType.gst] = root.lookupType('ProtoGpsGst')
    this.types[GpsMessageType.gsv] = root.lookupType('ProtoGpsGsv')
    this.types[GpsMessageType.vtg] = root.lookupType('ProtoGpsVtg')
    this.types[GpsMessageType.zda] = root.lookupType('ProtoGpsZda')
  }

  deserialize(type: number, buffer: Buffer): ProtoGps | false {
    const protoType = this.types[type]
    if (!protoType) {
      throw new Error(`Gps deserializer not initialized for type ${type}`)
    }

    return this.deserializer.deserialize(protoType, buffer)
  }
}

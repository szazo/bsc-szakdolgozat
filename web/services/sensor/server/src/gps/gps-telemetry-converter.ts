import * as debug from 'debug'
import { injectable } from "inversify";
import { GpsDeserializer } from "./gps-deserializer";
import { GpsMessageType } from './gps-message-type'
import * as TransformApi from '@av/transform-server-api'
import * as Schema from './gps-proto-schema'
import { GpsTelemetry } from './gps-telemetry'
import { GpsDecoder } from './gps-decoder'

const log = debug('av:GpsTelemetryConverter')

@injectable()
export class GpsTelemetryConverter {

  constructor(
    private gpsDeserializer: GpsDeserializer,
    private decoder: GpsDecoder) {
  }

  convert (telemetries: TransformApi.RawTelemetry[]) {

    const priority = [ GpsMessageType.gga, GpsMessageType.gll, GpsMessageType.vtg, GpsMessageType.rmc ]
    const sorted = telemetries.sort((a, b) => priority.indexOf(a.messageType) - priority.indexOf(b.messageType) )

    let output: Partial<GpsTelemetry> = {}
    for (let rawTelemetry of sorted) {
      output = {
        ...output,
        ...this.convertOne(rawTelemetry)
      }
    }

    return output
  }

  private convertOne(rawTelemetry: TransformApi.RawTelemetry): Partial<GpsTelemetry> {
    const gpsType = <GpsMessageType> rawTelemetry.messageType

    // deserialize the content
    const deserialized = this.gpsDeserializer.deserialize(rawTelemetry.messageType, rawTelemetry.content)
    if (!deserialized) {
      return {}
    }

    let telemetry: Partial<GpsTelemetry> = {}
    switch (gpsType) {
      case GpsMessageType.gga:
        telemetry = this.processGga(<Schema.ProtoGpsGga> deserialized)
        break
      case GpsMessageType.gll:
        telemetry = this.processGll(<Schema.ProtoGpsGll> deserialized)
        break
      case GpsMessageType.vtg:
        telemetry = this.processVtg(<Schema.ProtoGpsVtg> deserialized)
        break
      case GpsMessageType.rmc:
        telemetry = this.processRmc(<Schema.ProtoGpsRmc> deserialized)
        break
    }

    return telemetry
  }

  private processRmc(rmc: Schema.ProtoGpsRmc): Partial<GpsTelemetry> {
    if (!rmc.valid) {
      return {}
    }
    return {
      position: this.decoder.decodeGpsPosition(rmc),
      trueTrackDegrees: this.decoder.decodeGpsFloat(rmc.course),
      groundSpeedKnots: this.decoder.decodeGpsFloat(rmc.speed)
    }
  }

  private processGga(gga: Schema.ProtoGpsGga): Partial<GpsTelemetry> {

    if (gga.fixQuality === 0) {
      console.log('invalid')
      return {}
    }
    
    return {
      position: this.decoder.decodeGpsPosition(gga),
      altitudeMeters: this.decoder.decodeAltitudeHeightMeters(gga.altitude, gga.altitudeUnits),
      heightMeters: this.decoder.decodeAltitudeHeightMeters(gga.height, gga.heightUnits)
    }
  }

  private processGll(gll: Schema.ProtoGpsGll): Partial<GpsTelemetry> {

    if (gll.status !== Schema.ProtoGpsStatus.STATUS_DATA_VALID) {
      return {}
    }

    return {
      position: this.decoder.decodeGpsPosition(gll)
    }
  }

  private processVtg(vtg: Schema.ProtoGpsVtg): Partial<GpsTelemetry> {

    return {
      trueTrackDegrees: this.decoder.decodeGpsFloat(vtg.trueTrackDegrees),
      magneticTrackDegrees: this.decoder.decodeGpsFloat(vtg.magneticTrackDegrees),
      groundSpeedKnots: this.decoder.decodeGpsFloat(vtg.speedKnots)
    }
  }
}

import * as Common from './gps-common-proto-schema'
export * from './gps-common-proto-schema'

export type ProtoGps = ProtoGpsRmc | ProtoGpsGga | ProtoGpsGsa |
      ProtoGpsGll | ProtoGpsGst | ProtoGpsGsv |
      ProtoGpsVtg | ProtoGpsZda

export interface ProtoGpsRmc {
  // The UTC time of the fix
  time: Common.ProtoGpsTime;
  // Whether the data is valid or not
  valid: boolean
  latitude: Common.ProtoGpsFloat
  longitude: Common.ProtoGpsFloat
  // Ground speed in knots
  speed: Common.ProtoGpsFloat
  // True track 
  course: Common.ProtoGpsFloat
  // UTC date
  date: Common.ProtoGpsDate
  // Magnetic variation
  variation: Common.ProtoGpsFloat
}

export interface  ProtoGpsGga {
  time: Common.ProtoGpsTime
  latitude: Common.ProtoGpsFloat
  longitude: Common.ProtoGpsFloat
  fixQuality: number
  satellitesTracked: number
  hdop: Common.ProtoGpsFloat
  altitude: Common.ProtoGpsFloat
  altitudeUnits: number
  height: Common.ProtoGpsFloat
  heightUnits: number
  dgpsAge: Common.ProtoGpsFloat
}

export interface ProtoGpsGsa {
  mode: ProtoGpsMode
  fixType: ProtoGpsFixType
  sats: number[]
  pdop: Common.ProtoGpsFloat
  hdop: Common.ProtoGpsFloat
  vdop: Common.ProtoGpsFloat
}

export interface ProtoGpsGll {
  latitude: Common.ProtoGpsFloat
  longitude: Common.ProtoGpsFloat
  time: Common.ProtoGpsTime
  status: ProtoGpsStatus
  mode: ProtoGpsMode
}

export interface ProtoGpsGst {
  time: Common.ProtoGpsTime
  rmsDeviation: Common.ProtoGpsFloat
  semiMajorDeviation: Common.ProtoGpsFloat
  semiMinorDeviation: Common.ProtoGpsFloat
  semiMajorOrientation: Common.ProtoGpsFloat
  latitudeErrorDeviation: Common.ProtoGpsFloat
  longitudeErrorDeviation: Common.ProtoGpsFloat
  altitudeErrorDeviation: Common.ProtoGpsFloat
}

interface ProtoGpsSatInfo {
  nr: number
  elevation: number
  azimuth: number
  snr: number
}

export interface ProtoGpsGsv {
  totalMsgs: number
  msgNr: number
  totalSats: number
  sats: ProtoGpsSatInfo[]
}

export interface ProtoGpsVtg {
  trueTrackDegrees: Common.ProtoGpsFloat
  magneticTrackDegrees: Common.ProtoGpsFloat
  speedKnots: Common.ProtoGpsFloat
  speedKph: Common.ProtoGpsFloat
  faaMode: ProtoGpsFaaMode
}

export interface ProtoGpsZda {
  time: Common.ProtoGpsTime
  date: Common.ProtoGpsDate
  hourOffset: number
  minuteOffset: number
}

enum ProtoGpsFixType {
  FIX_NONE = 1,
  FIX_2D = 2,
  FIX_3D = 3,
  FIX_UNKNOWN = 4
}

enum ProtoGpsMode {
  MODE_AUTO = 1,
  MODE_FORCED = 2,
  MODE_UNKNOWN = 3
}

enum ProtoGpsFaaMode {
  FAA_MODE_AUTONOMOUS = 1,
  FAA_MODE_DIFFERENTIAL = 2,
  FAA_MODE_ESTIMATED = 3,
  FAA_MODE_MANUAL = 4,
  FAA_MODE_SIMULATED = 5,
  FAA_MODE_NOT_VALID = 6,
  FAA_MODE_PRECISE = 7,
  FAA_MODE_UNKNOWN = 8
}

export enum ProtoGpsStatus {
  STATUS_DATA_VALID = 1,
  STATUS_DATA_NOT_VALID = 2,
  STATUS_UNKNOWN = 3
}

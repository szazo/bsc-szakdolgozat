export enum GpsMessageType {
  invalid = -1,
  unknown = 0,
  rmc = 1,
  gga = 2,
  gsa = 3,
  gll = 4,
  gst = 5,
  gsv = 6,
  vtg = 7,
  zda = 8,
}

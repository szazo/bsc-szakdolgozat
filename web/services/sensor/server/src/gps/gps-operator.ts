import * as rxjs from 'rxjs';
import * as op from 'rxjs/operators';
import { injectable } from "inversify";
import * as TransformApi from '@av/transform-server-api'
import { gpsTelemetryToTelemetry } from './gps-telemetry';
import { GpsTelemetryConverter } from './gps-telemetry-converter';
import { GpsLinearInterpolate } from './gps-linear-interpolate';

@injectable()
export class GpsOperator {

  constructor(private converter: GpsTelemetryConverter,
              private interpolate: GpsLinearInterpolate) {
  }

  create(sampleInterval: bigint, input$: rxjs.Observable<TransformApi.RawTelemetriesByType>) {

    return input$.pipe(
      op.map(x => ({ time: x.time, data: this.converter.convert(x.telemetries) })),
      TransformApi.linearInterpolate(sampleInterval, this.interpolate.interpolate.bind(this.interpolate)),
      op.map(x => ({ time: x.time, data: gpsTelemetryToTelemetry(x.data) }))
    )
  }
}

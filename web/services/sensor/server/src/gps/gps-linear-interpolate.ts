import { injectable } from 'inversify';
import { GpsTelemetry } from './gps-telemetry'

@injectable()
export class GpsLinearInterpolate {

  interpolate(_x0: bigint,
              _y0: Partial<GpsTelemetry>,
              _x1: bigint,
              _y1: Partial<GpsTelemetry>,
              _x: bigint): Partial<GpsTelemetry> {

    return _y0;
  }
}

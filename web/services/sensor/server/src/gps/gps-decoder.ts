import * as debug from 'debug'
import { injectable } from "inversify";
import * as Schema from './gps-proto-schema'
import { GpsPosition } from './gps-telemetry'

const log = debug('av:GpsDecoder')

@injectable()
export class GpsDecoder {

  decodeAltitudeHeightMeters(value: Schema.ProtoGpsFloat, unit: number) {
    
    let unitChar = String.fromCharCode(unit)
    switch (unitChar) {
      case 'M':
        return this.decodeGpsFloat(value)
      default:
        log('Unsupported unit: ', unitChar)
        return undefined
    }
  }

  decodeGpsFloat(gpsFloat: Schema.ProtoGpsFloat) {
    if (gpsFloat.scale === 0) {
      return gpsFloat.value
    }
    return gpsFloat.value / gpsFloat.scale
  }

  decodeGpsPosition(input: { latitude: Schema.ProtoGpsFloat, longitude: Schema.ProtoGpsFloat }): GpsPosition {
    return {
      latitude: this.decodeGpsCoordinate(input.latitude),
      longitude: this.decodeGpsCoordinate(input.longitude)
    }
  }

  private decodeGpsCoordinate(gpsFloat: Schema.ProtoGpsFloat) {

    if (gpsFloat.scale === 0) {
      return 0
    }

    const degrees = Math.floor(gpsFloat.value / (gpsFloat.scale * 100));
    const minutes = Math.floor(gpsFloat.value % (gpsFloat.scale * 100));

    return degrees + minutes / (60 * gpsFloat.scale);
  }
}

import { injectable } from 'inversify'
import * as moment from 'moment'
import * as Api from '@av/transform-server-api'
import { GpsMessageBase } from './gps-message-base'
import { GpsMessageType } from './gps-message-type'
import { GpsDeserializer } from './gps-deserializer'
import { ProtoGpsGga } from './gps-proto-schema'
import { GpsDecoder } from './gps-decoder'

@injectable()
export class GpsAltitudeDecoder {

  constructor(private gpsDeserializer: GpsDeserializer,
              private gpsDecoder: GpsDecoder) {
  }

  tryDecode(input: Api.RawTelemetriesByType) {
    if (input.messageBase != GpsMessageBase) {
      return null
    }

    const found = input.telemetries.find(x => x.messageType === GpsMessageType.gga)
    if (!found) {
      return null
    }

    const deserialized = this.gpsDeserializer.deserialize(found.messageType, found.content)
    if (!deserialized) {
      return null
    }

    const gga = <ProtoGpsGga> deserialized;
    const altitudeMeters = this.gpsDecoder.decodeAltitudeHeightMeters(gga.altitude, gga.altitudeUnits)
    if (!altitudeMeters) {
      console.error('Unable to decode altitude', gga)
      return null
    }

    return altitudeMeters
  }
}

import { injectable } from 'inversify'
import * as moment from 'moment'
import * as Api from '@av/transform-server-api'
import { GpsMessageBase } from './gps-message-base'
import { GpsMessageType } from './gps-message-type'
import { GpsDeserializer } from './gps-deserializer'
import { ProtoGpsRmc } from './gps-proto-schema'

@injectable()
export class GpsTimeDecoder {

  constructor(private gpsDeserializer: GpsDeserializer) {
  }

  tryDecode(input: Api.RawTelemetriesByType) {
    if (input.messageBase != GpsMessageBase) {
      return null
    }

    const found = input.telemetries.find(x => x.messageType === GpsMessageType.rmc)
    if (!found) {
      return null
    }

    const deserialized = this.gpsDeserializer.deserialize(found.messageType, found.content)
    if (!deserialized) {
      return null
    }

    const rmc = <ProtoGpsRmc> deserialized;
    if (!rmc.valid) {
      return null
    }

    const time = moment.utc([ rmc.date.year + 2000, rmc.date.month - 1, rmc.date.day,
                              rmc.time.hours, rmc.time.minutes, rmc.time.seconds ])

    return time
  }
}

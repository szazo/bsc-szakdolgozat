import * as Telemetry  from '@av/telemetry-server'

export function gpsTelemetryToTelemetry(input: Partial<GpsTelemetry>): Partial<Telemetry.Telemetry> {
  return {
    position: input.position || Telemetry.MissingData,
    altitudeMeters: input.altitudeMeters || Telemetry.MissingData,
    heightMeters: input.heightMeters || Telemetry.MissingData,
    trueTrackDegrees: input.trueTrackDegrees || Telemetry.MissingData,
    magneticTrackDegrees: input.magneticTrackDegrees || Telemetry.MissingData,
    groundSpeedKnots: input.groundSpeedKnots || Telemetry.MissingData
  }
}

export interface GpsTelemetry {
  // the gps position
  position: GpsPosition
  // the altitude in meters
  altitudeMeters: number
  // the height in meters
  heightMeters: number
  // true track
  trueTrackDegrees: number
  // magnetic track
  magneticTrackDegrees: number
  // ground speed in knots
  groundSpeedKnots: number
}

// position in DDD.DDD format
export interface GpsPosition {
  latitude: number
  longitude: number
}

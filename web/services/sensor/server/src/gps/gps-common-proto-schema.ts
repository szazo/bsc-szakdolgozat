export interface ProtoGpsDate {
  day: number
  month: number
  year: number
}

export interface ProtoGpsTime {
  hours: number
  minutes: number
  seconds: number
  microseconds: number
}

export interface ProtoGpsFloat {
  value: number
  scale: number
}

import * as IoC from 'inversify'
import * as BootstrapApi from '@av/bootstrap'
import * as TransformApi from '@av/transform-server-api'
import * as Common from './common'
import * as Gyro from './gyro'
import * as Gps from './gps'
import * as Pressure from './pressure'
import * as Airspeed from './airspeed'
import * as FlightMeta from './flight-meta'
import * as Bootstrap from './bootstrap'
import { Telemetry } from '@av/telemetry-api'

const SAMPLE_INTERVAL = BigInt(1000 * 10) // 10ms

export const sensorModule = new IoC.ContainerModule(bind => {

  // Common
  bind(Common.ProtoDeserializer).toSelf()

  // Bootstrap
  bind<BootstrapApi.BootstrapModule>(BootstrapApi.BootstrapModuleSymbol).to(Bootstrap.SensorBootstrap)

  // Gyro
  bind(Gyro.GyroDecoder).toSelf().inSingletonScope()
  bind(Gyro.GyroDeserializer).toSelf().inSingletonScope()
  bind(Gyro.GyroTelemetryConverter).toSelf().inSingletonScope()
  bind(Gyro.GyroOperator).toSelf().inSingletonScope()
  bind(Gyro.GyroLinearInterpolate).toSelf().inSingletonScope()
  bind<TransformApi.ConverterModule<Telemetry>>(TransformApi.ConverterModuleSymbol).toDynamicValue(c => {

    const gyro = c.container.get(Gyro.GyroOperator)
    const operator: TransformApi.SensorOperatorFunction<Telemetry> = (input$) => gyro.create(SAMPLE_INTERVAL, input$)
    return {
      messageBase: 0x01,
      operator: operator
    }
  })

  // Gps
  bind(Gps.GpsDecoder).toSelf().inSingletonScope()
  bind(Gps.GpsTimeDecoder).toSelf().inSingletonScope()
  bind(Gps.GpsAltitudeDecoder).toSelf().inSingletonScope()
  bind(Gps.GpsDeserializer).toSelf().inSingletonScope()
  bind(Gps.GpsTelemetryConverter).toSelf().inSingletonScope()
  bind(Gps.GpsOperator).toSelf().inSingletonScope()
  bind(Gps.GpsLinearInterpolate).toSelf().inSingletonScope()
  bind<TransformApi.ConverterModule<Telemetry>>(TransformApi.ConverterModuleSymbol).toDynamicValue(c => {

    const gps = c.container.get(Gps.GpsOperator)
    const operator: TransformApi.SensorOperatorFunction<Telemetry> = (input$) => gps.create(SAMPLE_INTERVAL, input$)
    return {
      messageBase: Gps.GpsMessageBase,
      operator: operator
    }
  })

  // Pressure
  bind(Pressure.PressureDeserializer).toSelf().inSingletonScope()
  bind(Pressure.PressureTelemetryConverter).toSelf().inSingletonScope()
  bind(Pressure.PressureOperator).toSelf().inSingletonScope()
  bind(Pressure.PressureLinearInterpolate).toSelf().inSingletonScope()
  bind<TransformApi.ConverterModule<Telemetry>>(TransformApi.ConverterModuleSymbol).toDynamicValue(c => {

    const pressure = c.container.get(Pressure.PressureOperator)
    const operator: TransformApi.SensorOperatorFunction<Telemetry> = (input$) => pressure.create(SAMPLE_INTERVAL, input$)
    return {
      messageBase: Pressure.PressureMessageBase,
      operator: operator
    }
  })

  // Airspeed
  bind(Airspeed.AirspeedDeserializer).toSelf().inSingletonScope()
  bind(Airspeed.AirspeedTelemetryConverter).toSelf().inSingletonScope()
  bind(Airspeed.AirspeedOperator).toSelf().inSingletonScope()
  bind(Airspeed.AirspeedLinearInterpolate).toSelf().inSingletonScope()
  bind<TransformApi.ConverterModule<Telemetry>>(TransformApi.ConverterModuleSymbol).toDynamicValue(c => {

    const airspeed = c.container.get(Airspeed.AirspeedOperator)
    const operator: TransformApi.SensorOperatorFunction<Telemetry> = (input$) => airspeed.create(SAMPLE_INTERVAL, input$)
    return {
      messageBase: Airspeed.AirspeedMessageBase,
      operator: operator
    }
  })

  // FlightMeta
  bind<TransformApi.FlightMetaOperator>(TransformApi.FlightMetaOperatorSymbol)
        .to(FlightMeta.FlightMetaOperator).inSingletonScope()
})

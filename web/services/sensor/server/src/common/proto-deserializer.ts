import { injectable } from 'inversify'
import * as Protobuf from 'protobufjs'
import * as debug from 'debug'
import * as path from 'path'

const debugLog = debug('av:ProtoDeserializer')

@injectable()
export class ProtoDeserializer {
  async initialize(proto: any) {
    const appDir = path.dirname(process.argv[1])
    const filename: string = path.join(appDir, proto)
    
    const root = await Protobuf.load(filename)

    return root
  }

  deserialize<T>(protoType: Protobuf.Type, buffer: Buffer): T | false {
    try {
      return <T> <unknown> protoType.decode(buffer)
    }
    catch (err) {
      debugLog(`Error while deserializing message with type ${protoType.name}`, err, buffer.toString('hex'))
      return false
    }
  }
}

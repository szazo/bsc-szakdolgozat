import { injectable } from 'inversify';
import * as rxjs from 'rxjs';
import * as moment from 'moment'
import * as Api from '@av/transform-server-api'
import * as Gps from '../gps'

class FlightMetaOperatorInstance {

  private currentMeta: Partial<Api.FlightMeta> = {}

  constructor(private gpsTimeDecoder: Gps.GpsTimeDecoder,
              private gpsAltitudeDecoder: Gps.GpsAltitudeDecoder) {
    
  }

  next(input: Api.RawTelemetriesByType) {

    // start time
    if (!this.currentMeta.startTime) {
      const decoded = this.gpsTimeDecoder.tryDecode(input)
      if (decoded) {
        this.currentMeta.startTime = decoded
      }
    }

    // length
    if (!this.currentMeta.lengthUS || input.time > this.currentMeta.lengthUS) {
      this.currentMeta.lengthUS = input.time
    }

    // altitude
    const altitude = this.gpsAltitudeDecoder.tryDecode(input)
    if (altitude && (!this.currentMeta.maxAltitudeMeters || altitude > this.currentMeta.maxAltitudeMeters)) {
      this.currentMeta.maxAltitudeMeters = altitude
    }
  }

  complete(): Api.FlightMeta {

    return {
      startTime: this.currentMeta.startTime || moment(),
      lengthUS: this.currentMeta.lengthUS || 0n,
      maxAltitudeMeters: this.currentMeta.maxAltitudeMeters || 0
    }
  }
}

@injectable()
export class FlightMetaOperator implements Api.FlightMetaOperator {

  constructor(private gpsTimeDecoder: Gps.GpsTimeDecoder,
              private gpsAltitudeDecoder: Gps.GpsAltitudeDecoder) {
  }

  create(): rxjs.OperatorFunction<Api.RawTelemetriesByType, Api.FlightMeta> {

    return input$ => new rxjs.Observable(observer => {

      const instance = new FlightMetaOperatorInstance(this.gpsTimeDecoder, this.gpsAltitudeDecoder)
      
      input$.subscribe({
        next(x) {
          instance.next(x)
        },
        error(err) { observer.error(err) },
        complete() {
          const meta = instance.complete()
          console.log('Completing meta', meta)
          observer.next(meta)
          observer.complete()
        }
      })
    })
  }
}

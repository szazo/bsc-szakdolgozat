export interface ProtoAirspeed {
  adcReading: number
  speed: number
}

import * as Telemetry  from '@av/telemetry-server'

export function airspeedTelemetryToTelemetry(input: Partial<AirspeedTelemetry>): Partial<Telemetry.Telemetry> {
  return {
    airspeed: Telemetry.missingIfUndefined(input.speed)
  }
}

export interface AirspeedTelemetry {
  speed: number
}

import { injectable } from 'inversify';
import { linearInterpolateFunc } from '@av/transform-server-api'
import { AirspeedTelemetry } from './airspeed-telemetry'

@injectable()
export class AirspeedLinearInterpolate {

  interpolate(x0: bigint,
              y0: Partial<AirspeedTelemetry>,
              x1: bigint,
              y1: Partial<AirspeedTelemetry>,
              x: bigint): Partial<AirspeedTelemetry> {

    return {
      speed: this.interpolateNumber(x0, y0.speed, x1, y1.speed, x)
    }
  }

  private interpolateNumber(x0: bigint,
                                y0: number | undefined,
                                x1: bigint,
                                y1: number | undefined,
                                x: bigint) {

    if (!y0) {
      return undefined
    }

    if (!y1) {
      return y0
    }

    return linearInterpolateFunc(x0, y0, x1, y1, x)
  }
}

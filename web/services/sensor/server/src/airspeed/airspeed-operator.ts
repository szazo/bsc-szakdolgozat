import * as rxjs from 'rxjs';
import * as op from 'rxjs/operators';
import { injectable } from "inversify";
import * as TransformApi from '@av/transform-server-api'
import { AirspeedTelemetryConverter } from './airspeed-telemetry-converter'
import { airspeedTelemetryToTelemetry } from './airspeed-telemetry';
import { AirspeedLinearInterpolate } from './airspeed-linear-interpolate';

@injectable()
export class AirspeedOperator {

  constructor(private converter: AirspeedTelemetryConverter,
              private interpolate: AirspeedLinearInterpolate) {
  }

  create(sampleInterval: bigint, input$: rxjs.Observable<TransformApi.RawTelemetriesByType>) {

    return input$.pipe(
      op.map(x => ({ time: x.time, data: this.converter.convert(x.telemetries) })),
      TransformApi.linearInterpolate(sampleInterval, this.interpolate.interpolate.bind(this.interpolate)),
      op.map(x => ({ time: x.time, data: airspeedTelemetryToTelemetry(x.data) }))
    )
  }
}

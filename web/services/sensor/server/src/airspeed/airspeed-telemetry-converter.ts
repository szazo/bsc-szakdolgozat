import { injectable } from "inversify";
import * as TransformApi from '@av/transform-server-api'
import { AirspeedDeserializer } from "./airspeed-deserializer";
import { AirspeedTelemetry } from './airspeed-telemetry';

@injectable()
export class AirspeedTelemetryConverter {

  constructor(private airspeedDeserializer: AirspeedDeserializer) {
  }

  convert (telemetries: TransformApi.RawTelemetry[]) {

    let output: Partial<AirspeedTelemetry> = {}
    for (let rawTelemetry of telemetries) {
      output = {
        ...output,
        ...this.convertOne(rawTelemetry)
      }
    }
    return output
  }

  private convertOne(rawTelemetry: TransformApi.RawTelemetry): Partial<AirspeedTelemetry> {

    const deserialized = this.airspeedDeserializer.deserialize(rawTelemetry.content)
    if (!deserialized) {
      return {}
    }

    return {
      speed: deserialized.adcReading
    }
  }
}

import { injectable } from 'inversify'
import * as Protobuf from 'protobufjs'
import * as Common from '../common'
const proto = require('./proto/airspeed.proto')
import { ProtoAirspeed } from './proto/airspeed-proto-schema'

@injectable()
export class AirspeedDeserializer {

  private MessageType?: Protobuf.Type

  constructor(private deserializer: Common.ProtoDeserializer) {
  }

  async initialize() {
    const root = await this.deserializer.initialize(proto)
    this.MessageType = root.lookupType('ProtoAirspeed')
  }

  deserialize(buffer: Buffer): ProtoAirspeed | false {
    if (!this.MessageType) {
      throw new Error('Not initialized')
    }

    return this.deserializer.deserialize(this.MessageType, buffer)
  }
}

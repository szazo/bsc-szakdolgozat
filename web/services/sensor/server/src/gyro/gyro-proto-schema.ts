import * as Common from './gyro-common-proto-schema'

export * from './gyro-common-proto-schema'

export interface ProtoGyro {
  quaternion: Common.ProtoQuaternionQ30
  gyro: Common.ProtoGyroRawAxes
  accel: Common.ProtoGyroRawAxes
  heading: Common.ProtoGyroRawAxes
  temp: number
}

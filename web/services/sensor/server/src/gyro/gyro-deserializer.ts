import { injectable } from 'inversify'
import * as path from 'path'
import * as Protobuf from 'protobufjs'
require('./proto/gyro_common.proto')
const proto = require('./proto/gyro.proto')
import { ProtoGyro } from './gyro-proto-schema'

@injectable()
export class GyroDeserializer {

  private MessageType?: Protobuf.Type

  constructor() {
  }

  async initialize() {
    const appDir = path.dirname(process.argv[1])
    const filename: string = path.join(appDir, proto)
    
    const root = await Protobuf.load(filename)
    this.MessageType = root.lookupType('ProtoGyro')
  }

  deserialize(buffer: Buffer): ProtoGyro | false {
    if (!this.MessageType) {
      throw new Error('Not initialized')
    }

    try {
      return <ProtoGyro> <unknown> this.MessageType.decode(buffer)
    }
    catch (err) {
      return false
    }
  }
}

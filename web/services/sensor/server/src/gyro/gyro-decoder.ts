import { injectable } from "inversify";
import * as Schema from './gyro-proto-schema'
import { GyroTelemetry } from "./gyro-telemetry";

interface GyroInput {
  quaternion: Schema.ProtoQuaternionQ30
  gyro: Schema.ProtoGyroRawAxes
  accel: Schema.ProtoGyroRawAxes
  heading: Schema.ProtoGyroRawAxes
}

@injectable()
export class GyroDecoder {

  decode(input: GyroInput): GyroTelemetry {
    return {
      orientation: this.deserializeQuaternionQ30(input.quaternion),
      gyro: this.deserializeGyroRawAxes(input.gyro),
      accel: this.decodeAccelGravity(input.accel),
      heading: this.deserializeGyroRawAxes(input.heading)
    }
  }

  private decodeAccelGravity(input: Schema.ProtoGyroRawAxes) {
    const fsr = 8; // 8G  // TODO: encode in the message
    const int16Max = 0x7fff
    const resolution = fsr / int16Max

    return {
      x: input.x * resolution,
      y: input.y * resolution,
      z: input.z * resolution
    }
  }

  private deserializeGyroRawAxes(axes: Schema.ProtoGyroRawAxes) {
    return {
      x: axes.x,
      y: axes.y,
      z: axes.z
    }
  }

  // private deserializeGyroQ16RawAxes(axes: Schema.ProtoGyroRawAxes) {
  //   return {
  //     x: this.q16ToNumber(axes.x),
  //     y: this.q16ToNumber(axes.y),
  //     z: this.q16ToNumber(axes.z)
  //   }
  // }

  private deserializeQuaternionQ30(quaternion: Schema.ProtoQuaternionQ30) {
    return {
      w: this.q30ToNumber(quaternion.w),
      x: this.q30ToNumber(quaternion.x),
      y: this.q30ToNumber(quaternion.y),
      z: this.q30ToNumber(quaternion.z)
    }
  }

  // https://en.wikipedia.org/wiki/Q_%28number_format%29
  private q30ToNumber(q30: number) {
    return q30 * Math.pow(2, -30)
  }

  // private q16ToNumber(q16: number) {
  //   return q16 * Math.pow(2, -16)
  // }
}

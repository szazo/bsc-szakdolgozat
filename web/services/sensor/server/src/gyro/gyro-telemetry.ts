import * as Telemetry  from '@av/telemetry-server'

export function gyroTelemetryToTelemetry(input: Partial<GyroTelemetry>): Partial<Telemetry.Telemetry> {
  return {
    orientation: input.orientation || Telemetry.MissingData,
    gyro: input.gyro || Telemetry.MissingData,
    accel: input.accel || Telemetry.MissingData,
  }
}

export interface GyroTelemetry {
  orientation: OrientationQuaternion
  gyro: Axes
  accel: Axes
  heading: Axes
}

export interface OrientationQuaternion {
  x: number
  y: number
  z: number
  w: number
}

export interface Axes {
  x: number
  y: number
  z: number
}

import { injectable } from 'inversify';
import { linearInterpolateFunc } from '@av/transform-server-api'
import { GyroTelemetry, OrientationQuaternion, Axes } from './gyro-telemetry';

@injectable()
export class GyroLinearInterpolate {

  interpolate(x0: bigint,
              y0: Partial<GyroTelemetry>,
              x1: bigint,
              y1: Partial<GyroTelemetry>,
              x: bigint): Partial<GyroTelemetry> {
    return {
      orientation: GyroLinearInterpolate.interpolateQuaternion(x0, y0.orientation, x1, y1.orientation, x),
      gyro: GyroLinearInterpolate.interpolateAxes(x0, y0.gyro, x1, y1.gyro, x),
      accel: GyroLinearInterpolate.interpolateAxes(x0, y0.accel, x1, y1.accel, x),
      heading: GyroLinearInterpolate.interpolateAxes(x0, y0.heading, x1, y1.heading, x)
    }
  }

  static interpolateQuaternion(x0: bigint,
                                y0: OrientationQuaternion | undefined,
                                x1: bigint,
                                y1: OrientationQuaternion | undefined,
                                x: bigint) {

    if (!y0) {
      return undefined
    }

    if (!y1) {
      return y0
    }

    return {
      x: this.linearInterpolate(x0, y0.x, x1, y1.x, x),
      y: this.linearInterpolate(x0, y0.y, x1, y1.y, x),
      z: this.linearInterpolate(x0, y0.z, x1, y1.z, x),
      w: this.linearInterpolate(x0, y0.w, x1, y1.w, x)
    }
  }

  private static interpolateAxes(x0: bigint, y0: Axes | undefined, x1: bigint, y1: Axes | undefined, x: bigint) {

    if (!y0) {
      return undefined
    }

    if (!y1) {
      return y0
    }
    
    return {
      x: this.linearInterpolate(x0, y0.x, x1, y1.x, x),
      y: this.linearInterpolate(x0, y0.y, x1, y1.y, x),
      z: this.linearInterpolate(x0, y0.z, x1, y1.z, x)
    }
  }

  private static linearInterpolate(x0: bigint, y0: number, x1: bigint, y1: number, x: bigint) {
    return linearInterpolateFunc(x0, y0, x1, y1, x)
  }
}

import * as rxjs from 'rxjs';
import * as op from 'rxjs/operators';
import { injectable } from "inversify";
import * as TransformApi from '@av/transform-server-api'
import { GyroTelemetryConverter } from './gyro-telemetry-converter'
import { gyroTelemetryToTelemetry } from './gyro-telemetry';
import { GyroLinearInterpolate } from './gyro-linear-interpolate';

@injectable()
export class GyroOperator {

  constructor(private converter: GyroTelemetryConverter,
              private interpolate: GyroLinearInterpolate) {
  }

  create(sampleInterval: bigint, input$: rxjs.Observable<TransformApi.RawTelemetriesByType>) {

    return input$.pipe(
      op.map(x => ({ time: x.time, data: this.converter.convert(x.telemetries) })),
      TransformApi.linearInterpolate(sampleInterval, this.interpolate.interpolate.bind(this.interpolate)),
      op.map(x => ({ time: x.time, data: gyroTelemetryToTelemetry(x.data) }))
    )
  }
}

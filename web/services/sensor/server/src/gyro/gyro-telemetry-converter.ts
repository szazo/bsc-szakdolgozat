import { injectable } from "inversify";
import * as TransformApi from '@av/transform-server-api'
import { GyroDeserializer } from "./gyro-deserializer";
import { GyroTelemetry } from './gyro-telemetry';
import { GyroDecoder } from './gyro-decoder'

@injectable()
export class GyroTelemetryConverter {

  constructor(private gyroDeserializer: GyroDeserializer,
              private decoder: GyroDecoder) {
  }

  convert (telemetries: TransformApi.RawTelemetry[]) {

    let output: Partial<GyroTelemetry> = {}
    for (let rawTelemetry of telemetries) {
      output = {
        ...output,
        ...this.convertOne(rawTelemetry)
      }
    }
    return output
  }

  private convertOne(rawTelemetry: TransformApi.RawTelemetry): Partial<GyroTelemetry> {

    // deserialize the content
    const deserialized = this.gyroDeserializer.deserialize(rawTelemetry.content)
    if (!deserialized) {
      return {}
    }

    const decoded = this.decoder.decode(deserialized)
    
    let checksum = Math.pow(decoded.orientation.x, 2) + Math.pow(decoded.orientation.y, 2) +
          Math.pow(decoded.orientation.z, 2) + Math.pow(decoded.orientation.w, 2)
    checksum = Math.round(checksum * 10000) / 10000
    if (checksum !== 1) {
      return {}
    }
    
    return decoded
  }
}

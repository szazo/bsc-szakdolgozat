export interface ProtoGyroRawAxes {
  x: number
  y: number
  z: number
}

export interface ProtoQuaternionQ30 {
  w: number
  x: number
  y: number
  z: number
}

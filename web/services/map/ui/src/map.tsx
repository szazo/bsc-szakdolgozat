import * as React from 'react'
import * as MapBoxGL from 'mapbox-gl'
import { CustomLayer, ObjectMapPosition } from './custom-layer'
import { Quaternion } from './glider'
export { ObjectMapPosition } from './custom-layer'

interface MapProps {
  position: ObjectMapPosition
  orientation: Quaternion
  flyTo?: ObjectMapPosition
  className?: string
  mode: MapMode
}

export enum MapMode {
  default,
  follow
}

// TODO: config
const apiKey = 'pk.eyJ1Ijoic3phem8iLCJhIjoiY2p1d3p5ZmF5MGg2bjQzcnY0eDYxMXhsdyJ9.dmo510hgo0uiLx9O_bxOZg'

export class Map extends React.PureComponent<MapProps> {

  private layer?: CustomLayer
  private map?: MapBoxGL.Map

  componentDidMount() {
    (MapBoxGL as any).accessToken = apiKey
    const container = this.refs.container as HTMLDivElement
    const map = new MapBoxGL.Map({
      style: 'mapbox://styles/mapbox/satellite-streets-v11',
      container: container,
      antialias: true,
      pitch: 60,
      zoom: 17
    })
    this.map = map

    const layer = new CustomLayer('3dmodel')
    layer.updatePosition(this.props.position)
    this.layer = layer

    map.on('style.load', () => {
      map.addLayer(layer)
    })

    /* let self = this
     * setInterval(() => { */
    //  if (!self) { return ;}
      /* this!.map!.panTo({ lng: self!.props.position.longitude, lat: self!.props.position.latitude},
       *                  {duration: 10, easing: (x) => x, animate: false }) */
      
//    }, 10)
  }

  componentDidUpdate(prevProps: MapProps) {
    if (!this.layer || !this.map) {
      return
    }

    if (this.props.flyTo && (!prevProps.flyTo || JSON.stringify(prevProps.flyTo) !== JSON.stringify(this.props.flyTo))) {
      /* const mercator = MapBoxGL.MercatorCoordinate.fromLngLat({ lng: this.props.flyTo.longitude, lat: this.props.flyTo.latitude},
       *                                                         this.props.flyTo.altitudeMeters ) */
      this.map!.jumpTo({
        center: { lng: this.props.flyTo.longitude, lat: this.props.flyTo.latitude},
        zoom: 17
      })
      console.log('FLYTO22', this.props.flyTo)
    } else {
      if (this.props.mode == MapMode.follow) {
        this.map.panTo({ lng: this.props.position.longitude, lat: this.props.position.latitude},
                         { animate: false })
      }
    }

    this.layer.updatePosition(this.props.position)
    this.layer.updateOrientation(this.props.orientation)
  }

  render() {
    return (
      <div ref="container" className={this.props.className} />
    )
  }
}

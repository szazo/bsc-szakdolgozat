import * as React from 'react'
import { MissingData, MissingDataType, Orientation, GpsPosition, Axes } from '@av/telemetry-api'
import * as Three from 'three'

interface InfoPanelProps {
  input: InfoPanelTelemetry
}

type WithMissing<T> = {
  [P in keyof T]: T[P] | MissingDataType;
}

interface InfoPanelTelemetryBase {
  orientation: Orientation
  position: GpsPosition
  altitudeMeters: number
  groundSpeedKnots: number
  pressurehPa: number
  temperatureCelsius: number
  relativeHumidity: number
  accel: Axes
}

export type InfoPanelTelemetry = WithMissing<InfoPanelTelemetryBase>

export const InfoPanel: React.FC<InfoPanelProps> = ({ input }) => {

  const orientation = formatOrientation(input)
  const acceleration = formatAcceleration(input)
  const position = formatPosition(input)
  const altitude = formatAltitude(input)
  const speed = formatSpeed(input)
  const environment = formatEnvironment(input)
  
  return <div className="w-48 p-2 border flex flex-col">
    <div className="flex-grow">
      <div className="text-lg text-blue-600">Orientation</div>
      <Label label="Pitch:" value={orientation.pitch} />
      <Label label="Roll:" value={orientation.roll} />
      <Label label="Heading:" value={orientation.heading} />
      <div className="text-lg text-blue-600">Acceleration</div>
      <Label label="Transverse:" value={acceleration.x} />
      <Label label="Longitudinal:" value={acceleration.y} />
      <Label label="Vertical:" value={acceleration.z} />
      <div className="text-lg text-blue-600">Position</div>
      <Label label="Longitude:" value={position.longitude} />
      <Label label="Latitude:" value={position.latitude} />
      <Label label="Altitude (MSL):" value={altitude.altitude} />
      <div className="text-lg text-blue-600">Speed</div>
      <Label label="Ground speed:" value={speed.groundSpeed} />
    </div>
    <div className="">
      <div className="text-lg text-blue-600">Environment</div>
      <Label label="Pressure:" value={environment.pressure} />
      <Label label="Temperature:" value={environment.temperature} />
      <Label label="Humidity:" value={environment.humidity} />
    </div>
  </div>
}

interface LabelProps {
  label: string
  value: string
}

const Label: React.FC<LabelProps> = ({label, value}) => {

  return <div className="text-sm text-gray-700">{label} {value}</div>
}

function formatEnvironment(input: InfoPanelTelemetry) {

  const missingOrRound = (input: number | MissingDataType, decimal: number, format: (x: number) => string) => {
    if (input === MissingData) { return '-' }

    return format(round(input, decimal))
  }

  return {
    pressure: missingOrRound(input.pressurehPa, 1, (x) => `${x} hPa`),
    temperature: missingOrRound(input.temperatureCelsius, 1, (x) => `${x}°`),
    humidity: missingOrRound(input.relativeHumidity, 1, (x) => `${x}%`)
  }
}

function formatPosition(input: InfoPanelTelemetry) {
  
  if (input.position === MissingData) {
    return {
      longitude: '-',
      latitude: '-',
    }
  }

  return {
    longitude: round(input.position.longitude, 5).toString(),
    latitude: round(input.position.latitude, 5).toString()
  }
}

function formatSpeed(input: InfoPanelTelemetry) {

  if (input.groundSpeedKnots === MissingData) {
    return {
      groundSpeed: '-'
    }
  }

  return {
    groundSpeed: round(input.groundSpeedKnots * 1.85200, 1).toString() + ' km/h'
  }
}

function formatAltitude(input: InfoPanelTelemetry) {
  if (input.altitudeMeters === MissingData) {
    return {
      altitude: '-'
    }
  }

  return {
    altitude: round(input.altitudeMeters, 1).toString() + ' m'
  }
}

function formatOrientation(input: InfoPanelTelemetry) {

  if (input.orientation === MissingData) {
    return {
      pitch: '-',
      roll: '-',
      heading: '-'
    }
  }

  const euler = new Three.Euler().setFromQuaternion(new Three.Quaternion(input.orientation.x,
                                                                         input.orientation.y,
                                                                         input.orientation.z,
                                                                         input.orientation.w), 'ZXY') // ZXY

  const rollDegree = Math.round(Three.Math.radToDeg(euler.y))
  const rollOrientation = rollDegree == 0 ? '' : rollDegree > 0 ? 'left ' : 'right '

  const pitchDegree = Math.round(Three.Math.radToDeg(euler.x))
  const pitchText = pitchDegree == 0 ? '' : pitchDegree > 0 ? 'nose ' : 'tail '

  return {
    pitch: `${pitchText} ${Math.abs(pitchDegree)}°` ,
    roll: `${rollOrientation} ${Math.abs(rollDegree)}°`,
    heading: Math.round(Three.Math.radToDeg(Math.PI - euler.z)).toString() + '°'
  }
}

function formatAcceleration(input: InfoPanelTelemetry) {

  if (input.accel === MissingData) {
    return {
      x: '-',
      y: '-',
      z: '-'
    }
  }

  const format = (input: number) => `${round(input, 1)} G`
  
  return {
    x: format(input.accel.x),
    y: format(input.accel.y),
    z: format(input.accel.z)
  }
}

function round(input: number, decimal: number) {
  return Math.round(input * Math.pow(10, decimal)) / Math.pow(10, decimal)
}

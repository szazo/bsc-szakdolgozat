import * as Three from 'three'
import * as MapBoxGL from 'mapbox-gl'
import { Glider, Quaternion } from './glider'

export interface ObjectMapPosition {
  longitude: number
  latitude: number
  altitudeMeters: number
}

export interface ObjectPosition {
  x: number
  y: number
  z: number
  scale: number
}

export class CustomLayer {

  public readonly type = 'custom'
  public readonly renderingMode = '3d'

  private map?: MapBoxGL.Map
  private renderer?: Three.WebGLRenderer
  private scene?: Three.Scene
  private camera?: Three.Camera
  private glider?: Glider

  private position?: ObjectMapPosition
  private objectPosition?: ObjectPosition

  constructor(public id: string) {
  }

  updatePosition(position: ObjectMapPosition) {
    this.position = position
    if (this.glider) {
      this.repositionObject(this.glider, position)
    }
  }

  updateOrientation(orientation: Quaternion) {
    if (this.glider) {
      this.glider.updateOrientation(orientation)
    }
  }

  private repositionObject(_object: Glider, position: ObjectMapPosition) {
    const mercator = this.positionToMercatorUnits(position)
    const x = mercator.x// / mercator.unitPerMeter
    const y = mercator.y// / mercator.unitPerMeter
    const z = mercator.z// / mercator.unitPerMeter
//    object.matrix.setPosition(x, y, z)

    this.objectPosition = {
      x, y, z, scale: mercator.unitPerMeter
    }
  }

  private positionToMercatorUnits(position: ObjectMapPosition) {
    const mercator = MapBoxGL.MercatorCoordinate.fromLngLat({ lng: position.longitude, lat: position.latitude},
                                                            position.altitudeMeters )
    // TODO: new version
    const scale = (mercator as any).meterInMercatorCoordinateUnits()

    return { x: mercator.x, y: mercator.y, z: mercator.z || 0, unitPerMeter: scale
    }
  }

  onAdd(map: MapBoxGL.Map, gl: WebGLRenderingContext) {
    console.log('onAdd')
    const renderer = new Three.WebGLRenderer({
      canvas: map.getCanvas(),
      context: gl,
      antialias: true
    })
    renderer.autoClear = false
    this.renderer = renderer

    this.scene = new Three.Scene()
    this.camera = new Three.PerspectiveCamera()

    this.glider = new Glider(this.scene)
    this.glider.load()
    // this.model = this.createObject()
    // this.model.matrixAutoUpdate = false
    // this.scene.add(this.model)

    this.createLights(this.scene)

    if (this.position) {
      this.repositionObject(this.glider, this.position)
    }

    this.map = map
  }

  private createLights(scene: Three.Scene) {
    const directionalLight = new Three.DirectionalLight(0xffffff);
    directionalLight.position.set(0, -70, 100).normalize();
    scene.add(directionalLight);
    
    var directionalLight2 = new Three.DirectionalLight(0xffffff);
    directionalLight2.position.set(0, 70, 100).normalize();
    scene.add(directionalLight2);

    // var ambient = new Three.AmbientLight(0xffffff)
    // scene.add(ambient)
  }

  render(_gl: WebGLRenderingContext, viewProjectionMatrix: number[]) {
    if (!this.map || !this.renderer || !this.scene || !this.camera || !this.position || !this.glider || !this.objectPosition) {
      return
    }

    // The map's camera matrix. It projects spherical mercator coordinates to gl coordinates.
    const mercatorToViewProjection = new Three.Matrix4().fromArray(viewProjectionMatrix)
    
    // const positionMercator = this.positionToMercatorUnits(this.position)
    const cameraMatrix = new Three.Matrix4()
          .setPosition(this.objectPosition.x, this.objectPosition.y, this.objectPosition.z)
          .scale(new Three.Vector3(this.objectPosition.scale * 3,
                                   -this.objectPosition.scale * 3,
                                   this.objectPosition.scale * 3))
    const cameraMatrixGl = cameraMatrix.premultiply(mercatorToViewProjection)

    this.camera.projectionMatrix = cameraMatrixGl

    this.renderer.state.reset();
    this.renderer.render(this.scene, this.camera)
    this.map.triggerRepaint()
  }
}

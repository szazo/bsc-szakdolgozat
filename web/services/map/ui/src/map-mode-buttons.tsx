import * as React from 'react'
import * as classnames from 'classnames'
import { MapMode } from './map'

interface ButtonProps {
  active?: boolean
  disabled?: boolean
  onClick: () => void
}

const Button: React.FC<ButtonProps> = ({ active, disabled, onClick, children }) => {

  const classes = classnames('inline-block border py-1 px-2 m-1 capitalize text-xs', {
    'border-gray-800 bg-gray-800 text-white': active,
    'border-white hover:border-gray-200 hover:bg-gray-200': !active,
    'text-gray-800': !active && !disabled,
    'text-gray-400': !active && disabled
  })

  return <button onClick={onClick} className={classes}>{children}</button>
}

interface MapModeButtonsProps {
  mode: MapMode
  onFollowClick: () => void
  onLocateClick: () => void
}

export const MapModeButtons: React.FC<MapModeButtonsProps> = ({ mode, onFollowClick, onLocateClick  }) => {

  return <>
    <Button onClick={onFollowClick} active={mode === MapMode.follow}>Follow</Button>
    <Button onClick={onLocateClick} disabled={mode == MapMode.follow} >Locate</Button>
  </>
}

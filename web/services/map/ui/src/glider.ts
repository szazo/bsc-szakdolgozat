import * as Three from 'three'
const gliderObj = require('file-loader!./glider.obj')
const OBJLoader = require('three-obj-loader');
OBJLoader(Three)

export interface Quaternion {
  x: number
  y: number
  z: number
  w: number
}

export function defaultQuaternion() {
  return {
    x: 0, y: 0, z: 0, w: 1
  }
}

export class Glider {

  private currentMesh: Three.Mesh

  constructor(private scene: Three.Scene) {

    const material = new Three.MeshLambertMaterial({ color: 0x00ff00 });
    const geometry = new Three.BoxGeometry( 10, 10, 10 );
    this.currentMesh = new Three.Mesh(geometry, material);
    scene.add(this.currentMesh)
  }

  private counter = 0;

  updateOrientation(quaternion: Quaternion) {

    let q = new Three.Quaternion()
    this.counter+=Math.PI / 180
//    q.setFromEuler(new Three.Euler(Math.PI / 2, Math.PI / 2 + Math.PI / 4, 0))
    q.setFromEuler(new Three.Euler(Math.PI / 2, -0.0872664626, 0)) // magnetic deviation
    // X: pitch (előre +)   Y: yaw (bal: +)    Z: roll (jobb: +)

    const inputQuaternion = new Three.Quaternion(quaternion.x,
                                                 quaternion.y,
                                                 quaternion.z,
                                                 quaternion.w)
//  const tmpEuler = new Three.Euler().setFromQuaternion(inputQuaternion)
//  console.log('EULER', tmpEuler.x * (180 / Math.PI), tmpEuler.y * (180 / Math.PI), tmpEuler.z * (180 / Math.PI))
//  tmpEuler.set(tmpEuler.x, tmpEuler.z, tmpEuler.y)
    
    //nst tmp = new Three.Quaternion().setFromEuler(new Three.Euler(Math.PI / 2, 0, 0))
    q = q.multiplyQuaternions(inputQuaternion, q)
    // q.multiply(new Three.Quaternion(quaternion.x,
    //                                 quaternion.y,
    //                                 quaternion.z,
    //                                 quaternion.w))
    this.currentMesh.quaternion.copy(q)
    
   //  this.currentMesh.quaternion.set(quaternion.x,
   //                                  quaternion.y,
   //                                  quaternion.z,
   //                                  quaternion.w)

   // this.currentMesh.rotateX(Math.PI / 2)
   // this.currentMesh.rotateY(-0.0872664626) // magnetic declanation
  }

  load() {
    var loader = new (Three as any).OBJLoader();
    const url = gliderObj
    console.log('URL', url)
    loader.load(url, (obj: any) => {
     this.scene.remove(this.currentMesh)
      this.currentMesh = obj
      this.scene.add(obj)
    }, undefined, (err: unknown) => {
      console.error('Error loading object', err)
    });
  }

  get mesh() {
    return this.currentMesh
  }
}

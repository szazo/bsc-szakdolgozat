import * as rxjs from 'rxjs';

export const ConverterModuleSymbol = Symbol.for('ConverterModule')

export interface ConverterModuleResultItem<T> {
  time: bigint,
  data: Partial<T>
}

export interface RawTelemetriesByType {
  time: bigint
  messageBase: number
  telemetries: RawTelemetry[]
}

export interface RawTelemetry {
  messageType: number
  content: Buffer
}

export type SensorOperatorFunction<T> = rxjs.OperatorFunction<RawTelemetriesByType, ConverterModuleResultItem<T>>

export interface ConverterModule<T> {
  messageBase: number
  operator: SensorOperatorFunction<T>
}

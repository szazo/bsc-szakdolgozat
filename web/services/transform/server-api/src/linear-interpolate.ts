import * as rxjs from 'rxjs';

interface InterpolateInput<T> {
  time: bigint
  data: T
}

interface InterpolateOutput<T> {
  time: bigint
  data: T
}

interface LinearInterpolateFunction<T> {
  (x0: bigint, y0: T, x1: bigint, y1: T, x: bigint): T
}

interface InternalResultItem<T extends object> {
  time: bigint,
  data: T
}

// TODO: rename
export function linearInterpolateFunc(x0: bigint, y0: number, x1: bigint, y1: number, x: bigint) {
  return (y0 * Number(x1 - x) + y1 * Number(x - x0)) / Number(x1 - x0)
}

// TODO: rename
export function linearInterpolate<T extends object>(
  targetSampleInterval: bigint,
  interpolateFunc: LinearInterpolateFunction<T>): rxjs.OperatorFunction<InterpolateInput<T>, InterpolateOutput<T>> {

  return input$ => {
    let interpolate = new LinearInterpolate<T>(targetSampleInterval, interpolateFunc)
    return interpolate.execute(input$)
  }
}

class LinearInterpolate<T extends object> {

  private lastTelemetryTime: bigint | null = null
  private lastOutput: T | null = null
  private lastTargetTime: bigint = BigInt(0)

  constructor(private targetSampleInterval: bigint,
              private interpolateFunc: LinearInterpolateFunction<T>) {
  }
  
  execute(input$: rxjs.Observable<InterpolateInput<T>>): rxjs.Observable<InterpolateOutput<T>> {

    const self = this
    return new rxjs.Observable(observer => {

      return input$.subscribe({
        next(x) {

          const result = self.processNext(x)
          for (const item of result) {
            observer.next(item)
          }
        },
        error(err) { observer.error(err) },
        complete() { observer.complete() }
      })
    })
  }

  private processNext(input: InterpolateInput<T>) {
    let result: InternalResultItem<T>[]
    if (this.lastTelemetryTime === null) {
      result = this.processFirstItem(input.time, input.data)
    } else {
      result = this.processOtherItem(input.time, input.data)
    }

    return result
  }

  private processFirstItem(telemetryTime: bigint, converted: T) {

    console.log('ProcessFirst', telemetryTime)

    let resultItems: InternalResultItem<T>[] = []
    const previousTargetTime = (telemetryTime / this.targetSampleInterval) * this.targetSampleInterval

    for (let time = 0n; time <= previousTargetTime; time += this.targetSampleInterval) {
      resultItems.push({
        time: BigInt(time),
        data: {} as T
      })
    }

    if (resultItems.length > 0 && previousTargetTime === telemetryTime) {
      resultItems[resultItems.length - 1] = {
        time: previousTargetTime,
        data: converted
      }
    }

    this.lastTargetTime = previousTargetTime
    this.lastTelemetryTime = telemetryTime
    this.lastOutput = converted

    return resultItems
  }

  private processOtherItem(telemetryTime: bigint, converted: T) {

    if (this.lastTelemetryTime === null || this.lastOutput === null) {
      throw new Error('missing last telemetry')
    }
    
    let resultItems: InternalResultItem<T>[] = []
    let startTime = this.lastTargetTime + this.targetSampleInterval
    for (let time = startTime; time <= telemetryTime; time += this.targetSampleInterval) {

      let value = converted
      if (time !== telemetryTime) {
        value = this.interpolateFunc(this.lastTelemetryTime, this.lastOutput,
                                     telemetryTime, converted, time)
      }
      
      resultItems.push({
        time: time,
        data: value
      })
    }

    this.lastTargetTime = (telemetryTime / this.targetSampleInterval) * this.targetSampleInterval
    this.lastTelemetryTime = telemetryTime
    this.lastOutput = converted

    return resultItems
  }
}

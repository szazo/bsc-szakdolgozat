import * as moment from 'moment'
import * as rxjs from 'rxjs';
import { RawTelemetriesByType } from './converter-module'

export const FlightMetaOperatorSymbol = Symbol.for('FlightMetaOperator')

export interface FlightMeta {
  startTime: moment.Moment
  lengthUS: bigint
  maxAltitudeMeters: number
}

export interface FlightMetaOperator {
  create(): rxjs.OperatorFunction<RawTelemetriesByType, FlightMeta>
}

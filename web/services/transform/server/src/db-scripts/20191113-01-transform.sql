ALTER TABLE transform
      ADD COLUMN raw_telemetry_file_revision TIMESTAMP WITH TIME ZONE NOT NULL;


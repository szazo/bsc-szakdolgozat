CREATE TABLE transform (
       id SERIAL PRIMARY KEY,
       raw_telemetry_file_id INT NOT NULL REFERENCES raw_telemetry_file(id),
       flight_id INT NOT NULL REFERENCES flight(id),
       phase INT NOT NULL,
       created_at TIMESTAMP WITH TIME ZONE NOT NULL,
       updated_at TIMESTAMP WITH TIME ZONE NOT NULL
);

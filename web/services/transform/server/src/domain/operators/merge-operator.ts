import { injectable } from 'inversify'
import * as rxjs from 'rxjs';
import { ConverterModuleResultItem } from '@av/transform-server-api'

export interface MergeResultItem<T> {
  time: bigint,
  data: T
}

@injectable()
export class MergeOperatorInstance<T> {

  private items: ConverterModuleResultItem<T>[] = []

  constructor(private isFilled: (data: Partial<T>) => boolean,
              private fill: (data: Partial<T>) => T,
              private emit: (items: MergeResultItem<T>[]) => void) {
  }

  next(item: ConverterModuleResultItem<T>) {

    let current = this.items.find(x => x.time === item.time)
    if (!current) {
      this.items.push(item)
    } else {
      let newData = {
        ...current.data,
        ...item.data
      }
      current.data = newData
    }

    this.items = this.items.sort((a, b) => Number(a.time - b.time))

    let lastIndex = -1
    for (let i = 0; i < this.items.length; i++) {
      if (this.isFilled(this.items[i].data)) {
        lastIndex = i
      } else {
        break
      }
    }

    const MAX_IN_QUEUE = 1000
    if (lastIndex <= 0 && this.items.length > MAX_IN_QUEUE) {
      for (let i = 0; i < 1000; i++) {
        let item = this.items[i]
        item.data = this.fill(item.data)
      }
      lastIndex = MAX_IN_QUEUE - 1
    }

    if (lastIndex >= 0) {
      const removedElements = this.items.splice(0, lastIndex + 1)
      if (this.emit) {
        this.emit(<MergeResultItem<T>[]> removedElements)
      }
    }
  }

  complete() {
    console.log('finishing', this.items.length)
    if (this.items.length === 0) {
      return
    }

    if (this.emit) {
      this.emit(<MergeResultItem<T>[]> this.items.map(x => ({ time: x.time,
                                                                data: this.fill(x.data) })))
    }
  }
}

@injectable()
export class MergeOperatorFactory<T> {

  constructor(private isFilled: (data: Partial<T>) => boolean,
              private fill: (data: Partial<T>) => T) {
  }

  operator(): rxjs.OperatorFunction<ConverterModuleResultItem<T>, MergeResultItem<T>[]> {
    return input$ => new rxjs.Observable(observer => {

      const instance = new MergeOperatorInstance(this.isFilled, this.fill, (items) => observer.next(items))
      
      return input$.subscribe({
        next(x) {
          instance.next(x)
        },
        error(err) { observer.error(err) },
        complete() { instance.complete(); observer.complete() }
      })
    })
  }
}

import * as rxjs from 'rxjs';
import * as op from 'rxjs/operators';
import { injectable, inject } from 'inversify';
import * as RawTelemetry from '@av/raw-telemetry-server'

@injectable()
export class RawTelemetrySource {

  constructor(@inject(RawTelemetry.RawTelemetryRepositorySymbol)
              private rawTelemetryRepository: RawTelemetry.RawTelemetryRepository) {}

  async process(rawTelemetryFileId: number, output: rxjs.Subject<RawTelemetry.RawTelemetry>) {

    const PAGE_SIZE = 1000
    try {
      let offset = 0
      while (true) {
        let items = await this.rawTelemetryRepository.query(rawTelemetryFileId, 'time', offset, PAGE_SIZE)
        if (items.length == 0) {
          break
        }

        await this.processPage(items, output)
        offset += items.length
      }
      
      output.complete()
    }
    catch (err) {
      output.error(err)
    }
  }

  private async processPage(rawTelemetries: RawTelemetry.RawTelemetry[], output: rxjs.Subject<RawTelemetry.RawTelemetry>) {
    if (rawTelemetries.length == 0) {
      return
    }

    for (const rawTelemetry of rawTelemetries) {

      output.next(rawTelemetry)
    }
  }
}

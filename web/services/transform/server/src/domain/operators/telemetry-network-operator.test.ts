import * as rxjs from 'rxjs';
import * as op from 'rxjs/operators';
import * as td from 'testdouble'
import { TelemetryNetworkOperator } from "./telemetry-network-operator";
import { Subject } from 'rxjs';
import { ConverterModuleResultItem, RawTelemetriesByType } from '@av/transform-server-api'
import { RawTelemetry } from '@av/raw-telemetry-server';
import { MergeOperatorFactory } from './merge-operator';
import { windowByTime } from './window-by-time-operator';
import { groupByMessageType } from './group-by-message-type-operator';

interface MockTelemetry {
  data1: string | 'missing'
  data2: string | 'missing'
  data3: string | 'missing'
}

function isFilled(input: Partial<MockTelemetry>) {
  return input.data1 !== undefined && input.data2 !== undefined && input.data3 !== undefined
}

function fillWithMissing(input: Partial<MockTelemetry>) {
  return {
    data1: input.data1 || 'missing',
    data2: input.data2 || 'missing',
    data3: input.data3 || 'missing'
  }
}

function moduleOperator(prop: keyof MockTelemetry): rxjs.OperatorFunction<RawTelemetriesByType, ConverterModuleResultItem<MockTelemetry>> {
  return input$ => input$.pipe(op.map(x => {
    let content = x.telemetries.reduce((prev, cur) => prev + '-' + cur.content.toString(), '')
    let result: Partial<MockTelemetry> = {}
    result[prop] = content
    return {
      time: x.time,
      data: result
    }
  }))
}

describe('network-operator', () => {

  beforeEach(() => {
    td.reset()
  })

  it('should', async () => {
    // given
    const inputSubject = new Subject<RawTelemetry>()
    const mergeOperator = new MergeOperatorFactory<MockTelemetry>(isFilled, fillWithMissing)

    const module1 = { messageBase: 1, operator: moduleOperator('data1') }
    const module2 = { messageBase: 2, operator: moduleOperator('data2') }
    const module3 = { messageBase: 3, operator: moduleOperator('data3') }
    const operator = new TelemetryNetworkOperator<MockTelemetry>([ module1, module2, module3 ], mergeOperator).create()

    const telemetryByType$ = inputSubject.pipe(windowByTime(),
                                         op.map(x => x.pipe(groupByMessageType())),
                                         op.mergeAll(),
                                         op.share())

    const output = telemetryByType$.pipe(operator)

    // when
    const result = output.forEach(x => {
      console.log(x)
    })
    emitInput(inputSubject)

    await result
    console.log('Complete')
  })


  function emitInput(inputSubject: Subject<RawTelemetry>) {
    let serial = 1n
    for (let i = 0n; i < 10n; i++) {
      for (let messageBase = 1; messageBase <= 3; messageBase++) {

        if ((Number(i) % messageBase) !== 0) {
          continue
        }
        
        for (let k = 0; k < 2; k++) {
          inputSubject.next({
            serial: serial++,
            sensorId: messageBase,
            time: i,
            messageType: (messageBase << 8) + (k + 1),
            content: Buffer.from(messageBase + '@' + (k + 1).toString(), 'utf-8')
        })
        }
      }
    }
    inputSubject.complete()    
  }
})

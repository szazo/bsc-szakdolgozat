import * as rxjs from 'rxjs';
import * as op from 'rxjs/operators';
import { injectable, inject } from 'inversify';
import { TelemetryRepository, TelemetryRepositorySymbol, Telemetry } from '@av/telemetry-server';
import { MergeResultItem } from './merge-operator'

@injectable()
export class TelemetryWriterOperator {

  constructor(
    @inject(TelemetryRepositorySymbol)
    private telemetryRepository: TelemetryRepository) {
  }

  create(flightId: number): rxjs.OperatorFunction<MergeResultItem<Telemetry>[], void> {

    // await this.dbContext.withContext(async (context) => {

    return input$ => {
      let start = 0
      const MAXIMUM_CONCURRENT_SAVE = 1
      const concurrent = input$.pipe(op.mergeMap(async items => {
        start++
        await this.telemetryRepository.insertTelemetry(flightId, items)
        start--
      }, MAXIMUM_CONCURRENT_SAVE))

      return concurrent
    }
  }
}

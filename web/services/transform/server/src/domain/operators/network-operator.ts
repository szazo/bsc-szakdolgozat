import * as rxjs from 'rxjs';
import * as op from 'rxjs/operators';
import { injectable, inject } from 'inversify';
import { RawTelemetry } from '@av/raw-telemetry-server'
import * as Api from '@av/transform-server-api'
import { Telemetry } from '@av/telemetry-api'
import { windowByTime } from './window-by-time-operator'
import { groupByMessageType } from './group-by-message-type-operator'
import { TelemetryWriterOperator } from './telemetry-writer-operator';
import { FlightMetaWriterOperator } from './flight-meta-writer-operator';
import { TelemetryNetworkOperator } from './telemetry-network-operator'

@injectable()
export class NetworkOperator {

  constructor(private telemetryNetworkOperator: TelemetryNetworkOperator<Telemetry>,
              private telemetryWriter: TelemetryWriterOperator,
              @inject(Api.FlightMetaOperatorSymbol)
              private flightMetaOperator: Api.FlightMetaOperator,
              private flightMetaWriterOperator: FlightMetaWriterOperator) {
  }

  create(flightId: number): rxjs.OperatorFunction<RawTelemetry, void> {

    return input$ => {
      const telemetryByType$ = input$.pipe(windowByTime(),
                                           op.map(x => x.pipe(groupByMessageType())),
                                           op.mergeAll(),
                                           op.share())

      const telemetry = telemetryByType$.pipe(this.telemetryNetworkOperator.create(),
                                         this.telemetryWriter.create(flightId));
      const flightMeta = telemetryByType$.pipe(this.flightMetaOperator.create(),
                                               this.flightMetaWriterOperator.create(flightId))
      
      return rxjs.merge(telemetry, flightMeta)
    }
  }
}

import * as rxjs from 'rxjs';
import * as op from 'rxjs/operators';
import { RawTelemetry } from '@av/raw-telemetry-server'

export function windowByTime(): rxjs.OperatorFunction<RawTelemetry, rxjs.Observable<RawTelemetry>> {
  return input$ => new rxjs.Observable(observer => {

    let startTime: bigint | null = null
    let currentTime: bigint | null = null
    let currentTarget: rxjs.Subject<RawTelemetry>

    return input$.subscribe({
      next(x) {

        if (startTime === null) {
          startTime = x.time
        }

        const time = x.time - startTime
        if (currentTime === null || currentTime !== time) {
          if (currentTarget) {
            currentTarget.complete()
          }
          currentTime = time
          currentTarget = new rxjs.Subject<RawTelemetry>()
          observer.next(currentTarget)
        }

        currentTarget.next({
          ...x,
          time: time
        })
      },
      error(err) { observer.error(err) },
      complete() { if (currentTarget) { currentTarget.complete() }; observer.complete()  }
    })
  })
}

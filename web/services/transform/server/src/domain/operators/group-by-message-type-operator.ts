import * as rxjs from 'rxjs';
import * as op from 'rxjs/operators';
import { RawTelemetry } from '@av/raw-telemetry-server'
import { RawTelemetriesByType } from '@av/transform-server-api'

export function groupByMessageType(): rxjs.OperatorFunction<RawTelemetry, RawTelemetriesByType> {
  return input$ => input$.pipe(
    op.groupBy(x => (x.messageType >> 8)),
    op.mergeMap(group$ => group$.pipe(op.bufferCount(Number.MAX_SAFE_INTEGER))),
    op.map(x => ({ time: x[0].time,
                   messageBase: x[0].messageType >> 8,
                   telemetries: x.map(y => ({
                     messageType: y.messageType & 0xFF,
                     content: y.content })) })))
}

import * as rxjs from 'rxjs';
import * as op from 'rxjs/operators';
import { injectable, multiInject } from 'inversify';
import * as Api from '@av/transform-server-api'
import { MergeOperatorFactory, MergeResultItem } from './merge-operator'

@injectable()
export class TelemetryNetworkOperator<T> {

  constructor(
    @multiInject(Api.ConverterModuleSymbol)
    private modules: Api.ConverterModule<T>[],
    private mergeOperator: MergeOperatorFactory<T>) {
  }

  create(): rxjs.OperatorFunction<Api.RawTelemetriesByType, MergeResultItem<T>[]> {

    return input$ => {

      let moduleOutputs = []
      for (let sensorModule of this.modules) {
        const moduleOutput = input$.pipe(
          op.filter(x => x.messageBase === sensorModule.messageBase), sensorModule.operator)
        moduleOutputs.push(moduleOutput)
      }

      const mergedModuleOutputs = rxjs.merge(moduleOutputs).pipe(op.mergeAll(),
                                                                 this.mergeOperator.operator())
      return mergedModuleOutputs
    }
  }
}

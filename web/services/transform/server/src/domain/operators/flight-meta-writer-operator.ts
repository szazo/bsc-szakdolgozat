import * as rxjs from 'rxjs';
import * as op from 'rxjs/operators';
import { injectable, inject } from 'inversify';
import { TelemetryRepository, TelemetryRepositorySymbol } from '@av/telemetry-server';
import * as Api from '@av/transform-server-api'

@injectable()
export class FlightMetaWriterOperator {

  constructor(
    @inject(TelemetryRepositorySymbol)
    private telemetryRepository: TelemetryRepository) {
  }

  create(flightId: number): rxjs.OperatorFunction<Api.FlightMeta, void> {

    return input$ => {

      return input$.pipe(op.mergeMap(async (x) => {
        const flight = await this.telemetryRepository.loadFlight(flightId)
        if (!flight) {
          throw new Error(`Invalid flight id: ${flightId}`)
        }

        flight.updateMeta(x.startTime, Number(x.lengthUS), x.maxAltitudeMeters)
        await this.telemetryRepository.saveFlight(flight)

        await this.telemetryRepository.saveFlight(flight)
      }))
    }
  }
}

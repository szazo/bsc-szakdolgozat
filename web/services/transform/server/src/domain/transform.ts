import * as moment from 'moment'

export enum TransformPhase {
  none,
  transforming,
  transformed,
  error
}

export class TransformState {
  id: number = 0
  rawTelemetryFileId: number = 0
  rawTelemetryFileRevision: moment.Moment | null = null
  targetFlightId: number = 0
  phase: TransformPhase = TransformPhase.none
}

export class Transform {

  private state: TransformState = new TransformState()

  load(state: TransformState) {
    this.state = state
  }

  toState() {
    return this.state
  }

  create(rawTelemetryFileId: number, targetFlightId: number, rawTelemetryFileRevision: moment.Moment) {
    this.state = {
      id: 0,
      rawTelemetryFileId: rawTelemetryFileId,
      rawTelemetryFileRevision: rawTelemetryFileRevision,
      targetFlightId: targetFlightId,
      phase: TransformPhase.transforming
    }
  }

  setAsTransformed() {
    this.state.phase = TransformPhase.transformed
  }

  setAsError() {
    this.state.phase = TransformPhase.error
  }
}

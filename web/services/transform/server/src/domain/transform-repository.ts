import * as moment from 'moment'
import { Transform } from './transform'

export const TransformRepositorySymbol = Symbol.for('TransformRepository')

interface NonSyncedFileResult {
  rawTelemetryFileId: number
  revision: moment.Moment
}

export interface TransformRepository {
  findNextNonSyncedFile(): Promise<NonSyncedFileResult | null>
  save(transform: Transform, transactionContext?: unknown): Promise<void>
}

import * as rxjs from 'rxjs'
import { injectable, inject } from 'inversify';
import { Transaction, TransactionSymbol } from '@av/db'
import { RawTelemetry } from '@av/raw-telemetry-server'
import { TransformRepositorySymbol, TransformRepository } from './transform-repository';
import { Transform } from './transform';
import { NetworkOperator, RawTelemetrySource } from './operators'
import { TelemetryRepository, TelemetryRepositorySymbol, Flight } from '@av/telemetry-server';

@injectable()
export class TransformCommandHandler {

  constructor(
    private networkOperatorNG: NetworkOperator,
    private rawTelemetrySource: RawTelemetrySource,
    @inject(TransformRepositorySymbol)
    private transformRepository: TransformRepository,
    @inject(TelemetryRepositorySymbol)
    private telemetryRepository: TelemetryRepository,
    @inject(TransactionSymbol)
    private transaction: Transaction) {}

  async execute() {
    const nonSynced = await this.transformRepository.findNextNonSyncedFile()
    if (nonSynced == null) {
      return
    }

    console.log('PROCESS', nonSynced.revision.toISOString())

    const flight = new Flight()
    flight.createDraft()
    const transform = new Transform()

    await this.transaction.withTransaction(async (transactionContext) => {

      await this.telemetryRepository.saveFlight(flight, transactionContext)
      transform.create(nonSynced.rawTelemetryFileId, flight.id, nonSynced.revision)
      await this.transformRepository.save(transform, transactionContext)
    })

    const input$ = new rxjs.Subject<RawTelemetry>()
    const network$ = this.networkOperatorNG.create(flight.id)
    const result$ = input$.pipe(network$)

    const networkResult = result$.forEach(_x => {})

    await this.rawTelemetrySource.process(nonSynced.rawTelemetryFileId, input$)
    await networkResult
    console.log('FINISHED!!!')

    const finishedFlight = (await this.telemetryRepository.loadFlight(flight.id))!
    finishedFlight.setAsActive()
    this.telemetryRepository.saveFlight(finishedFlight)

    transform.setAsTransformed()
    this.transformRepository.save(transform)
  }
}

// import { expect } from 'chai'
// import * as td from 'testdouble'
// import { InterpolateTransform, TelemetryConverter, Telemetry, MissingData } from './transform-plugin'

// const sampleInterval = 5n;

// describe('transform-plugin', () => {

//   let converterMock: TelemetryConverter<number>
//   let transform: InterpolateTransform<number>

//   beforeEach(() => {
//     converterMock = td.object<TelemetryConverter<number>>()
//     transform = new InterpolateTransform(sampleInterval, converterMock)    
//   })

//   afterEach(() => {
//     td.reset()
//   })

//   function createTelemetry(params: { time: number, data: number}): Telemetry {
//     return {
//       time: BigInt(params.time),
//       messageType: params.data,
//       content: Buffer.from([])
//     }
//   }
//   describe('when first measurement', () => {

//   describe('on zero time', () => {

//     it('should return', async () => {
//       // given
//       const telemetry = createTelemetry({ time: 0, data: 42})
//       td.when(converterMock.convert(telemetry)).thenReturn(42)

//       // when
//       const result = await transform.process(telemetry)

//       // then
//       expect(result).eql([
//         { time: 0n, data: 42 }
//       ])
//     })

//   })

//   describe('on interval boundary', () => {

//     it('should fill with Missing and return at boundary', async () => {
//       // given
//       const telemetry = createTelemetry({ time: 15, data: 42})
//       td.when(converterMock.convert(telemetry)).thenReturn(42)

//       // when
//       const result = await transform.process(telemetry)

//       // then
//       expect(result).eql([
//         { time: 0n, data: MissingData },
//         { time: 5n, data: MissingData },
//         { time: 10n, data: MissingData },
//         { time: 15n, data: 42 }
//       ])
//     })

//   })

//   describe('not on interval boundary', () => {

//     it('should fill with Missing', async () => {
//       // given
//       const telemetry = createTelemetry({ time: 16, data: 42})
//       td.when(converterMock.convert(telemetry)).thenReturn(42)

//       // when
//       const result = await transform.process(telemetry)

//       // then
//       expect(result).eql([
//         { time: 0n, data: MissingData },
//         { time: 5n, data: MissingData },
//         { time: 10n, data: MissingData },
//         { time: 15n, data: MissingData }
//       ])
//     })
//   })
//   })

//   describe('when next measurement', () => {

//     describe('on next interval boundary', () => {

//       it('should not interpolate', async () => {

//         // given
//         const first = createTelemetry({ time: 15, data: 42})
//         td.when(converterMock.convert(first)).thenReturn(42)
//         await transform.process(first)

//         // when
//         const second = createTelemetry({ time: 20, data: 43})
//         td.when(converterMock.convert(second)).thenReturn(43)
//         const result = await transform.process(second)

//         // then
//         expect(result).eql([
//           { time: 20n, data: 43 }
//         ])
//       })
//     })

//     describe('on next interval boundary after zero', () => {

//       it('should not interpolate', async () => {

//         // given
//         const first = createTelemetry({ time: 0, data: 42})
//         td.when(converterMock.convert(first)).thenReturn(42)
//         await transform.process(first)

//         // when
//         const second = createTelemetry({ time: 5, data: 43})
//         td.when(converterMock.convert(second)).thenReturn(43)
//         const result = await transform.process(second)

//         // then
//         expect(result).eql([
//           { time: 5n, data: 43 }
//         ])
//       })
//     })

//     describe('on boundary with between intervals', () => {

//       it('should interpolate', async () => {

//         // given
//         const first = createTelemetry({ time: 5, data: 42})
//         td.when(converterMock.convert(first)).thenReturn(42)
//         await transform.process(first)

//         // when
//         const second = createTelemetry({ time: 15, data: 44})
//         td.when(converterMock.convert(second)).thenReturn(44)
//         td.when(converterMock.interpolate(5n, 42, 15n, 44, 10n)).thenReturn(43)
//         const result = await transform.process(second)

//         // then
//         expect(result).eql([
//           { time: 10n, data: 43 },
//           { time: 15n, data: 44 }
//         ])
//       })
//     })

//     describe('not on boundary but with between intervals', () => {

//       it('should interpolate', async () => {

//         // given
//         const first = createTelemetry({ time: 5, data: 42})
//         td.when(converterMock.convert(first)).thenReturn(42)
//         await transform.process(first)

//         // when
//         const second = createTelemetry({ time: 16, data: 45})
//         td.when(converterMock.convert(second)).thenReturn(45)
//         td.when(converterMock.interpolate(5n, 42, 16n, 45, 10n)).thenReturn(43)
//         td.when(converterMock.interpolate(5n, 42, 16n, 45, 15n)).thenReturn(44)
//         const result = await transform.process(second)

//         // then
//         expect(result).eql([
//           { time: 10n, data: 43 },
//           { time: 15n, data: 44 }
//         ])
//       })
//     })

//     describe('before next interval boundary', () => {

//       it('should not return', async () => {

//         // given
//         const first = createTelemetry({ time: 5, data: 42})
//         td.when(converterMock.convert(first)).thenReturn(42)
//         await transform.process(first)

//         // when
//         const second = createTelemetry({ time: 6, data: 43})
//         td.when(converterMock.convert(second)).thenReturn(43)
//         const result = await transform.process(second)

//         // then
//         expect(result).eql([])
//       })
//     })

//     describe('after empty result', () => {

//       describe('next after boundary', () => {

//       it('should use last value for interpolation', async () => {

//         // given
//         const first = createTelemetry({ time: 5, data: 42})
//         td.when(converterMock.convert(first)).thenReturn(42)
//         await transform.process(first)

//         const second = createTelemetry({ time: 6, data: 43})
//         td.when(converterMock.convert(second)).thenReturn(43)
//         await transform.process(second)

//         // when
//         const third = createTelemetry({ time: 11, data: 45})
//         td.when(converterMock.convert(third)).thenReturn(45)
//         td.when(converterMock.interpolate(6n, 43, 11n, 45, 10n)).thenReturn(44)
//         const result = await transform.process(third)

//         // then
//         expect(result).eql([
//           { time: 10n, data: 44 }
//         ])
//       })
//       })

//       describe('next on boundary', () => {

//       it('should use boundary value without interpolation', async () => {

//         // given
//         const first = createTelemetry({ time: 5, data: 42})
//         td.when(converterMock.convert(first)).thenReturn(42)
//         await transform.process(first)

//         const second = createTelemetry({ time: 6, data: 43})
//         td.when(converterMock.convert(second)).thenReturn(43)
//         await transform.process(second)

//         // when
//         const third = createTelemetry({ time: 10, data: 45})
//         td.when(converterMock.convert(third)).thenReturn(45)
//         const result = await transform.process(third)

//         // then
//         expect(result).eql([
//           { time: 10n, data: 45 }
//         ])
//       })
//       })
//     })
//   })
// })

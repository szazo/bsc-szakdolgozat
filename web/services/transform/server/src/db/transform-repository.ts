import * as moment from 'moment'
import * as Slonik from 'slonik';
import { SlonikPoolSymbol, SlonikTransaction } from '@av/db'
import { RawTelemetryFilePhase } from '@av/raw-telemetry-server'
import { inject, injectable } from 'inversify'
import * as Domain from '../domain'

@injectable()
export class TransformRepository implements Domain.TransformRepository {

  constructor(
    @inject(SlonikPoolSymbol)
    private pool: Slonik.DatabasePoolType,
    private slonikTransaction: SlonikTransaction) {
  }

  async findNextNonSyncedFile() {

    const found = await this.pool.query(Slonik.sql<{ id: number, revision: number }>`
      SELECT raw_telemetry_file.id, revision FROM
        raw_telemetry_file
      LEFT JOIN 
        transform
      ON transform.raw_telemetry_file_id = raw_telemetry_file.id
      WHERE raw_telemetry_file.phase = ${RawTelemetryFilePhase.eof} AND transform.id IS NULL
      LIMIT 1
    `)

    if (found.rowCount === 0) {
      return null
    }

    const first = found.rows[0]
    return {
      rawTelemetryFileId: first.id,
      revision: moment(first.revision)
    }    
  }

  async save(transform: Domain.Transform, transactionContext?: unknown) {

    const timeString = moment().toISOString()
    const connection = this.slonikTransaction.resolveTransactionContext(transactionContext)

    const state = transform.toState()
    const revisionString = state.rawTelemetryFileRevision ? state.rawTelemetryFileRevision.toISOString() : null
    
    if (!state.id) {

      const result = await connection.query(Slonik.sql<{ id: number }>`
        INSERT INTO transform 
          (raw_telemetry_file_id, raw_telemetry_file_revision, flight_id, phase, created_at, updated_at) 
        VALUES 
          (${state.rawTelemetryFileId}, ${revisionString}, ${state.targetFlightId}, ${state.phase}, ${timeString}, ${timeString})
        RETURNING ID`)


      const id = result.rows[0].id
      transform.load({
        ...state,
        id: id
      })
    } else {

      await connection.query(Slonik.sql`
          UPDATE transform SET 
            raw_telemetry_file_id = ${state.rawTelemetryFileId},
            raw_telemetry_file_revision = ${revisionString},
            flight_id = ${state.targetFlightId},
            phase = ${state.phase},
            updated_at = ${timeString}
          WHERE id = ${state.id}
      `)
    }
  }
}

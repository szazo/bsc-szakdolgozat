import { injectable } from 'inversify';
import { BootstrapModule } from '@av/bootstrap'
import { TransformJob } from '../app';

@injectable()
export class TransformBootstrap implements BootstrapModule {

  constructor(private transformJob: TransformJob) {}

  async start() {
    this.transformJob.start()
  }

  async stop() {
    this.transformJob.stop()
  }
}

import * as IoC from 'inversify'
import * as BootstrapApi from '@av/bootstrap'
import * as Db from './db'
import * as Domain from './domain'
import * as App from './app'
import * as Bootstrap from './bootstrap'
import * as TelemetryServer from '@av/telemetry-server'

export const transformModule = new IoC.ContainerModule(bind => {

  // Bootstrap
  bind<BootstrapApi.BootstrapModule>(BootstrapApi.BootstrapModuleSymbol).to(Bootstrap.TransformBootstrap)

  // App
  bind(App.TransformJob).toSelf().inSingletonScope()

  // Domain
  bind(Domain.TransformCommandHandler).toSelf()
  bind(Domain.RawTelemetrySource).toSelf()
  bind(Domain.TelemetryWriterOperator).toSelf()
  bind(Domain.FlightMetaWriterOperator).toSelf()
  bind(Domain.TelemetryNetworkOperator).toSelf()
  bind(Domain.NetworkOperator).toSelf()
  bind(Domain.MergeOperatorFactory).toDynamicValue(_context => {
    return new Domain.MergeOperatorFactory<TelemetryServer.Telemetry>(TelemetryServer.isTelemetryFilled,
                                       TelemetryServer.fillTelemetryWithMissing)
  })

  // Repository
  bind<Domain.TransformRepository>(Domain.TransformRepositorySymbol).to(Db.TransformRepository)
})

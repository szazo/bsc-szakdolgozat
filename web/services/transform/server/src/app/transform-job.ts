import { injectable } from 'inversify'
import { TransformCommandHandler } from '../domain'

@injectable()
export class TransformJob {

  private intervalId!: NodeJS.Timer

  constructor (private commandHandler: TransformCommandHandler) {}

  start () {
    this.intervalId = setInterval(async () => {
      await this.commandHandler.execute()
    }, 10000) as any
  }

  stop () {
    clearInterval(this.intervalId)
  }
}

const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin')
const nodeExternals = require('webpack-node-externals')

let isDevelopment = process.env.NODE_ENV == 'development'

module.exports = {
  mode: isDevelopment ? 'development' : 'production',
  entry: './lib/index.js',
  output: {
    filename: 'index.js',
    path: path.resolve('dist')
  },
  target: 'node',
  externals: [
    nodeExternals({
      modulesDir: path.resolve('../../../node_modules'),
      whitelist: [
        // include some packages, because resources should be packed
          /^@av\/.*/,
          // /^@xx\/.*/
      ]
    })
  ],
  module: {
    rules: [
      {
        test: /\.proto$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
        }
      }
    ]
  },
  plugins: [
    new CopyWebpackPlugin([
      {
        context: '../../../node_modules/@av/client/dist',
        from: '**/*',
        to: 'public',
      }
    ]),
  ]
};

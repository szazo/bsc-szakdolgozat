import { createContainer } from './di.root'
import { Server } from './server'

createContainer().then(container => {
  const server = container.get<Server>(Server);
  server.start().catch((err) => {
    console.error(err)
  })
})

// const server = new Server(
//   new LiveBroadcast(new LiveBroadcastDeserializer()),
//                     new Sync());
// server.start()

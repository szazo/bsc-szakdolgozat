import { inject, multiInject, injectable } from 'inversify'
import * as express from 'express'
import { ExpressSymbol } from '@av/express-server'
import { ExpressApiModuleSymbol, ExpressApiModule } from '@av/server-api'

@injectable()
export class ExpressApi {

  constructor(
    @inject(ExpressSymbol)
    private app: express.Express,
    @multiInject(ExpressApiModuleSymbol)
    private apiModules: ExpressApiModule[]) {
  }

  initialize() {
    const apiRouter = express.Router()
    for (const apiModule of this.apiModules) {
      apiModule.initialize(apiRouter)
    }
    this.app.use('/api', apiRouter)
  }
}

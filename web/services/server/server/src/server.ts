import { injectable, inject, multiInject } from 'inversify'
import * as express from 'express'
import * as http from 'http'
import * as path from 'path'
import { ExpressSymbol, HttpServerFactorySymbol } from '@av/express-server'
import { BootstrapModuleSymbol, BootstrapModule } from '@av/bootstrap';
import { ExpressApi } from './express-api'

@injectable()
export class Server {

  private port: number = 3000;

  constructor(
    @inject(ExpressSymbol)
    private expressApp: express.Express,
    @inject(HttpServerFactorySymbol)
    private serverFactory: (app: express.Express) => http.Server,
    private expressApi: ExpressApi,
    @multiInject(BootstrapModuleSymbol)
    private modules: BootstrapModule[]) {
  }

  async start() {
    const server = this.startExpress()
    try {
      for (let module of this.modules) {
        await module.start();
      }
    } catch (err) {
      server.close();
      throw err
    }
  }

  async stop() {
      for (let module of this.modules) {
        await module.stop();
      }
  }

  private startExpress() {

    let appDir = ''
    if (require.main && require.main.filename) {
      appDir = path.dirname(require.main.filename)
    } else {
      appDir = path.dirname(process.argv[1])
    }

    const publicDir = path.join(appDir, 'public')
    this.expressApi.initialize()
    this.expressApp.use(express.static(publicDir))
    this.expressApp.get('/*', function(_req, res) {
      res.sendFile(path.join(publicDir, "index.html"), (err: Error) => {
        res.status(500).send(err)
      })
    })

    const server = this.serverFactory(this.expressApp)
    server.listen(this.port, () => { console.log(`Listening on ${this.port}...`) })
    return server
  }
}

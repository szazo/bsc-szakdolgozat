import * as IoC from 'inversify'
import 'reflect-metadata'
import { dbModule } from '@av/db'
import { expressModule } from '@av/express-server'
import { liveBroadcastModule }from '@av/live-broadcast-server'
import { rawTelemetryModule } from '@av/raw-telemetry-server'
import { syncModule } from '@av/sync-server'
import { transformModule } from '@av/transform-server'
import { sensorModule } from '@av/sensor-server'
import { telemetryModule } from '@av/telemetry-server'
import { playbackModule } from '@av/playback-server'
import { serverModule } from './di'

export async function createContainer() {
  const container = new IoC.Container();
  container.load(dbModule,
                 expressModule,
                 liveBroadcastModule,
                 rawTelemetryModule,
                 syncModule,
                 transformModule,
                 sensorModule,
                 playbackModule,
                 serverModule,
                 telemetryModule)

  return container
}

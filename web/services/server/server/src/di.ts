import * as IoC from 'inversify'
import { Server } from './server'
import { ExpressApi } from './express-api'

export const serverModule = new IoC.ContainerModule(bind => {

  bind(ExpressApi).toSelf().inSingletonScope()
  bind(Server).toSelf().inSingletonScope()
})

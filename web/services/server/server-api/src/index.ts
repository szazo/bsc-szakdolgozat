import * as express from 'express'

export const ExpressApiModuleSymbol = Symbol.for('ExpressApiModule')

export interface ExpressApiModule {
  initialize(router: express.Router): void
}

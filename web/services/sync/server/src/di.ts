import * as IoC from 'inversify'
import * as BootstrapApi from '@av/bootstrap'
import * as App from './app'
import * as Bootstrap from './bootstrap'
import * as Mqtt from './mqtt'

export const syncModule = new IoC.ContainerModule(bind => {

  // Bootstrap
  bind<BootstrapApi.BootstrapModule>(BootstrapApi.BootstrapModuleSymbol).to(Bootstrap.SyncBootstrap)

  // App
  bind(App.SyncMessageDeserializer).toSelf().inSingletonScope()
  bind(App.SyncMeasurementDeserializer).toSelf().inSingletonScope()
  bind(App.SyncPublishCommandHandler).toSelf()

  // Mqtt
  bind(Mqtt.SyncMqtt).toSelf().inSingletonScope()
})

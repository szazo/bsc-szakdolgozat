import { injectable, inject } from 'inversify';
import * as mqtt from 'mqtt'
import * as App from '../app'

@injectable()
export class SyncMqtt {

  private client?: mqtt.MqttClient

  constructor(
    private publishHandler: App.SyncPublishCommandHandler) {
    
  }

  async start() {
    const client =  mqtt.connect('mqtt://mqtt', {
      clientId: 'sync_client1-b',
      clean: false
    })
    this.client = client;

    client.on('connect', (connack: any) => {
      if (!connack.sessionPresent) {
        const options = {
          qos: <mqtt.QoS>1 // at least once
        }
        client.subscribe('sync', options, (err) => {
          if (!err) {
            console.log('subscribed to sync topic')
          } else {
            console.error('Subscribe error', err)
          }
        })
      }
    })

    client.handleMessage = async (packet, callback) => {

      if (packet.cmd !== 'publish') {
        callback(undefined, packet)
        return
      }

      const publishPacket = <mqtt.IPublishPacket> packet
      
      let error = undefined

      try {
        this.publishHandler.execute(publishPacket.payload as Buffer)        
      } catch (err) {
        console.error('Error storing message', err)
        error = err
      }
      callback(error, packet)
    }
  }

  async stop() {
    if (this.client) {
      this.client.end();
    }
  }
}

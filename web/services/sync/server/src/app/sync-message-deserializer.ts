import { injectable } from 'inversify'
import * as path from 'path'
import * as Protobuf from 'protobufjs'
const proto = require('./proto/sync_message.proto')
import { ProtoSyncMessage } from './sync-message-proto-schema'

@injectable()
export class SyncMessageDeserializer {

  private MessageType?: Protobuf.Type

  constructor() {
  }

  async initialize() {
    const appDir = path.dirname(process.argv[1])
    const filename: string = path.join(appDir, proto)
    
    const root = await Protobuf.load(filename)
    this.MessageType = root.lookupType('ProtoSyncMessage')
  }

  deserialize(buffer: Buffer): ProtoSyncMessage {
    if (!this.MessageType) {
      throw new Error('Not initialized')
    }

    const message = this.MessageType.decode(buffer)
    return <ProtoSyncMessage> <unknown> message
  }
}

import { injectable, inject } from 'inversify';
import * as RawTelemetry from '@av/raw-telemetry-server'
import { SyncMessageDeserializer } from './sync-message-deserializer'
import { ProtoSyncMeasurement } from './sync-measurement-proto-schema'
import { SyncMeasurementDeserializer } from './sync-measurement-deserializer'

@injectable()
export class SyncPublishCommandHandler {

  constructor(
    private deserializer: SyncMessageDeserializer,
    private measurementDeserializer: SyncMeasurementDeserializer,
    @inject(RawTelemetry.RawTelemetryRepositorySymbol)
    private rawTelemetryRepository: RawTelemetry.RawTelemetryRepository) {
    
  }

  async execute(payload: Buffer) {
    const message = this.deserializer.deserialize(payload)
    let measurements = this.measurementDeserializer.deserialize(message.measurement)

    await this.rawTelemetryRepository.insert(message.clientId,
                                             message.filename,
                                             message.eof,
                                             measurements.map(x => this.mapMeasurement(x)))
  }

  private mapMeasurement(measurement: ProtoSyncMeasurement): RawTelemetry.RawTelemetry {
    return {
      serial: BigInt(measurement.serial),
      time: BigInt(measurement.time),
      sensorId: measurement.sensorId,
      messageType: measurement.messageType,
      content: measurement.content
    }
  }
}

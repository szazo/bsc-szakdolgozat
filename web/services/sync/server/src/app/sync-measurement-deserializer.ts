import { injectable } from 'inversify'
import * as path from 'path'
import * as Protobuf from 'protobufjs'
const proto = require('./proto/sync_measurement.proto')
import { ProtoSyncMeasurement } from './sync-measurement-proto-schema'

@injectable()
export class SyncMeasurementDeserializer {

  private MessageType?: Protobuf.Type

  constructor() {
  }

  async initialize() {
    const appDir = path.dirname(process.argv[1])
    const filename: string = path.join(appDir, proto)
    
    const root = await Protobuf.load(filename)
    this.MessageType = root.lookupType('ProtoSyncMeasurement')
  }

  deserialize(buffer: Buffer): ProtoSyncMeasurement[] {
    if (!this.MessageType) {
      throw new Error('Not initialized')
    }

    const reader = Protobuf.Reader.create(buffer)
    let items: ProtoSyncMeasurement[] = []
    while (reader.pos < reader.len) {
      items.push(<ProtoSyncMeasurement> <unknown> this.MessageType.decodeDelimited(reader))
    }

    return items
  }
}

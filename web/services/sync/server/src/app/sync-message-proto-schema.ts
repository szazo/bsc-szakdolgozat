export interface ProtoSyncMessage {
  clientId: string
  filename: string
  startCursor: number
  endCursor: number
  eof: boolean
  measurement: Buffer
}

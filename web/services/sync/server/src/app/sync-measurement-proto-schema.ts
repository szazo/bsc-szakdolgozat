
export interface ProtoSyncMeasurement {
  serial: Long
  time: Long
  sensorId: number
  messageType: number
  content: Buffer
}

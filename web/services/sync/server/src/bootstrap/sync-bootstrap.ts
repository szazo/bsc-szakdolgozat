import { injectable } from 'inversify';
import { BootstrapModule } from '@av/bootstrap'
import * as App from '../app'
import * as Mqtt from '../mqtt'

@injectable()
export class SyncBootstrap implements BootstrapModule {

  constructor(private messageDeserializer: App.SyncMessageDeserializer,
              private measurementDeserializer: App.SyncMeasurementDeserializer,
              private sync: Mqtt.SyncMqtt ) {
  }

  async start() {
    await this.messageDeserializer.initialize();
    await this.measurementDeserializer.initialize();
    await this.sync.start();
  }

  async stop() {
    await this.sync.stop();
  }
}

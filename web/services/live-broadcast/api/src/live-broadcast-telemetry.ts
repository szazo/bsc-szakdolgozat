export interface LiveBroadcastTelemetry {
  orienation: LiveBroadcastOrientation
  position: Position
  compass: Compass
  acceleration: Acceleration
  angularAcceleration: Acceleration
  altitude: Altitude
  airspeed: number
  pressure: number
  humidity: number
  temperature: number
}

export interface Altitude {
  altitude: number
  altitudeUnits: string
  height: number
  heightUnits: string
}

export interface Acceleration {
  x: number
  y: number
  z: number
}

export interface Compass {
  x: number
  y: number
  z: number
}

export interface Position {
  latitude: number
  longitude: number
}

export interface LiveBroadcastOrientation {
  quaternion: Quaternion
}

export interface Quaternion {
  w: number
  x: number
  y: number
  z: number
}

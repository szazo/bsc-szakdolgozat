import * as React from 'react'
import * as io from 'socket.io-client'
import JSONPretty = require('react-json-pretty')
import { Telemetry, MissingData, defaultTelemetry } from '@av/telemetry-api'
import { Map, MapMode, ObjectMapPosition, Quaternion, defaultQuaternion,
         InfoPanel, InfoPanelTelemetry, MapModeButtons } from '@av/map-ui'

interface State {
  telemetry: Telemetry
  mapPosition: ObjectMapPosition
  mapOrientation: Quaternion
  flyTo?: ObjectMapPosition
  mapMode: MapMode
}

export class LiveBroadcast extends React.Component<{}, State> {

  private socket?: any

  constructor(props: {}) {
    super(props)

    this.state = {
      mapPosition: {
        longitude: 18.986944444444447,
        latitude: 47.4497222,
        altitudeMeters: 0
      },
      mapOrientation: defaultQuaternion(),
      telemetry: defaultTelemetry(),
      mapMode: MapMode.follow
    }
  }

  componentWillUnmount() {
    if (this.socket) {
      this.socket.close()
    }
  }

  componentDidMount() {
    this.socket = io()
    
    this.socket.on('telemetry', (msg: Telemetry) => {

      const updated = {
        ...this.state
      }

      if (msg.position !== MissingData) {
        updated.mapPosition.latitude = msg.position.latitude
        updated.mapPosition.longitude = msg.position.longitude
      }

      if (msg.altitudeMeters !== MissingData) {
        updated.mapPosition.altitudeMeters = msg.altitudeMeters
      }

      if (msg.orientation != MissingData) {
        updated.mapOrientation = msg.orientation
      }

      updated.telemetry = msg
      
      this.setState(updated)
    })
  }

  private recenter() {
    this.setState({
      ...this.state,
      flyTo: {
        latitude: this.state.mapPosition.latitude,
        longitude: this.state.mapPosition.longitude,
        altitudeMeters: 0
      }
    })
  }

  private switchMode() {
    this.setState({
      ...this.state,
      mapMode: this.state.mapMode === MapMode.follow ? MapMode.default : MapMode.follow
    })
  }

  render() {

    return <div className="flex flex-col h-full">
      <div className="flex flex-grow">
        <Map className="flex-grow"
          mode={this.state.mapMode}
          position={this.state.mapPosition}
          orientation={this.state.mapOrientation}
          flyTo={this.state.flyTo} />
        {/* <div className="flex-grow h-full">
            <Map2 telemetry={this.state.telemetry} />
            </div> */}
        {/* <button onClick={() => this.recenter()}>Recenter</button> */}
        <InfoPanel input={this.state.telemetry} />
      </div>
      <div>
        <MapModeButtons mode={this.state.mapMode}
          onFollowClick={() => this.switchMode()}
          onLocateClick={() => this.recenter()} />
      </div>
    </div>
  }
}

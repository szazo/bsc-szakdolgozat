export interface ProtoLiveBroadcastTelemetry {
  gps: ProtoLiveBroadcastGps
  gyro: ProtoLiveBroadcastGyro
  airspeed: ProtoLiveBroadcastAirspeed
  pressure: ProtoLiveBroadcastPressure
}

export interface ProtoLiveBroadcastAirspeed {
  age: SensorTime
  speed: number
}

export interface ProtoLiveBroadcastPressure {
  age: SensorTime
  pressure: number
  temperature: number
  humidity: number
}

export interface ProtoLiveBroadcastGps {
  time: ProtoLiveBroadcastGpsTime
  position: ProtoLiveBroadcastGpsPosition
  altitude: ProtoLiveBroadcastGpsAltitude
  speed: ProtoLiveBroadcastGpsSpeed
  track: ProtoLiveBroadcastGpsTrack
  quality: ProtoLiveBroadcastGpsQuality
  faa: ProtoLiveBroadcastGpsFAA
}

export type SensorTime = Long

export interface ProtoLiveBroadcastGpsPosition {
  age: SensorTime
  latitude: ProtoGpsFloat
  longitude: ProtoGpsFloat
}

interface ProtoLiveBroadcastGpsTime {
  age: SensorTime
  date: ProtoGpsDate
  time: ProtoGpsTime
  hour_offset: number
  minute_offset: number
}

interface ProtoLiveBroadcastGpsQuality {
  age: SensorTime
  fix_quality: number
  satellites_tracked: number
  hdop: ProtoGpsFloat
  dgps_age: ProtoGpsFloat
}

interface ProtoLiveBroadcastGpsFAA {
  age: SensorTime
  faa_mode: number
}

interface ProtoLiveBroadcastGpsSpeed {
  age: SensorTime
  speed_knots: ProtoGpsFloat
  speed_kph: ProtoGpsFloat
}

interface ProtoLiveBroadcastGpsTrack {
  age: SensorTime
  true_track_degrees: ProtoGpsFloat
  magnetic_track_degrees: ProtoGpsFloat
}

export interface ProtoLiveBroadcastGpsAltitude {
  age: SensorTime
  altitude: ProtoGpsFloat
  altitudeUnits: number
  height: ProtoGpsFloat
  heightUnits: number
}

interface ProtoGpsDate {
  day: number
  month: number
  year: number
}

interface ProtoGpsTime {
  hours: number
  minutes: number
  seconds: number
  microseconds: number
}

export interface ProtoGpsFloat {
  value: number
  scale: number
}

export interface ProtoLiveBroadcastGyro {
  age:  SensorTime
  quaternion: ProtoQuaternionQ30
  gyro: ProtoGyroRawAxes
  accel: ProtoGyroRawAxes
  heading: ProtoGyroRawAxes
}

export interface ProtoGyroRawAxes {
  x: number
  y: number
  z: number
}

export interface ProtoQuaternionQ30 {
  w: number
  x: number
  y: number
  z: number
}

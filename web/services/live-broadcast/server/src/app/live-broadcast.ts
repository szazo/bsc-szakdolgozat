import { inject, injectable } from 'inversify';
import * as mqtt from 'mqtt'
import * as io from 'socket.io'
import * as http from 'http'
import * as express from 'express'
import { ExpressSymbol, HttpServerFactorySymbol } from '@av/express-server'
import { LiveBroadcastTelemetry } from '@av/live-broadcast-api'
import { LiveBroadcastDeserializer } from './live-broadcast-deserializer';
import { EventEmitter } from 'events';
import { ProtoLiveBroadcastTelemetry } from './live_broadcast.proto.schema';
import { Telemetry, fillTelemetryWithMissing } from '@av/telemetry-api'
import { GpsDecoder, GyroDecoder } from '@av/sensor-server'

@injectable()
export class LiveBroadcast {

  private ioServer!: io.Server
  private telemetry?: Telemetry
  private mqtt?: mqtt.MqttClient;

  constructor (
    @inject(ExpressSymbol)
    private expressApp: express.Express,
    @inject(HttpServerFactorySymbol)
    private serverFactory: (app: express.Express) => http.Server,
    private deserializer: LiveBroadcastDeserializer,
    private gpsDecoder: GpsDecoder,
    private gyroDecoder: GyroDecoder) {

  }

  async start() {
    this.ioServer = io(this.serverFactory(this.expressApp))
    this.mqtt = this.connectToMqtt();
    this.configureSocket();
  }

  async stop() {
    if (this.mqtt) {
      this.mqtt.end()
    }
    this.ioServer.close()
  }

  private messageArrived(telemetry: Telemetry) {
    this.telemetry = telemetry
    this.emitCurrentTelemetry(this.ioServer)
    console.log('MESSAGE', JSON.stringify(telemetry))    
  }

  private emitCurrentTelemetry(target: EventEmitter | io.Server) {
    if (this.telemetry) {
      this.emitTelemetry(target, this.telemetry)
    }
  }

  private emitTelemetry(target: EventEmitter | io.Server, telemetry: Telemetry) {
    target.emit('telemetry', telemetry)
  }

  private configureSocket() {

    this.ioServer.on('connection', (socket) => {
      console.log('user connected')
      this.emitCurrentTelemetry(socket)

      socket.on('disconnect', (arg) => {
        console.log('user disconnected', arg)
      })
    })

    console.log(this.telemetry)
  }

  private convertToTelemetry(input: ProtoLiveBroadcastTelemetry): Telemetry {

    let result: Partial<Telemetry> = {}

    if (Number(input.gyro.age) !== 0) {
      const decoded = this.gyroDecoder.decode(input.gyro)
      result = {
        ...result,
        ...decoded
      }
    }

    if (Number(input.gps.position.age) !== 0) {
      result.position = this.gpsDecoder.decodeGpsPosition(input.gps.position)
    }

    if (Number(input.gps.altitude.age) !== 0) {
      result.altitudeMeters = this.gpsDecoder.decodeAltitudeHeightMeters(input.gps.altitude.altitude,
                                                                         input.gps.altitude.altitudeUnits)
    }

    return fillTelemetryWithMissing(result)
  }

  private connectToMqtt() {
    const client =  mqtt.connect('mqtt://mqtt', {
      clientId: 'client1',
      clean: true
    })

    client.on('connect', (connack: any) => {
      if (!connack.sessionPresent) {
        client.subscribe('live', (err) => {
          if (!err) {
            console.log('subscribed')
          } else {
            console.error('Subscribe error', err)
          }
        })
      }
    })

    client.on('message', (_topic, message) => {

      const deserialized = this.deserializer.deserialize(message)
      if (deserialized != null) {
        this.messageArrived(this.convertToTelemetry(deserialized))
      }
      // if (deserialized != null) {
      //   const telemetry: LiveBroadcastTelemetry = {
      //     orienation: {
      //       quaternion: deserialized.gyro.orientation
      //     },
      //     acceleration: {
      //       x: deserialized.gyro.accel.x,
      //       y: deserialized.gyro.accel.y,
      //       z: deserialized.gyro.accel.z
      //     },
      //     angularAcceleration: {
      //       x: deserialized.gyro.gyro.x,
      //       y: deserialized.gyro.gyro.y,
      //       z: deserialized.gyro.gyro.z
      //     },
      //     compass: {
      //       x: deserialized.gyro.heading.x,
      //       y: deserialized.gyro.heading.y,
      //       z: deserialized.gyro.heading.z
      //     },
      //     position: {
      //       latitude: deserialized.gps.position.latitude,
      //       longitude: deserialized.gps.position.longitude
      //     },
      //     altitude: {
      //       altitude: deserialized.gps.altitude.altitude,
      //       altitudeUnits: deserialized.gps.altitude.altitudeUnits,
      //       height: deserialized.gps.altitude.height,
      //       heightUnits: deserialized.gps.altitude.heightUnits
      //     },
      //     airspeed: deserialized.airspeed.speed,
      //     pressure: deserialized.pressure.pressure,
      //     humidity: deserialized.pressure.humidity,
      //     temperature: deserialized.pressure.temperature

      //   }            
    })

    return client
  }

  
}

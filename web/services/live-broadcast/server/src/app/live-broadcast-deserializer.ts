import * as path from 'path'
import { injectable } from 'inversify'
import { load, Type } from 'protobufjs';
import { LiveBroadcastTelemetry } from '@av/live-broadcast-api'
const proto = require('./proto/live_broadcast.proto')
require('@av/sensor-server/dist/gps/proto/gps_common.proto')
require('@av/sensor-server/dist/gyro/proto/gyro_common.proto')
import * as Schema from './live_broadcast.proto.schema'
import { ProtoLiveBroadcastGps, ProtoLiveBroadcastGpsPosition, ProtoLiveBroadcastTelemetry } from './live_broadcast.proto.schema';
import { Telemetry } from '@av/telemetry-api'
//import * as proto from './live_broadcast.proto'

@injectable()
export class LiveBroadcastDeserializer {

  private MessageType?: Type
  // private gpsDeserializer: GpsDeserializer
  // private gyroDeserializer: GyroDeserializer
  // private airspeedDeserializer: AirspeedDeserializer
  // private pressureDeserializer: PressureDeserializer

  constructor() {
    // this.gpsDeserializer = new GpsDeserializer();
    // this.gyroDeserializer = new GyroDeserializer();
    // this.airspeedDeserializer = new AirspeedDeserializer();
    // this.pressureDeserializer = new PressureDeserializer();
  }

  async initialize() {

    const appDir = path.dirname(process.argv[1])
    const filename: string = path.join(appDir, proto)
    
    const root = await load(filename)
    this.MessageType = root.lookupType('ProtoLiveBroadcastTelemetry')
  }

  deserialize(buffer: Buffer): Schema.ProtoLiveBroadcastTelemetry | null {
    if (!this.MessageType) {
      throw new Error('Not initialized')
    }

    try {
      const message = this.MessageType.decodeDelimited(buffer)
      return <Schema.ProtoLiveBroadcastTelemetry> <unknown> message

    } catch (err) {
      console.error('Error while deserializing message', err)
      return null
    }
  }

  // private process(message: Schema.ProtoLiveBroadcastTelemetry) {

  //   let gps = this.gpsDeserializer.deserialize(message.gps)
  //   let gyro = this.gyroDeserializer.deserialize(message.gyro)
  //   let airspeed = this.airspeedDeserializer.deserialize(message.airspeed)
  //   let pressure = this.pressureDeserializer.deserialize(message.pressure)

  //   return {
  //     gps: gps,
  //     gyro: gyro,
  //     airspeed: airspeed,
  //     pressure: pressure
  //   }
  // }
}

// class AirspeedDeserializer {
//   public deserialize(airspeed: Schema.ProtoLiveBroadcastAirspeed) {
//     return {
//       speed: airspeed.speed
//     }
//   }
// }

// class PressureDeserializer {
//   public deserialize(input: Schema.ProtoLiveBroadcastPressure) {
//     return {
//       pressure: input.pressure,
//       temperature: input.temperature,
//       humidity: input.humidity
//     }
//   }
// }

// class GyroDeserializer {
//   public deserialize(gyro: Schema.ProtoLiveBroadcastGyro) {
//     return {
//       orientation: this.deserializeQuaternionQ30(gyro.quaternion),
//       gyro: this.deserializeGyroRawAxes(gyro.gyro),
//       accel: this.deserializeGyroRawAxes(gyro.accel),
//       heading: this.deserializeGyroRawAxes(gyro.heading)
//     }
//   }

//   private deserializeGyroRawAxes(axes: Schema.ProtoGyroRawAxes) {
//     return {
//       x: axes.x,
//       y: axes.y,
//       z: axes.z
//     }
//   }

//   private deserializeGyroQ16RawAxes(axes: Schema.ProtoGyroRawAxes) {
//     return {
//       x: this.q16ToNumber(axes.x),
//       y: this.q16ToNumber(axes.y),
//       z: this.q16ToNumber(axes.z)
//     }
//   }

//   private deserializeQuaternionQ30(quaternion: Schema.ProtoQuaternionQ30) {
//     return {
//       w: this.q30ToNumber(quaternion.w),
//       x: this.q30ToNumber(quaternion.x),
//       y: this.q30ToNumber(quaternion.y),
//       z: this.q30ToNumber(quaternion.z)
//     }
//   }

//   // https://en.wikipedia.org/wiki/Q_%28number_format%29
//   private q30ToNumber(q30: number) {
//     return q30 * Math.pow(2, -30)
//   }

//   private q16ToNumber(q16: number) {
//     return q16 * Math.pow(2, -16)
//   }
// }

// class GpsDeserializer {

//   public deserialize(gps: ProtoLiveBroadcastGps) {
//     return {
//       position: this.deserializePosition(gps.position),
//       altitude: this.deserializeAltitude(gps.altitude)
//     }
//   }

//   public deserializePosition(position: Schema.ProtoLiveBroadcastGpsPosition) {
//     return {
// //      age: this.deserializeAge(position.age),
//       latitude: this.deserializeGpsCoordinate(position.latitude),
//       longitude: this.deserializeGpsCoordinate(position.longitude)
//     }
//   }

//   public deserializeAltitude(altitude: Schema.ProtoLiveBroadcastGpsAltitude) {
//     return {
// //      age: this.deserializeAge(altitude.age),
//       altitude: this.deserializeGpsFloat(altitude.altitude),
//       altitudeUnits: this.numberToChar(altitude.altitudeUnits),
//       height: this.deserializeGpsFloat(altitude.height),
//       heightUnits: this.numberToChar(altitude.heightUnits)
//     }
//   }

//   private numberToChar(num: number) {
//     return String.fromCharCode(num)
//   }

//   // private deserializeAge(age: Schema.SensorTime) {
//   //   return age;
//   // }

//   private deserializeGpsFloat(gpsFloat: Schema.ProtoGpsFloat) {

//     return gpsFloat.value / gpsFloat.scale
//   }

//   private deserializeGpsCoordinate(gpsFloat: Schema.ProtoGpsFloat) {

//     const degrees = Math.floor(gpsFloat.value / (gpsFloat.scale * 100));
//     const minutes = Math.floor(gpsFloat.value % (gpsFloat.scale * 100));

//     return degrees + minutes / (60 * gpsFloat.scale);
//   }
// }


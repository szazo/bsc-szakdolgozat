import { injectable } from 'inversify';
import { BootstrapModule } from '@av/bootstrap'
import * as App from '../app';

@injectable()
export class LiveBroadcastBootstrap implements BootstrapModule {

  constructor(private liveBroadcast: App.LiveBroadcast,
              private deserializer: App.LiveBroadcastDeserializer) {}

  async start() {
    await this.deserializer.initialize();
    this.liveBroadcast.start()
  }

  async stop() {
    this.liveBroadcast.stop()
  }
}

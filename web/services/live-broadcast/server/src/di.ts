import * as IoC from 'inversify'
import * as BootstrapApi from '@av/bootstrap'
import * as App from './app'
import * as Bootstrap from './bootstrap'

export const liveBroadcastModule = new IoC.ContainerModule(bind => {

  // Bootstrap
  bind<BootstrapApi.BootstrapModule>(BootstrapApi.BootstrapModuleSymbol).to(Bootstrap.LiveBroadcastBootstrap)

  // App
  bind(App.LiveBroadcast).toSelf().inSingletonScope()
  bind(App.LiveBroadcastDeserializer).toSelf().inSingletonScope()
})

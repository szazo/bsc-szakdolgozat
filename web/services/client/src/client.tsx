import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { Main } from './main'

class Bootstrap {

  start() {
    ReactDOM.render(<Main />,
                    document.getElementById('root'))
  }
}

const bootstrap = new Bootstrap();
bootstrap.start();

import * as React from 'react'
import { FrontendApp } from '@av/frontend-ui'

export class Main extends React.Component {

  render() {
    return <>
      <FrontendApp />
    </>
  }
}

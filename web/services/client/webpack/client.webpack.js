const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = () => {

  let isDevelopment = process.env.NODE_ENV == 'development'

  const config = {
    mode: isDevelopment ? 'development' : 'production',
    entry: './lib/client.js',
    output: {
      filename: '[name].[chunkhash].js',
      path: path.resolve('dist'),
      publicPath: '/'
    },
    target: 'web',
    // node: {
    //   fs: 'empty'
    // },
    module: {
      rules: [
        {
          test: /\.css$/,
          use: [
            isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
            'css-loader',
            {
              loader: 'postcss-loader',
              options: {
                plugins: [
                  require('tailwindcss'),
                  require('autoprefixer')
                ]
              }
            }
          ]
        },
      {
        test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        // Limiting the size of the woff fonts breaks font-awesome ONLY for the extract text plugin
        // loader: "url?limit=10000"
        use: "url-loader"
      },
      {
        test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
        use: 'file-loader'
      }
      ]
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: 'src/index.ejs'
      }),
      new MiniCssExtractPlugin({
        filename: "css/[name].css",
        chunkFilename: "css/[id].css"
      })
    ]
  }

  return config
}

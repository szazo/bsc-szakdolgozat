ALTER TABLE flight
      ADD COLUMN end_time TIMESTAMP WITH TIME ZONE,
      DROP COLUMN length,
      DROP COLUMN max_altitude;


CREATE TABLE flight (
       id SERIAL PRIMARY KEY,
       phase INT NOT NULL,
       start_time TIMESTAMP WITH TIME ZONE,
       end_time TIMESTAMP WITH TIME ZONE
);

CREATE TABLE telemetry (
       id SERIAL,
       flight_id INT NOT NULL REFERENCES flight(id),
       time BIGINT NOT NULL,
       latitude DOUBLE PRECISION,
       longitude DOUBLE PRECISION,
       orientation_x DOUBLE PRECISION,
       orientation_y DOUBLE PRECISION,
       orientation_z DOUBLE PRECISION,
       orientation_w DOUBLE PRECISION,
       accel_x DOUBLE PRECISION,
       accel_y DOUBLE PRECISION,
       accel_z DOUBLE PRECISION,
       gyro_x DOUBLE PRECISION,
       gyro_y DOUBLE PRECISION,
       gyro_z DOUBLE PRECISION,
       altitude DOUBLE PRECISION,
       height DOUBLE PRECISION,
       true_track DOUBLE PRECISION,
       magnetic_track DOUBLE PRECISION,
       ground_speed DOUBLE PRECISION,
       airspeed DOUBLE PRECISION,
       pressure DOUBLE PRECISION,
       humidity DOUBLE PRECISION,
       temperature DOUBLE PRECISION,
       created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
       PRIMARY KEY(id, flight_id, time)
);

SELECT create_hypertable('telemetry', 'time', chunk_time_interval => 864000000000);

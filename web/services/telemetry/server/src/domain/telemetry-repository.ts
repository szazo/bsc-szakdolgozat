import { TelemetryEntry } from './telemetry'
import { Flight, FlightPhase } from './flight'

export const TelemetryRepositorySymbol = Symbol.for('TelemetryRepository')

export interface TelemetryRepository {
  queryTelemetry(flightId: number, startTime: bigint, endTime: bigint): Promise<TelemetryEntry[]>;
  insertTelemetry(flighId: number, telemetries: TelemetryEntry[], dbContext?: unknown): Promise<void>
  saveFlight(flight: Flight, transactionContext?: unknown): Promise<number>;
  loadFlight(id: number): Promise<Flight | null>;
  flights(phase: FlightPhase): Promise<Flight[]>
}

import * as moment from 'moment'

export enum FlightPhase {
  none,
  draft,
  active,
  deleted
}

export interface FlightState {
  id: number
  phase: FlightPhase
  lengthUS: number
  startTime: moment.Moment | null
  maxAltitudeMeters: number
}

function defaultFlightState(): FlightState {
  return {
    id: 0,
    phase: FlightPhase.none,
    lengthUS: 0,
    startTime: null,
    maxAltitudeMeters: 0
  }
}

export class Flight {
  private state: FlightState = defaultFlightState()

  load(state: FlightState) {
    this.state = state
  }

  toState() {
    return this.state
  }

  createDraft() {

    this.state = {
      ...defaultFlightState(),
      id: 0,
      phase: FlightPhase.draft
    }
  }

  updateMeta(startTime: moment.Moment, lengthUS: number, maxAltitudeMeters: number) {
    this.state.startTime = startTime
    this.state.lengthUS = lengthUS
    this.state.maxAltitudeMeters = maxAltitudeMeters
  }

  setAsActive() {
    this.state.phase = FlightPhase.active
  }

  setAsDeleted() {
    this.state.phase = FlightPhase.deleted
  }

  get id() {
    return this.state.id
  }

  get phase() {
    return this.state.phase
  }

  get startTime() {
    return this.state.startTime
  }

  get endTime() {
    return this.state.startTime ? this.state.startTime.clone().add(Number(this.state.lengthUS / 1000)) : null
  }

  get lengthUS() {
    return this.state.lengthUS
  }

  get maxAltitudeMeters() {
    return this.state.maxAltitudeMeters
  }

  get isActive() {
    return this.state.phase == FlightPhase.active
  }
}

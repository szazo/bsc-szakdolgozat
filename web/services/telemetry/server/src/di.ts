import * as IoC from 'inversify'
import * as Db from './db'
import * as Domain from './domain'

export const telemetryModule = new IoC.ContainerModule(bind => {

  // Repository
  bind<Domain.TelemetryRepository>(Domain.TelemetryRepositorySymbol).to(Db.TelemetryRepository)
})

import { inject, injectable } from 'inversify';
import * as moment from 'moment'
import * as Slonik from 'slonik';
import { SlonikPoolSymbol, SlonikTransaction, SlonikContext } from '@av/db'
import * as Domain from '../domain'

interface FlightRecord {
  id: number
  phase: number
  start_time: Date
  length: number
  max_altitude: number
}

type TelemetryRecord = {
  flight_id: number
  time: bigint
  altitude: number
  height: number
  true_track: number
  magnetic_track: number
  ground_speed: number
  pressure: number
  temperature: number
  humidity: number,
  airspeed: number
} & OrientationRecord & GyroRecord & AccelRecord & PositionRecord

interface OrientationRecord {
  orientation_x: number | null
  orientation_y: number | null
  orientation_z: number | null
  orientation_w: number | null
}

interface GyroRecord {
  gyro_x: number
  gyro_y: number
  gyro_z: number
}

interface AccelRecord {
  accel_x: number
  accel_y: number
  accel_z: number
}

interface PositionRecord {
  latitude: number
  longitude: number
}

const SELECT_FLIGHT_FRAGMENT = Slonik.sql`
      SELECT
        id,
        phase,
        start_time,
        length,
        max_altitude
      FROM 
        flight 
`

@injectable()
export class TelemetryRepository implements Domain.TelemetryRepository {

  constructor(
    @inject(SlonikPoolSymbol)
    private pool: Slonik.DatabasePoolType,
    private slonikTransaction: SlonikTransaction,
    private slonikContext: SlonikContext) {}

  async loadFlight(id: number): Promise<Domain.Flight | null> {
    const found = await this.pool.query(Slonik.sql<FlightRecord>`
      ${SELECT_FLIGHT_FRAGMENT}
      WHERE id = ${id}
    `)

    if (found.rowCount === 0) {
      return null
    }

    const flight = new Domain.Flight()
    flight.load(this.flightRecordToState(found.rows[0]))
    return flight
  }

  async flights(phase: Domain.FlightPhase): Promise<Domain.Flight[]> {
    const found = await this.pool.query(Slonik.sql<FlightRecord>`
      ${SELECT_FLIGHT_FRAGMENT}
      WHERE phase = ${phase}
      ORDER BY start_time DESC
    `)

    return found.rows.map(row => {
      let flight = new Domain.Flight()
      flight.load(this.flightRecordToState(row))

      return flight
    })
  }

  async saveFlight(flight: Domain.Flight, transactionContext?: unknown) {

    const connection = this.slonikTransaction.resolveTransactionContext(transactionContext)

    const state = flight.toState()
    const record = this.flightStateToRecord(state)
    if (state.id) {
      await connection.query(Slonik.sql`
        UPDATE flight SET 
          phase = ${record.phase},
          start_time = ${record.startTime},
          length = ${record.length},
          max_altitude = ${record.maxAltitude}
        WHERE id = ${state.id}
      `)
      return state.id
    } else {
      const result = await connection.query(Slonik.sql<{ id: number }>`
        INSERT INTO flight (phase, start_time, length, max_altitude) VALUES 
        (${record.phase}, ${record.startTime}, ${record.length}, ${record.maxAltitude}) RETURNING id
     `)
      const id = result.rows[0].id
      flight.load({
        ...state,
        id: id
      })

      return id
    }
  }

  private flightStateToRecord(state: Domain.FlightState) {
    return {
      phase: state.phase,
      startTime: state.startTime ? state.startTime.toISOString() : null,
      length: Number(state.lengthUS),
      maxAltitude: state.maxAltitudeMeters
    }
  }

  private flightRecordToState(record: FlightRecord): Domain.FlightState {
    console.log('Flight record', record)
    return {
      id: record.id,
      phase: record.phase,
      startTime: record.start_time ? moment(record.start_time) : null,
      lengthUS: record.length,
      maxAltitudeMeters: record.max_altitude
    }
  }

  async queryTelemetry(flightId: number, startTime: bigint, endTime: bigint): Promise<Domain.TelemetryEntry[]> {
    const result = await this.pool.query(Slonik.sql<TelemetryRecord>`
      SELECT * FROM 
        telemetry 
      WHERE 
        flight_id = ${flightId} AND
        time BETWEEN ${Number(startTime)} AND ${Number(endTime)}
      ORDER BY time
    `)

    return result.rows.map(x => ({ time: x.time, data: this.recordToTelemetry(x) }))
  }

  async insertTelemetry(flightId: number, telemetries: Domain.TelemetryEntry[], dbContext?: unknown) {

    const connection = this.slonikContext.resolveContext(dbContext)
    
    const rows = telemetries.map(x => {

      return ([
        flightId,
        x.time,
        ...this.positionToRecord(x.data.position),
        ...this.orientationToRecord(x.data.orientation),
        ...this.axesToRecord(x.data.accel),
        ...this.axesToRecord(x.data.gyro),
        this.nullIfMissing(x.data.altitudeMeters), this.nullIfMissing(x.data.heightMeters),
        this.nullIfMissing(x.data.trueTrackDegrees), this.nullIfMissing(x.data.magneticTrackDegrees),
        this.nullIfMissing(x.data.groundSpeedKnots),
        this.nullIfMissing(x.data.pressurehPa), this.nullIfMissing(x.data.temperatureCelsius), this.nullIfMissing(x.data.relativeHumidity),
        this.nullIfMissing(x.data.airspeed)
    ] )})

//     for (const row of rows) {
//       const query = Slonik.sql`
//       INSERT INTO telemetry (flight_id, time, 
//         latitude, longitude,
//         orientation_x, orientation_y, orientation_z, orientation_w,
//         accel_x, accel_y, accel_z,
//         gyro_x, gyro_y, gyro_z,
//         altitude, height,
//         true_track, magnetic_track,
//         ground_speed
//       )
//       VALUES (
//         ${row[0] as any}, ${Number(row[1] as any)}, 
//         ${row[2] as any}, ${row[3] as any}, 
//         ${row[4] as any}, ${row[5] as any},${row[6] as any}, ${row[7] as any},
//         ${row[8] as any}, ${row[9] as any},${row[10] as any},
//         ${row[11] as any}, ${row[12] as any},${row[13] as any},
//         ${row[14] as any}, ${row[15] as any},
//         ${row[16] as any}, ${row[17] as any},
//         ${row[18] as any}
//       )
// `
//      await connection.query(query)
//     }

    
      const query = Slonik.sql`
      INSERT INTO telemetry (flight_id, time, 
        latitude, longitude,
        orientation_x, orientation_y, orientation_z, orientation_w,
        accel_x, accel_y, accel_z,
        gyro_x, gyro_y, gyro_z,
        altitude, height,
        true_track, magnetic_track,
        ground_speed,
        pressure, temperature, humidity,
        airspeed
      )
      SELECT * FROM
      ${Slonik.sql.unnest(rows, [
        'int4', 'int8',
        'float8', 'float8',
        'float8', 'float8', 'float8', 'float8',
        'float8', 'float8', 'float8',
        'float8', 'float8', 'float8',
        'float8', 'float8',
        'float8', 'float8',
        'float8',
        'float8', 'float8', 'float8',
        'float8'
      ])}
`
    await connection.query(query)
  }

  private recordToTelemetry(record: TelemetryRecord): Domain.Telemetry {
    return {
      orientation: this.recordToOrientation(record),
      gyro: this.gyroRecordToAxes(record),
      accel: this.accelRecordToAxes(record),
      position: this.recordToPosition(record),
      altitudeMeters: this.missingIfNull(record.altitude),
      heightMeters: this.missingIfNull(record.height),
      trueTrackDegrees: this.missingIfNull(record.true_track),
      magneticTrackDegrees: this.missingIfNull(record.magnetic_track),
      groundSpeedKnots: this.missingIfNull(record.ground_speed),
      pressurehPa: this.missingIfNull(record.pressure),
      temperatureCelsius: this.missingIfNull(record.temperature),
      relativeHumidity: this.missingIfNull(record.humidity),
      airspeed: this.missingIfNull(record.airspeed)
    }
  }

  private orientationToRecord(value: Domain.Orientation | Domain.MissingDataType) {
    if (value === Domain.MissingData) {
      return [ null, null, null, null ]
    }

    return [ value.x, value.y, value.z, value.w ]
  }

  private recordToOrientation(record: OrientationRecord) {
    if (record.orientation_x === null) {
      return Domain.MissingData
    }

    return {
      x: record.orientation_x,
      y: record.orientation_y!,
      z: record.orientation_z!,
      w: record.orientation_w!
    }
  }

  private axesToRecord(value: Domain.Axes | Domain.MissingDataType) {
    if (value === Domain.MissingData) {
      return [ null, null, null ]
    }

    return [ value.x, value.y, value.z ]
  }

  private gyroRecordToAxes(record: GyroRecord) {
    if (record.gyro_x === null) {
      return Domain.MissingData
    }

    return {
      x: record.gyro_x,
      y: record.gyro_y,
      z: record.gyro_z
    }
  }

  private accelRecordToAxes(record: AccelRecord) {
    if (record.accel_x === null) {
      return Domain.MissingData
    }

    return {
      x: record.accel_x,
      y: record.accel_y,
      z: record.accel_z
    }
  }

  private positionToRecord(value: Domain.GpsPosition | Domain.MissingDataType) {
    if (value === Domain.MissingData) {
      return [ null, null ]
    }

    return [ value.latitude, value.longitude ]
  }

  private recordToPosition(record: PositionRecord) {
    if (record.latitude === null) {
      return Domain.MissingData
    }

    return {
      latitude: record.latitude,
      longitude: record.longitude
    }
  }

  private nullIfMissing<T>(value: T | Domain.MissingDataType) {
    if (value === Domain.MissingData) { return null }

    return value
  }

  private missingIfNull<T>(recordValue: T | null) {
    if (recordValue === null) { return Domain.MissingData }

    return recordValue
  }
}

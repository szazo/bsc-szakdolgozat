export const MissingData = 'missing-data'
export type MissingDataType = typeof MissingData

type WithMissing<T> = {
    [P in keyof T]: T[P] | MissingDataType;
}

export interface TelemetryEntry {
  time: bigint
  data: Telemetry
}

export type Telemetry = WithMissing<TelemetryBase>

interface TelemetryBase {
  orientation: Orientation
  gyro: Axes
  accel: Axes
  position: GpsPosition
  altitudeMeters: number
  heightMeters: number
  trueTrackDegrees: number
  magneticTrackDegrees: number
  groundSpeedKnots: number
  pressurehPa: number
  temperatureCelsius: number
  relativeHumidity: number
  airspeed: number
}

export interface Orientation {
  x: number
  y: number
  z: number
  w: number
}

export interface Axes {
  x: number
  y: number
  z: number
}

// position in DDD.DDD format
export interface GpsPosition {
  latitude: number
  longitude: number
}

type TelemetryPropery = keyof TelemetryBase
const telemetryProperties: TelemetryPropery[] = [
  'orientation', 'gyro', 'accel', 'position', 'altitudeMeters', 'heightMeters', 'trueTrackDegrees', 'magneticTrackDegrees', 'groundSpeedKnots', 'pressurehPa', 'temperatureCelsius', 'relativeHumidity' ]

export function isTelemetryFilled(telemetry: Partial<Telemetry>) {
  const isFilled = <T>(value: undefined | T | MissingDataType) => {
    return value !== undefined
  }

  return isFilled(telemetry.orientation) &&
        isFilled(telemetry.gyro) &&
        isFilled(telemetry.accel) &&
        isFilled(telemetry.position) &&
        isFilled(telemetry.altitudeMeters) &&
        isFilled(telemetry.heightMeters) &&
        isFilled(telemetry.trueTrackDegrees) &&
        isFilled(telemetry.magneticTrackDegrees) &&
        isFilled(telemetry.groundSpeedKnots)
}

export const defaultTelemetry = () => fillTelemetryWithMissing({})

export function fillTelemetryWithMissing(telemetry: Partial<Telemetry>): Telemetry {

  let result = { ...telemetry }
  for (const property of telemetryProperties) {
    if (result[property] === undefined) {
      result[property] = MissingData
    }
  }

  return <Telemetry> result
}

export function missingIfUndefined<T>(input: T | undefined) {
  if (input === undefined) {
    return MissingData
  }

  return input
}

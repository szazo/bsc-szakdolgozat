import { inject, injectable } from 'inversify'
import * as moment from 'moment'
import * as Slonik from 'slonik';
import { SlonikPoolSymbol } from '@av/db'
import * as Domain from '../domain'

interface TelemetryQueryRow {
  serial: number
  time: number
  sensor_id: number
  message_type: number
  content: Buffer
}

@injectable()
export class RawTelemetryRepository implements Domain.RawTelemetryRepository {

  constructor(
    @inject(SlonikPoolSymbol)
    private pool: Slonik.DatabasePoolType) {
  }

  async query(rawTelemetryFileId: number, order: Domain.Order, offset: number, limit: number): Promise<Domain.RawTelemetry[]> {

    const sql = Slonik.sql<TelemetryQueryRow>`
      SELECT
        serial, time, sensor_id, message_type, content
      FROM
        raw_telemetry
      WHERE
        raw_telemetry_file_id = ${rawTelemetryFileId}
        ORDER BY ${Slonik.sql.identifier([order])}
      LIMIT ${limit} OFFSET ${offset}
`
    const found = await this.pool.query(sql)

    return found.rows.map(x => ({
      serial: BigInt(x.serial),
      time: BigInt(x.time),
      sensorId: x.sensor_id,
      messageType: x.message_type,
      content: x.content
    }))
  }

  async insert(clientId: string, filename: string, eof: boolean, items: Domain.RawTelemetry[]): Promise<void> {

    let fileId = await this.queryFileId(clientId, filename)
    if (!fileId) {
      fileId = await this.tryInsertFile(clientId, filename, moment())
    }
    if (!fileId) {
      fileId = await this.queryFileId(clientId, filename)
    }

    if (fileId === null) {
      throw new Error(`Raw telemetry file couldn't be created`)
    }

    if (items.length > 0) {
      this.insertTelemetries(fileId!, items)
    }

    if (eof) {
      await this.updateFilePhase(fileId!, Domain.RawTelemetryFilePhase.eof)
    }
  }

  private async queryFileId(clientId: string, filename: string) {
    const fileIdResult = await this.pool.query(Slonik.sql<{id: number}>`
      SELECT id FROM raw_telemetry_file WHERE client_id = ${clientId} AND filename = ${filename}
    `)

    if (fileIdResult.rowCount > 0) {
      return fileIdResult.rows[0].id
    }

    return null
  }
  
  private async tryInsertFile(clientId: string, filename: string, time: moment.Moment) {
    const timeString = time.toISOString()
    const result = await this.pool.query(Slonik.sql<{ id: number }>`
      INSERT INTO raw_telemetry_file 
        (client_id, filename, phase, revision, created_at, updated_at) 
      VALUES 
        (${clientId}, ${filename}, ${Domain.RawTelemetryFilePhase.syncing}, ${timeString}, ${timeString}, ${timeString}) 
      ON CONFLICT DO NOTHING RETURNING ID`)

    if (result.rowCount > 0) {
      return result.rows[0].id
    }

    return null
  }

  private async updateFilePhase(fileId: number, phase: Domain.RawTelemetryFilePhase) {
    await this.pool.query(Slonik.sql`
          UPDATE raw_telemetry_file SET 
            phase = ${phase},
            updated_at = ${moment().toISOString()}
          WHERE id = ${fileId}
    `)
  }

  private async insertTelemetries(fileId: number, input: Domain.RawTelemetry[]) {
    const time = moment().toISOString()
    const rows = input.map(x => ([
      x.serial, x.time, fileId, x.messageType, x.sensorId, x.content, time, time
    ]))
    await this.pool.query(Slonik.sql`
      INSERT INTO raw_telemetry (
        serial, 
        time, 
        raw_telemetry_file_id,
        message_type,
        sensor_id,
        content,
        created_at,
        updated_at
      )
      SELECT * FROM
      ${Slonik.sql.unnest(rows, [
        'int8', 
        'int8',
        'int4',
        'int4',
        'int4',
        'bytea',
        'timestamptz',
        'timestamptz'
          ])}
      ON CONFLICT DO NOTHING
`);
  }  
}

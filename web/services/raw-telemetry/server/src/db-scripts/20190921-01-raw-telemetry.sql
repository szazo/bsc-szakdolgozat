CREATE TABLE raw_telemetry_file (
       id SERIAL PRIMARY KEY,
       client_id VARCHAR(255) NOT NULL,
       filename VARCHAR(255) NOT NULL,
       phase INT NOT NULL,
       revision TIMESTAMP WITH TIME ZONE NOT NULL,
       created_at TIMESTAMP WITH TIME ZONE NOT NULL,
       updated_at TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE raw_telemetry (
       serial BIGINT NOT NULL,
       time BIGINT NOT NULL,
       raw_telemetry_file_id INT NOT NULL REFERENCES raw_telemetry_file(id),
       message_type INT NOT NULL,
       sensor_id INT NOT NULL,
       content BYTEA NOT NULL,
       created_at TIMESTAMP WITH TIME ZONE NOT NULL,
       updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
       PRIMARY KEY(serial, time)
);

SELECT create_hypertable('raw_telemetry', 'time', chunk_time_interval => 864000000000);

import * as IoC from 'inversify'
import * as Db from './db'
import * as Domain from './domain'

export const rawTelemetryModule = new IoC.ContainerModule(bind => {

  // Repository
  bind<Domain.RawTelemetryRepository>(Domain.RawTelemetryRepositorySymbol).to(Db.RawTelemetryRepository)
})


export interface RawTelemetry {
  serial: bigint
  time: bigint
  sensorId: number
  messageType: number
  content: Buffer
}

export enum RawTelemetryFilePhase {
  none,
  syncing,
  eof
}

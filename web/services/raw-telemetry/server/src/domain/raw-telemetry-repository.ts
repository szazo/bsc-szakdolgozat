import { RawTelemetry } from './raw-telemetry'

export const RawTelemetryRepositorySymbol = Symbol.for('RawTelemetryRepository')

export type Order = 'time' | 'serial'

export interface RawTelemetryRepository {
  insert(clientId: string, filename: string, eof: boolean, items: RawTelemetry[]): Promise<void>;
  query(rawTelemetryFileId: number, order: Order, offset: number, limit: number): Promise<RawTelemetry[]>
}

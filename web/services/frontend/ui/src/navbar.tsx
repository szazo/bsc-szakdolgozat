import * as React from 'react'
import * as Router from 'react-router'
import * as RouterDom from 'react-router-dom'
import * as classnames from 'classnames'

interface NavLinkProps {
  to: string
  exact?: boolean
  children: (isActive: boolean) => React.ReactNode
}

const NavLink: React.SFC<NavLinkProps> = ({ to, exact, children }) => {

  return <Router.Route path={to} exact={exact} children={(props) => {
    const active = props.match
    return children(!!active)
  }} />
}

interface NavItemProps {
  to: string
  exact?: boolean
}

const NavItem: React.SFC<NavItemProps> = ({to, exact, children}) => {

  return <li className="mr-3">
  <NavLink to={to} exact={exact}>
  {(isActive) => {

    const classes = classnames('inline-block border rounded py-1 px-3', {
      'border-blue-500 bg-blue-500 text-white': isActive,
      'border-white hover:border-gray-200 text-blue-500 hover:bg-gray-200': !isActive
    })

    return <RouterDom.Link to={to} className={classes}>{children}</RouterDom.Link>
      }}
    </NavLink>
  </li>
}

export class Navbar extends React.Component {

  render() {
    return <nav role="navigation" aria-label="main navigation" className="border-blue-500 border-b-2 p-2 bg-gray-100">
      <ul className="flex">
        <NavItem to="/playback">Playback</NavItem>
        <NavItem to="/live" exact>Live Broadcast</NavItem>
      </ul>
    </nav>
  }
}

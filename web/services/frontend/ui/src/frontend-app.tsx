import * as moment from 'moment'
import * as React from 'react'
import * as RouterDom from 'react-router-dom'
import { Playback } from '@av/playback-ui'
import { LiveBroadcast } from '@av/live-broadcast-ui'
import { Navbar } from './navbar'

require(__dirname + '/styles/frontend.css')

moment.locale('hu')

export class FrontendApp extends React.Component {

  render() {
    return <RouterDom.BrowserRouter>
      <div className="h-full flex flex-col">
        <Navbar />
        <div className="flex-grow min-h-0"> {/* https://moduscreate.com/blog/how-to-fix-overflow-issues-in-css-flex-layouts/ */}
          <RouterDom.Switch>
            <RouterDom.Route exact path='/'
              component={()=>(<RouterDom.Redirect to='/playback'/>)}/>
            <RouterDom.Route path="/live">
              <LiveBroadcast />
              </RouterDom.Route>
              <RouterDom.Route path="/playback/:flightId?" component={Playback} />
          </RouterDom.Switch>
        </div>
      </div>
    </RouterDom.BrowserRouter>
  }
}

import * as IoC from 'inversify'
import * as express from 'express'
import * as http from 'http'

export const ExpressSymbol = Symbol.for('ExpressApp')
export const HttpServerFactorySymbol = Symbol.for('Factory<HttpServer>')

let server: http.Server | null = null

export const expressModule = new IoC.ContainerModule((bind) => {
  bind<express.Express>(ExpressSymbol)
        .toDynamicValue(() => express())
        .inSingletonScope()

  bind<IoC.interfaces.Factory<http.Server>>(HttpServerFactorySymbol).toFactory<http.Server>(_c => {
    return (app: express.Express) => {
      if (!server) {
        server = new http.Server(app)
      }

      return server
    }
  })
})

import { inject, injectable } from 'inversify'
import * as Slonik from 'slonik';
import { SlonikPoolSymbol } from './slonik'
import * as TransactionApi from '@av/db-api'

@injectable()
export class SlonikTransaction implements TransactionApi.Transaction {

  constructor(
    @inject(SlonikPoolSymbol)
    private pool: Slonik.DatabasePoolType) {
  }

  withTransaction<TResult>(func: (transactionConnection: Slonik.DatabaseTransactionConnectionType) => Promise<TResult>): PromiseLike<TResult> {

    return this.pool.transaction(async (transactionConnection) => {
      return await func(transactionConnection)
    })
  }

  resolveTransactionContext(transactionContext?: unknown) {
    return transactionContext ? <Slonik.DatabaseTransactionConnectionType> transactionContext : this.pool
  }
}

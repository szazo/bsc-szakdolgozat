import { inject, injectable } from 'inversify'
import * as Slonik from 'slonik';
import { SlonikPoolSymbol } from './slonik'
import * as Api from '@av/db-api'

@injectable()
export class SlonikContext implements Api.DbContext {

  constructor(
    @inject(SlonikPoolSymbol)
    private pool: Slonik.DatabasePoolType) {
  }

  withContext<TResult>(func: (connection: Slonik.DatabasePoolConnectionType) => Promise<TResult>): PromiseLike<TResult> {

    return this.pool.connect(async (connection) => {
      return await func(connection)
    })
  }

  resolveContext(context?: unknown) {
    // console.log('CONTEXT', context)
    if (!context) {
      return this.pool
    }

    return <Slonik.DatabasePoolConnectionType> context
  }
}

import * as IoC from 'inversify'
import * as Slonik from 'slonik';
import * as Api from '@av/db-api'
import { environmentDbConfig } from './db-config'
import { SlonikPoolSymbol, slonikConnectionString } from './slonik';
import { SlonikTransaction } from './slonik-transaction';
import { SlonikContext } from './slonik-context'

export const dbModule = new IoC.ContainerModule(bind => {

  bind<Slonik.DatabasePoolType>(SlonikPoolSymbol).toDynamicValue(_context => {
    const config = environmentDbConfig()
    const pool = Slonik.createPool(slonikConnectionString(config))

    return pool
  }).inSingletonScope()
  
  bind<Api.Transaction>(Api.TransactionSymbol).to(SlonikTransaction)
  bind<Api.DbContext>(Api.DbContextSymbol).to(SlonikContext)
  bind(SlonikTransaction).toSelf()
  bind(SlonikContext).toSelf()
})

import { DbConfig } from "./db-config";

export const SlonikPoolSymbol = Symbol.for('SlonikPool')

export function slonikConnectionString(config: DbConfig) {
  return `postgres://${config.username}:${config.password}@${config.host}/${config.database}`
}

export interface DbConfig {
  host: string
  database: string
  username: string
  password: string
}

export function environmentDbConfig(): DbConfig {
  return {
    host: process.env.PCFG_DB_HOST || '',
    database: process.env.PCFG_DB_DATABASE || '',
    username: process.env.PCFG_DB_USERNAME || '',
    password: process.env.PCFG_DB_PASSWORD || ''
  }
}

export const TransactionSymbol = Symbol.for('Transaction')

export interface Transaction {
  withTransaction<TResult>(func: (context: unknown) => Promise<TResult>): PromiseLike<TResult>
}

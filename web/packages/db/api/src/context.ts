export const DbContextSymbol = Symbol.for('DbContext')

export interface DbContext {
  withContext<TResult>(func: (context: unknown) => Promise<TResult>): PromiseLike<TResult>
}

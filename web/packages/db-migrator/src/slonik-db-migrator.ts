import * as Slonik from 'slonik'
import { setupSlonikMigrator, SlonikMigrator } from '@slonik/migrator'
import { DbConfig, slonikConnectionString } from '@av/db'

export class SlonikDbMigrator {

  private migrator: SlonikMigrator

  constructor(config: DbConfig, private path: string) {

    const pool = Slonik.createPool(slonikConnectionString(config))

    this.migrator = setupSlonikMigrator({
      migrationsPath: path,
      slonik: pool
    })
  }

  async up() {
    console.info('Migrating...')
    const applied = await this.migrator.up()
    if (applied.length > 0) {
      console.info('Executed migrations: ', applied.map(x => x.file))
    } else {
      console.info('No migration executed')
    }
  }

  async down() {
    console.info('Rolling back last migration...')
    const rolledBack = await this.migrator.down()
    if (rolledBack.length > 0) {
      console.info('Rolled back migration: ', rolledBack.map(x => x.file))
    } else {
      console.info('No migration rolled back')
    }
  }

  create(migration: string) {
    this.migrator.create(migration)
    console.log(`Migration created with name ${migration} in directory ${this.path}`)
  }
}

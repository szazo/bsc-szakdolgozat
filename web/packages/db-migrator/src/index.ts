import 'reflect-metadata'
import * as Slonik from 'slonik'
import { setupSlonikMigrator } from '@slonik/migrator'
import { environmentDbConfig } from '@av/db'
import { SlonikDbMigrator } from './slonik-db-migrator'

async function main(args: string[]) {

  if (args.length < 4) {
    console.warn('USAGE: migrate <migration files path> up|down|create <name>')
    return 1
  }

  const path = args[2]
  const command = args[3].toLowerCase()
  let migrator = new SlonikDbMigrator(environmentDbConfig(), path)

  try {
    switch (command) {
      case 'up':
        await migrator.up()
        break
      case 'down':
        await migrator.down()
        break
      case 'create':
        if (args.length < 5) {
          console.warn('Missing name of the migration to be created')
          return 1
        }
        migrator.create(args[4])
        break
      default:
        console.warn(`Unknown command ${command}`)
        return 1
    }

    return 0
  } catch (ex) {
    console.error(ex)
    return 1
  }
}

main(process.argv).then(resultCode => { process.exit(resultCode) })

import * as IoC from 'inversify'
import getDecorators from 'inversify-inject-decorators'

export const container = new IoC.Container()

const { lazyInject } = getDecorators(container)
export { lazyInject }

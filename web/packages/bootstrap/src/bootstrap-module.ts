
export const BootstrapModuleSymbol = Symbol.for('BootstrapModule')

export interface BootstrapModule {
  start():Promise<void>
  stop(): Promise<void>
}

#include "pressure_serializer.h"
#include "pressure.pb.h"

bool pressure_serialize(const SerializerMessageType message_type,
                        const void *data, pb_ostream_t *stream, void *arg) {

  const PressureMessage *message = (const PressureMessage *) data;
  ProtoPressure target = {
    .pressure = message->pressure,
    .temperature = message->temperature,
    .humidity = message->humidity
  };
  return pb_encode_submessage(stream, ProtoPressure_fields, &target);
}

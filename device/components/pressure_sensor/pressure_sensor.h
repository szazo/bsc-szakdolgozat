#ifndef PRESSURE_SENSOR_H
#define PRESSURE_SENSOR_H

#include "esp_timer.h"
#include "bmp280.h"
#include "error.h"
#include "sensor_api.h"

#define PRESSURE_MESSAGE_BASE 0x04

typedef enum {
  PRESSURE = MEASUREMENT_TYPE(PRESSURE_MESSAGE_BASE, 1)
} PressureMessageType;

AV_ERROR_DEFINE(PRESSURE_SENSOR_CREATE_TIMER_ERROR, 0x0B01)
AV_ERROR_DEFINE(PRESSURE_SENSOR_START_ERROR, 0x0B02)
AV_ERROR_DEFINE(PRESSURE_SENSOR_STOP_ERROR, 0x0B03)
AV_ERROR_DEFINE(PRESSURE_SENSOR_READ_ERROR, 0x0B04)

#define PRESSURE_SENSOR() \
  { .init = pressure_sensor_api_init, \
    .deinit = pressure_sensor_api_deinit, \
    .start = pressure_sensor_api_start, \
    .stop_and_wait = pressure_sensor_api_stop, \
    .read = pressure_sensor_api_read, \
    .read_return = pressure_sensor_api_return \
  }

#define PRESSURE_SENSOR_DESCRIPTOR(ID, SENSOR) \
  { .id = ID, \
    .sensor = PRESSURE_SENSOR(), \
    .arg = SENSOR }

typedef struct {
  float pressure;
  float temperature;
  float humidity;
} PressureMessage;

#define PRESSURE_MEASUREMENT_SIZE (sizeof(Measurement) + sizeof(PressureMessage))

typedef struct {

  /**
   * The identifier of the sensor, it should be sent in the measurement.
   */
  SensorId sensor_id;

  /**
   * Reference to the sensor api.
   */
  SensorApiHandle sensor_api;

  /**
   * The timer which is used to schedule reading from the sensor.
   */
  esp_timer_handle_t timer;

  /**
   * The sensor instance used to read the values.
   */
  bmp280_t bmp280;

  /**
   * We don't allocate from the heap for each measurment, we use this buffer when
   * read requested by the governor.
   */
  char measurement_buffer[PRESSURE_MEASUREMENT_SIZE];

} PressureSensor;

typedef void* PressureSensorHandle;

ErrorResult pressure_sensor_create(PressureSensorHandle *handle);
void pressure_sensor_delete(PressureSensorHandle sensor_handle);

bool pressure_sensor_api_init(const SensorApiHandle sensor_api_handle, const SensorId id, void *arg);
void pressure_sensor_api_deinit(void *arg);

bool pressure_sensor_api_start(void *arg);
void pressure_sensor_api_stop(void *arg);

Measurement* pressure_sensor_api_read(void *arg);
void pressure_sensor_api_return(Measurement *measurement, void *arg);

#endif


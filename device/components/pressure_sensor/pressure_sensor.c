#include "pressure_sensor.h"
#include <freertos/FreeRTOS.h>
#include <string.h>

//#include "freertos/FreeRTOS.h"
#include "esp_log.h"

#define SDA_GPIO 23
#define SCL_GPIO 22

static const char *TAG = "pressure_sensor";

#define PRESSURE_SAMPLING_US 100000 // 100 ms

/**
 * Timer callback for sensor reading.
 */
static void timer_callback(void* arg);

ErrorResult pressure_sensor_create(PressureSensorHandle *handle) {
  PressureSensor *me = malloc(sizeof(PressureSensor));
  const esp_timer_create_args_t timer_args = {
    .callback = &timer_callback,
    .name = "pressure",
    .arg = me
  };
  esp_err_t err = esp_timer_create(&timer_args, &me->timer);
  if (err != ESP_OK) {
    free(me);
    return AV_ERROR_WITH_REASON(PRESSURE_SENSOR_CREATE_TIMER_ERROR, err);
  }

  ESP_LOGI(TAG, "Pressure sensor created");
  *handle = me;
  return AV_OK_RESULT;
}

void pressure_sensor_delete(PressureSensorHandle handle) {
  PressureSensor *me = handle;
  esp_timer_delete(me->timer);
  free(me);
  ESP_LOGI(TAG, "Pressure sensor deleted");
}

bool pressure_sensor_api_init(const SensorApiHandle sensor_api_handle, const SensorId id, void *arg) {

  ESP_LOGI(TAG, "Initializing pressure");

  PressureSensor *me = arg;
  me->sensor_id = id;
  me->sensor_api = sensor_api_handle;

  while (i2cdev_init() != ESP_OK)
    {
      printf("Could not init I2Cdev library\n");
      vTaskDelay(250 / portTICK_PERIOD_MS);
    }

  bmp280_params_t params;
  bmp280_init_default_params(&params);
  memset(&me->bmp280, 0, sizeof(bmp280_t));
  while (bmp280_init_desc(&me->bmp280, BMP280_I2C_ADDRESS_0, 0, SDA_GPIO, SCL_GPIO) != ESP_OK)
    {
      printf("Could not init device descriptor\n");
      vTaskDelay(250 / portTICK_PERIOD_MS);
    }

  esp_err_t res;
  while ((res = bmp280_init(&me->bmp280, &params)) != ESP_OK)
    {
      printf("Could not init BMP280, err: %d\n", res);
      vTaskDelay(250 / portTICK_PERIOD_MS);
    }

  ESP_LOGI(TAG, "Pressure sensor initialized");
  return true;
}

void pressure_sensor_api_deinit(void *arg) {
  
}

bool pressure_sensor_api_start(void *arg) {
  PressureSensor *me = arg;
  esp_err_t err = esp_timer_start_periodic(me->timer, PRESSURE_SAMPLING_US);
  if (err != ESP_OK) {
    av_error_publish_with_reason(SEVERITY_ERROR, PRESSURE_SENSOR_START_ERROR, err);
    return false;
  }

  return true;
}

void pressure_sensor_api_stop(void *arg) {
  PressureSensor *me = arg;
  esp_err_t err = esp_timer_stop(me->timer);
  if (err != ESP_OK) {
    av_error_publish_with_reason(SEVERITY_ERROR, PRESSURE_SENSOR_STOP_ERROR, err);
  }
}

Measurement* pressure_sensor_api_read(void *arg) {

  PressureSensor *me = arg;
  Measurement *measurement = (Measurement *) me->measurement_buffer;

  set_measurement_header(measurement,
                         me->sensor_id,
                         sensor_get_time(),
                         PRESSURE,
                         sizeof(PressureMessage));
  //ESP_LOGI(TAG, "adc reading: %d", adc_reading);

  PressureMessage *message = (PressureMessage *)measurement->data;
  esp_err_t err = bmp280_read_float(&me->bmp280, &message->temperature, &message->pressure, &message->humidity);
  if (err != ESP_OK) {
    av_error_publish_with_reason(SEVERITY_ERROR, PRESSURE_SENSOR_READ_ERROR, err);
    return NULL;
  }

  /* printf("Pressure: %.2f Pa, Temperature: %.2f C", pressure, temperature); */
  /* printf(", Humidity: %.2f\n", humidity); */

  return measurement;
}

void pressure_sensor_api_return(Measurement *measurement, void *arg) {
  // NOP: because we use static buffer
}

static void timer_callback(void* arg)
{
  PressureSensor *me = arg;
  sensor_send_request(me->sensor_api, me->sensor_id);
}

#ifndef PRESSURE_SERIALIZER_H
#define PRESSURE_SERIALIZER_H

#include "pressure_sensor.h"
#include "measurement_serializer.h"

bool pressure_serialize(const SerializerMessageType message_type, const void *data, pb_ostream_t *stream, void *arg);

#endif

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "esp_log.h"

#include "dummy_sensor.h"

static const int SENSOR_TASK_STACK_SIZE = 2 * 1024;
static const char *TAG = "dummy_sensor";

const static int STOPPED_BIT = BIT0;

static void sensor_task(void *param);

/* void dummy_sensor_create_descriptor(DummySensor *me, SensorDescriptor *descriptor) { */
/*   descriptor->init = &dummy_sensor_api_init; */
/*   descriptor->start = &dummy_sensor_api_start; */
/*   descriptor->stop = &dummy_sensor_api_stop; */
/*   descriptor->arg = me; */
/* } */

DummySensorHandle dummy_sensor_create() {
  DummySensor *sensor = malloc(sizeof(DummySensor));
  sensor->stopped_event_group = xEventGroupCreate();
  ESP_LOGI(TAG, "Dummy sensor created");
  return sensor;
}

void dummy_sensor_delete(DummySensorHandle handle) {
  DummySensor *me = handle;
  vEventGroupDelete(me->stopped_event_group);
  free(me);
  ESP_LOGI(TAG, "Dummy sensor deleted");
}

bool dummy_sensor_api_init(const SensorApiHandle sensor_handle, const SensorId id, void *arg) {
  DummySensor *me = arg;
  me->sensor_id = id;
  return true;
}

void dummy_sensor_api_deinit(void *arg) {
}


bool dummy_sensor_api_start(void *arg) {

  DummySensor *me = arg;
  me->task_stop_request = false;

  ESP_LOGI(TAG, "Starting dummy sensor...");

  BaseType_t task_result = xTaskCreatePinnedToCore(sensor_task, "dummy_sensor_task",
                                                   SENSOR_TASK_STACK_SIZE,
                                                   me, 5,
                                                   &me->sensor_task_handle, APP_CPU_NUM);

  if (task_result != pdPASS) {
    ESP_LOGE(TAG, "Couldn't create sensor task");
    return false;
  }

  return true;
}

void dummy_sensor_api_stop(void *arg) {
  DummySensor *me = arg;
  me->task_stop_request = true;

  xEventGroupWaitBits(me->stopped_event_group,
                      STOPPED_BIT,
                      false,
                      true, portMAX_DELAY);
}


static void sensor_task(void *param) {

  DummySensor *me = param;
  
  while (!me->task_stop_request) {

    ESP_LOGI(TAG, "Dummy sensor task %d", me->sensor_id);
    vTaskDelay(pdMS_TO_TICKS(1000));
  }

  
  vTaskDelete(NULL);
}

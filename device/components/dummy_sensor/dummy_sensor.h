#ifndef DUMMY_SENSOR_H
#define DUMMY_SENSOR_H

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"

#include "sensor_api.h"

#define DUMMY_SENSOR() \
  { .init = dummy_sensor_api_init, \
    .deinit = dummy_sensor_api_deinit, \
    .start = dummy_sensor_api_start, \
    .stop_and_wait = dummy_sensor_api_stop \
  }

#define DUMMY_SENSOR_DESCRIPTOR(ID, SENSOR) \
  { .id = ID, \
    .sensor = DUMMY_SENSOR(), \
    .arg = SENSOR }

typedef struct {
  TaskHandle_t sensor_task_handle;
  const SensorApiHandle sensor_handle;

  SensorId sensor_id;
  
  /**
   * Whether stop is requested from outside.
   */
  volatile bool task_stop_request;

  /**
   * Whether the sensor task successfully stopped.
   */
  EventGroupHandle_t stopped_event_group;

} DummySensor;

typedef void* DummySensorHandle;

DummySensorHandle dummy_sensor_create();
void dummy_sensor_delete(DummySensorHandle handle);
bool dummy_sensor_api_init(const SensorApiHandle sensor_handle, const SensorId id, void *arg);
bool dummy_sensor_api_start(void *arg);
void dummy_sensor_api_stop(void *arg);
void dummy_sensor_api_deinit(void *arg);

#endif

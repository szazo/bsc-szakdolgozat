#ifndef AIRSPEED_SERIALIZER_H
#define AIRSPEED_SERIALIZER_H

#include "airspeed_sensor.h"
#include "measurement_serializer.h"

bool airspeed_serialize(const SerializerMessageType message_type, const void *data, pb_ostream_t *stream, void *arg);

#endif

#include "airspeed_sensor.h"

#include "freertos/FreeRTOS.h"
#include "esp_adc_cal.h"
#include "driver/adc.h"
#include "esp_log.h"

static const char *TAG = "airspeed_sensor";

/**
 * Timer callback for sensor reading.
 */
static void timer_callback(void* arg);

static void configure_adc(AirspeedSensor *me);

#define AIRSPEED_SAMPLING_US 100000 // 100 ms
#define DEFAULT_VREF 1070 // Use adc2_vref_to_gpio() to obtain a better estimate
#define NO_OF_SAMPLES 32 // count of samples for multisampling

static const adc1_channel_t channel = ADC1_GPIO34_CHANNEL;
static const adc_atten_t attenuation = ADC_ATTEN_DB_11;

ErrorResult airspeed_sensor_create(AirspeedSensorHandle *handle) {
  AirspeedSensor *me = malloc(sizeof(AirspeedSensor));

  const esp_timer_create_args_t timer_args = {
    .callback = &timer_callback,
    .name = "airspeed",
    .arg = me
  };
  esp_err_t err = esp_timer_create(&timer_args, &me->timer);
  if (err != ESP_OK) {
    free(me);
    return AV_ERROR_WITH_REASON(AIRSPEED_SENSOR_CREATE_TIMER_ERROR, err);
  }
  
  ESP_LOGI(TAG, "Airspeed sensor created");
  *handle = me;
  return AV_OK_RESULT;
}

void airspeed_sensor_delete(AirspeedSensorHandle handle) {
  AirspeedSensor *me = handle;
  esp_timer_delete(me->timer);
  free(me);
  ESP_LOGI(TAG, "Airspeed sensor deleted");
}

bool airspeed_sensor_api_init(const SensorApiHandle sensor_api_handle, const SensorId id, void *arg) {
  AirspeedSensor *me = arg;
  me->sensor_id = id;
  me->sensor_api = sensor_api_handle;
  configure_adc(me);
  return true;
}

static void log_characterize_type(esp_adc_cal_value_t val_type)
{
    if (val_type == ESP_ADC_CAL_VAL_EFUSE_TP) {
      ESP_LOGI(TAG, "Characterized using Two Point Value\n");
    } else if (val_type == ESP_ADC_CAL_VAL_EFUSE_VREF) {
      ESP_LOGI(TAG, "Characterized using eFuse Vref\n");
    } else {
      ESP_LOGI(TAG, "Characterized using Default Vref\n");
    }
}

static void configure_adc(AirspeedSensor *me) {
  adc1_config_width(ADC_WIDTH_BIT_12);
  adc1_config_channel_atten(channel, attenuation);

  me->adc_chars = calloc(1, sizeof(esp_adc_cal_characteristics_t));
  esp_adc_cal_value_t val_type = esp_adc_cal_characterize(ADC_UNIT_1,
                                                          attenuation,
                                                          ADC_WIDTH_BIT_12,
                                                          DEFAULT_VREF,
                                                          me->adc_chars);
  log_characterize_type(val_type);
}

void airspeed_sensor_api_deinit(void *arg) {
  
}

bool airspeed_sensor_api_start(void *arg) {
  AirspeedSensor *me = arg;
  esp_err_t err = esp_timer_start_periodic(me->timer, AIRSPEED_SAMPLING_US);
  if (err != ESP_OK) {
    av_error_publish_with_reason(SEVERITY_ERROR, AIRSPEED_SENSOR_START_ERROR, err);
    return false;
  }

  return true;
}

void airspeed_sensor_api_stop(void *arg) {
  AirspeedSensor *me = arg;
  esp_err_t err = esp_timer_stop(me->timer);
  if (err != ESP_OK) {
    av_error_publish_with_reason(SEVERITY_ERROR, AIRSPEED_SENSOR_STOP_ERROR, err);
  }
}

Measurement* airspeed_sensor_api_read(void *arg) {
  AirspeedSensor *me = arg;

  uint32_t adc_reading = 0;
  //Multisampling
  for (int i = 0; i < NO_OF_SAMPLES; i++) {
    adc_reading += adc1_get_raw((adc1_channel_t)channel);
  }
  adc_reading /= NO_OF_SAMPLES;

  Measurement *measurement = (Measurement *) me->measurement_buffer;
  AirspeedMessage *message = (AirspeedMessage *)measurement->data;

  message->adc_reading = adc_reading;

  set_measurement_header(measurement,
                         me->sensor_id,
                         sensor_get_time(),
                         AIRSPEED,
                         sizeof(AirspeedMessage));

  //ESP_LOGI(TAG, "adc reading: %d", adc_reading);
  return measurement;
}

void airspeed_sensor_api_return(Measurement *measurement, void *arg) {
  // NOP: because we use static buffer
}

static void timer_callback(void* arg)
{
  AirspeedSensor *me = arg;
  sensor_send_request(me->sensor_api, me->sensor_id);
}

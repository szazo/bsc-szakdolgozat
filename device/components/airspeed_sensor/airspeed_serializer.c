#include "airspeed_serializer.h"
#include "airspeed.pb.h"

bool airspeed_serialize(const SerializerMessageType message_type,
                        const void *data, pb_ostream_t *stream, void *arg) {

  const AirspeedMessage *message = (const AirspeedMessage *) data;
  ProtoAirspeed target = {
    .adc_reading = message->adc_reading
  };
  return pb_encode_submessage(stream, ProtoAirspeed_fields, &target);
}

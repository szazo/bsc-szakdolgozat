#ifndef AIRSPEED_SENSOR_H
#define AIRSPEED_SENSOR_H

#include "esp_timer.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"

#include "error.h"
#include "sensor_api.h"

#define AIRSPEED_MESSAGE_BASE 0x03

typedef enum {
  AIRSPEED = MEASUREMENT_TYPE(AIRSPEED_MESSAGE_BASE, 1)
} AirspeedMessageType;

AV_ERROR_DEFINE(AIRSPEED_SENSOR_CREATE_TIMER_ERROR, 0x0901)
AV_ERROR_DEFINE(AIRSPEED_SENSOR_START_ERROR, 0x0902)
AV_ERROR_DEFINE(AIRSPEED_SENSOR_STOP_ERROR, 0x0903)

#define AIRSPEED_SENSOR() \
  { .init = airspeed_sensor_api_init, \
    .deinit = airspeed_sensor_api_deinit, \
    .start = airspeed_sensor_api_start, \
    .stop_and_wait = airspeed_sensor_api_stop, \
    .read = airspeed_sensor_api_read, \
    .read_return = airspeed_sensor_api_return \
  }

#define AIRSPEED_SENSOR_DESCRIPTOR(ID, SENSOR) \
  { .id = ID, \
    .sensor = AIRSPEED_SENSOR(), \
    .arg = SENSOR }

typedef struct {
  int16_t adc_reading;
} AirspeedMessage;

#define AIRSPEED_MEASUREMENT_SIZE sizeof(Measurement) + sizeof(AirspeedMessage)

typedef struct {

  /**
   * The identifier of the sensor, it should be sent in the measurement.
   */
  SensorId sensor_id;

  /**
   * Reference to the sensor api.
   */
  SensorApiHandle sensor_api;

  /**
   * The timer which is used to schedule reading from the sensor.
   */
  esp_timer_handle_t timer;

  /**
   * Measured ADC characteristic at initialization
   */
  esp_adc_cal_characteristics_t *adc_chars;

  /**
   * We don't allocate from the heap for each measurment, we use this buffer when
   * read requested by the governor.
   */
  char measurement_buffer[AIRSPEED_MEASUREMENT_SIZE];

} AirspeedSensor;

typedef void* AirspeedSensorHandle;

ErrorResult airspeed_sensor_create(AirspeedSensorHandle *handle);
void airspeed_sensor_delete(AirspeedSensorHandle sensor_handle);

bool airspeed_sensor_api_init(const SensorApiHandle sensor_api_handle, const SensorId id, void *arg);
bool airspeed_sensor_api_start(void *arg);
void airspeed_sensor_api_stop(void *arg);
void airspeed_sensor_api_deinit(void *arg);
Measurement* airspeed_sensor_api_read(void *arg);
void airspeed_sensor_api_return(Measurement *measurement, void *arg);

#endif

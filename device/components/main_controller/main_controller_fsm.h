#ifndef MAIN_CONTROLLER_FSM_H
#define MAIN_CONTROLLER_FSM_H

#include "telemetry_writer.h"
#include "sync.h"
#include "live_broadcast.h"
#include "dashboard_view.h"
#include "message_queue_api.h"
#include "view.h"

typedef enum {
  MAIN_PHASE_NONE,
  MAIN_PHASE_INIT,
  MAIN_PHASE_STANDBY,
  MAIN_PHASE_TRACKING,
  MAIN_PHASE_STOPPED
} MainPhase;

typedef enum {
  MAIN_EVENT_START,
  MAIN_EVENT_STOP,
  MAIN_EVENT_PUSH_BUTTON_PRESSED
} MainEvent;

typedef struct {
  MainPhase phase;
  MainPhase requested_phase;
  DashboardModel dashboard_view_model;
  int dashboard_view_id;
  
  ViewApiHandle view_api;
  TelemetryWriterHandle telemetry_writer;
  SyncHandle sync;
  LiveBroadcastHandle live_broadcast;
  DashboardView dashboard_view;
} MainFSM;

void main_controller_fsm_create(MainFSM *me,
                                ViewApiHandle view_api, DashboardView dashboard_view,
                                TelemetryWriterHandle telemetry_writer, SyncHandle sync,
                                LiveBroadcastHandle live_broadcast);
void main_controller_fsm_delete(MainFSM *me);
void main_controller_fsm_start(MainFSM *me);
void main_controller_fsm_stop(MainFSM *me);
void main_controller_fsm_push_button_pressed(MainFSM *me);

#endif

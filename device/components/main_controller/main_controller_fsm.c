#include "main_controller_fsm.h"

#include "dashboard_view.h"

static void execute(MainFSM *me, MainEvent event);
static void phase_init(MainFSM *me, MainEvent event);
static void phase_standby(MainFSM *me, MainEvent event);
static void phase_tracking(MainFSM *me, MainEvent event);
static void refresh_view(MainFSM *me, DashboardPhase phase);
static void transition(MainFSM *me, MainPhase next_phase);
static void suspend_live_broadcast(MainFSM *me);
static void resume_live_broadcast(MainFSM *me);

void main_controller_fsm_create(MainFSM *me,
                                ViewApiHandle view_api, DashboardView dashboard_view,
                                TelemetryWriterHandle telemetry_writer, SyncHandle sync,
                                LiveBroadcastHandle live_broadcast) {
  me->phase = MAIN_PHASE_INIT;
  me->view_api = view_api;
  me->dashboard_view = dashboard_view;
  me->telemetry_writer = telemetry_writer;
  me->sync = sync;
  me->live_broadcast = live_broadcast;
}

void main_controller_fsm_delete(MainFSM *me) {
  // NOP
}

void main_controller_fsm_start(MainFSM *me) {
  execute(me, MAIN_EVENT_START);
}

void main_controller_fsm_stop(MainFSM *me) {
  execute(me, MAIN_EVENT_STOP);
}

void main_controller_fsm_push_button_pressed(MainFSM *me) {
  execute(me, MAIN_EVENT_PUSH_BUTTON_PRESSED);
}

static void execute(MainFSM *me, MainEvent event) {
  me->requested_phase = me->phase;
  
  switch (me->phase) {
  case MAIN_PHASE_NONE:
    // NOP
    break;
  case MAIN_PHASE_INIT:
    phase_init(me, event);
    break;
  case MAIN_PHASE_STANDBY:
    phase_standby(me, event);
    break;
  case MAIN_PHASE_TRACKING:
    phase_tracking(me, event);
    break;
  case MAIN_PHASE_STOPPED:
    // NOP
    break;
  }

  if (me->requested_phase != me->phase) {
    me->phase = me->requested_phase;
  }
}

static void phase_init(MainFSM *me, MainEvent event) {

  if (event == MAIN_EVENT_STOP) {
    transition(me, MAIN_PHASE_STOPPED);
    return;
  }
  
  if (event != MAIN_EVENT_START) {
    return;
  }

  me->dashboard_view_id = view_open(me->view_api, dashboard_view_draw, &me->dashboard_view);
  refresh_view(me, DASHBOARD_STANDBY);

  sync_resume(me->sync);

  transition(me, MAIN_PHASE_STANDBY);
}

static void phase_standby(MainFSM *me, MainEvent event) {

  if (event == MAIN_EVENT_STOP) {
    sync_pause(me->sync);
    transition(me, MAIN_PHASE_STOPPED);
    return;
  }

  if (event != MAIN_EVENT_PUSH_BUTTON_PRESSED) {
    return;
  }

  sync_pause(me->sync);
  refresh_view(me, DASHBOARD_RUNNING);

  ErrorResult err = telemetry_writer_start(me->telemetry_writer);
  if (err.code != AV_OK) { 
    av_error_publish_result(SEVERITY_ERROR, err);
  }

  resume_live_broadcast(me);
  
  transition(me, MAIN_PHASE_TRACKING);
}

static void phase_tracking(MainFSM *me, MainEvent event) {

  if (event == MAIN_EVENT_STOP) {
    telemetry_writer_stop_and_wait(me->telemetry_writer);
    suspend_live_broadcast(me);
    transition(me, MAIN_PHASE_STOPPED);
    return;
  }

  if (event != MAIN_EVENT_PUSH_BUTTON_PRESSED) {
    return;
  }

  telemetry_writer_stop_and_wait(me->telemetry_writer);

  suspend_live_broadcast(me);

  refresh_view(me, DASHBOARD_STANDBY);

  sync_resume(me->sync);

  transition(me, MAIN_PHASE_STANDBY);
}

static void suspend_live_broadcast(MainFSM *me) {

#ifdef CONFIG_LIVE_BROADCAST_ENABLED
  ErrorResult err = live_broadcast_suspend(me->live_broadcast);
  if (err.code != AV_OK) { 
    av_error_publish_result(SEVERITY_ERROR, err);
  }
#endif

}

static void resume_live_broadcast(MainFSM *me) {

#ifdef CONFIG_LIVE_BROADCAST_ENABLED
  ErrorResult err = live_broadcast_resume(me->live_broadcast);
  if (err.code != AV_OK) {
    av_error_publish_result(SEVERITY_ERROR, err);
  }
#endif
}

static void refresh_view(MainFSM *me, DashboardPhase phase) {
  me->dashboard_view_model.phase = phase;
  dashboard_view_update(&me->dashboard_view, me->dashboard_view_model);
  view_draw(me->view_api, me->dashboard_view_id, false);
}

static void transition(MainFSM *me, MainPhase next_phase) {  
  me->requested_phase = next_phase;
  printf("Transition to: %d\n", next_phase);
}

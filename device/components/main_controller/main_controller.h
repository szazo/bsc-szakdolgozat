#ifndef MAIN_CONTROLLER_H
#define MAIN_CONTROLLER_H

MESSAGE_DECLARE_BASE(MAIN_CONTROLLER_EVENTS);
enum {
  MAIN_START_COMMAND
};

ErrorResult main_controller_start(MainFSM *me, MessageQueueHandle message_queue);
ErrorResult main_controller_stop(MainFSM *me, MessageQueueHandle message_queue);

#endif

#ifndef DASHBOARD_VIEW_H
#define DASHBOARD_VIEW_H

#include <stdbool.h>
#include "view_api.h"

typedef enum {
  DASHBOARD_NONE,
  DASHBOARD_STANDBY,
  DASHBOARD_RUNNING
} DashboardPhase;

typedef struct {
  DashboardPhase phase;
} DashboardModel;

#define DEFAULT_DASHBOARD_MODEL (DashboardModel) {  \
    .phase = DASHBOARD_NONE                         \
      }

typedef struct {
  DashboardModel model;
} DashboardView;

void dashboard_view_init(DashboardView *me);
void dashboard_view_update(DashboardView *me, DashboardModel model);
void dashboard_view_draw(void *arg, GraphicsHandle graphics, bool redraw);

#endif

#include "unity.h"
#include <stdio.h>
#include <string.h>

#include "driver/spi_master.h"
#include "view.h"
#include "dashboard_view.h"

#define TEST_TAG "[dashboard_view]"

EPaperHandle epaper;
ViewHandle view;
DashboardView dashboard_view;

static void spi_init() {
    esp_err_t ret;
    spi_bus_config_t buscfg;
    memset(&buscfg, 0, sizeof(spi_bus_config_t));
    buscfg.mosi_io_num = CONFIG_SPI_MOSI_GPIO;
    buscfg.sclk_io_num = CONFIG_SPI_CLK_GPIO;
    buscfg.miso_io_num = -1;
    buscfg.quadwp_io_num = -1;
    buscfg.quadhd_io_num = -1;

    //Initialize the SPI bus
    ret=spi_bus_initialize(HSPI_HOST, &buscfg, 0);
    assert(ret==ESP_OK);
}

static void setup() {
  /* spi_init(); */
  ErrorResult err = epaper_create(&epaper);
  TEST_ASSERT_EQUAL(AV_OK, err.code);

  ViewConfig config = {
    .width = 200,
    .height = 200
  };
  err = view_create(&config, epaper, &view);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
}

static void teardown() {
  ErrorResult err = view_delete(view);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
}

TEST_CASE("should_draw", TEST_TAG)
{
  // given
  setup();

  dashboard_view_init(&dashboard_view);
  int view_id = view_open(view, dashboard_view_draw, &dashboard_view);

  // when
  dashboard_view_update(&dashboard_view, (DashboardModel) { .phase = DASHBOARD_STANDBY });
  view_draw(view, view_id, true);
  dashboard_view_update(&dashboard_view, (DashboardModel) { .phase = DASHBOARD_RUNNING });
  view_draw(view, view_id, true);

  // then
  teardown();
}


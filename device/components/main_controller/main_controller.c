#include "message_queue_api.h"
#include "input_scanner.h"
#include "main_controller_fsm.h"
#include "main_controller.h"

MESSAGE_DEFINE_BASE(MAIN_CONTROLLER_EVENTS);

static void event_handler(void *arg, MessageEventBase event_base, int32_t event_id, void *event_data);

ErrorResult main_controller_start(MainFSM *me, MessageQueueHandle message_queue) {

  ErrorResult err = message_queue_register(message_queue,
                                   INPUT_SCANNER_EVENTS,
                                   ROTARY_PUSH_BUTTON_PRESSED,
                                   event_handler,
                                   me);
  if (err.code != AV_OK) { return err; }
  err = message_queue_register(message_queue,
                                  MAIN_CONTROLLER_EVENTS,
                                  MAIN_START_COMMAND,
                                  event_handler,
                                  me);

  return err;
}

ErrorResult main_controller_stop(MainFSM *me, MessageQueueHandle message_queue) {
  ErrorResult err = message_queue_unregister(message_queue,
                                     INPUT_SCANNER_EVENTS,
                                     ROTARY_PUSH_BUTTON_PRESSED,
                                     event_handler);
  if (err.code != AV_OK) { return err; }
  err = message_queue_unregister(message_queue,
                                  MAIN_CONTROLLER_EVENTS,
                                  MAIN_START_COMMAND,
                                  event_handler);
  return err;
}

static void event_handler(void *arg, MessageEventBase event_base, int32_t event_id, void *event_data) {

  MainFSM *me = arg;
  if (event_base == MAIN_CONTROLLER_EVENTS) {
    switch (event_id) {
    case MAIN_START_COMMAND:
      main_controller_fsm_start(me);
      break;
    }
  } else if (event_base == INPUT_SCANNER_EVENTS) {
    switch (event_id) {
    case ROTARY_PUSH_BUTTON_PRESSED:
      main_controller_fsm_push_button_pressed(me);
      break;
    }
  }
}

#include <stdlib.h>
#include <stdio.h>

#include "dashboard_view.h"
#include "view_api.h"

#define PHASE_X 25
#define PHASE_Y 50
#define PHASE_WIDTH 150
#define PHASE_HEIGHT 50

void dashboard_view_init(DashboardView *me) {
  me->model = (DashboardModel) {
    .phase = DASHBOARD_NONE
  };
}

void dashboard_view_update(DashboardView *me, DashboardModel model) {
  me->model = model;
}

void dashboard_view_draw(void *arg, GraphicsHandle graphics, bool redraw) {

  DashboardView *me = arg;
  graphics_draw_filled_rectangle(graphics, PHASE_X, PHASE_Y,
                                 PHASE_X + PHASE_WIDTH, PHASE_Y + PHASE_HEIGHT,
                                 GRAPHICS_COLORED);

  char *text = "";
  switch (me->model.phase) {
  case DASHBOARD_NONE:
    text = "";
    break;
  case DASHBOARD_RUNNING:
    text = "TRACKING";
    break;
  case DASHBOARD_STANDBY:
    text = "STANDBY";
    break;
  }
  printf("TEXT: %s\n", text);
  graphics_drawstring_at(graphics, PHASE_X + 10, PHASE_Y + 10, text, &Font24, GRAPHICS_UNCOLORED);
}

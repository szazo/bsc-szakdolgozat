#include "mock_source.h"

static const char *MOCK_TAG = "mock_source";

MockSource* mock_source_create() {
  MockSource *me = malloc(sizeof(MockSource));
  me->measurement_ring = xRingbufferCreate(2048,
                                           RINGBUF_TYPE_NOSPLIT);

  return me;
}

void mock_source_delete(MockSource *me) {
  vRingbufferDelete(me->measurement_ring);
  free(me);
}

void mock_source_send_measurement(MockSource *me, Measurement *measurement) {
  UBaseType_t result = xRingbufferSend(me->measurement_ring,
                                       measurement,
                                       sizeof(Measurement) + measurement->size,
                                       pdMS_TO_TICKS(10));
  if (result != pdTRUE) {
    ESP_LOGE(MOCK_TAG, "Couldn't add to ring buffer, data size: %zu, result: %d",
             sizeof(Measurement) + measurement->size, result);
    size_t free_size = xRingbufferGetCurFreeSize(me->measurement_ring);
    if (free_size < measurement->size) {
      ESP_LOGE(MOCK_TAG, "Sensor ring buffer full, free size: %zu", free_size);
    }
    abort();
  }
}

Measurement *mock_source_read_measurement(void *arg, TickType_t ticks_to_wait) {

  MockSource *me = arg;
  
  size_t item_size;
  Measurement *item = xRingbufferReceive(me->measurement_ring, &item_size, ticks_to_wait);
  if (item == NULL) {
    return NULL;
  }

  return item;
}

void mock_source_return_measurement(void *arg, Measurement *measurement) {
  MockSource *me = arg;
  vRingbufferReturnItem(me->measurement_ring, measurement);
}

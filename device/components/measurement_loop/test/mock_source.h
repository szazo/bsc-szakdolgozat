#ifndef MOCK_SOURCE_H
#define MOCK_SOURCE_H

#include "freertos/FreeRTOS.h"
#include "freertos/ringbuf.h"
#include "esp_log.h"

#include "sensor_api.h"

typedef struct {
  RingbufHandle_t measurement_ring;
} MockSource;

#define MOCK_SOURCE(me) { .read_measurement = mock_source_read_measurement, \
      .return_measurement = mock_source_return_measurement, \
      .arg = me }

MockSource* mock_source_create();
void mock_source_delete(MockSource *me);
void mock_source_send_measurement(MockSource *me, Measurement *measurement);
Measurement *mock_source_read_measurement(void *arg, TickType_t ticks_to_wait);
void mock_source_return_measurement(void *arg, Measurement *measurement);

#endif

#include "unity.h"

#include "freertos/FreeRTOS.h"
#include "esp_log.h"

#include "measurement_loop.h"
#include "mock_source.h"

static const char *TAG = "test_measurement_loop";

typedef struct {
  char* id;
  int call_count;
  void *call_args[10];
} MockHandler;

#define MOCK_HANDLER(ID) { .id = ID, .call_count = 0 }

static void mock_handler_handle(const Measurement *measurement, void *arg) {

  MockHandler *me = arg;
  me->call_args[me->call_count] = arg;
  me->call_count++;
}

TEST_CASE("create_should_create", "[measurement_loop]")
{
  // given
  MockSource *source = mock_source_create();
  MeasurementSource descriptor = MOCK_SOURCE(source);
  
  MeasurementLoopHandle handle;
  ErrorResult err = measurement_loop_create(&descriptor, &handle);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
  measurement_loop_start(handle);
    
  // when
  vTaskDelay(pdMS_TO_TICKS(5));
  measurement_loop_stop_and_wait(handle);
  measurement_loop_delete(handle);

  // then
  // TODO: how can check memory leak
}

TEST_CASE("register_should_register", "[measurement_loop]")
{
  // given
  MockSource *source = mock_source_create();
  MeasurementSource descriptor = MOCK_SOURCE(source);
  MeasurementLoopHandle handle;
  ErrorResult err = measurement_loop_create(&descriptor, &handle);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
  measurement_loop_start(handle);

  MockHandler handler1 = MOCK_HANDLER("1");
  MockHandler handler2 = MOCK_HANDLER("2");

  ErrorResult is_registered1 = measurement_loop_register(handle, mock_handler_handle, &handler1);
  ErrorResult is_registered2 = measurement_loop_register(handle, mock_handler_handle, &handler2);

  // when
  Measurement measurement;
  measurement.time = sensor_get_time();
  measurement.sensor_id = 42;
  measurement.message_type = 1;
  measurement.size = 0;
  
  mock_source_send_measurement(source, &measurement);

  // then
  vTaskDelay(pdMS_TO_TICKS(10));
  TEST_ASSERT_EQUAL(AV_OK, is_registered1.code);
  TEST_ASSERT_EQUAL(AV_OK, is_registered2.code);
  TEST_ASSERT_EQUAL(1, handler1.call_count);
  TEST_ASSERT_EQUAL(1, handler2.call_count);
  TEST_ASSERT_EQUAL(&handler1, handler1.call_args[0]);
  TEST_ASSERT_EQUAL(&handler2, handler2.call_args[0]);

  measurement_loop_stop_and_wait(handle);
  measurement_loop_delete(handle);
  mock_source_delete(source);
}

TEST_CASE("unregister_should_unregister", "[measurement_loop]")
{
  // given
  MockSource *source = mock_source_create();
  MeasurementSource descriptor = MOCK_SOURCE(source);
  MeasurementLoopHandle handle;
  ErrorResult err = measurement_loop_create(&descriptor, &handle);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
  measurement_loop_start(handle);

  MockHandler handler1 = MOCK_HANDLER("1");
  MockHandler handler2 = MOCK_HANDLER("1");

  measurement_loop_register(handle, mock_handler_handle, &handler1);
  measurement_loop_register(handle, mock_handler_handle, &handler2);

  Measurement measurement;
  measurement.time = sensor_get_time();
  measurement.sensor_id = 42;
  measurement.message_type = 1;
  measurement.size = 0;
  
  mock_source_send_measurement(source, &measurement);
  vTaskDelay(pdMS_TO_TICKS(10));

  // when
  ErrorResult is_unregistered = measurement_loop_unregister(handle, mock_handler_handle, &handler1);
  mock_source_send_measurement(source, &measurement);

  // then
  vTaskDelay(pdMS_TO_TICKS(10));
  TEST_ASSERT_EQUAL(1, handler1.call_count);
  TEST_ASSERT_EQUAL(2, handler2.call_count);
  TEST_ASSERT_EQUAL(AV_OK, is_unregistered.code);

  measurement_loop_stop_and_wait(handle);
  measurement_loop_delete(handle);
  mock_source_delete(source);
}

#ifndef MEASUREMENT_LOOP_H
#define MEASUREMENT_LOOP_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

#include "error.h"
#include "sensor_api.h"
#include "measurement_loop_api.h"

AV_ERROR_DEFINE(MEASUREMENT_LOOP_TASK_CREATE_ERROR, 0x0301)
AV_ERROR_DEFINE(MEASUREMENT_LOOP_EVENT_GROUP_CREATE_ERROR, 0x0302)
AV_ERROR_DEFINE(MEASUREMENT_LOOP_MAXIMUM_REGISTRATION_COUNT_ERROR, 0x0303)
AV_ERROR_DEFINE(MEASUREMENT_LOOP_NOT_REGISTERED_ERROR, 0x0304)

typedef void* MeasurementLoopHandle;

typedef struct RegistrationItem {
  MeasurementHandler *handler;
  void *handler_arg;
  struct RegistrationItem *next;
} RegistrationItem;

#define MAX_REGISTRATIONS 10

/**
 * Function type that will be used to read the next recorded measurement (from the ring).
 */
typedef Measurement* (ReadMeasurement) (void *arg, TickType_t ticks_to_wait);

/**
 * Function type used to return measurement after processed.
 */
typedef void (ReturnMeasurement) (void *arg, Measurement *measurement);

typedef struct {
  ReadMeasurement *read_measurement;
  ReturnMeasurement *return_measurement;
  void *arg;
} MeasurementSource;

typedef struct {
  TaskHandle_t loop_task_handle;
  volatile bool stopped;
  EventGroupHandle_t stopped_event_group;
  MeasurementSource source;

  /**
   * Head of the registration linked list.
   */
  RegistrationItem *registration_list;

  /**
   * Static pool for registration items to avoid frequent heap operations.
   */
  RegistrationItem item_pool[MAX_REGISTRATIONS];

  /**
   * The count of active registrations.
   */
  int registration_count;

} MeasurementLoop;

ErrorResult measurement_loop_create(MeasurementSource *source, MeasurementLoopHandle *handle);
void measurement_loop_delete(MeasurementLoopHandle handle);

ErrorResult measurement_loop_start(MeasurementLoopHandle handle);
void measurement_loop_stop_and_wait(MeasurementLoopHandle handle);

#endif

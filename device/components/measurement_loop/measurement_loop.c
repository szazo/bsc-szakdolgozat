#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "utlist.h"
#include "measurement_loop.h"

static const int LOOP_TASK_STACK_SIZE = 3 * 1024;
static const char *TAG = "measurement_loop";
const static int STOPPED_BIT = BIT0;

static void loop_task(void *param);

ErrorResult measurement_loop_create(MeasurementSource *source, MeasurementLoopHandle *handle) {
  MeasurementLoop *loop = malloc(sizeof(MeasurementLoop));
  loop->registration_list = NULL;
  loop->registration_count = 0;
  loop->stopped_event_group = xEventGroupCreate();
  loop->loop_task_handle = NULL;
  loop->source = *source; // copy (only pointers)
  if (loop->stopped_event_group == NULL) {
    return AV_ERROR(MEASUREMENT_LOOP_EVENT_GROUP_CREATE_ERROR);
  }
  loop->stopped = false;

  *handle = loop;

  return AV_OK_RESULT;
}

void measurement_loop_delete(MeasurementLoopHandle handle) {
  MeasurementLoop *me = handle;
  vEventGroupDelete(me->stopped_event_group);
  free(me);
}

ErrorResult measurement_loop_start(MeasurementLoopHandle handle) {
  MeasurementLoop *me = handle;
  
  BaseType_t task_result = xTaskCreatePinnedToCore(loop_task, "measurement_loop", LOOP_TASK_STACK_SIZE,
                                                   me, CONFIG_MEASUREMENT_LOOP_TASK_PRIORITY,
                                                   &me->loop_task_handle, CONFIG_MEASUREMENT_LOOP_TASK_CPU);
  if (task_result != pdPASS) {
    return AV_ERROR(MEASUREMENT_LOOP_TASK_CREATE_ERROR);
  }

  return AV_OK_RESULT;
}

void measurement_loop_stop_and_wait(MeasurementLoopHandle handle) {

  MeasurementLoop *me = handle;
  me->stopped = true;

  xEventGroupWaitBits(me->stopped_event_group,
                      STOPPED_BIT,
                      false,
                      true, portMAX_DELAY);
}

ErrorResult measurement_loop_register(MeasurementLoopHandle handle, MeasurementHandler *handler, void *arg) {
  MeasurementLoop *me = handle;

  if (me->registration_count >= MAX_REGISTRATIONS) {
    return AV_ERROR(MEASUREMENT_LOOP_MAXIMUM_REGISTRATION_COUNT_ERROR);
  }

  RegistrationItem *item = &me->item_pool[me->registration_count];
  item->handler = handler;
  item->handler_arg = arg;
  LL_APPEND(me->registration_list, item);
  me->registration_count++;

  return AV_OK_RESULT;
}

ErrorResult measurement_loop_unregister(MeasurementLoopHandle handle, MeasurementHandler *handler, void *arg) {

  MeasurementLoop *me = handle;

  RegistrationItem* found = NULL;
  RegistrationItem* el;
  LL_FOREACH(me->registration_list, el) {
    if (el->handler == handler && el->handler_arg == arg) {
      found = el;
    }
  }

  if (found != NULL) {
    LL_DELETE(me->registration_list, found);
    me->registration_count--;
    return AV_OK_RESULT;
  }

  return AV_ERROR(MEASUREMENT_LOOP_NOT_REGISTERED_ERROR);
}

/* // TODO: REMOVE */
/* #include "gyro_sensor.h" */

//int counter = 0;

static void loop_task(void *param) {

  MeasurementLoop *me = param;

  while (!me->stopped) {

    Measurement *measurement = me->source.read_measurement(me->source.arg, pdMS_TO_TICKS(100));
    if (measurement == NULL) {
      continue;
    }
    if (me->registration_count > 0) {
      RegistrationItem* el;
      LL_FOREACH(me->registration_list, el) {
        el->handler(measurement, el->handler_arg);
      }

      /* if (false && measurement->sensor_id == 1) { */

      /*   GyroMessage *message = (GyroMessage *) measurement->data; */

      /*   counter++; */
      /*   if ((counter % 10) == 0) { */
      /*     ESP_LOGI(TAG, "%s", message->debug_text); */
      /* } */
      /* ESP_LOGI(TAG, "message arrived, sensor id: %d message_type: %d size: %d", */
      /*          measurement->sensor_id, */
      /*          measurement->message_type, */
      /*          measurement->size); */
      //}
    }

    me->source.return_measurement(me->source.arg, measurement);
  }

  xEventGroupSetBits(me->stopped_event_group, STOPPED_BIT);
  vTaskDelete(NULL);
}

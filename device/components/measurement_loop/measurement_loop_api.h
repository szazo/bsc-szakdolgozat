#ifndef MEASUREMENT_LOOP_API_H
#define MEASUREMENT_LOOP_API_H

#include "error.h"
#include "sensor_api.h"

typedef void* MeasurementLoopAPIHandle;

typedef void (MeasurementHandler) (const Measurement *measurement, void *arg);

ErrorResult measurement_loop_register(MeasurementLoopAPIHandle handle, MeasurementHandler *handler, void *arg);
ErrorResult measurement_loop_unregister(MeasurementLoopAPIHandle handle, MeasurementHandler *handler, void *arg);

#endif

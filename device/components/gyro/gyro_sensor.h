#ifndef GYRO_SENSOR_H
#define GYRO_SENSOR_H

#include "error.h"
#include "sensor_api.h"

AV_ERROR_DEFINE(GYRO_SENSOR_TIME_QUEUE_ERROR, 0x0A01)

#define GYRO_MESSAGE_BASE 0x01

typedef enum {
  GYRO = MEASUREMENT_TYPE(GYRO_MESSAGE_BASE, 1)
} GyroMessageType;

/**
 * Quaternion in Q30 signed fixed point format.
 */
typedef struct {
  int32_t w;
  int32_t x;
  int32_t y;
  int32_t z;
} QuaternionQ30;
#define DEFAULT_QUATERNION_Q30 ((QuaternionQ30) { \
      .w = 0,                                     \
        .x = 0,                                   \
        .y = 0,                                   \
        .z = 0                                    \
        })

typedef struct {
  int16_t x;
  int16_t y;
  int16_t z;
} RawAxes;
#define DEFAULT_RAW_AXES ((RawAxes) { \
      .x = 0,                                     \
        .y = 0,                                   \
        .z = 0,                                   \
        })

typedef struct {
  QuaternionQ30 quaternion;
  RawAxes gyro;
  RawAxes accel;
  RawAxes heading;
  int16_t temp;
  /* char debug_text[200]; */
} GyroMessage;

#ifdef __cplusplus

#include "dmp/defines.hpp"

/**
 * Size of a gyro measurement. A fixed buffer will be allocated with this size.
 */
#define GYRO_MEASUREMENT_SIZE sizeof(Measurement) + sizeof(GyroMessage)

class GyroSensor {
 public:
  GyroSensor();
  ~GyroSensor();
  void init(SensorId id, SensorApiHandle api);
  void deinit();
  void start();
  void stop();
  Measurement *read();
  void read_return(Measurement *measurement);
  SensorId id;
  SensorApiHandle api;
 private:
  QueueHandle_t sensor_request_times;
  void init_compass();
  /**
   * Reads the next package from the fifo buffer.
   */
  bool read_fifo();
  /**
   * For debug purposes.
   */
  void format_message(GyroMessage *message, char* buffer);
  mpud::MPUdmp mpu;
  uint8_t packet_length;
  uint8_t fifo_buffer[mpud::kDMPMaxPacketLength];
  static IRAM_ATTR void mpuISR(TaskHandle_t taskHandle);
  inline void configure_isr();
  /**
   * We don't allocate from the heap for each measurment, we use this buffer when
   * read requested by the governor.
   */
  char measurement_buffer[GYRO_MEASUREMENT_SIZE];
};

#endif

/* typedef struct { */
/*   SensorHandle handle; */
/*   SensorId id; */
/* } GyroSensor; */

#ifdef __cplusplus
extern "C" {
#endif

typedef void* GyroSensorHandle;

GyroSensorHandle gyro_sensor_create();
void gyro_sensor_delete(GyroSensorHandle sensor_handle);

bool gyro_sensor_api_init(const SensorApiHandle sensor_handle, const SensorId id, void *arg);
void gyro_sensor_api_deinit(void *arg);
bool gyro_sensor_api_start(void *arg);
bool gyro_sensor_api_stop(void *arg);
Measurement *gyro_sensor_api_read(void *arg);
void gyro_sensor_api_return(Measurement *measurement, void *arg);

#ifdef __cplusplus
}
#endif

#define GYRO_SENSOR() \
  { .init = gyro_sensor_api_init, \
    .deinit = gyro_sensor_api_deinit, \
    .start = gyro_sensor_api_start, \
    .stop_and_wait = gyro_sensor_api_stop, \
    .read = gyro_sensor_api_read, \
    .read_return = gyro_sensor_api_return \
  }

#define GYRO_SENSOR_DESCRIPTOR(ID, SENSOR_INSTANCE) \
  { .id = ID, \
    .sensor = GYRO_SENSOR(), \
    .arg = SENSOR_INSTANCE }

#endif

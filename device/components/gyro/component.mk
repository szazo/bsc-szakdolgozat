COMPONENT_ADD_INCLUDEDIRS=.

gyro_sensor.o: gyro.pb.c gyro_common.pb.c

%.pb.c %.pb.h: $(COMPONENT_PATH)/%.proto
	$(PROTOC_WITH_OPTS) --proto_path=$(COMPONENT_PATH) --nanopb_out=$(COMPONENT_PATH) $<

#include "esp_log.h"
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"

#include "MPUdmp.hpp"
#include "mpu/types.hpp"
#include "mpu/math.hpp"

#include "gyro_sensor.h"

static const char *TAG = "gyro_sensor";
static const int INTERRUPT_PIN = CONFIG_GYRO_INTERRUPT_GPIO;
static const int SENSOR_REQUEST_TIMES_QUEUE_SIZE = 40;

GyroSensor::GyroSensor() {
  this->sensor_request_times = xQueueCreate(SENSOR_REQUEST_TIMES_QUEUE_SIZE, sizeof(SensorTime));  
}

GyroSensor::~GyroSensor() {
  vQueueDelete(this->sensor_request_times);
}

void GyroSensor::init(SensorId id, SensorApiHandle api) {

  this->id = id;
  this->api = api;

  ESP_LOGI(TAG, "Initializing gyro sensor with id: %d", id);

  ESP_LOGI(TAG, "Gyro sensor initializing");

  this->mpu.setBus(i2c0);
  this->mpu.setAddr(mpud::MPU_I2CADDRESS_AD0_LOW);

  while (esp_err_t err = this->mpu.testConnection()) {
    ESP_LOGE(TAG, "Failed to connect to the MPU, error=%#X", err);
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }
  ESP_LOGI(TAG, "MPU connection successful!");

  ESP_ERROR_CHECK(this->mpu.initialize());  // initialize the chip and set initial configurations

  // Self-Test --> disabled, caused automatic calibration trouble
  // ESP_LOGI(TAG, "Performing self test...");
  // mpud::selftest_t retSelfTest;
  // while (esp_err_t err = this->mpu.selfTest(&retSelfTest)) {
  //   ESP_LOGE(TAG, "Failed to perform MPU Self-Test, error=%#X", err);
  //   vTaskDelay(1000 / portTICK_PERIOD_MS);
  // }

  // ESP_LOGI(TAG, "MPU Self-Test result: Gyro=%s Accel=%s",  //
  //          (retSelfTest & mpud::SELF_TEST_GYRO_FAIL ? "FAIL" : "OK"),
  //          (retSelfTest & mpud::SELF_TEST_ACCEL_FAIL ? "FAIL" : "OK"));

  // Calibrate --> disabled because interfered with automatic calibration
  // ESP_LOGI(TAG, "Calibrating accelerometer and gyroscope...");
  // mpud::raw_axes_t accelBias, gyroBias;
  // ESP_ERROR_CHECK(this->mpu.computeOffsets(&accelBias, &gyroBias));
  // ESP_ERROR_CHECK(this->mpu.setAccelOffset(accelBias));
  // ESP_ERROR_CHECK(this->mpu.setGyroOffset(gyroBias));

  // Configuring FIFO
  ESP_LOGI(TAG, "Configuring FIFO...");
  // ESP_ERROR_CHECK(this->mpu.setFIFOConfig(mpud::FIFO_CFG_TEMPERATURE));

  // Initializing compass
  this->init_compass();
  
  // Configuration
  ESP_LOGI(TAG, "Enabling DMP...");
  ESP_ERROR_CHECK(this->mpu.loadDMPFirmware());
  ESP_ERROR_CHECK(this->mpu.setDMPFeatures(mpud::DMP_FEATURE_LP_6X_QUAT |
                                           mpud::DMP_FEATURE_GYRO_CAL | // enable automatic calibration
                                           mpud::DMP_FEATURE_SEND_CAL_GYRO | // send calibrated gyro
                                           mpud::DMP_FEATURE_SEND_RAW_ACCEL)); // we can only send raw accel
  ESP_ERROR_CHECK(this->mpu.enableDMP());

  ESP_LOGI(TAG, "Configuring...");
  ESP_ERROR_CHECK(this->mpu.setAccelFullScale(mpud::ACCEL_FS_8G));
  ESP_ERROR_CHECK(this->mpu.setDigitalLowPassFilter(mpud::DLPF_188HZ)); // minimal delay
  ESP_ERROR_CHECK(this->mpu.setDMPOutputRate(200)); // it is the max

  ESP_LOGI(TAG, "DMP enabled: %d", this->mpu.getDMPEnabled());

  this->packet_length = this->mpu.getDMPPacketLength(this->mpu.getDMPFeatures());
  ESP_LOGI(TAG, "FIFO packet length is %d", this->packet_length);
  
  ESP_LOGI(TAG, "FIFO config: %d", this->mpu.getFIFOConfig());
}

void GyroSensor::init_compass() {
  ESP_LOGI(TAG, "Initializing compass...");
  ESP_LOGI(TAG, "Testing compass connection...");
  while (esp_err_t err = this->mpu.compassTestConnection()) {
    ESP_LOGE(TAG, "Failed to perform compass connection test, error=%#X", err);
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }

  ESP_ERROR_CHECK(this->mpu.setAuxI2CEnabled(true)); // it is missing from MPU.cpp
  ESP_LOGI(TAG, "Compass sensitivity is: %d", this->mpu.compassGetSensitivity());
}

void GyroSensor::start() {
  ESP_ERROR_CHECK(this->mpu.resetFIFO());  // start clean

  ESP_LOGI(TAG, "Enabling interrupt...");
  this->configure_isr();
  ESP_LOGI(TAG, "Interrupt enabled");
}

void GyroSensor::stop() {
  gpio_isr_handler_remove((gpio_num_t) INTERRUPT_PIN);
}

inline void GyroSensor::configure_isr() {

  ESP_ERROR_CHECK(this->mpu.setDMPInterruptMode(mpud::DMP_INT_MODE_CONTINUOUS));

  // configure GPIO
  constexpr gpio_config_t isr_gpio_config = {
    .pin_bit_mask = 1ULL << INTERRUPT_PIN,
    .mode = GPIO_MODE_INPUT,
    .pull_up_en = GPIO_PULLUP_DISABLE,
    .pull_down_en = GPIO_PULLDOWN_ENABLE,
    .intr_type = (gpio_int_type_t) GPIO_INTR_POSEDGE
  };

  ESP_ERROR_CHECK(gpio_config(&isr_gpio_config));
  ESP_ERROR_CHECK(gpio_isr_handler_add((gpio_num_t) INTERRUPT_PIN, GyroSensor::mpuISR, this));

  // configure interrupt
  static constexpr mpud::int_config_t interrupt_config {
    .level = mpud::INT_LVL_ACTIVE_HIGH,
    .drive = mpud::INT_DRV_PUSHPULL,
    .mode  = mpud::INT_MODE_PULSE50US,
    .clear = mpud::INT_CLEAR_STATUS_REG
      };
  ESP_ERROR_CHECK(this->mpu.setInterruptConfig(interrupt_config));
  ESP_ERROR_CHECK(this->mpu.setInterruptEnabled(mpud::INT_EN_DMP_READY));
}

IRAM_ATTR void GyroSensor::mpuISR(void *arg)
{
  GyroSensor *me = (GyroSensor *) arg;

  SensorTime time = sensor_get_time();
  BaseType_t is_higher_priority_task_woken;
  BaseType_t result = xQueueSendFromISR(me->sensor_request_times,
                                         &time, &is_higher_priority_task_woken);

  sensor_send_request_from_isr(me->api, me->id);

  // if (result != pdTRUE) {
  //   av_error_publish_with_reason(SEVERITY_ERROR, 0xFFFF, result);
  // }

  if (is_higher_priority_task_woken) {
    portYIELD_FROM_ISR();
  }
}

Measurement *GyroSensor::read() {

  bool success = this->read_fifo();
  if (!success) {
    xQueueReset(this->sensor_request_times);
    return NULL;
  }

  // uint16_t fifocount = this->mpu.getFIFOCount();
  // // if (fifocount > 25) {
  //   int messages = uxQueueMessagesWaiting(this->sensor_request_times);
  //   if (fifocount + 1 != messages) {
  //     printf("DIFF: %d, FIFO: %d\n", messages, fifocount);
  //   }
  // // }

  SensorTime time;
  if (xQueueReceive(this->sensor_request_times, &time, 0) == pdFALSE) {
    time = sensor_get_time();
  }

  Measurement *measurement = (Measurement *) this->measurement_buffer;
  GyroMessage *message = (GyroMessage *)measurement->data;

  mpud::quat_q30_t quaternion;
  if (esp_err_t err = this->mpu.getDMPQuaternion(this->fifo_buffer, &quaternion)) {
    ESP_LOGE(TAG, "Error decoding quaternion, %#X", err);
    return NULL;
  }
  message->quaternion.w = quaternion.w;
  message->quaternion.x = quaternion.x;
  message->quaternion.y = quaternion.y;
  message->quaternion.z = quaternion.z;
  
  mpud::raw_axes_t accel;
  if (esp_err_t err = this->mpu.getDMPAccel(this->fifo_buffer, &accel)) {
    ESP_LOGE(TAG, "Error decoding acceleration data, %#X", err);
    return NULL;
  }

  message->accel.x = accel.x;
  message->accel.y = accel.y;
  message->accel.z = accel.z;

  mpud::raw_axes_t gyro;
  if (esp_err_t err = this->mpu.getDMPGyro(this->fifo_buffer, &gyro)) {
    ESP_LOGE(TAG, "Error decoding gyro data, %#X", err);
    return NULL;
  }

  message->gyro.x = gyro.x;
  message->gyro.y = gyro.y;
  message->gyro.z = gyro.z;
  
  mpud::raw_axes_t heading;
  if (esp_err_t err = this->mpu.heading(&heading)) {
    ESP_LOGE(TAG, "Error reading heading data, %#X", err);
    return NULL;
  }

  message->heading.x = heading.x;
  message->heading.y = heading.y;
  message->heading.z = heading.z;

  // if (esp_timer_get_time() % 100 == 0) {
  //   printf("Heading: %f, %f, %f\n",
  //          mpud::math::gyroDegPerSec(gyro.x, mpud::GYRO_FS_2000DPS),
  //          mpud::math::gyroDegPerSec(gyro.y, mpud::GYRO_FS_2000DPS),
  //          mpud::math::gyroDegPerSec(gyro.z, mpud::GYRO_FS_2000DPS));
  // }

  if (esp_err_t err = this->mpu.temperature(&message->temp)) {
    ESP_LOGE(TAG, "Error reading temperature data, %#X", err);
    return NULL;
  }

  // this->format_message(message, message->debug_text);

  set_measurement_header(measurement,
                         this->id,
                         time,
                         GYRO,
                         sizeof(GyroMessage));

  return measurement;
}

// void GyroSensor::format_message(GyroMessage *message, char* buffer) {

//   const mpud::quat_q30_t quaternion = {
//     .w = message->quaternion.w,
//     .x = message->quaternion.x,
//     .y = message->quaternion.y,
//     .z = message->quaternion.z,
//   };

//   mpud::axes_q16_t axes = mpud::quaternionToEuler(quaternion);

//   mpud::raw_axes_t accel_raw = {
//     .x = message->accel.x,
//     .y = message->accel.y,
//     .z = message->accel.z
//   };
//   mpud::float_axes_t accelG = mpud::accelGravity(accel_raw, mpud::ACCEL_FS_8G);

//   mpud::raw_axes_t gyro_raw = {
//     .x = message->gyro.x,
//     .y = message->gyro.y,
//     .z = message->gyro.z
//   };  
//   mpud::float_axes_t gyro_dps = mpud::gyroDegPerSec(gyro_raw, mpud::GYRO_FS_2000DPS);

//   sprintf(buffer, "qyro: (%+3.2f %+3.2f %+3.2f) raw_gyro DPS (%+7.2f %+7.2f %+7.2f) raw_accel G (%+6.2f %+6.2f %+6.2f) heading uT: (%d %d %d) temp (%3.2f)",
//           mpud::q16_to_float(axes.x), mpud::q16_to_float(axes.y), mpud::q16_to_float(axes.z),
//           gyro_dps[0], gyro_dps[1], gyro_dps[2],
//           accelG.x, accelG.y, accelG.z,
//           message->heading.x, message->heading.y, message->heading.z,
//           mpud::tempCelsius(message->temp));
// }

inline bool GyroSensor::read_fifo() {
  uint16_t fifocount = this->mpu.getFIFOCount();

  if (esp_err_t err = this->mpu.lastError()) {
    ESP_LOGE(TAG, "Error reading fifo count, %#X", err);
    this->mpu.resetFIFO();
    return false;
  }

  if (fifocount > this->packet_length * 2) {
    if (!(fifocount % this->packet_length)) {
      ESP_LOGE(TAG, "Sample Rate too high!, not keeping up the pace!, count: %d", fifocount);
    }
    else {
      ESP_LOGE(TAG, "FIFO Count misaligned! Expected: %d, Actual: %d", this->packet_length, fifocount);
    }
    this->mpu.resetFIFO();
    return false;
  }

  if (esp_err_t err = this->mpu.readFIFO(this->packet_length, this->fifo_buffer)) {
    ESP_LOGE(TAG, "Error reading sensor data, %#X", err);
    this->mpu.resetFIFO();
    return false;
  }

  return true;
}

void GyroSensor::read_return(Measurement *measurement) {
  // NOP, because we use static buffer
}

void GyroSensor::deinit() {

  ESP_LOGI(TAG, "Deinitializing gyro sensor with id: %d", this->id);

  ESP_LOGI(TAG, "Disabling interrupt...");
  ESP_ERROR_CHECK(this->mpu.setInterruptEnabled(mpud::INT_EN_NONE));
  ESP_ERROR_CHECK(gpio_isr_handler_remove((gpio_num_t) INTERRUPT_PIN));
  ESP_LOGI(TAG, "Interrupt disabled");

  ESP_LOGI(TAG, "Disabling DMP...");
  this->mpu.disableDMP();
  ESP_LOGI(TAG, "Resetting magnetometer...");
  this->mpu.compassReset();
  ESP_LOGI(TAG, "Resetting sensor...");
  this->mpu.reset();

  ESP_LOGI(TAG, "Gyro sensor deinitialized");
}

extern "C" GyroSensorHandle gyro_sensor_create() {

  GyroSensor *sensor = new GyroSensor();

  return sensor;
}

extern "C" void gyro_sensor_delete(GyroSensorHandle sensor_handle) {
  GyroSensor *sensor = (GyroSensor *) sensor_handle;
  delete sensor;
}

extern "C" bool gyro_sensor_api_init(const SensorApiHandle api, const SensorId id, void *arg) {

  GyroSensor *sensor = (GyroSensor *) arg;
  sensor->init(id, api);
  
  return true;
}

extern "C" bool gyro_sensor_api_start(void *arg) {

  GyroSensor *sensor = (GyroSensor *) arg;
  sensor->start();
  
  return true;
}

extern "C" bool gyro_sensor_api_stop(void *arg) {

  GyroSensor *sensor = (GyroSensor *) arg;
  sensor->stop();
  
  return true;
}

extern "C" void gyro_sensor_api_deinit(void *arg) {

  GyroSensor *sensor = (GyroSensor *) arg;
  sensor->deinit();
}

extern "C" Measurement *gyro_sensor_api_read(void *arg) {
  GyroSensor *sensor = (GyroSensor *) arg;
  return sensor->read();
}

extern "C" void gyro_sensor_api_return(Measurement *measurement, void *arg) {
  GyroSensor *sensor = (GyroSensor *) arg;
  sensor->read_return(measurement);
}

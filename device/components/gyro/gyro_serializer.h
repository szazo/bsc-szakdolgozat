#ifndef GYRO_SERIALIZER_H
#define GYRO_SERIALIZER_H

#include "measurement_serializer.h"
#include "gyro_common.pb.h"
#include "gyro_sensor.h"

bool gyro_serialize(const SerializerMessageType message_type, const void *data, pb_ostream_t *stream, void *arg);

void gyro_serialize_raw_axes(const RawAxes *source, ProtoGyroRawAxes *target);
void gyro_serialize_quaternion_q30(const QuaternionQ30 *source, ProtoGyroQuaternionQ30 *target);

#endif

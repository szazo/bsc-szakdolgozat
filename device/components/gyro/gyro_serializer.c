#include "gyro_serializer.h"
#include "gyro_sensor.h"
#include "gyro.pb.h"

static void serialize_root(const GyroMessage *source, ProtoGyro *target);

bool gyro_serialize(const SerializerMessageType message_type, const void *data, pb_ostream_t *stream, void *arg) {

  const GyroMessage *message = (const GyroMessage *) data;

  ProtoGyro target = ProtoGyro_init_default;
  serialize_root(message, &target);
  bool success = pb_encode_submessage(stream, ProtoGyro_fields, &target);

  return success;
}

static void serialize_root(const GyroMessage *source, ProtoGyro *target) {
  gyro_serialize_quaternion_q30(&source->quaternion, &target->quaternion);
  gyro_serialize_raw_axes(&source->gyro, &target->gyro);
  gyro_serialize_raw_axes(&source->accel, &target->accel);
  gyro_serialize_raw_axes(&source->heading, &target->heading);
  target->temp = source->temp;
}

void gyro_serialize_raw_axes(const RawAxes *source, ProtoGyroRawAxes *target) {
  target->x = source->x;
  target->y = source->y;
  target->z = source->z;
}

void gyro_serialize_quaternion_q30(const QuaternionQ30 *source, ProtoGyroQuaternionQ30 *target) {
  target->x = source->x;
  target->y = source->y;
  target->z = source->z;
  target->w = source->w;
}

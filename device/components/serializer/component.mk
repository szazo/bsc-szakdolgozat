COMPONENT_ADD_INCLUDEDIRS=.

serializer.o: sensor_file.pb.c

%.pb.c %.pb.h: $(COMPONENT_PATH)/%.proto
	$(PROTOC_WITH_OPTS) --proto_path=$(COMPONENT_PATH) --nanopb_out=$(COMPONENT_PATH) $<

#ifndef MEASUREMENT_SERIALIZER_H
#define MEASUREMENT_SERIALIZER_H

#include "error.h"
#include "sensor_api.h"
#include "serializer.h"

#define SERIALIZER_DESCRIPTOR(MESSAGE_TYPE_BASE, SERIALIZE, ARG) {  \
    .message_type_base = MESSAGE_TYPE_BASE,                         \
    .serialize = SERIALIZE,                                         \
    .arg = ARG }

AV_ERROR_DEFINE(MEASUREMENT_SERIALIZE_ERR_SERIALIZE_ERROR, 0x0C11)

ErrorResult measurement_serializer_serialize(SerializerHandle handle,
                                             const Measurement *measurement,
                                             pb_ostream_t *out_stream);

#endif

#include <stdlib.h>
#include "esp_log.h"

#include "serializer.h"
#include "sample_message.h"

static const char *TAG = "sample_message";

ErrorResult serialize_sample_measurement(Measurement *measurement, pb_ostream_t *stream) {

  SerializerDescriptor descriptors[] = {
    SAMPLE_SERIALIZER_DESCRIPTOR(SAMPLE_MESSAGE_BASE, NULL)
  };
  
  SerializerHandle serializer = serializer_create(descriptors, 1);

  ErrorResult result = measurement_serializer_serialize(serializer,
                                                                       measurement,
                                                                       stream);

  serializer_delete(serializer);

  return result;
}

Measurement* create_sample_measurement(int64_t time, int value1, double value2) {
  Measurement *measurement = malloc(sizeof(Measurement) + sizeof(SampleMessage));
  set_measurement_header(measurement,
                         1,
                         time,
                         MEASUREMENT_TYPE(SAMPLE_MESSAGE_BASE, 0x01),
                         sizeof(SampleMessage));
  SampleMessage *message = (SampleMessage *)measurement->data;
  message->number1 = value1;
  message->number2 = value2;

  return measurement;
}

bool sample_message_serialize(const MessageType message_type,
                              const void *data,
                              pb_ostream_t *stream,
                              void *arg) {

  const SampleMessage *message = (const SampleMessage *) data;
  
  ProtoSampleMessage proto_message = {
    .number1 = message->number1,
    .number2 = message->number2
  };

  bool success = pb_encode_submessage(stream, ProtoSampleMessage_fields, &proto_message);

  return success;
}

bool sample_message_deserialize(pb_istream_t *stream, const pb_field_t *field, void **arg) {

  ProtoSampleMessage proto_message = ProtoSampleMessage_init_zero;

  //  uint64_t varint;
  /* pb_decode_varint(stream, &varint); */
  /* printf("DECODED VARINT: %llu\n", varint); */
  bool success = pb_decode(stream, ProtoSampleMessage_fields, &proto_message);
  if (!success) {
    ESP_LOGE(TAG, "sample message deserialize failed, error: %s\n", stream->errmsg);
    return false;
  }

  SampleMessage *message = *arg;
  message->number1 = proto_message.number1;
  message->number2 = proto_message.number2;

  return success;
}

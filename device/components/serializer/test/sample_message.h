#ifndef SAMPLE_MESSAGE_H
#define SAMPLE_MESSAGE_H

#include <stdbool.h>
#include "pb_common.h"
#include "pb_encode.h"
#include "pb_decode.h"
#include "sample.pb.h"

#include "error.h"
#include "sensor_api.h"
#include "measurement_serializer.h"

#define SAMPLE_MESSAGE_BASE 0x1

typedef struct {
  int number1;
  double number2;
} SampleMessage;

bool sample_message_serialize(const MessageType message_type,
                              const void *data,
                              pb_ostream_t *stream,
                              void *arg);
bool sample_message_deserialize(pb_istream_t *stream,
                                const pb_field_t *field,
                                void **arg);

Measurement* create_sample_measurement(int64_t time, int value1, double value2);
ErrorResult serialize_sample_measurement(Measurement *measurement, pb_ostream_t *stream);

#define SAMPLE_SERIALIZER_DESCRIPTOR(MESSAGE_TYPE_BASE, ARG) { \
    .message_type_base = MESSAGE_TYPE_BASE,                    \
    .serialize = sample_message_serialize, \
    .arg = ARG }

#endif

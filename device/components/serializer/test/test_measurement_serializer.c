#include "unity.h"
#include "pb_common.h"
#include "pb_encode.h"
#include "pb_decode.h"

#include "sensor_api.h"
#include "serializer.h"
#include "measurement_serializer.h"
#include "sensor_file.pb.h"
#include "sample_message.h"

#define TEST_TAG "[measurement_serializer]"

//bool encode_measurement_content(pb_ostream_t *stream, const pb_field_t *field, void * const *arg) {

TEST_CASE("measurement_serialize_should_serialize", TEST_TAG)
{
  // given
  const MessageTypeBase type_base = 0x1;
  const MessageType message_type = (type_base << 8) + 0x01;

  SerializerDescriptor descriptors[] = {
    SAMPLE_SERIALIZER_DESCRIPTOR(type_base, NULL)
  };
  SerializerHandle serializer = serializer_create(descriptors, 1);

  uint8_t buffer[100];
  pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));
  //pb_encode(&stream, Example_fields, &mymessage);
  
  // when
  Measurement *measurement = malloc(sizeof(Measurement) + sizeof(SampleMessage));
  set_measurement_header(measurement,
                         12,
                         424242424242,
                         message_type,
                         sizeof(SampleMessage));
  SampleMessage *message = (SampleMessage *)measurement->data;
  message->number1 = 41;
  message->number2 = 42.42;
  
  ErrorResult result = measurement_serializer_serialize(serializer,
                                                        measurement,
                                                        &stream);

  // then
  TEST_ASSERT_GREATER_THAN(10, stream.bytes_written);
  TEST_ASSERT_EQUAL(AV_OK, result.code);

  pb_istream_t in_stream = pb_istream_from_buffer(buffer, stream.bytes_written);

  SampleMessage decoded_message;
  ProtoMeasurement decoded_measurement = ProtoMeasurement_init_zero;
  decoded_measurement.content.funcs.decode = &sample_message_deserialize;
  decoded_measurement.content.arg = &decoded_message;

  bool decode_success = pb_decode_delimited(&in_stream,
                                            ProtoMeasurement_fields,
                                            &decoded_measurement);

  TEST_ASSERT_TRUE(decode_success);
  TEST_ASSERT_EQUAL(measurement->time, decoded_measurement.time);
  TEST_ASSERT_EQUAL(measurement->sensor_id, decoded_measurement.sensor_id);
  TEST_ASSERT_EQUAL(measurement->message_type, decoded_measurement.message_type);
  TEST_ASSERT_EQUAL(measurement->size, decoded_measurement.message_size);
  TEST_ASSERT_EQUAL(message->number1, decoded_message.number1);
  TEST_ASSERT_EQUAL(message->number2, decoded_message.number2);

  free(measurement);
}

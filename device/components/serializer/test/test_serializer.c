#include "unity.h"
#include "esp_log.h"
#include "pb_common.h"

#include "serializer.h"

static const char *TAG = "test_measurement_storage";
#define TEST_TAG "[measurement_storage]"

typedef struct {
  int call_count;
} MockSerializer;

#define DEFAULT_MOCK_SERIALIZER() { \
  .call_count = 0 }

static bool mock_measurement_serialize(const SerializerMessageType message_type,
                                       const void *data,
                                       pb_ostream_t *stream,
                                       void *arg) {

  MockSerializer *me = arg;
  me->call_count++;

  return true;
}

#define MOCK_SERIALIZER_DESCRIPTOR(MESSAGE_TYPE_BASE, ARG ) { \
    .message_type_base = MESSAGE_TYPE_BASE, \
    .serialize = mock_measurement_serialize, \
    .arg = ARG }

TEST_CASE("serialize_should_call_plugin_when_base_match", TEST_TAG)
{
  // given
  const SerializerMessageTypeBase type_base = 0x1;
  const SerializerMessageType type1 = (type_base << 8) + 0x01;
  const SerializerMessageType type2 = (type_base << 8) + 0x02;

  MockSerializer mock_serializer = DEFAULT_MOCK_SERIALIZER();
  
  SerializerDescriptor descriptors[] = {
    MOCK_SERIALIZER_DESCRIPTOR(type_base, &mock_serializer)
  };
  SerializerHandle serializer = serializer_create(descriptors, 1);

  // when
  char *data1 = "MESSAGE1";
  serializer_serialize(serializer, type1, data1, NULL);

  char *data2 = "MESSAGE2";
  ErrorResult result = serializer_serialize(serializer, type2, data2, NULL);

  // then
  TEST_ASSERT_EQUAL(2, mock_serializer.call_count);
  TEST_ASSERT_EQUAL(AV_OK, result.code);
  serializer_delete(serializer);
}

TEST_CASE("serialize_should_not_call_plugin_when_base_doesnot_match", TEST_TAG)
{
  // given
  const SerializerMessageTypeBase type_base1 = 0x1;
  const SerializerMessageTypeBase type_base2 = 0x2;
  const SerializerMessageType type1 = (type_base1 << 8) + 0x01;
  const SerializerMessageType type2 = (type_base2 << 8) + 0x02;

  MockSerializer mock_serializer = DEFAULT_MOCK_SERIALIZER();
  
  SerializerDescriptor descriptors[] = {
    MOCK_SERIALIZER_DESCRIPTOR(type_base1, &mock_serializer)
  };
  SerializerHandle serializer = serializer_create(descriptors, 1);

  // when
  char *data1 = "MESSAGE1";
  ErrorResult result1 = serializer_serialize(serializer, type1, data1, NULL);

  char *data2 = "MESSAGE2";
  ErrorResult result2 = serializer_serialize(serializer, type2, data2, NULL);

  // then
  TEST_ASSERT_EQUAL(1, mock_serializer.call_count);
  TEST_ASSERT_EQUAL(AV_OK, result1.code);
  TEST_ASSERT_EQUAL(SERIALIZE_ERR_NOT_FOUND, result2.code);
  serializer_delete(serializer);
}

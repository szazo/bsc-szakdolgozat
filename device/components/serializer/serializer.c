#include <stdlib.h>
#include "pb_common.h"
#include "serializer.h"
#include "sensor_api.h"

SerializerHandle serializer_create(SerializerDescriptor *descriptors, uint8_t count) {
  Serializer *serializer = malloc(sizeof(Serializer));
  serializer->serializers = descriptors;
  serializer->serializer_count = count;
  return serializer;
}

ErrorResult serializer_serialize(SerializerHandle handle,
                                 SerializerMessageType message_type,
                                 const void *data,
                                 pb_ostream_t *out_stream) {

  Serializer *me = (Serializer*) handle;

  SerializerDescriptor *descriptor = NULL;
  bool found = false;
  for (int i = 0; i < me->serializer_count; i++) {
    descriptor = &me->serializers[i];
    if (DECODE_MEASUREMENT_BASE_TYPE(message_type) == descriptor->message_type_base) {
      found = true;
      break;
    }
  }

  if (!found) {
    return AV_ERROR(SERIALIZE_ERR_NOT_FOUND);
  }

  descriptor->serialize(message_type, data, out_stream, descriptor->arg);

  return AV_OK_RESULT;
}

void serializer_delete(SerializerHandle handle) {
  free(handle);
}

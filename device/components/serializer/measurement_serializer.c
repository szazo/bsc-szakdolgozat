#include "pb_common.h"
#include "pb_encode.h"

#include "serializer.h"
#include "measurement_serializer.h"
#include "sensor_file.pb.h"

typedef struct {
  const Measurement *measurement;
  const SerializerHandle plugin_serializer;
  ErrorResult result;
} SerializerParams;

static bool encode_measurement_content(pb_ostream_t *stream, const pb_field_t *field, void * const *arg);


ErrorResult measurement_serializer_serialize(SerializerHandle handle,
                                                     const Measurement *measurement,
                                                     pb_ostream_t *out_stream) {

  SerializerParams params = {
    .measurement = measurement,
    .plugin_serializer = handle
  };
  ProtoMeasurement proto_measurement = {
    .time = measurement->time,
    .sensor_id = measurement->sensor_id,
    .message_type = measurement->message_type,
    .message_size = measurement->size,
    .content = {
      .funcs = {
        .encode = encode_measurement_content
      },
      .arg = &params
    }
  };

  bool success = pb_encode_delimited(out_stream, ProtoMeasurement_fields, &proto_measurement);
  if (!success) {

    return AV_ERROR_WITH_REASON(MEASUREMENT_SERIALIZE_ERR_SERIALIZE_ERROR, params.result.code);
  }

  return AV_OK_RESULT;
}

static bool encode_measurement_content(pb_ostream_t *stream,
                                       const pb_field_t *field,
                                       void * const *arg) {

  SerializerParams *params = (SerializerParams *) *arg;

  if (!pb_encode_tag_for_field(stream, field)) {
    return false;
  }
  
  params->result = serializer_serialize(params->plugin_serializer,
                                             params->measurement->message_type,
                                             params->measurement->data,
                                             stream);

  return params->result.code == AV_OK;
}

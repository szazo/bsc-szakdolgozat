#ifndef SERIALIZER_H
#define SERIALIZER_H

#include "error.h"
#include "pb_encode.h"

typedef void* SerializerHandle;
typedef int16_t SerializerMessageType;
typedef int8_t SerializerMessageTypeBase;

typedef bool (MeasurementSerialize) (const SerializerMessageType message_type,
                                     const void *data,
                                     pb_ostream_t *out_stream,
                                     void *arg);

typedef struct {
  SerializerMessageTypeBase message_type_base;
  MeasurementSerialize *serialize;
  void *arg;  
} SerializerDescriptor;

typedef struct {
  SerializerDescriptor *serializers;
  uint8_t serializer_count;
} Serializer;

AV_ERROR_DEFINE(SERIALIZE_ERR_NOT_FOUND, 0x0C01)

SerializerHandle serializer_create(SerializerDescriptor *descriptors, uint8_t count);
ErrorResult serializer_serialize(SerializerHandle handle,
                                 SerializerMessageType message_type,
                                 const void *data,
                                 pb_ostream_t *out_stream);
void serializer_delete(SerializerHandle handle);

#endif

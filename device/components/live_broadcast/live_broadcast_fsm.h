#ifndef LIVE_BROADCAST_FSM_H
#define LIVE_BROADCAST_FSM_H

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "error.h"

AV_ERROR_DEFINE(LIVE_BROADCAST_FSM_OUTPUT_QUEUE_FULL, 0x0501)
AV_ERROR_DEFINE(LIVE_BROADCAST_FSM_INVALID_PHASE, 0x0502)

typedef enum {
  LIVE_BROADCAST_FSM_PHASE_NONE,
  LIVE_BROADCAST_PHASE_STOPPED,
  LIVE_BROADCAST_PHASE_WAIT_FOR_SERVER,
  LIVE_BROADCAST_PHASE_BROADCASTING,
} LiveBroadcastFSMPhase;

typedef enum {
  LIVE_BROADCAST_INPUT_START,
  LIVE_BROADCAST_INPUT_STOP,
  LIVE_BROADCAST_INPUT_CONNECTED,
  LIVE_BROADCAST_INPUT_DISCONNECTED,
  LIVE_BROADCAST_INPUT_MEASUREMENT
} LiveBroadcastFSMInputEvent;

typedef enum {
  LIVE_BROADCAST_OUTPUT_MEASUREMENT_REGISTER,
  LIVE_BROADCAST_OUTPUT_MEASUREMENT_UNREGISTER,
  LIVE_BROADCAST_OUTPUT_MQTT_OPEN,
  LIVE_BROADCAST_OUTPUT_MQTT_CLOSE,
  LIVE_BROADCAST_OUTPUT_SEND_TELEMETRY
} LiveBroadcastFSMOutputEvent;

typedef struct {
  LiveBroadcastFSMPhase phase;
  LiveBroadcastFSMPhase requested_phase;  
  QueueHandle_t output_events;
  bool is_connected;
  int64_t last_measurement_time;
} LiveBroadcastFSM;

void live_broadcast_fsm_init(LiveBroadcastFSM *me, QueueHandle_t output_events);
ErrorResult live_broadcast_fsm_execute(LiveBroadcastFSM *me, LiveBroadcastFSMInputEvent event);
LiveBroadcastFSMPhase live_broadcast_fsm_phase(LiveBroadcastFSM *me);

#endif

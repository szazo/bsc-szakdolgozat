#include "esp_log.h"
#include "pb_encode.h"

#include "live_broadcast_measurement.h"
#include "live_broadcast.pb.h"
#include "gps_measurement.h"
#include "gyro_serializer.h"
#include "gps_serializer.h"

static void serialize_gps(LiveBroadcastGps *gps, ProtoLiveBroadcastGps *target);
static void serialize_live_broadcast_gps_time(LiveBroadcastGpsTime *source, ProtoLiveBroadcastGpsTime *target);
static void serialize_gps_position(LiveBroadcastGpsPosition *position, ProtoLiveBroadcastGpsPosition *target);
static void serialize_gps_quality(LiveBroadcastGpsQuality *source, ProtoLiveBroadcastGpsQuality *target);
static void serialize_gps_faa(LiveBroadcastGpsFAA *source, ProtoLiveBroadcastGpsFAA *target);
static void serialize_gps_speed(LiveBroadcastGpsSpeed *source, ProtoLiveBroadcastGpsSpeed *target);
static void serialize_gps_track(LiveBroadcastGpsTrack *source, ProtoLiveBroadcastGpsTrack *target);
static void serialize_gps_altitude(LiveBroadcastGpsAltitude *source, ProtoLiveBroadcastGpsAltitude *target);

static void serialize_gyro(LiveBroadcastGyro *source, ProtoLiveBroadcastGyro *target);

static void serialize_airspeed(LiveBroadcastAirspeed *source, ProtoLiveBroadcastAirspeed *target);
static void serialize_pressure(LiveBroadcastPressure *source, ProtoLiveBroadcastPressure *target);

int live_broadcast_serialize(LiveBroadcastTelemetry *telemetry, uint8_t *buffer, int size) {

  ProtoLiveBroadcastTelemetry target = ProtoLiveBroadcastTelemetry_init_default;

  serialize_gps(&telemetry->gps, &target.gps);
  serialize_gyro(&telemetry->gyro, &target.gyro);
  serialize_airspeed(&telemetry->airspeed, &target.airspeed);
  serialize_pressure(&telemetry->pressure, &target.pressure);

  pb_ostream_t stream = pb_ostream_from_buffer(buffer, size);
  bool success = pb_encode_delimited(&stream, ProtoLiveBroadcastTelemetry_fields, &target);
  if (!success) {
    ESP_LOGE("LB", "error encoding message: %s", stream.errmsg);
    return -1;
  }

  return stream.bytes_written;
}

static void serialize_airspeed(LiveBroadcastAirspeed *source, ProtoLiveBroadcastAirspeed *target) {
  target->age = source->age;
  target->speed = source->speed;
}

static void serialize_pressure(LiveBroadcastPressure *source, ProtoLiveBroadcastPressure *target) {
  target->age = source->age;
  target->pressure = source->pressure;
  target->temperature = source->temperature;
  target->humidity = source->humidity;
}

static void serialize_gyro(LiveBroadcastGyro *source, ProtoLiveBroadcastGyro *target) {
  target->age = source->age;
  gyro_serialize_quaternion_q30(&source->quaternion, &target->quaternion);
  gyro_serialize_raw_axes(&source->gyro, &target->gyro);
  gyro_serialize_raw_axes(&source->accel, &target->accel);
  gyro_serialize_raw_axes(&source->heading, &target->heading);
}

static void serialize_gps(LiveBroadcastGps *gps, ProtoLiveBroadcastGps *target) {
  serialize_live_broadcast_gps_time(&gps->time, &target->time);
  serialize_gps_position(&gps->position, &target->position);
  serialize_gps_altitude(&gps->altitude, &target->altitude);
  serialize_gps_speed(&gps->speed, &target->speed);
  serialize_gps_track(&gps->track, &target->track);
  serialize_gps_quality(&gps->quality, &target->quality);
  serialize_gps_faa(&gps->faa, &target->faa);
}

static void serialize_live_broadcast_gps_time(LiveBroadcastGpsTime *source, ProtoLiveBroadcastGpsTime *target) {
  target->age = source->age;
  gps_serialize_date(&source->date, &target->date);
  gps_serialize_time(&source->time, &target->time);
  target->hour_offset = source->hour_offset;
  target->minute_offset = source->minute_offset;
}

static void serialize_gps_quality(LiveBroadcastGpsQuality *source, ProtoLiveBroadcastGpsQuality *target) {
  target->age = source->age;
  target->fix_quality = source->fix_quality;
  target->satellites_tracked = source->satellites_tracked;
  gps_serialize_float(&source->hdop, &target->hdop);
  gps_serialize_float(&source->dgps_age, &target->dgps_age);
}

static void serialize_gps_faa(LiveBroadcastGpsFAA *source, ProtoLiveBroadcastGpsFAA *target) {
  target->age = source->age;
  target->faa_mode = source->faa_mode;
}

static void serialize_gps_speed(LiveBroadcastGpsSpeed *source, ProtoLiveBroadcastGpsSpeed *target) {
  target->age = source->age;
  gps_serialize_float(&source->speed_knots, &target->speed_knots);
  gps_serialize_float(&source->speed_kph, &target->speed_kph);
}

static void serialize_gps_track(LiveBroadcastGpsTrack *source, ProtoLiveBroadcastGpsTrack *target) {
  target->age = source->age;
  gps_serialize_float(&source->true_track_degrees, &target->true_track_degrees);
  gps_serialize_float(&source->magnetic_track_degrees, &target->magnetic_track_degrees);
}

static void serialize_gps_altitude(LiveBroadcastGpsAltitude *source, ProtoLiveBroadcastGpsAltitude *target) {
  target->age = source->age;
  gps_serialize_float(&source->altitude, &target->altitude);
  target->altitude_units = source->altitude_units;
  gps_serialize_float(&source->height, &target->height);
  target->height_units = source->height_units;
}


static void serialize_gps_position(LiveBroadcastGpsPosition *position, ProtoLiveBroadcastGpsPosition *target) {
  target->age = position->age;
  gps_serialize_float(&position->latitude, &target->latitude);
  gps_serialize_float(&position->longitude, &target->longitude);
}

#include "measurement_loop_api.h"
#include "live_broadcast_fsm.h"
#include "live_broadcast_serialize.h"
#include "live_broadcast.h"

#define LIVE_BROADCAST_TASK_STACK_SIZE 4096
#define INPUT_EVENT_QUEUE_SIZE 10
#define INPUT_EVENT_QUEUE_TIMEOUT_MS 0
#define OUTPUT_EVENT_QUEUE_SIZE 10

static void task(void *arg, TaskHandle task);
static void mqtt_state_change_handler(void *arg, MqttClientState state);
static void measurement_handler(const Measurement *measurement, void *arg);
static void send_telemetry(LiveBroadcast *me);
static ErrorResult send_input_event(LiveBroadcast *me, LiveBroadcastFSMInputEvent event);
static ErrorResult execute_output_event(LiveBroadcast *me, LiveBroadcastFSMOutputEvent event);

ErrorResult live_broadcast_create(LiveBroadcastConfig config,
                                     MqttClientHandle mqtt_client,
                                     MeasurementLoopAPIHandle measurement_loop,
                                     LiveBroadcast *me) {

  me->config = config;
  me->mqtt_client = mqtt_client;
  me->measurement_loop = measurement_loop;
  me->fsm_input_events = xQueueCreate(INPUT_EVENT_QUEUE_SIZE, sizeof(LiveBroadcastFSMInputEvent));
  if (me->fsm_input_events == NULL) {
    return AV_ERROR(LIVE_BROADCAST_QUEUE_CREATE_ERROR);
  }
  me->fsm_output_events = xQueueCreate(OUTPUT_EVENT_QUEUE_SIZE, sizeof(LiveBroadcastFSMOutputEvent));
  if (me->fsm_output_events == NULL) {
    return AV_ERROR(LIVE_BROADCAST_QUEUE_CREATE_ERROR);
  }

  ErrorResult err = task_create(&me->task);
  if (err.code != AV_OK) {
    vQueueDelete(me->fsm_input_events);
  }

  MqttClientCallback mqtt_callback = {
    .state_changed = mqtt_state_change_handler,
    .arg = me
  };
  mqtt_client_register_callback(mqtt_client, mqtt_callback);

  live_broadcast_fsm_init(&me->fsm, me->fsm_output_events);
  return AV_OK_RESULT;
}

void live_broadcast_delete(LiveBroadcast *me) {
  task_delete(&me->task);
}

ErrorResult live_broadcast_start(LiveBroadcast *me) {
  TaskConfig task_config = {
    .name = "live_broadcast_task",
    .priority = me->config.task_priority,
    .core_id = me->config.task_cpu,
    .stack_depth = LIVE_BROADCAST_TASK_STACK_SIZE,
    .task_function = task,
    .task_param = me
  };

  ErrorResult err = task_start(&me->task, &task_config);
  if (err.code != AV_OK) {
    return err;
  }

  return AV_OK_RESULT;
}

void live_broadcast_stop_and_wait(LiveBroadcast *me) {
  task_stop_and_wait(&me->task);
}

ErrorResult live_broadcast_resume(LiveBroadcast *me) {
  return send_input_event(me, LIVE_BROADCAST_INPUT_START);
}

ErrorResult live_broadcast_suspend(LiveBroadcast *me) {
  return send_input_event(me, LIVE_BROADCAST_INPUT_STOP);
}

static void mqtt_state_change_handler(void *arg, MqttClientState state) {

  LiveBroadcast *me = arg;

  LiveBroadcastFSMInputEvent event;
  switch (state) {
  case MQTT_CLIENT_STATE_CONNECTED:
    event = LIVE_BROADCAST_INPUT_CONNECTED;
    break;
  case MQTT_CLIENT_STATE_DISCONNECTED:
    event = LIVE_BROADCAST_INPUT_DISCONNECTED;
    break;
  default:
    return;
  }

  send_input_event(me, event);
}

static void measurement_handler(const Measurement *measurement, void *arg) {
  LiveBroadcast *me = arg;
  live_broadcast_process_measurement(measurement, &me->telemetry);

  send_input_event(me, LIVE_BROADCAST_INPUT_MEASUREMENT);
}

static ErrorResult send_input_event(LiveBroadcast *me, LiveBroadcastFSMInputEvent event) {
  if (xQueueSend(me->fsm_input_events, &event, 0) != pdTRUE) {
    return AV_ERROR(LIVE_BROADCAST_QUEUE_FULL_ERROR);
  }

  return AV_OK_RESULT;
}

static void task(void *arg, TaskHandle task) {

  LiveBroadcast *me = arg;
  
  while (true) {

    if (task_is_stop_requested(task)) {
      send_input_event(me, LIVE_BROADCAST_INPUT_STOP);
    }
    
    LiveBroadcastFSMInputEvent input_event;
    if (xQueueReceive(me->fsm_input_events, &input_event, pdMS_TO_TICKS(10)) == pdFALSE) {
      continue;
    }

    ErrorResult err = live_broadcast_fsm_execute(&me->fsm, input_event);
    if (err.code != AV_OK) {
      av_error_publish_result(SEVERITY_ERROR, err);
      continue;
    }

    LiveBroadcastFSMOutputEvent output_event;
    while (xQueueReceive(me->fsm_output_events, &output_event, 0) == pdTRUE) {
      ErrorResult err = execute_output_event(me, output_event);
      if (err.code != AV_OK) {
        av_error_publish_result(SEVERITY_ERROR, err);
      }
    }

    if (task_is_stop_requested(task) &&
        live_broadcast_fsm_phase(&me->fsm) == LIVE_BROADCAST_PHASE_STOPPED) {
      break;
    }
  }
}

static ErrorResult execute_output_event(LiveBroadcast *me, LiveBroadcastFSMOutputEvent event) {
  switch (event) {
  case LIVE_BROADCAST_OUTPUT_MEASUREMENT_REGISTER:
    {
      return measurement_loop_register(me->measurement_loop, measurement_handler, me);
    }
  case LIVE_BROADCAST_OUTPUT_MEASUREMENT_UNREGISTER:
    {
      return measurement_loop_unregister(me->measurement_loop, measurement_handler, me);
    }
  case LIVE_BROADCAST_OUTPUT_MQTT_OPEN:
    {
      return mqtt_client_open(me->mqtt_client);
    }
  case LIVE_BROADCAST_OUTPUT_MQTT_CLOSE:
    {
      ErrorResult err =  send_input_event(me, LIVE_BROADCAST_INPUT_DISCONNECTED);
      if (err.code != AV_OK) {
        av_error_publish_result(SEVERITY_ERROR, err);
      }
      return mqtt_client_close(me->mqtt_client);
    }
  case LIVE_BROADCAST_OUTPUT_SEND_TELEMETRY:
    {
      send_telemetry(me);
      return AV_OK_RESULT;
    }
  default:
    return AV_OK_RESULT;
  }
}

static void send_telemetry(LiveBroadcast *me) {
  int bytes_written = live_broadcast_serialize(&me->telemetry, me->buffer, sizeof(me->buffer));
  if (bytes_written <= 0) {
    av_error_publish(SEVERITY_ERROR, LIVE_BROADCAST_SERIALIZATION_ERROR);
    return;
  }

  ErrorResult publish_result = mqtt_client_publish(me->mqtt_client,
                                                   CONFIG_MQTT_LIVE_BROADCAST_TOPIC,
                                                   (char *) me->buffer,
                                                   bytes_written,
                                                   QOS_AT_MOST_ONCE,
                                                   true);

  if (publish_result.code == AV_OK || publish_result.code == MQTT_NOT_CONNECTED_ERROR) {
    return;
  }
  
  av_error_publish_result(SEVERITY_ERROR, publish_result);
}

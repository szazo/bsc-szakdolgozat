#include "unity.h"

//#include "freertos/FreeRTOS.h"
/* #include "freertos/task.h" */
#include "message_queue.h"
#include "live_broadcast.h"
#include "measurement_loop.h"
#include "../../measurement_loop/test/mock_source.h"

#define TEST_TAG "[live_broadcast]"

static MqttClientHandle mqtt_client;
static LiveBroadcast live_broadcast;
static MeasurementLoopHandle measurement_loop;

static MockSource *mock_source;

static void setup() {
  tcpip_adapter_init();

  ErrorResult err = mqtt_client_create((MqttClientSettings) { .uri = CONFIG_MQTT_SERVER_URL }, &mqtt_client);
  TEST_ASSERT_EQUAL(AV_OK, err.code);

  mock_source = mock_source_create();
  MeasurementSource descriptor = MOCK_SOURCE(mock_source);
  err = measurement_loop_create(&descriptor, &measurement_loop);
  TEST_ASSERT_EQUAL(AV_OK, err.code);

  measurement_loop_start(measurement_loop);

  LiveBroadcastConfig config = {
    .task_priority = CONFIG_LIVE_BROADCAST_TASK_PRIORITY,
    .task_cpu = CONFIG_LIVE_BROADCAST_TASK_CPU
  };
  err = live_broadcast_create(config, mqtt_client, measurement_loop, &live_broadcast);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
}

static void tear_down() {
  measurement_loop_stop_and_wait(measurement_loop);
  measurement_loop_delete(measurement_loop);

  mqtt_client_delete(mqtt_client);

  live_broadcast_delete(&live_broadcast);
}

TEST_CASE("should_create_and_delete", TEST_TAG)
{
  // given
  setup();

  // then
  tear_down();
}

static void trigger_flight_started() {
}

static void trigger_flight_stopped() {
}

TEST_CASE("should_get_measurements_when_flight_started", TEST_TAG)
{
  setup();

  // given

  // when
  trigger_flight_started();

  vTaskDelay(pdMS_TO_TICKS(10));
  
  Measurement measurement;
  measurement.time = sensor_get_time();
  measurement.sensor_id = 42;
  measurement.message_type = 1;
  measurement.size = 0;
  
  mock_source_send_measurement(mock_source, &measurement);
  vTaskDelay(pdMS_TO_TICKS(10));

  // then
  trigger_flight_stopped();
  vTaskDelay(pdMS_TO_TICKS(10));
  
  vTaskDelay(pdMS_TO_TICKS(5000));
  tear_down();
}

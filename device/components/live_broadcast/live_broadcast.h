#ifndef LIVE_BROADCAST_H
#define LIVE_BROADCAST_H

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"

#include "error.h"
#include "stoppable_task.h"
#include "mqtt_adapter.h"
#include "measurement_loop_api.h"
#include "live_broadcast_measurement.h"
#include "live_broadcast_fsm.h"

AV_ERROR_DEFINE(LIVE_BROADCAST_QUEUE_CREATE_ERROR, 0xA011)
AV_ERROR_DEFINE(LIVE_BROADCAST_QUEUE_FULL_ERROR, 0xA012)
AV_ERROR_DEFINE(LIVE_BROADCAST_SERIALIZATION_ERROR, 0xA013)

#define LIVE_BROADCAST_BUFFER_LENGTH 300

typedef struct {
  uint8_t task_priority;
  TaskCPU task_cpu;
} LiveBroadcastConfig;

typedef struct {
  Task task;
  MqttClientHandle mqtt_client;
  MeasurementLoopAPIHandle measurement_loop;
  LiveBroadcastConfig config;
  QueueHandle_t fsm_input_events;
  QueueHandle_t fsm_output_events;
  LiveBroadcastTelemetry telemetry;
  LiveBroadcastFSM fsm;
  uint8_t buffer[LIVE_BROADCAST_BUFFER_LENGTH];
} LiveBroadcast;

typedef LiveBroadcast* LiveBroadcastHandle;

ErrorResult live_broadcast_create(LiveBroadcastConfig config,
                                     MqttClientHandle mqtt_client,
                                     MeasurementLoopAPIHandle measurement_loop,
                                     LiveBroadcast *me);
void live_broadcast_delete(LiveBroadcast *me);
ErrorResult live_broadcast_start(LiveBroadcast *me);
void live_broadcast_stop_and_wait(LiveBroadcast *me);

ErrorResult live_broadcast_resume(LiveBroadcast *me);
ErrorResult live_broadcast_suspend(LiveBroadcast *me);

#endif

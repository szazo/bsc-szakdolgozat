COMPONENT_ADD_INCLUDEDIRS=.

live_broadcast.o: live_broadcast.pb.c

%.pb.c %.pb.h: $(COMPONENT_PATH)/%.proto
	$(PROTOC_WITH_OPTS) --proto_path=$(COMPONENT_PATH) \
		--proto_path=$(COMPONENT_PATH)/../gps \
		--proto_path=$(COMPONENT_PATH)/../gyro \
		--nanopb_out=$(COMPONENT_PATH)  $<

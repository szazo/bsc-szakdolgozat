#ifndef LIVE_BROADCAST_MEASUREMENT_H
#define LIVE_BROADCAST_MEASUREMENT_H

#include "gps_measurement.h"
#include "gyro_sensor.h"
#include "gps_measurement.h"

#define DEFAULT_GPS_FLOAT ((struct minmea_float) { .value = 0, .scale = 0 })
#define DEFAULT_GPS_DATE ((struct minmea_date) { .day = 0, .month = 0, .year = 0 })
#define DEFAULT_GPS_TIME ((struct minmea_time) { .hours = 0, .minutes = 0, .seconds = 0, .microseconds = 0 })
#define DEFAULT_AGE (0)

typedef struct {
  SensorTime age;
  struct minmea_float latitude;
  struct minmea_float longitude;
} LiveBroadcastGpsPosition;
#define DEFAULT_LIVE_BROADCAST_GPS_POSITION ((LiveBroadcastGpsPosition){ \
      .age = DEFAULT_AGE,                                               \
        .latitude = DEFAULT_GPS_FLOAT,                                  \
        .longitude = DEFAULT_GPS_FLOAT                                  \
        })

typedef struct {
  SensorTime age;
  struct minmea_date date;
  struct minmea_time time;
  int hour_offset;
  int minute_offset;
} LiveBroadcastGpsTime;
#define DEFAULT_LIVE_BROADCAST_GPS_TIME ((LiveBroadcastGpsTime) { \
      .age = DEFAULT_AGE,                                         \
        .date = DEFAULT_GPS_DATE,                                 \
        .time = DEFAULT_GPS_TIME,                                 \
        .hour_offset = 0,                                         \
        .minute_offset = 0                                       \
        })

typedef struct {
  SensorTime age;
  int fix_quality;
  int satellites_tracked;
  struct minmea_float hdop;
  struct minmea_float dgps_age;
} LiveBroadcastGpsQuality;
#define DEFAULT_LIVE_BROADCAST_GPS_QUALITY ((LiveBroadcastGpsQuality) { \
      .age = DEFAULT_AGE,                                               \
        .fix_quality = 0,                                               \
        .satellites_tracked = 0,                                        \
        .hdop = DEFAULT_GPS_FLOAT,                                      \
        .dgps_age = DEFAULT_GPS_FLOAT                                   \
        })

typedef struct {
  SensorTime age;
  enum minmea_faa_mode faa_mode;
} LiveBroadcastGpsFAA;
#define DEFAULT_LIVE_BROADCAST_GPS_FAA ((LiveBroadcastGpsFAA) { \
      .age = DEFAULT_AGE,                                               \
        .faa_mode = 0,                                                  \
        })

typedef struct {
  SensorTime age;
  struct minmea_float speed_knots;
  struct minmea_float speed_kph;
} LiveBroadcastGpsSpeed;
#define DEFAULT_LIVE_BROADCAST_GPS_SPEED ((LiveBroadcastGpsSpeed) { \
      .age = DEFAULT_AGE,                                               \
        .speed_knots = DEFAULT_GPS_FLOAT,                               \
        .speed_kph = DEFAULT_GPS_FLOAT                                  \
        })

typedef struct {
  SensorTime age;
  struct minmea_float true_track_degrees;
  struct minmea_float magnetic_track_degrees;
} LiveBroadcastGpsTrack;
#define DEFAULT_LIVE_BROADCAST_GPS_TRACK ((LiveBroadcastGpsTrack) { \
      .age = DEFAULT_AGE,                                               \
        .true_track_degrees = DEFAULT_GPS_FLOAT,                        \
        .magnetic_track_degrees = DEFAULT_GPS_FLOAT                     \
        })

typedef struct {
  SensorTime age;
  struct minmea_float altitude;
  char altitude_units;
  struct minmea_float height;
  char height_units;
} LiveBroadcastGpsAltitude;
#define DEFAULT_LIVE_BROADCAST_GPS_ALTITUDE ((LiveBroadcastGpsAltitude) { \
      .age = DEFAULT_AGE,                                               \
        .altitude = DEFAULT_GPS_FLOAT,                                  \
        .altitude_units = 0,                                            \
        .height = DEFAULT_GPS_FLOAT,                                    \
        .height_units = 0                                               \
        })

typedef struct {
  LiveBroadcastGpsTime time;
  LiveBroadcastGpsPosition position;
  LiveBroadcastGpsAltitude altitude;
  LiveBroadcastGpsSpeed speed;
  LiveBroadcastGpsTrack track;
  LiveBroadcastGpsQuality quality;
  LiveBroadcastGpsFAA faa;
} LiveBroadcastGps;
#define DEFAULT_LIVE_BROADCAST_GPS ((LiveBroadcastGps){ \
      .time = DEFAULT_LIVE_BROADCAST_GPS_TIME,            \
        .position = DEFAULT_LIVE_BROADCAST_GPS_POSITION,  \
        .altitude = DEFAULT_LIVE_BROADCAST_GPS_ALTITUDE,  \
        .speed = DEFAULT_LIVE_BROADCAST_GPS_SPEED,        \
        .track = DEFAULT_LIVE_BROADCAST_GPS_TRACK,        \
        .quality = DEFAULT_LIVE_BROADCAST_GPS_QUALITY,    \
        .faa = DEFAULT_LIVE_BROADCAST_GPS_FAA             \
        })

typedef struct {
  SensorTime age;
  QuaternionQ30 quaternion;
  RawAxes gyro;
  RawAxes accel;
  RawAxes heading;
} LiveBroadcastGyro;
#define DEFAULT_LIVE_BROADCAST_GYRO ((LiveBroadcastGyro){ \
    .age = DEFAULT_AGE,                                   \
        .quaternion = DEFAULT_QUATERNION_Q30,             \
        .gyro = DEFAULT_RAW_AXES,                         \
        .accel = DEFAULT_RAW_AXES,                        \
        .heading = DEFAULT_RAW_AXES                       \
})

typedef struct {
  SensorTime age;
  int16_t speed;
} LiveBroadcastAirspeed;
#define DEFAULT_LIVE_BROADCAST_AIRSPEED ((LiveBroadcastAirspeed){ \
      .age = DEFAULT_AGE,                                         \
        .speed = 0,                                               \
})

typedef struct {
  SensorTime age;
  float pressure;
  float temperature;
  float humidity;
} LiveBroadcastPressure;
#define DEFAULT_LIVE_BROADCAST_PRESSURE ((LiveBroadcastPressure){       \
      .age = DEFAULT_AGE,                                               \
        .pressure = 0,                                                  \
        .temperature = 0,                                               \
        .humidity = 0,                                                  \
})

typedef struct {
  LiveBroadcastGps gps;
  LiveBroadcastGyro gyro;
  LiveBroadcastAirspeed airspeed;
  LiveBroadcastPressure pressure;
} LiveBroadcastTelemetry;

#define DEFAULT_LIVE_BROADCAST_TELEMETRY ((LiveBroadcastTelemetry){ \
      .gps = DEFAULT_LIVE_BROADCAST_GPS,                                \
        .gyro = DEFAULT_LIVE_BROADCAST_GYRO,                            \
        .airspeed = DEFAULT_LIVE_BROADCAST_AIRSPEED,                    \
        .pressure = DEFAULT_LIVE_BROADCAST_PRESSURE                     \
        })

void live_broadcast_process_measurement(const Measurement *measurement, LiveBroadcastTelemetry *telemetry);

#endif

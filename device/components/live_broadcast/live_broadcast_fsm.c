#include "live_broadcast_fsm.h"

static ErrorResult phase_stopped(LiveBroadcastFSM *me, LiveBroadcastFSMInputEvent event);
static ErrorResult phase_wait_for_server(LiveBroadcastFSM *me, LiveBroadcastFSMInputEvent event);
static ErrorResult phase_broadcasting(LiveBroadcastFSM *me, LiveBroadcastFSMInputEvent event);
static ErrorResult send_output_event(LiveBroadcastFSM *me, LiveBroadcastFSMOutputEvent event);
static void transition(LiveBroadcastFSM *me, LiveBroadcastFSMPhase next_phase);

#define OUTPUT_EVENT_QUEUE_TIMEOUT_MS 0

void live_broadcast_fsm_init(LiveBroadcastFSM *me, QueueHandle_t output_events) {
  me->output_events = output_events;
  me->is_connected = false;
  me->phase = LIVE_BROADCAST_PHASE_STOPPED;
  me->last_measurement_time = 0;
}

LiveBroadcastFSMPhase live_broadcast_fsm_phase(LiveBroadcastFSM *me) {
  return me->phase;
}

ErrorResult live_broadcast_fsm_execute(LiveBroadcastFSM *me, LiveBroadcastFSMInputEvent event) {
  me->requested_phase = me->phase;

  ErrorResult err;
  
  switch (me->phase) {
  case LIVE_BROADCAST_PHASE_STOPPED:
    err = phase_stopped(me, event);
    break;
  case LIVE_BROADCAST_PHASE_WAIT_FOR_SERVER:
    err = phase_wait_for_server(me, event);
    break;
  case LIVE_BROADCAST_PHASE_BROADCASTING:
    err = phase_broadcasting(me, event);
    break;
  default:
    err = AV_ERROR(LIVE_BROADCAST_FSM_INVALID_PHASE);
    break;
  }

  if (me->requested_phase != me->phase) {
    me->phase = me->requested_phase;
  }

  return err;
}

static ErrorResult phase_stopped(LiveBroadcastFSM *me, LiveBroadcastFSMInputEvent event) {

  switch (event) {
  case LIVE_BROADCAST_INPUT_START:
    {
      if (me->is_connected) {
        ErrorResult err = send_output_event(me, LIVE_BROADCAST_OUTPUT_MEASUREMENT_REGISTER);
        if (err.code != AV_OK) {
          return err;
        }
        transition(me, LIVE_BROADCAST_PHASE_BROADCASTING);
        return AV_OK_RESULT;
      }

      ErrorResult err = send_output_event(me, LIVE_BROADCAST_OUTPUT_MQTT_OPEN);
      if (err.code != AV_OK) {
        return err;
      }
      transition(me, LIVE_BROADCAST_PHASE_WAIT_FOR_SERVER);
      return AV_OK_RESULT;
    }

  case LIVE_BROADCAST_INPUT_CONNECTED:
    {
      me->is_connected = true;
      return AV_OK_RESULT;
    }

  case LIVE_BROADCAST_INPUT_DISCONNECTED:
    {
      me->is_connected = false;
      return AV_OK_RESULT;
    }
  default:
    return AV_OK_RESULT;
  }
}

static ErrorResult phase_wait_for_server(LiveBroadcastFSM *me, LiveBroadcastFSMInputEvent event) {
  
  switch (event) {
  case LIVE_BROADCAST_INPUT_STOP:
    {
      ErrorResult err = send_output_event(me, LIVE_BROADCAST_OUTPUT_MQTT_CLOSE);
      if (err.code != AV_OK) {
        return err;
      }
      transition(me, LIVE_BROADCAST_PHASE_STOPPED);
      return AV_OK_RESULT;
    }

  case LIVE_BROADCAST_INPUT_CONNECTED:
    {
      me->is_connected = true;
      ErrorResult err = send_output_event(me, LIVE_BROADCAST_OUTPUT_MEASUREMENT_REGISTER);
      if (err.code != AV_OK) {
        return err;
      }
      transition(me, LIVE_BROADCAST_PHASE_BROADCASTING);
      return AV_OK_RESULT;
      default:
        return AV_OK_RESULT;
    }
  }
}

static ErrorResult phase_broadcasting(LiveBroadcastFSM *me, LiveBroadcastFSMInputEvent event) {

  switch (event) {
  case LIVE_BROADCAST_INPUT_DISCONNECTED:
    {
      me->is_connected = false;
      ErrorResult err = send_output_event(me, LIVE_BROADCAST_OUTPUT_MEASUREMENT_UNREGISTER);
      if (err.code != AV_OK) {
        return err;
      }
      transition(me, LIVE_BROADCAST_PHASE_WAIT_FOR_SERVER);
      return AV_OK_RESULT;
    }
  case LIVE_BROADCAST_INPUT_STOP:
    {
      ErrorResult err = send_output_event(me, LIVE_BROADCAST_OUTPUT_MQTT_CLOSE);
      if (err.code != AV_OK) {
        return err;
      }
      err = send_output_event(me, LIVE_BROADCAST_OUTPUT_MEASUREMENT_UNREGISTER);
      if (err.code != AV_OK) {
        return err;
      }
      transition(me, LIVE_BROADCAST_PHASE_STOPPED);
      return AV_OK_RESULT;
    }
  case LIVE_BROADCAST_INPUT_MEASUREMENT:
    {
      int64_t time = esp_timer_get_time();
      if ((time - me->last_measurement_time) > CONFIG_LIVE_BROADCAST_SEND_INTERVAL_MS * 1000) { // us

        ErrorResult err = send_output_event(me, LIVE_BROADCAST_OUTPUT_SEND_TELEMETRY);
        me->last_measurement_time = time;

        return err;
      }

      return AV_OK_RESULT;
    }
  default:
    return AV_OK_RESULT;
  }
}

static ErrorResult send_output_event(LiveBroadcastFSM *me, LiveBroadcastFSMOutputEvent event) {
  if (xQueueSend(me->output_events, &event, OUTPUT_EVENT_QUEUE_TIMEOUT_MS) != pdTRUE) {
    return AV_ERROR(LIVE_BROADCAST_FSM_OUTPUT_QUEUE_FULL);
  }

  return AV_OK_RESULT;
}

static void transition(LiveBroadcastFSM *me, LiveBroadcastFSMPhase next_phase) {
  printf("live broadcast switch phase to %d\n", next_phase);
  me->requested_phase = next_phase;
}


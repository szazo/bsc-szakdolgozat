#include "esp_log.h"
#include "pb_encode.h"
#include "sensor_api.h"
#include "live_broadcast_measurement.h"
#include "live_broadcast.pb.h"
#include "gps_sensor.h"
#include "airspeed_sensor.h"
#include "pressure_sensor.h"

static void process_gyro_message(const Measurement *measurement, LiveBroadcastTelemetry *telemetry);
static void process_gps_message(const Measurement *measurement, LiveBroadcastTelemetry *telemetry);
static void process_gps_rmc(const SensorTime time,
                            struct minmea_sentence_rmc *rmc,
                            LiveBroadcastGps *telemetry);
static void process_gps_vtg(const SensorTime time,
                            struct minmea_sentence_vtg *vtg,
                            LiveBroadcastGps *telemetry);
static void process_gps_gga(const SensorTime time,
                            struct minmea_sentence_gga *gga,
                            LiveBroadcastGps *telemetry);
static void process_gps_zda(const SensorTime time,
                            struct minmea_sentence_zda *zda,
                            LiveBroadcastGps *telemetry);
static void process_airspeed_message(const Measurement *measurement, LiveBroadcastTelemetry *telemetry);
static void process_pressure_message(const Measurement *measurement, LiveBroadcastTelemetry *telemetry);

void live_broadcast_process_measurement(const Measurement *measurement, LiveBroadcastTelemetry *telemetry) {

  int base_type = DECODE_MEASUREMENT_BASE_TYPE(measurement->message_type);
  switch (base_type) {
  case GYRO_MESSAGE_BASE:
    process_gyro_message(measurement, telemetry);
    break;
  case GPS_MESSAGE_BASE:
    process_gps_message(measurement, telemetry);
    break;
  case AIRSPEED_MESSAGE_BASE:
    process_airspeed_message(measurement, telemetry);
    break;
  case PRESSURE_MESSAGE_BASE:
    process_pressure_message(measurement, telemetry);
    break;
  }
}

static void process_gps_message(const Measurement *measurement, LiveBroadcastTelemetry *telemetry) {
  char type = DECODE_MEASUREMENT_TYPE(measurement->message_type);

  GpsData *data = (GpsData *) measurement->data;

  switch (type) {
  case MINMEA_SENTENCE_RMC:
    process_gps_rmc(measurement->time, &(data->u.rmc), &telemetry->gps);
    break;
  case MINMEA_SENTENCE_VTG:
    process_gps_vtg(measurement->time, &(data->u.vtg), &telemetry->gps);
    break;
  case MINMEA_SENTENCE_GGA:
    process_gps_gga(measurement->time, &(data->u.gga), &telemetry->gps);
    break;
  case MINMEA_SENTENCE_ZDA:
    process_gps_zda(measurement->time, &(data->u.zda), &telemetry->gps);
    break;
  }
}

static void process_gps_gga(const SensorTime time, struct minmea_sentence_gga *gga, LiveBroadcastGps *telemetry) {

  if (gga->fix_quality != 0) {
    // time
    telemetry->time = (LiveBroadcastGpsTime) {
      .age = time,
      .date = telemetry->time.date, // use previous date
      .time = gga->time,
      .hour_offset = telemetry->time.hour_offset,
      .minute_offset = telemetry->time.minute_offset
    };

    // position
    telemetry->position = (LiveBroadcastGpsPosition) {
      .age = time,
      .latitude = gga->latitude,
      .longitude = gga->longitude
    };

    // altitude
    telemetry->altitude = (LiveBroadcastGpsAltitude) {
      .age = time,
      .altitude = gga->altitude,
      .altitude_units = gga->altitude_units,
      .height = gga->height,
      .height_units = gga->height_units
    };
  }

  // quality
  telemetry->quality = (LiveBroadcastGpsQuality) {
    .age = time,
    .fix_quality = gga->fix_quality,
    .satellites_tracked = gga->satellites_tracked,
    .hdop = gga->hdop,
    .dgps_age = gga->dgps_age
  };
}

static void process_gps_rmc(const SensorTime time, struct minmea_sentence_rmc *rmc, LiveBroadcastGps *telemetry) {
  if (!rmc->valid) {
    return;
  }

  // time
  telemetry->time = (LiveBroadcastGpsTime) {
    .age = time,
    .date = rmc->date,
    .time = rmc->time,
    .hour_offset = telemetry->time.hour_offset, // use previous
    .minute_offset = telemetry->time.minute_offset
  };

  // position
  telemetry->position = (LiveBroadcastGpsPosition) {
    .age = time,
    .latitude = rmc->latitude,
    .longitude = rmc->longitude
  };

  // speed
  if (telemetry->speed.age != time) {
    telemetry->speed = (LiveBroadcastGpsSpeed) {
      .age = time,
      .speed_knots = rmc->speed,
      .speed_kph = { 0, 0 }
    };
  }

  // course
  if (telemetry->track.age != time) {
    telemetry->track = (LiveBroadcastGpsTrack) {
      .age = time,
      .true_track_degrees = rmc->course,
      .magnetic_track_degrees = { 0, 0 }
    };
  }
}

static void process_gps_vtg(const SensorTime time,
                            struct minmea_sentence_vtg *vtg,
                            LiveBroadcastGps *telemetry) {

  telemetry->speed = (LiveBroadcastGpsSpeed) {
    .age = time,
    .speed_knots = vtg->speed_knots,
    .speed_kph = vtg->speed_kph
  };

  telemetry->faa = (LiveBroadcastGpsFAA) {
    .age = time,
    .faa_mode = vtg->faa_mode
  };

  telemetry->track = (LiveBroadcastGpsTrack) {
    .true_track_degrees = vtg->true_track_degrees,
    .magnetic_track_degrees = vtg->magnetic_track_degrees
  };
}

static void process_gps_zda(const SensorTime time,
                            struct minmea_sentence_zda *zda,
                            LiveBroadcastGps *telemetry) {

  telemetry->time = (LiveBroadcastGpsTime) {
    .age = time,
    .date = zda->date,
    .time = zda->time,
    .hour_offset = zda->hour_offset,
    .minute_offset = zda->minute_offset
  };
}

static void process_gyro_message(const Measurement *measurement, LiveBroadcastTelemetry *telemetry) {

  GyroMessage *message = (GyroMessage *) measurement->data;
  telemetry->gyro.age = measurement->time;
  telemetry->gyro.quaternion = message->quaternion;
  telemetry->gyro.gyro = message->gyro;
  telemetry->gyro.accel = message->accel;
  telemetry->gyro.heading = message->heading;
}

static void process_airspeed_message(const Measurement *measurement, LiveBroadcastTelemetry *telemetry) {
  AirspeedMessage *message = (AirspeedMessage *) measurement->data;
  telemetry->airspeed.age = measurement->time;
  telemetry->airspeed.speed = message->adc_reading;
}

static void process_pressure_message(const Measurement *measurement, LiveBroadcastTelemetry *telemetry) {
  PressureMessage *message = (PressureMessage *) measurement->data;
  telemetry->pressure.age = measurement->time;
  telemetry->pressure.pressure = message->pressure;
  telemetry->pressure.temperature = message->temperature;
  telemetry->pressure.humidity = message->humidity;
}

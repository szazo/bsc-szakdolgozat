#ifndef MESSAGE_QUEUE_H
#define MESSAGE_QUEUE_H

#include "error.h"
#include "stoppable_task.h"
#include "message_queue_api.h"

AV_ERROR_DEFINE(MESSAGE_QUEUE_CREATE_OUT_OF_MEMORY, 0x0D11)
AV_ERROR_DEFINE(MESSAGE_QUEUE_CREATE_EVENT_QUEUE_ERROR, 0x0D12)
AV_ERROR_DEFINE(MESSAGE_QUEUE_DELETE_EVENT_QUEUE_ERROR, 0x0D13)
AV_ERROR_DEFINE(MESSAGE_QUEUE_QUEUE_RUN_ERROR, 0x0D14)

typedef struct {
  uint8_t task_priority;
  TaskCPU task_core_id;
} MessageQueueConfig;

typedef struct {
  esp_event_loop_handle_t event_loop;
  MessageQueueConfig config;
  Task task;
} MessageQueue;

ErrorResult message_queue_create(MessageQueueConfig config, MessageQueueHandle *result);
ErrorResult message_queue_delete(MessageQueueHandle handle);

ErrorResult message_queue_start(MessageQueueHandle handle);
void message_queue_stop_and_wait(MessageQueueHandle handle);

#endif

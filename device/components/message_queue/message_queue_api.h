#ifndef MESSAGE_QUEUE_API_H
#define MESSAGE_QUEUE_API_H

#include "error.h"

#include "esp_event.h"
#include "esp_event_loop.h"

AV_ERROR_DEFINE(MESSAGE_QUEUE_REGISTER_ERROR, 0x0D01)
AV_ERROR_DEFINE(MESSAGE_QUEUE_UNREGISTER_ERROR, 0x0D02)
AV_ERROR_DEFINE(MESSAGE_QUEUE_POST_ERROR, 0x0D03)
AV_ERROR_DEFINE(MESSAGE_QUEUE_POST_TIMEOUT_ERROR, 0x0D04)

#define MESSAGE_DECLARE_BASE(BASE) ESP_EVENT_DECLARE_BASE(BASE)
#define MESSAGE_DEFINE_BASE(BASE) ESP_EVENT_DEFINE_BASE(BASE)

typedef void* MessageQueueHandle;

typedef esp_event_handler_t loop_event_handler;
typedef esp_event_base_t MessageEventBase;
typedef int32_t MessageEvent;

typedef void (*MessageEventHandler)(void* event_handler_arg, 
                                 MessageEventBase event_base, 
                                 MessageEvent event_id, 
                                 void* event_data);

ErrorResult message_queue_register(MessageQueueHandle handle,
                                      MessageEventBase event_base,
                                      MessageEvent event_id,
                                      MessageEventHandler event_handler,
                                      void *event_handler_arg);

ErrorResult message_queue_register_multiple(MessageQueueHandle handle,
                                               MessageEventBase event_base,
                                               const MessageEvent events[],
                                               int count,
                                               MessageEventHandler event_handler,
                                               void *event_handler_arg);

ErrorResult message_queue_unregister(MessageQueueHandle handle,
                                        MessageEventBase event_base,
                                        MessageEvent event_id,
                                        MessageEventHandler event_handler);

ErrorResult message_queue_unregister_multiple(MessageQueueHandle handle,
                                                 MessageEventBase event_base,
                                                 const MessageEvent events[],
                                                 int count,
                                                 MessageEventHandler event_handler);

ErrorResult message_queue_post(MessageQueueHandle handle,
                                  MessageEventBase event_base,
                                  MessageEvent event_id);

ErrorResult message_queue_post_data(MessageQueueHandle handle,
                                  MessageEventBase event_base,
                                  MessageEvent event_id,
                                  void *event_data,
                                  size_t event_data_size);

#endif

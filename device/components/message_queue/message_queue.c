#include "esp_log.h"
#include "freertos/FreeRTOS.h"

#include "message_queue.h"

#define MESSAGE_QUEUE_QUEUE_SIZE 10
#define MESSAGE_QUEUE_TASK_STACK_SIZE (3 * 1024)

#define MESSAGE_QUEUE_POST_TIMEOUT_TICKS pdMS_TO_TICKS(10)

static const char *TAG = "message_queue";

static void loop_task(void* args, TaskHandle task);

ErrorResult message_queue_create(MessageQueueConfig config, MessageQueueHandle *handle) {

  MessageQueue *me = malloc(sizeof(MessageQueue));
  if (me == NULL) {
    return AV_ERROR(MESSAGE_QUEUE_CREATE_OUT_OF_MEMORY);
  }

  me->config = config;
  ErrorResult err = task_create(&me->task);
  if (err.code != AV_OK) {
    return err;
  }

  esp_event_loop_args_t loop_args = {
    .queue_size = MESSAGE_QUEUE_QUEUE_SIZE,
    .task_name = NULL
  };
  
  esp_err_t event_loop_err = esp_event_loop_create(&loop_args, &(me->event_loop));
  if (event_loop_err != ESP_OK) {
    task_delete(&me->task);
    free(me);

    return AV_ERROR_WITH_REASON(MESSAGE_QUEUE_CREATE_EVENT_QUEUE_ERROR, event_loop_err);
  }

  *handle = me;

  return AV_OK_RESULT;
}

ErrorResult message_queue_delete(MessageQueueHandle handle) {
  MessageQueue *me = handle;

  task_delete(&me->task);

  esp_err_t event_loop_err = esp_event_loop_delete(me->event_loop);
  if (event_loop_err != ESP_OK) {
    return AV_ERROR_WITH_REASON(MESSAGE_QUEUE_DELETE_EVENT_QUEUE_ERROR, event_loop_err);
  }

  return AV_OK_RESULT;
}

ErrorResult message_queue_start(MessageQueueHandle handle) {
  MessageQueue *me = handle;

  TaskConfig task_config = {
    .name = "message_queue",
    .priority = me->config.task_priority,
    .core_id = me->config.task_core_id,
    .stack_depth = MESSAGE_QUEUE_TASK_STACK_SIZE,
    .task_function = loop_task,
    .task_param = me
  };

  ErrorResult err = task_start(&me->task, &task_config);
  if (err.code != AV_OK) {
    return err;
  }
  
  return AV_OK_RESULT;
}

void message_queue_stop_and_wait(MessageQueueHandle handle) {
  MessageQueue *me = handle;

  task_stop_and_wait(&me->task);
}

ErrorResult message_queue_register_multiple(MessageQueueHandle handle,
                                               MessageEventBase event_base,
                                               const MessageEvent events[],
                                               int count,
                                               MessageEventHandler event_handler,
                                               void *event_handler_arg) {

  int i;
  ErrorResult err;
  for (i = 0; i < count; i++) {
    err = message_queue_register(handle, event_base, events[i], event_handler, event_handler_arg);

    if (err.code != AV_OK) {
      // unregister already registered events
      for (int j = 0; j < i; j++) {
        message_queue_unregister(handle, event_base, events[j], event_handler);
      }

      err = AV_ERROR_WITH_REASON(MESSAGE_QUEUE_REGISTER_ERROR, err.code);
      return err;
    }
  }

  return AV_OK_RESULT;
}

ErrorResult message_queue_register(MessageQueueHandle handle,
                                      esp_event_base_t event_base,
                                      MessageEvent event_id,
                                      MessageEventHandler event_handler,
                                      void *event_handler_arg) {
  MessageQueue *me = handle;
  esp_err_t err = esp_event_handler_register_with(me->event_loop,
                                                 event_base,
                                                 event_id,
                                                 event_handler,
                                                 event_handler_arg);
  if (err == ESP_OK) {
    return AV_OK_RESULT;
  }

  return AV_ERROR_WITH_REASON(MESSAGE_QUEUE_REGISTER_ERROR, err);
}

ErrorResult message_queue_unregister_multiple(MessageQueueHandle handle,
                                                 MessageEventBase event_base,
                                                 const MessageEvent events[],
                                                 int count,
                                                 MessageEventHandler event_handler) {

  int i;
  ErrorResult err;
  for (i = 0; i < 2; i++) {
    err = message_queue_unregister(handle, event_base, events[i], event_handler);
    if (err.code != AV_OK) {
      av_error_publish_result(SEVERITY_ERROR, err);
    }
  }

  if (err.code != AV_OK) {
    return AV_ERROR_WITH_REASON(MESSAGE_QUEUE_UNREGISTER_ERROR, err.code);
  }

  return AV_OK_RESULT;
}

ErrorResult message_queue_unregister(MessageQueueHandle handle,
                                        MessageEventBase event_base,
                                        MessageEvent event_id,
                                        MessageEventHandler event_handler) {

  MessageQueue *me = handle;
  esp_err_t err = esp_event_handler_unregister_with(me->event_loop,
                                                 event_base,
                                                 event_id,
                                                 event_handler);
  if (err == ESP_OK) {
    return AV_OK_RESULT;
  }

  return AV_ERROR_WITH_REASON(MESSAGE_QUEUE_UNREGISTER_ERROR, err);
}

ErrorResult message_queue_post(MessageQueueHandle handle,
                                    MessageEventBase event_base,
                                    MessageEvent event_id) {
  return message_queue_post_data(handle, event_base, event_id, NULL, 0);
}

ErrorResult message_queue_post_data(MessageQueueHandle handle,
                                    MessageEventBase event_base,
                                    MessageEvent event_id,
                                    void *event_data,
                                    size_t event_data_size) {
  MessageQueue *me = handle;

  esp_err_t err = esp_event_post_to(me->event_loop,
                                    event_base,
                                    event_id,
                                    event_data,
                                    event_data_size,
                                    MESSAGE_QUEUE_POST_TIMEOUT_TICKS);

  if (err == ESP_OK) {
    return AV_OK_RESULT;
  }

  if (err == ESP_ERR_TIMEOUT) {
    return AV_ERROR_WITH_REASON(MESSAGE_QUEUE_POST_TIMEOUT_ERROR, err);
  }

  return AV_ERROR_WITH_REASON(MESSAGE_QUEUE_POST_ERROR, err);
}

static void loop_task(void* args, TaskHandle task) {
    esp_err_t err;
    MessageQueue *loop = args;

    while(!task_is_stop_requested(task)) {
      err = esp_event_loop_run(loop->event_loop, pdMS_TO_TICKS(100));
      if (err != ESP_OK) {
        av_error_publish(SEVERITY_ERROR, MESSAGE_QUEUE_QUEUE_RUN_ERROR);
      }
    }
}

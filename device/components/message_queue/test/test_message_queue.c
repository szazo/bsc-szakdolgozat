#include "unity.h"

#include "message_queue.h"

#define TEST_TAG "[sensor_governor]"

#define LOOP_CONFIG (MessageQueueConfig) { .task_priority = CONFIG_MESSAGE_QUEUE_TASK_PRIORITY, .task_core_id = CONFIG_MESSAGE_QUEUE_TASK_CPU }

MESSAGE_DECLARE_BASE(TEST_LOOP_EVENTS);
MESSAGE_DEFINE_BASE(TEST_LOOP_EVENTS);

static void mock_event_handler1(void *arg, MessageEventBase event_base, MessageEvent event_id, void *event_data) {
  *((int *) arg) = *((int*) event_data);
}

static void mock_event_handler2(void *arg, MessageEventBase event_base, MessageEvent event_id, void *event_data) {
  *((int *) arg) = *((int*) event_data);
}

enum {
  TEST_EVENT
};

TEST_CASE("should_create_and_delete", TEST_TAG)
{
  // given
  MessageQueueHandle loop;

  // when
  ErrorResult create_result = message_queue_create(LOOP_CONFIG, &loop);

  // then
  TEST_ASSERT_EQUAL(AV_OK, create_result.code);

  ErrorResult delete_result = message_queue_delete(loop);
  TEST_ASSERT_EQUAL(AV_OK, delete_result.code);  
}

TEST_CASE("should_start_and_stop", TEST_TAG)
{
  // given
  MessageQueueHandle loop;
  message_queue_create(LOOP_CONFIG, &loop);

  // when
  ErrorResult start_result = message_queue_start(loop);

  // then
  TEST_ASSERT_EQUAL(AV_OK, start_result.code);
  message_queue_stop_and_wait(loop);
}

TEST_CASE("should_multicast_to_registered_handlers", TEST_TAG)
{
  // given
  MessageQueueHandle loop;
  message_queue_create(LOOP_CONFIG, &loop);
  message_queue_start(loop);

  int arg1;
  int arg2;
  message_queue_register(loop, TEST_LOOP_EVENTS, TEST_EVENT, mock_event_handler1, &arg1);
  message_queue_register(loop, TEST_LOOP_EVENTS, TEST_EVENT, mock_event_handler2, &arg2);

  // when
  int event_data = 42;
  message_queue_post_data(loop, TEST_LOOP_EVENTS, TEST_EVENT, &event_data, sizeof(event_data));

  // then
  vTaskDelay(pdMS_TO_TICKS(100));

  TEST_ASSERT_EQUAL(42, arg1);
  TEST_ASSERT_EQUAL(42, arg2);
}

#include <stdio.h> 
#include <dirent.h>
#include <string.h>
#include <stdbool.h>
#include <sys/file.h>

#include "pb_common.h"
#include "pb_encode.h"
#include "pb_decode.h"
#include "sync_message.pb.h"
#include "sync_message_serializer.h"
#include "sync.h"

static void sync_task(void *arg, TaskHandle task);
static bool checkpoint_callback(void *arg, DirectorySyncFSMcheckpoint checkpoint, char *filename);
static ErrorResult measurement_callback(void *arg, SendCallbackData data);
static ErrorResult publish_measurement(Sync *me, SendCallbackData data);
static void busy_wait_for_enabled_state(Sync *me, TaskHandle task);

static bool serialize_measurement(uint8_t *buffer, int length);
static bool serialize_measurement_content(pb_ostream_t *stream, const pb_field_t *field, void * const *arg);

const static int SYNC_ENABLED_BIT = BIT0;

ErrorResult sync_create(MqttClientHandle mqtt_client, SyncConfig config, SyncHandle *sync_handle) {

  Sync *me = malloc(sizeof(Sync));
  me->config = config;
  me->mqtt_client = mqtt_client;
  me->sync_enabled_event_group = xEventGroupCreate();
  task_create(&me->sync_task);

  *sync_handle = me;

  directory_sync_fsm_create(&me->directory_sync_fsm,
                            config.path,
                            config.extension,
                            (DirectorySyncFSMCallbacks) {
                              .checkpoint_callback = checkpoint_callback,
                                .send_callback = measurement_callback,
                                .arg = me });

  DirectorySyncAPI directory_api = (DirectorySyncAPI) {
    .init = directory_sync_fsm_init,
    .proceed = directory_sync_fsm_proceed,
    .restart = directory_sync_fsm_restart,
    .stop = directory_sync_fsm_stop,
    .is_finished = directory_sync_fsm_is_finished,
    .last_error = directory_sync_fsm_last_error,
    .instance = &me->directory_sync_fsm
  };
  sync_fsm_create(&me->sync_fsm, CONFIG_SYNC_RESTART_MILLISECONDS, directory_api);

  return AV_OK_RESULT;
}

void sync_delete(SyncHandle sync_handle) {
  Sync *me = sync_handle;
  vEventGroupDelete(me->sync_enabled_event_group);
  task_delete(&me->sync_task);
  free(me);
}

static ErrorResult measurement_callback(void *arg, SendCallbackData data) {
  return publish_measurement(arg, data);
}

static ErrorResult publish_measurement(Sync *me, SendCallbackData data) {

  SyncMessageInput input = (SyncMessageInput) {
    .client_id = CONFIG_SYNC_CLIENT_ID,
    .filename = data.filename,
    .start_cursor = data.start_cursor,
    .end_cursor = data.end_cursor,
    .eof = data.eof,
    .buffer = data.buffer,
    .length = data.length
  };
  OutputBuffer output = (OutputBuffer) { .buffer = me->output_buffer,
                                         .size = CONFIG_SYNC_BUFFER_LENGTH,
                                         .bytes_written = 0 };
  bool encode_success = sync_message_serialize_measurement(input, &output);
  if (!encode_success) {
    return AV_ERROR(SYNC_SYNC_MESSAGE_SERIALIZE_ERROR);
  }

  ErrorResult publish_result = mqtt_client_publish(me->mqtt_client,
                                                   CONFIG_MQTT_SYNC_TOPIC,
                                                   (char *) output.buffer,
                                                   output.bytes_written,
                                                   QOS_AT_LEAST_ONCE,
                                                   true);
  if (publish_result.code != AV_OK) {
    return AV_ERROR_WITH_REASON(SYNC_SYNC_MESSAGE_SEND_ERROR, publish_result.code);
  }

  return AV_OK_RESULT;
}

static bool checkpoint_callback(void *arg, DirectorySyncFSMcheckpoint checkpoint, char *filename) {
  return true;
}

ErrorResult sync_start(SyncHandle sync_handle) {
  Sync *me = sync_handle;

  TaskConfig task_config = {
    .name = "sync_task",
    .priority = me->config.sync_task_priority,
    .core_id = me->config.sync_task_cpu,
    .stack_depth = 8192,
    .task_function = sync_task,
    .task_param = me
  };

  ErrorResult err = task_start(&me->sync_task, &task_config);
  if (err.code != AV_OK) {
    return AV_ERROR_WITH_REASON(SYNC_TASK_ERROR, err.code);
  }

  return AV_OK_RESULT;
}

void sync_stop_and_wait(SyncHandle sync_handle) {
  Sync *me = sync_handle;

  mqtt_client_close(me->mqtt_client);
  task_stop_and_wait(&me->sync_task);
}

// TODO: pause if there is no mqtt connection
void sync_pause(SyncHandle sync_handle) {
  Sync *me = sync_handle;

  xEventGroupClearBits(me->sync_enabled_event_group, SYNC_ENABLED_BIT);
  ErrorResult err = mqtt_client_close(me->mqtt_client);
  if (err.code != AV_OK) {
    av_error_publish_result(SEVERITY_WARNING, err);
  }
}

void sync_resume(SyncHandle sync_handle) {
  Sync *me = sync_handle;

  ErrorResult err = mqtt_client_open(me->mqtt_client);
  if (err.code != AV_OK) {
    av_error_publish_result(SEVERITY_WARNING, err);
  }

  xEventGroupSetBits(me->sync_enabled_event_group, SYNC_ENABLED_BIT);
}

static void sync_task(void *arg, TaskHandle task) {

  Sync *me = arg;

  sync_fsm_init(&me->sync_fsm);
  while (true) {
    busy_wait_for_enabled_state(me, task);
    if (task_is_stop_requested(task)) {
      sync_fsm_stop(&me->sync_fsm);
    }

    if (sync_fsm_phase(&me->sync_fsm) == SYNC_PHASE_STOPPED) {
      break;
    }

    sync_fsm_proceed(&me->sync_fsm);
  }
}

static void busy_wait_for_enabled_state(Sync *me, TaskHandle task) {

  do {
    if (task_is_stop_requested(task)) {
      return;
    }
  } while ((xEventGroupWaitBits(me->sync_enabled_event_group,
                                SYNC_ENABLED_BIT,
                                false,
                                true,
                                pdMS_TO_TICKS(500)) & SYNC_ENABLED_BIT) == 0);
}

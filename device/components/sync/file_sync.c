#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>
#include "pb_common.h"
#include "pb_decode.h"
#include "pb_encode.h"

#include "file_sync.h"
#include "proto_callback.h"
#include "sync_state.pb.h"
#include "sync_measurement.pb.h"
#include "sensor_file.pb.h"

typedef struct {
  int serial;
} SyncState;

// Used to store content in serialized form
typedef struct {
  uint8_t *buffer;
  int buffer_size;
  uint64_t size;
  ErrorResult error;
} ContentInfo;

#define ContentInfo_init(b, s) (ContentInfo) { .buffer = b, .buffer_size = s, .size = 0, .error = AV_OK_RESULT }

static ErrorResult seek_forward(FileSync *me, int count);
static bool is_file_locked(char *filepath);
static bool is_sync_ok_file_exists(FileSync *me);
static ErrorResult load_sync_state(FileSync *me, SyncState *result);
static ErrorResult save_sync_state(FileSync *me, SyncState *state);
static ErrorResult rename_sync_to_ok(FileSync *me);
static inline void create_sync_filepath(FileSync *me, char *result_filename);
static inline void create_sync_ok_filepath(FileSync *me, char *result_filename);
static bool deserialize(FileSync *me, ProtoMeasurement *measurement, ContentInfo *content);
static bool deserialize_content(pb_istream_t *stream, const pb_field_t *field, void **arg);
static bool serialize_sync(int serial,
                           pb_ostream_t *stream,
                           ProtoMeasurement *measurement,
                           ContentInfo *content);
static bool serialize_sync_content(pb_ostream_t *stream, const pb_field_t *field, void * const *arg);

void file_sync_create(FileSync *me) {
  me->phase = FILE_SYNC_NONE;
}

ErrorResult file_sync_open(FileSync *me, char *dir, char *filename, uint64_t *cursor) {
  
  if (me->phase != FILE_SYNC_NONE) {
    return AV_ERROR(FILE_SYNC_INVALID_PHASE_ERROR);
  }

  strcpy(me->dir, dir);
  strcpy(me->filename, filename);

  if (is_sync_ok_file_exists(me)) {
    return AV_ERROR(FILE_SYNC_ALREADY_FINISHED);
  }

  char filepath[MAX_PATH_LENGTH];
  sprintf(filepath, "%s/%s", me->dir, me->filename);

  if (is_file_locked(filepath)) {
    return AV_ERROR(FILE_SYNC_FILE_LOCKED);
  }

  SyncState state;
  ErrorResult err = load_sync_state(me, &state);
  if (err.code != AV_OK) {
    state.serial = 0;
    av_error_publish_result(SEVERITY_WARNING, err);
  }

  FILE *file = fopen(filepath, "rb");
  if (file == NULL) {
    return AV_ERROR(FILE_SYNC_FILE_NOT_FOUND);
  }
  flockfile(file);

  me->in_stream = proto_istream_from_file(file);

  if (state.serial > 0) {
    err = seek_forward(me, state.serial);
    if (err.code != AV_OK) {
      funlockfile(file);
      fclose(file);

      return err;
    }
  }

  me->current_cursor = state.serial;
  me->phase = FILE_SYNC_OPENED;

  *cursor = me->current_cursor;

  return AV_OK_RESULT;
}

ErrorResult file_sync_close(FileSync *me) {
  if (me->phase != FILE_SYNC_OPENED &&
      me->phase != FILE_SYNC_ERROR) {
    return AV_ERROR(FILE_SYNC_INVALID_PHASE_ERROR);
  }

  FILE *file = (FILE *) me->in_stream.state;
  funlockfile(file);
  fclose(file);

  me->phase = FILE_SYNC_CLOSED;

  return AV_OK_RESULT;
}

static ErrorResult seek_forward(FileSync *me, int count) {
  ContentInfo content = ContentInfo_init(me->content_buffer, CONFIG_SYNC_MAX_CONTENT_SIZE);
  ProtoMeasurement decoded_measurement = ProtoMeasurement_init_zero;

  while (count > 0) {
  
    bool decode_success = deserialize(me, &decoded_measurement, &content);

    if (!decode_success) {
      return AV_ERROR(FILE_SYNC_SEEK_ERROR);
    }

    count--;
  }

  return AV_OK_RESULT;
}

ErrorResult file_sync_serialize(FileSync *me,
                                int cursor, int count,
                                TargetBuffer *buffer, SerializeResult *result) {

  if (me->phase != FILE_SYNC_OPENED) {
    return AV_ERROR(FILE_SYNC_INVALID_PHASE_ERROR);
  }
  if (me->current_cursor != cursor) {
    return AV_ERROR(FILE_SYNC_INVALID_CURSOR_ERROR);
  }

  result->eof = false;
  result->cursor = me->current_cursor;

  pb_ostream_t out_stream = pb_ostream_from_buffer(buffer->buffer, buffer->size);

  ContentInfo content = ContentInfo_init(me->content_buffer, CONFIG_SYNC_MAX_CONTENT_SIZE);

  while (count > 0) {
  
    ProtoMeasurement decoded_measurement = ProtoMeasurement_init_zero;

    bool decode_success = deserialize(me, &decoded_measurement, &content);

    if (!decode_success) {

      if (content.error.code != AV_OK) {
        me->phase = FILE_SYNC_ERROR;
        return AV_ERROR_WITH_REASON(FILE_SYNC_FILE_ERROR, content.error.code);
      }
      
      if (me->in_stream.bytes_left == 0) {
        result->eof = true;
        return AV_OK_RESULT;
      }

      // check for file error
      if (ferror((FILE *)me->in_stream.state) == 0) {
        // there was no file error, it is non flushed file
        result->eof = true;
        return AV_OK_RESULT;
      }

      me->phase = FILE_SYNC_ERROR;
      return AV_ERROR(FILE_SYNC_FILE_ERROR);
    }

    me->current_cursor++;
    result->cursor = me->current_cursor;

    int serial = me->current_cursor;
    bool encode_success = serialize_sync(serial, &out_stream, &decoded_measurement, &content);
    if (!encode_success) {
      return AV_ERROR(FILE_SYNC_ENCODE_ERROR);
    }
    buffer->bytes_written = out_stream.bytes_written;

    if (me->in_stream.bytes_left == 0) {
      result->eof = true;
      // EOF
      break;
    }

    count--;
  }

  return AV_OK_RESULT;
}

ErrorResult file_sync_commit(FileSync *me, SerializeResult serialize_result) {

  if (me->phase != FILE_SYNC_OPENED) {
    return AV_ERROR(FILE_SYNC_INVALID_PHASE_ERROR);
  }

  SyncState sync_state = { .serial = serialize_result.cursor };
  ErrorResult err = save_sync_state(me, &sync_state);
  if (err.code != AV_OK) {
    return err;
  }

  if (serialize_result.eof) {
    err = rename_sync_to_ok(me);
    if (err.code != AV_OK) {
      return err;
    }
  }

  return AV_OK_RESULT;
}

static bool serialize_sync(int serial,
                           pb_ostream_t *stream,
                           ProtoMeasurement *measurement,
                           ContentInfo *content) {

  ProtoSyncMeasurement sync_measurement = ProtoSyncMeasurement_init_default;
  sync_measurement.serial = serial;
  sync_measurement.time = measurement->time;
  sync_measurement.sensor_id = measurement->sensor_id;
  sync_measurement.message_type = measurement->message_type;
  sync_measurement.content.funcs.encode = serialize_sync_content;
  sync_measurement.content.arg = content;

  return pb_encode_delimited(stream, ProtoSyncMeasurement_fields, &sync_measurement);
}

static bool serialize_sync_content(pb_ostream_t *stream, const pb_field_t *field, void * const *arg) {

  if (!pb_encode_tag_for_field(stream, field)) {
    return false;
  }

  ContentInfo *info = *arg;
  return pb_encode_string(stream, info->buffer, info->size);
}

static bool deserialize(FileSync *me, ProtoMeasurement *measurement, ContentInfo *content) {
  content->size = 0;

  measurement->content.funcs.decode = deserialize_content;
  measurement->content.arg = content;

  return pb_decode_delimited(&me->in_stream,
                             ProtoMeasurement_fields,
                             measurement);
}

static bool deserialize_content(pb_istream_t *stream, const pb_field_t *field, void **arg) {

  ContentInfo *info = *arg;
  if (stream->bytes_left > info->buffer_size) {
    info->error = AV_ERROR(FILE_SYNC_SMALL_CONTENT_BUFFER_ERROR);
    return false;
  }

  info->size = stream->bytes_left;
  bool success = pb_read(stream, info->buffer, stream->bytes_left);
  return success;
}

static ErrorResult save_sync_state(FileSync *me, SyncState *state) {
  char sync_filepath[MAX_PATH_LENGTH];
  create_sync_filepath(me, sync_filepath);

  FILE *file = fopen(sync_filepath, "wb");
  if (file == NULL) {
    return AV_ERROR(FILE_SYNC_STATE_FILE_ERROR);
  }

  ProtoSyncState proto_state = ProtoSyncState_init_default;
  proto_state.serial = state->serial;

  pb_ostream_t out_stream = {
    .callback = &proto_write_file_callback,
    .state = file,
    .max_size = SIZE_MAX,
    .bytes_written = 0,
    .errmsg = 0
  };
  bool encode_success = pb_encode_delimited(&out_stream, ProtoSyncState_fields, &proto_state);

  fclose(file);
  
  if (!encode_success) {
    return AV_ERROR(FILE_SYNC_STATE_ENCODE_ERROR);
  }

  return AV_OK_RESULT;
}

static ErrorResult load_sync_state(FileSync *me, SyncState *result) {

  char sync_filepath[MAX_PATH_LENGTH];
  create_sync_filepath(me, sync_filepath);

  FILE *file = fopen(sync_filepath, "rb");
  if (file == NULL) {
    result->serial = 0;
    return AV_OK_RESULT;
  }

  pb_istream_t in_stream = proto_istream_from_file(file);
  ProtoSyncState decoded_state = ProtoSyncState_init_zero;
  bool decode_success = pb_decode_delimited(&in_stream,
                                            ProtoSyncState_fields,
                                            &decoded_state);
  fclose(file);

  if (!decode_success) {
    return AV_ERROR(FILE_SYNC_STATE_DECODE_ERROR);
  }

  result->serial = decoded_state.serial;
  return AV_OK_RESULT;
}

static ErrorResult rename_sync_to_ok(FileSync *me) {

  char ok_filepath[MAX_PATH_LENGTH];
  create_sync_ok_filepath(me, ok_filepath);

  if (is_sync_ok_file_exists(me)) {
    remove(ok_filepath);
  }

  char sync_filepath[MAX_PATH_LENGTH];
  create_sync_filepath(me, sync_filepath);

  int result = rename(sync_filepath, ok_filepath);
  if (result != 0) {
    return AV_ERROR_WITH_REASON(FILE_SYNC_STATE_FILE_RENAME_ERROR, result);
  }

  return AV_OK_RESULT;
}

static inline void create_sync_filepath(FileSync *me, char *result_filename) {
  sprintf(result_filename, "%s/%s.sync", me->dir, me->filename);
}

static bool is_sync_ok_file_exists(FileSync *me) {
  char ok_file[MAX_PATH_LENGTH];
  create_sync_ok_filepath(me, ok_file);

  struct stat st;
  return stat(ok_file, &st) == 0;
}

static inline void create_sync_ok_filepath(FileSync *me, char *result_filename) {
  sprintf(result_filename, "%s/%s.ok", me->dir, me->filename);
}

static bool is_file_locked(char *filepath) {
  FILE *f = fopen(filepath, "rb");
  int lock_result = ftrylockfile(f);
  fclose(f);

  return lock_result != 0;
}


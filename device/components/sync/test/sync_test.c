#include "unity.h"
#include "sd_storage.h"
#include "mock_measurement_file.h"
#include "sync.h"
#include "tcpip_adapter.h"
#include "wifi.h"
#include "message_queue.h"

#define TEST_TAG "[sync]"

static MqttClientHandle mqtt_client;
static SyncHandle sync_handle;

MessageQueueHandle app_loop;
WifiHandle wifi;

static void wifi_setup() {
  tcpip_adapter_init();

  message_queue_create((MessageQueueConfig) { .task_priority = 8, .task_core_id = APP_CPU_NUM },
                          &app_loop);
  message_queue_start(app_loop);

  ErrorResult err = wifi_create(app_loop, &wifi);
  TEST_ASSERT_EQUAL(AV_OK, err.code);

  WifiStationSettings settings = {
    .ssid = CONFIG_WIFI_SSID,
    .password = CONFIG_WIFI_PASSWORD
  };
  err = wifi_init_station(&settings);
  TEST_ASSERT_EQUAL(AV_OK, err.code);

  err = wifi_start(wifi);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
}

static void wifi_teardown() {
  ErrorResult err = wifi_stop(wifi);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
  err = wifi_deinit(wifi);
  TEST_ASSERT_EQUAL(AV_OK, err.code);

  wifi_delete(wifi);
}

static void setup() {

  wifi_setup();
  
  ErrorResult err = mqtt_client_create((MqttClientSettings) { .uri = CONFIG_MQTT_SERVER_URL }, &mqtt_client);
  TEST_ASSERT_EQUAL(AV_OK, err.code);

  TEST_ASSERT_EQUAL(AV_OK, err.code);

  SyncConfig config = {
    .sync_task_priority = CONFIG_SYNC_TASK_PRIORITY,
    .sync_task_cpu = CONFIG_SYNC_TASK_CPU,
    .path = CONFIG_SD_MOUNT_POINT,
    .extension = ".sts"
  };
  err = sync_create(mqtt_client, config, &sync_handle);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
}

static void teardown() {
  sync_delete(sync_handle);
  mqtt_client_delete(mqtt_client);
  wifi_teardown();
}

TEST_CASE("sync_integration_test", TEST_TAG)
{
  // given
  setup();

  create_measurement_file("file1.sts", CONFIG_SYNC_BUCKET_LENGTH * 10);
  create_measurement_file("file2.sts", CONFIG_SYNC_BUCKET_LENGTH * 10);

  // when
  printf("syncing...\n");
  sync_start(sync_handle);
    
  vTaskDelay(pdMS_TO_TICKS(4000));
  
  // teardown
  sync_stop_and_wait(sync_handle);

  remove_measurement_file("file1.sts");
  remove_measurement_file("file2.sts");

  teardown();
}

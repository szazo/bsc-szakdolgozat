#include "unity.h"

#include "sd_storage.h"
#include "directory_walker.h"

#define TEST_TAG "[directory_walker]"

static void create_file(char *filename);
static void remove_file(char *filename);
static void find_next_and_assert(DirectoryWalker *walker, char *expected);

static void setup() {
}

static void teardown() {
}

TEST_CASE("should_find_all", TEST_TAG)
{
  // given
  setup();
  create_file("file1.xyz");
  create_file("file_other1.aaa");
  create_file("file2.xyz");
  create_file("file_other2.bbb");
  create_file("file3.xyz");
  create_file("file_other3.ccc");

  DirectoryWalker walker;
  ErrorResult err = directory_walker_open(&walker, CONFIG_SD_MOUNT_POINT, ".xyz");
  TEST_ASSERT_EQUAL(AV_OK, err.code);

  // when, then
  find_next_and_assert(&walker, "file1.xyz");
  find_next_and_assert(&walker, "file2.xyz");
  find_next_and_assert(&walker, "file3.xyz");

  char result_filename[CONFIG_FATFS_MAX_LFN];
  bool succeeded = director_walker_find_next_file(&walker, result_filename, CONFIG_FATFS_MAX_LFN);
  TEST_ASSERT_FALSE(succeeded);

  // teardown
  directory_walker_close(&walker);

  remove_file("file1.xyz");
  remove_file("file_other1.aaa");
  remove_file("file2.xyz");
  remove_file("file_other2.bbb");
  remove_file("file3.xyz");
  remove_file("file_other3.ccc");
  
  teardown();
}

static void find_next_and_assert(DirectoryWalker *walker, char *expected) {
  char result_filename[CONFIG_FATFS_MAX_LFN];
  bool find_succeeded = director_walker_find_next_file(walker, result_filename, CONFIG_FATFS_MAX_LFN);

  TEST_ASSERT_TRUE(find_succeeded);
  TEST_ASSERT_EQUAL_STRING(expected, result_filename);
}

static void create_file(char *filename) {
  char filepath[CONFIG_FATFS_MAX_LFN];
  sprintf(filepath, "%s/%s", CONFIG_SD_MOUNT_POINT, filename);
  FILE *f = fopen(filepath, "w");
  fclose(f);
}

static void remove_file(char *filename) {
  char filepath[CONFIG_FATFS_MAX_LFN];
  sprintf(filepath, "%s/%s", CONFIG_SD_MOUNT_POINT, filename);
  remove(filepath);
}

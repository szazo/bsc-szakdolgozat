#include "unity.h"
#include "sd_storage.h"
#include "mock_measurement_file.h"
#include "directory_sync_fsm.h"

#define TEST_TAG "[directory_sync_fsm]"

static DirectorySyncFSM sync_fsm;
static int send_callback_count;
static bool send_callback_result;
static DirectorySyncFSMcheckpoint last_checkpoint_callback_type;
static char last_checkpoint_callback_filename[CONFIG_FATFS_MAX_LFN];
static bool checkpoint_callback_result;
static SendCallbackData last_send_data;

static void proceed_and_assert(DirectorySyncFSM *sync_fsm, DirectorySyncFSMPhase expected_phase);

static void setup() {

  send_callback_count = 0;
  send_callback_result = true;
  last_checkpoint_callback_type = -1;
  last_checkpoint_callback_filename[0] = 0x0;
  checkpoint_callback_result = true;
}

static int dummy = 42;
static void *arg = &dummy;

static void teardown() {
}

static bool mock_checkpoint_callback(void *arg, DirectorySyncFSMcheckpoint checkpoint, char *filename) {
  TEST_ASSERT_EQUAL(42, *(int *)(arg));
  last_checkpoint_callback_type = checkpoint;
  strcpy(last_checkpoint_callback_filename, filename);
  return checkpoint_callback_result;
}

static ErrorResult mock_send_callback(void *arg, SendCallbackData data) {
  TEST_ASSERT_EQUAL(42, *(int *)(arg));
  last_send_data = data;
  send_callback_count++;
  return send_callback_result ? AV_OK_RESULT : AV_ERROR(0x42);
}

#define CALLBACKS (DirectorySyncFSMCallbacks) { .send_callback = mock_send_callback, .checkpoint_callback = mock_checkpoint_callback, arg = arg }

TEST_CASE("should_finish_when_directory_open_failed", TEST_TAG) {
  // setup
  setup();

  // given
  directory_sync_fsm_create(&sync_fsm, "/invalid_path", ".tst", CALLBACKS);

  // when
  directory_sync_fsm_proceed(&sync_fsm);

  // then
  TEST_ASSERT_EQUAL(SYNC_FSM_PHASE_FINISHED, directory_sync_fsm_phase(&sync_fsm));
  TEST_ASSERT_EQUAL(DIRECTORY_WALKER_DIRECTORY_OPEN_ERROR, directory_sync_fsm_last_error(&sync_fsm).code);
}

static void assert_callback_data(char *filename, int start_cursor, int end_cursor, bool eof) {
  TEST_ASSERT_EQUAL_STRING(filename, last_send_data.filename);
  TEST_ASSERT_EQUAL(start_cursor, last_send_data.start_cursor);
  TEST_ASSERT_EQUAL(end_cursor, last_send_data.end_cursor);
  TEST_ASSERT_EQUAL(eof, last_send_data.eof);
}

TEST_CASE("should_continue_when_not_finished_in_same_cycle", TEST_TAG) {
  // setup
  setup();

  // given
  create_measurement_file("file1.tst", CONFIG_SYNC_BUCKET_LENGTH * 2);
  directory_sync_fsm_create(&sync_fsm, CONFIG_SD_MOUNT_POINT, ".tst", CALLBACKS);

  proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_NEXT_FILE);
  proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_START_FILE);
  proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_PROCESS_FILE);

  // when, then
  proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_PROCESS_FILE);
  TEST_ASSERT_EQUAL(1, send_callback_count);
  assert_callback_data("file1.tst", 0, CONFIG_SYNC_BUCKET_LENGTH, false);

  // when, then
  proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_PROCESS_FILE);
  TEST_ASSERT_EQUAL(2, send_callback_count);
  assert_callback_data("file1.tst", CONFIG_SYNC_BUCKET_LENGTH, CONFIG_SYNC_BUCKET_LENGTH * 2, false);

  // when, then
  proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_NEXT_FILE);
  assert_callback_data("file1.tst", CONFIG_SYNC_BUCKET_LENGTH * 2, CONFIG_SYNC_BUCKET_LENGTH * 2, true);
  TEST_ASSERT_EQUAL(3, send_callback_count);

  // teardown
  remove_measurement_file("file1.tst");

  teardown();
}

TEST_CASE("should_continue_when_not_finished_in_separate_cycle", TEST_TAG) {
  // setup
  setup();

  // given, restart once
  create_measurement_file("file1.tst", CONFIG_SYNC_BUCKET_LENGTH * 2 + (CONFIG_SYNC_BUCKET_LENGTH / 2));
  for (int i = 0; i < 2; i++) {
    directory_sync_fsm_create(&sync_fsm, CONFIG_SD_MOUNT_POINT, ".tst", CALLBACKS);

    proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_NEXT_FILE);
    proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_START_FILE);
    proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_PROCESS_FILE);
    proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_PROCESS_FILE);

    TEST_ASSERT_EQUAL(i + 1, send_callback_count);
    assert_callback_data("file1.tst", i * CONFIG_SYNC_BUCKET_LENGTH, i * CONFIG_SYNC_BUCKET_LENGTH + CONFIG_SYNC_BUCKET_LENGTH, false);
    printf("OK\n");
  }

  send_callback_count = 0;

  printf("Continue\n");

  // when, then
  proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_NEXT_FILE);
  TEST_ASSERT_EQUAL(1, send_callback_count);
  assert_callback_data("file1.tst", CONFIG_SYNC_BUCKET_LENGTH * 2, CONFIG_SYNC_BUCKET_LENGTH * 2 + (CONFIG_SYNC_BUCKET_LENGTH / 2), true);

  // teardown
  remove_measurement_file("file1.tst");
  teardown();
}

TEST_CASE("should_stop_when_proceeding", TEST_TAG) {
  // setup
  setup();

  // given
  create_measurement_file("file1.tst", CONFIG_SYNC_BUCKET_LENGTH * 2);
  directory_sync_fsm_create(&sync_fsm, CONFIG_SD_MOUNT_POINT, ".tst", CALLBACKS);

  proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_NEXT_FILE);
  proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_START_FILE);
  proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_PROCESS_FILE);

  // when
  directory_sync_fsm_stop(&sync_fsm);

  // then
  TEST_ASSERT_EQUAL(SYNC_FSM_PHASE_FINISHED, directory_sync_fsm_phase(&sync_fsm));

  // teardown
  remove_measurement_file("file1.tst");

  teardown();
}

TEST_CASE("should_next_file_when_send_error", TEST_TAG) {
  // setup
  setup();

  // given
  create_measurement_file("file1.tst", CONFIG_SYNC_BUCKET_LENGTH - 1);
  create_measurement_file("file2.tst", CONFIG_SYNC_BUCKET_LENGTH - 1);
  directory_sync_fsm_create(&sync_fsm, CONFIG_SD_MOUNT_POINT, ".tst", CALLBACKS);
  send_callback_result = false;

  proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_NEXT_FILE);
  proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_START_FILE);
  proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_PROCESS_FILE);

  // when, then
  proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_NEXT_FILE);
  TEST_ASSERT_EQUAL(1, send_callback_count);
  assert_callback_data("file1.tst", 0, CONFIG_SYNC_BUCKET_LENGTH - 1, true);

  // teardown
  remove_measurement_file("file1.tst");
  remove_measurement_file("file2.tst");

  teardown();
}

/* TEST_CASE("should_retry_when_checkpoint_start_error", TEST_TAG) { */
/*   // setup */
/*   setup(); */

/*   // given */
/*   create_measurement_file("file1.tst", CONFIG_SYNC_BUCKET_LENGTH - 1); */
/*   directory_sync_fsm_create(&sync_fsm, CONFIG_SD_MOUNT_POINT, ".tst", CALLBACKS); */
/*   checkpoint_callback_result = false; */

/*   proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_NEXT_FILE); */
/*   proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_START_FILE); */

/*   // when, then */
/*   proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_START_FILE); */
/*   TEST_ASSERT_EQUAL(SYNC_FSM_CHECKPOINT_START_FILE, last_checkpoint_callback_type); */
/*   TEST_ASSERT_EQUAL_STRING("file1.tst", last_checkpoint_callback_filename); */

/*   // teardown */
/*   remove_measurement_file("file1.tst"); */
  
/*   teardown(); */
/* } */

/* TEST_CASE("should_retry_when_checkpoint_end_error", TEST_TAG) { */
/*   // setup */
/*   setup(); */

/*   // given */
/*   create_measurement_file("file1.tst", CONFIG_SYNC_BUCKET_LENGTH - 1); */
/*   directory_sync_fsm_create(&sync_fsm, CONFIG_SD_MOUNT_POINT, ".tst", CALLBACKS); */

/*   proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_NEXT_FILE); */
/*   proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_START_FILE); */
/*   proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_PROCESS_FILE); */
/*   checkpoint_callback_result = false; */


/*   // when, then */
/*   proceed_and_assert(&sync_fsm, SYNC_FSM_PHASE_PROCESS_FILE); */
/*   TEST_ASSERT_EQUAL(SYNC_FSM_CHECKPOINT_END_FILE, last_checkpoint_callback_type); */
/*   TEST_ASSERT_EQUAL_STRING("file1.tst", last_checkpoint_callback_filename); */

/*   // teardown */
/*   remove_measurement_file("file1.tst"); */

/*   teardown(); */
/* } */

TEST_CASE("should_sync_end_to_end", TEST_TAG)
{
  // setup
  setup();

  // given
  create_measurement_file("file1.tst", CONFIG_SYNC_BUCKET_LENGTH - 1);
  create_measurement_file("file2.tst", CONFIG_SYNC_BUCKET_LENGTH - 1);

  directory_sync_fsm_create(&sync_fsm, CONFIG_SD_MOUNT_POINT, ".tst", CALLBACKS);

  // when
  directory_sync_fsm_proceed(&sync_fsm);

  // then
  TEST_ASSERT_EQUAL(SYNC_FSM_PHASE_NEXT_FILE, directory_sync_fsm_phase(&sync_fsm));

  // when find next file
  directory_sync_fsm_proceed(&sync_fsm);

  // then start new file
  TEST_ASSERT_EQUAL(SYNC_FSM_PHASE_START_FILE, directory_sync_fsm_phase(&sync_fsm));
  TEST_ASSERT_EQUAL_STRING("file1.tst", directory_sync_fsm_current_file(&sync_fsm));

  // when proceed
  directory_sync_fsm_proceed(&sync_fsm);

  // then file is opened
  TEST_ASSERT_EQUAL(SYNC_FSM_PHASE_PROCESS_FILE, directory_sync_fsm_phase(&sync_fsm));
  TEST_ASSERT_EQUAL(0, send_callback_count);

  // when file is proceed
  directory_sync_fsm_proceed(&sync_fsm);

  // then file is read, then go to next file
  TEST_ASSERT_EQUAL(SYNC_FSM_PHASE_NEXT_FILE, directory_sync_fsm_phase(&sync_fsm));
  TEST_ASSERT_EQUAL(1, send_callback_count);
  assert_callback_data("file1.tst", 0, CONFIG_SYNC_BUCKET_LENGTH - 1, true);

  // when find next file
  directory_sync_fsm_proceed(&sync_fsm);

  // then start new file
  TEST_ASSERT_EQUAL(SYNC_FSM_PHASE_START_FILE, directory_sync_fsm_phase(&sync_fsm));
  TEST_ASSERT_EQUAL_STRING("file2.tst", directory_sync_fsm_current_file(&sync_fsm));

  // when proceed
  directory_sync_fsm_proceed(&sync_fsm);

  // then file is opened
  TEST_ASSERT_EQUAL(SYNC_FSM_PHASE_PROCESS_FILE, directory_sync_fsm_phase(&sync_fsm));
  TEST_ASSERT_EQUAL(1, send_callback_count);

  // when file is proceed
  directory_sync_fsm_proceed(&sync_fsm);

  // then file is read, then go to next file
  TEST_ASSERT_EQUAL(SYNC_FSM_PHASE_NEXT_FILE, directory_sync_fsm_phase(&sync_fsm));
  TEST_ASSERT_EQUAL(2, send_callback_count);
  assert_callback_data("file2.tst", 0, CONFIG_SYNC_BUCKET_LENGTH - 1, true);
  
  // when find next file
  directory_sync_fsm_proceed(&sync_fsm);

  // then finished
  TEST_ASSERT_EQUAL(SYNC_FSM_PHASE_FINISHED, directory_sync_fsm_phase(&sync_fsm));

  // teardown
  remove_measurement_file("file1.tst");
  remove_measurement_file("file2.tst");

  teardown();
}

static void proceed_and_assert(DirectorySyncFSM *sync_fsm, DirectorySyncFSMPhase expected_phase) {
  directory_sync_fsm_proceed(sync_fsm);
  TEST_ASSERT_EQUAL(expected_phase, directory_sync_fsm_phase(sync_fsm));
}

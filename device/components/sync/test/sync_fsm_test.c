#include "unity.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
/* #include "sd_storage.h" */
/* #include "mock_measurement_file.h" */

#include "sync_fsm.h"

#include "fff.h"
DEFINE_FFF_GLOBALS;
FAKE_VOID_FUNC(directory_sync_mock_init, DirectorySyncFSM *);
FAKE_VOID_FUNC(directory_sync_mock_proceed, DirectorySyncFSM *);
FAKE_VOID_FUNC(directory_sync_mock_restart, DirectorySyncFSM *);
FAKE_VALUE_FUNC(bool, directory_sync_mock_is_finished, DirectorySyncFSM *);
FAKE_VALUE_FUNC(ErrorResult, directory_sync_mock_last_error, DirectorySyncFSM *);
FAKE_VOID_FUNC(directory_sync_mock_stop, DirectorySyncFSM *);

/* List of fakes used by this unit tester */
#define FFF_FAKES_LIST(FAKE)                 \
  FAKE(directory_sync_mock_init)             \
  FAKE(directory_sync_mock_proceed)          \
  FAKE(directory_sync_mock_restart)          \
  FAKE(directory_sync_mock_stop)             \
  FAKE(directory_sync_mock_is_finished)      \
  FAKE(directory_sync_mock_last_error)       \

#define TEST_TAG "[sync_fsm]"

/* #include "mocks/mock_directory_sync_fsm.h" */

/* static MqttClientHandle mqtt_client; */
/* static SyncHandle sync_handle; */

static DirectorySyncFSM directory_sync;
static const DirectorySyncAPI api = {
    .init = directory_sync_mock_init,
    .proceed = directory_sync_mock_proceed,
    .restart = directory_sync_mock_restart,
    .stop = directory_sync_mock_stop,
    .is_finished = directory_sync_mock_is_finished,
    .last_error = directory_sync_mock_last_error,
    .instance = &directory_sync
  };
static SyncFSM fsm;

static void setup() {
  FFF_FAKES_LIST(RESET_FAKE);
}

TEST_CASE("directory_sync_init_should_be_called_when_create", TEST_TAG) {
  // setup
  setup();
  
  // when
  sync_fsm_create(&fsm, CONFIG_SYNC_RESTART_MILLISECONDS, api);

  // then
  TEST_ASSERT_EQUAL(1, directory_sync_mock_init_fake.call_count);
  TEST_ASSERT_EQUAL(SYNC_PHASE_SYNCING, sync_fsm_phase(&fsm));
}

TEST_CASE("syncing_proceed_should_continue_when_directory_not_finished", TEST_TAG)
{
  // setup
  setup();
  
  // given
  sync_fsm_create(&fsm, CONFIG_SYNC_RESTART_MILLISECONDS, api);
  directory_sync_mock_is_finished_fake.return_val = false;

  // when
  sync_fsm_proceed(&fsm);

  // then
  TEST_ASSERT_EQUAL(1, directory_sync_mock_proceed_fake.call_count);
  TEST_ASSERT_EQUAL(SYNC_PHASE_SYNCING, sync_fsm_phase(&fsm));
}

TEST_CASE("syncing_proceed_should_go_wait_for_restart_when_directory_finished", TEST_TAG)
{
  // setup
  setup();

  // given
  sync_fsm_create(&fsm, CONFIG_SYNC_RESTART_MILLISECONDS, api);
  directory_sync_mock_is_finished_fake.return_val = true;

  // when
  sync_fsm_proceed(&fsm);

  // then
  TEST_ASSERT_EQUAL(1, directory_sync_mock_proceed_fake.call_count);
  TEST_ASSERT_EQUAL(SYNC_PHASE_WAIT_FOR_RESTART, sync_fsm_phase(&fsm));
}

TEST_CASE("wait_for_restart_proceed_should_not_restart_when_time_not_passed", TEST_TAG)
{
  // setup
  setup();

  // given
  sync_fsm_create(&fsm, 5000, api);
  directory_sync_mock_is_finished_fake.return_val = true;
  sync_fsm_proceed(&fsm);

  // when
  sync_fsm_proceed(&fsm);

  // then
  TEST_ASSERT_EQUAL(0, directory_sync_mock_restart_fake.call_count);
  TEST_ASSERT_EQUAL(SYNC_PHASE_WAIT_FOR_RESTART, sync_fsm_phase(&fsm));
}

TEST_CASE("wait_for_restart_proceed_should_restart_when_time_passed", TEST_TAG)
{
  // setup
  setup();

  // given
  sync_fsm_create(&fsm, 10, api);
  directory_sync_mock_is_finished_fake.return_val = true;
  sync_fsm_proceed(&fsm);

  // when
  vTaskDelay(pdMS_TO_TICKS(20));
  sync_fsm_proceed(&fsm);

  // then
  TEST_ASSERT_EQUAL(1, directory_sync_mock_restart_fake.call_count);
  TEST_ASSERT_EQUAL(SYNC_PHASE_SYNCING, sync_fsm_phase(&fsm));
}

TEST_CASE("wait_for_restart_should_stop_when_stop_called", TEST_TAG)
{
  // setup
  setup();

  // given
  sync_fsm_create(&fsm, 5000, api);
  directory_sync_mock_is_finished_fake.return_val = true;
  sync_fsm_proceed(&fsm);

  // when
  sync_fsm_stop(&fsm);

  // then
  TEST_ASSERT_EQUAL(0, directory_sync_mock_restart_fake.call_count);
  TEST_ASSERT_EQUAL(SYNC_PHASE_STOPPED, sync_fsm_phase(&fsm));
}

TEST_CASE("syncing_should_stop_when_stop_called", TEST_TAG)
{
  // setup
  setup();

  // given
  sync_fsm_create(&fsm, CONFIG_SYNC_RESTART_MILLISECONDS, api);
  directory_sync_mock_is_finished_fake.return_val = false;
  sync_fsm_proceed(&fsm);

  // when
  sync_fsm_stop(&fsm);

  // then
  TEST_ASSERT_EQUAL(1, directory_sync_mock_proceed_fake.call_count);
  TEST_ASSERT_EQUAL(1, directory_sync_mock_stop_fake.call_count);
  TEST_ASSERT_EQUAL(SYNC_PHASE_STOPPED, sync_fsm_phase(&fsm));
}

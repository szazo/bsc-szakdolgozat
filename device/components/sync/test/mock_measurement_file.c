#include <sys/stat.h>
#include "unity.h"
#include "mock_measurement_file.h"
#include "pb_common.h"
#include "pb_encode.h"
#include "pb_decode.h"
#include "proto_callback.h"
#include "sync_state.pb.h"

void create_sync_ok_file(char *base_filename) {
  char filename[CONFIG_FATFS_MAX_LFN];
  sprintf(filename, "%s/%s.ok", CONFIG_SD_MOUNT_POINT, base_filename);
  FILE *f = fopen(filename, "w");
  fclose(f);
}

void remove_sync_ok_file(char *base_filename) {
  char filename[CONFIG_FATFS_MAX_LFN];
  sprintf(filename, "%s/%s.ok", CONFIG_SD_MOUNT_POINT, base_filename);
  remove(filename);
}

void remove_sync_file(char *base_filename) {
  char filename[CONFIG_FATFS_MAX_LFN];
  sprintf(filename, "%s/%s.sync", CONFIG_SD_MOUNT_POINT, base_filename);
  remove(filename);
}

void remove_measurement_file(char *filename) {
  char filepath[CONFIG_FATFS_MAX_LFN];
  sprintf(filepath, "%s/%s", CONFIG_SD_MOUNT_POINT, filename);
  remove(filepath);

  remove_sync_file(filename);
  remove_sync_ok_file(filename);
}

void create_measurement_file(char *filename, int count) {

  remove_measurement_file(filename);

  char filepath[CONFIG_FATFS_MAX_LFN];
  sprintf(filepath, "%s/%s", CONFIG_SD_MOUNT_POINT, filename);
  FILE *file = fopen(filepath, "wb");
  
  pb_ostream_t out_stream = {
    .callback = &proto_write_file_callback,
    .max_size = SIZE_MAX,
    .state = file,
    .errmsg = 0,
    .bytes_written = 0
  };

  for (int i = 0; i < count; i++) {
    int value1 = i + 1; double value2 = i + 1.1;
    Measurement *measurement = create_sample_measurement(i, value1, value2);
    ErrorResult result = serialize_sample_measurement(measurement, &out_stream);
    free(measurement);
    TEST_ASSERT_EQUAL(AV_OK, result.code);
  }
  fclose(file);
}

void assert_no_sync_state(char *filename) {
  char filepath[CONFIG_FATFS_MAX_LFN];
  sprintf(filepath, "%s/%s.sync", CONFIG_SD_MOUNT_POINT, filename);

  struct stat st;
  TEST_ASSERT_NOT_EQUAL(0, stat(filepath, &st));
}

void assert_sync_state(char *filename, int expected_serial, bool is_finished) {
  char filepath[CONFIG_FATFS_MAX_LFN];
  if (is_finished) {
    sprintf(filepath, "%s/%s.ok", CONFIG_SD_MOUNT_POINT, filename);
  } else {
    sprintf(filepath, "%s/%s.sync", CONFIG_SD_MOUNT_POINT, filename);
  }
  FILE *file = fopen(filepath, "rb");
  TEST_ASSERT_NOT_NULL(file);

  pb_istream_t stream = proto_istream_from_file(file);
  ProtoSyncState state = ProtoSyncState_init_zero;

  bool decode_success = pb_decode_delimited(&stream, ProtoSyncState_fields, &state);
  fclose(file);
  TEST_ASSERT_TRUE(decode_success);
  TEST_ASSERT_EQUAL(expected_serial, state.serial);
}

void assert_message(SampleMessage *message, int index) {
  TEST_ASSERT_EQUAL(index + 1, message->number1);
  TEST_ASSERT_EQUAL(index + 1.1, message->number2);
}

#include <stdio.h>
#include <string.h>

#include "unity.h"
#include "sd_storage.h"
#include "file_sync.h"
#include "mock_measurement_file.h"

#include "../../serializer/test/sample_message.h"

#define TEST_TAG "[file_synchronizer]"

static void assert_result_messages(SerializeResult *result, int start, int count);
static void decode_result(pb_istream_t *stream, SampleMessage *content_message);
static void open_and_assert(FileSync *sync, char *filename);
static void close(FileSync *sync);
static void commit(FileSync *sync, SerializeResult result);

#define BUFFER_LENGTH 200
static uint8_t buffer[BUFFER_LENGTH];
static TargetBuffer target_buffer = { buffer, BUFFER_LENGTH, 0 };
static FileSync sync;

static void setup() {
  file_sync_create(&sync);
}

static void teardown() {
}

TEST_CASE("should_start_from_beginning_when_not_yet_synchronized", TEST_TAG) {
  // given
  setup();

  const int MEASUREMENT_COUNT = 10;
  create_measurement_file("file1.avd", MEASUREMENT_COUNT);

  // when
  open_and_assert(&sync, "file1.avd");

  SerializeResult result;
  const int MEASUREMENT_TO_SYNC = 3;
  ErrorResult err = file_sync_serialize(&sync, 0, MEASUREMENT_TO_SYNC, &target_buffer, &result);

  // then
  TEST_ASSERT_EQUAL(AV_OK, err.code);
  TEST_ASSERT_FALSE(result.eof);

  assert_result_messages(&result, 0, MEASUREMENT_TO_SYNC);
  commit(&sync, result);
  assert_sync_state("file1.avd", MEASUREMENT_TO_SYNC, false);
  close(&sync);

  remove_measurement_file("file1.avd");
  teardown();
}

TEST_CASE("should_continue_when_part_already_synchronized", TEST_TAG) {
  // given
  setup();

  const int MEASUREMENT_COUNT = 10;
  create_measurement_file("file1.avd", MEASUREMENT_COUNT);
  open_and_assert(&sync, "file1.avd");
  SerializeResult result;
  const int MEASUREMENT_TO_SYNC = 3;
  ErrorResult err = file_sync_serialize(&sync, 0, MEASUREMENT_TO_SYNC, &target_buffer, &result);
  commit(&sync, result);
  close(&sync);

  // when
  file_sync_create(&sync);
  open_and_assert(&sync, "file1.avd");
  const int NEXT_MEASUREMENT_TO_SYNC = 2;
  err = file_sync_serialize(&sync, MEASUREMENT_TO_SYNC, NEXT_MEASUREMENT_TO_SYNC, &target_buffer, &result);

  // then
  TEST_ASSERT_EQUAL(AV_OK, err.code);
  TEST_ASSERT_FALSE(result.eof);

  assert_result_messages(&result, 3, NEXT_MEASUREMENT_TO_SYNC);
  commit(&sync, result);
  assert_sync_state("file1.avd", MEASUREMENT_TO_SYNC + NEXT_MEASUREMENT_TO_SYNC, false);
  close(&sync);

  remove_measurement_file("file1.avd");

  teardown();
}

TEST_CASE("should_stop_gracefully_when_eof", TEST_TAG) {
  // given
  setup();

  const int MEASUREMENT_COUNT = 2;
  create_measurement_file("file1.avd", MEASUREMENT_COUNT);

  // when
  open_and_assert(&sync, "file1.avd");

  SerializeResult result;
  const int MEASUREMENT_TO_SYNC = 3;
  ErrorResult err = file_sync_serialize(&sync, 0, MEASUREMENT_TO_SYNC, &target_buffer, &result);

  // then
  TEST_ASSERT_EQUAL(AV_OK, err.code);
  TEST_ASSERT_TRUE(result.eof);

  assert_result_messages(&result, 0, MEASUREMENT_COUNT);
  commit(&sync, result);
  assert_sync_state("file1.avd", MEASUREMENT_COUNT, true);
  close(&sync);

  remove_measurement_file("file1.avd");

  teardown();
}

TEST_CASE("should_stop_with_eof_when_decode_error", TEST_TAG) {
  // given
  setup();

  const int MEASUREMENT_COUNT = 2;
  create_measurement_file("file1.avd", MEASUREMENT_COUNT);

  // append some invalid byte
  char filepath[MAX_PATH_LENGTH];
  sprintf(filepath, "%s/%s", CONFIG_SD_MOUNT_POINT, "file1.avd");
  FILE *file = fopen(filepath, "a");
  fputc(42, file);
  fputc(43, file);
  fputc(44, file);
  fclose(file);

  // when
  open_and_assert(&sync, "file1.avd");

  SerializeResult result;
  const int MEASUREMENT_TO_SYNC = 3;
  ErrorResult err = file_sync_serialize(&sync, 0, MEASUREMENT_TO_SYNC, &target_buffer, &result);

  // then
  TEST_ASSERT_EQUAL(AV_OK, err.code);
  TEST_ASSERT_TRUE(result.eof);

  assert_result_messages(&result, 0, MEASUREMENT_COUNT);
  commit(&sync, result);
  assert_sync_state("file1.avd", MEASUREMENT_COUNT, true);
  close(&sync);

  remove_measurement_file("file1.avd");

  teardown();
}

TEST_CASE("should_stop_with_error_when_file_error", TEST_TAG) {
  // given
  setup();

  const int MEASUREMENT_COUNT = 10;
  create_measurement_file("file1.avd", MEASUREMENT_COUNT);
  open_and_assert(&sync, "file1.avd");

  // when
  fclose(sync.in_stream.state);
  /* remove_measurement_file("file1.avd"); */

  SerializeResult result;
  const int MEASUREMENT_TO_SYNC = 2;
  ErrorResult err = file_sync_serialize(&sync, 0, MEASUREMENT_TO_SYNC, &target_buffer, &result);

  // then
  TEST_ASSERT_EQUAL(FILE_SYNC_FILE_ERROR, err.code);
  TEST_ASSERT_FALSE(result.eof);
  
  assert_result_messages(&result, 0, 0);
  err = file_sync_commit(&sync, result);
  TEST_ASSERT_EQUAL(FILE_SYNC_INVALID_PHASE_ERROR, err.code);
  assert_no_sync_state("file1.avd");
  close(&sync);

  remove_measurement_file("file1.avd");

  teardown();
}

TEST_CASE("open_should_return_error_when_already_synchronized", TEST_TAG)
{
  // given
  setup();

  create_sync_ok_file("file1.avd");

  // when
  uint64_t cursor;
  ErrorResult err = file_sync_open(&sync, CONFIG_SD_MOUNT_POINT, "file1.avd", &cursor);

  // then
  remove_sync_ok_file("file1.avd");

  TEST_ASSERT_EQUAL(FILE_SYNC_ALREADY_FINISHED, err.code);
  teardown();
}

static void open_and_assert(FileSync *sync, char *filename) {
  uint64_t cursor;
  ErrorResult err = file_sync_open(sync, CONFIG_SD_MOUNT_POINT, "file1.avd", &cursor);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
}

static void close(FileSync *sync) {
  ErrorResult err = file_sync_close(sync);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
}


static void assert_result_messages(SerializeResult *result, int start, int count) {
  TEST_ASSERT_EQUAL(count + start, result->cursor);
  pb_istream_t stream = pb_istream_from_buffer(target_buffer.buffer, target_buffer.bytes_written);
  for (int i = start; i < count + start; i++) {
    SampleMessage message;
    decode_result(&stream, &message);
    assert_message(&message, i);
  }
}

static void commit(FileSync *sync, SerializeResult result) {
  ErrorResult err = file_sync_commit(sync, result);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
}

static void decode_result(pb_istream_t *stream, SampleMessage *content_message) {
  ProtoSyncMeasurement sync_measurement = ProtoSyncMeasurement_init_zero;
  sync_measurement.content.arg = content_message;
  sync_measurement.content.funcs.decode = &sample_message_deserialize;

  bool decode_success =  pb_decode_delimited(stream, ProtoSyncMeasurement_fields, &sync_measurement);
  TEST_ASSERT_TRUE(decode_success);
}

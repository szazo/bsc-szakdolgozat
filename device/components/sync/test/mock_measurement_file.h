#ifndef MOCK_MEASUREMENT_FILE_H
#define MOCK_MEASUREMENT_FILE_H

#include "sync_measurement.pb.h"
#include "sync_state.pb.h"

#include "../../serializer/test/sample_message.h"

void create_measurement_file(char *filename, int count);
void remove_measurement_file(char *filename);
void create_sync_ok_file(char *base_filename);
void remove_sync_ok_file(char *base_filename);
void remove_sync_file(char *base_filename);
void assert_sync_state(char *filename, int expected_serial, bool is_finished);
void assert_no_sync_state(char *filename);
void assert_message(SampleMessage *message, int index);

#endif

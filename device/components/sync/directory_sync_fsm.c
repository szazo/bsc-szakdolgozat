#include "directory_sync_fsm.h"

#include "esp_log.h"

static const char *TAG = "directory_sync_fsm";

static void directory_sync_fsm_execute(DirectorySyncFSM *me, DirectorySyncFSMEvent event);
static void phase_init(DirectorySyncFSM *me, DirectorySyncFSMEvent event);
static void phase_next_file(DirectorySyncFSM *me, DirectorySyncFSMEvent event);
static void phase_start_file(DirectorySyncFSM *me, DirectorySyncFSMEvent event);
static void phase_process_file(DirectorySyncFSM *me, DirectorySyncFSMEvent event);
static void phase_finished(DirectorySyncFSM *me, DirectorySyncFSMEvent event);
static void transition(DirectorySyncFSM *me, DirectorySyncFSMPhase next_phase);
static void close_current_directory(DirectorySyncFSM *me);
static ErrorResult close_current_file(DirectorySyncFSM *me);

void directory_sync_fsm_create(DirectorySyncFSM *me, char *directory, char *extension, DirectorySyncFSMCallbacks callbacks) {
  me->directory = directory;
  me->extension = extension;
  me->callbacks = callbacks;
  directory_sync_fsm_init(me);
}

void directory_sync_fsm_init(DirectorySyncFSM *me) {
  me->phase = SYNC_FSM_PHASE_INIT;
}

bool directory_sync_fsm_is_finished(DirectorySyncFSM *me) {
  return me->phase == SYNC_FSM_PHASE_FINISHED;
}

DirectorySyncFSMPhase directory_sync_fsm_phase(DirectorySyncFSM *me) {
  return me->phase;
}

ErrorResult directory_sync_fsm_last_error(DirectorySyncFSM *me) {
  return me->last_error;
}

char *directory_sync_fsm_current_file(DirectorySyncFSM *me) {
  return me->current_filename;
}

void directory_sync_fsm_proceed(DirectorySyncFSM *me) {
  directory_sync_fsm_execute(me, DIRECTORY_SYNC_EVENT_PROCEED);
}

void directory_sync_fsm_restart(DirectorySyncFSM *me) {
  directory_sync_fsm_execute(me, DIRECTORY_SYNC_EVENT_RESTART);
}

void directory_sync_fsm_stop(DirectorySyncFSM *me) {
  directory_sync_fsm_execute(me, DIRECTORY_SYNC_EVENT_STOP);
}

static void directory_sync_fsm_execute(DirectorySyncFSM *me, DirectorySyncFSMEvent event) {

  me->requested_phase = SYNC_FSM_PHASE_NONE;
  me->last_error = AV_OK_RESULT;
  
  switch (me->phase) {
  case SYNC_FSM_PHASE_NONE:
    // NOP
    break;
  case SYNC_FSM_PHASE_INIT:
    phase_init(me, event);
    break;
  case SYNC_FSM_PHASE_NEXT_FILE:
    phase_next_file(me, event);
    break;
  case SYNC_FSM_PHASE_START_FILE:
    phase_start_file(me, event);
    break;
  case SYNC_FSM_PHASE_PROCESS_FILE:
    phase_process_file(me, event);
    break;
  case SYNC_FSM_PHASE_FINISHED:
    phase_finished(me, event);
    break;
  }

  if (me->requested_phase != SYNC_FSM_PHASE_NONE) {
    me->phase = me->requested_phase;
    me->requested_phase = SYNC_FSM_PHASE_NONE;
  }
}

static void phase_init(DirectorySyncFSM *me, DirectorySyncFSMEvent event) {

  if (event == DIRECTORY_SYNC_EVENT_STOP) {
    transition(me, SYNC_FSM_PHASE_FINISHED);
    return;
  }

  if (event != DIRECTORY_SYNC_EVENT_PROCEED) {
    return;
  }
  
  ErrorResult err = directory_walker_open(&me->directory_walker, me->directory, me->extension);
  if (err.code != AV_OK) {
    me->last_error = err;
    transition(me, SYNC_FSM_PHASE_FINISHED);
    return;
  }

  transition(me, SYNC_FSM_PHASE_NEXT_FILE);
}

static void phase_next_file(DirectorySyncFSM *me, DirectorySyncFSMEvent event) {

  if (event == DIRECTORY_SYNC_EVENT_STOP) {
    close_current_directory(me);
    transition(me, SYNC_FSM_PHASE_FINISHED);
    return;
  }

  if (event != DIRECTORY_SYNC_EVENT_PROCEED) {
    return;
  }

  bool found = director_walker_find_next_file(&me->directory_walker, me->current_filename, CONFIG_FATFS_MAX_LFN);
  if (!found) {
    close_current_directory(me);
    transition(me, SYNC_FSM_PHASE_FINISHED);
    return;
  }
  
  transition(me, SYNC_FSM_PHASE_START_FILE);
}

static void phase_start_file(DirectorySyncFSM *me, DirectorySyncFSMEvent event) {

  if (event == DIRECTORY_SYNC_EVENT_STOP) {
    close_current_directory(me);
    transition(me, SYNC_FSM_PHASE_FINISHED);
    return;
  }

  if (event != DIRECTORY_SYNC_EVENT_PROCEED) {
    return;
  }

  file_sync_create(&me->file_sync);
  ErrorResult err = file_sync_open(&me->file_sync, me->directory, me->current_filename, &me->current_cursor);
  if (err.code != AV_OK) {
    transition(me, SYNC_FSM_PHASE_NEXT_FILE);
    return;
  }

  ESP_LOGI(TAG, "open succeeded, syncing file: %s", me->current_filename);

  bool send_succeeded = me->callbacks.checkpoint_callback(me->callbacks.arg,
                                                          SYNC_FSM_CHECKPOINT_START_FILE, me->current_filename);
  if (!send_succeeded) {
    // try again
    close_current_file(me);
    transition(me, SYNC_FSM_PHASE_START_FILE);
    return;
  }

  transition(me, SYNC_FSM_PHASE_PROCESS_FILE);
}

static void phase_process_file(DirectorySyncFSM *me, DirectorySyncFSMEvent event) {

  if (event == DIRECTORY_SYNC_EVENT_STOP) {
    close_current_file(me);
    close_current_directory(me);
    transition(me, SYNC_FSM_PHASE_FINISHED);
    return;
  }

  if (event != DIRECTORY_SYNC_EVENT_PROCEED) {
    return;
  }

  TargetBuffer target_buffer = { me->buffer, CONFIG_SYNC_BUFFER_LENGTH, 0 };
  SerializeResult result;
  ErrorResult err = file_sync_serialize(&me->file_sync,
                                        me->current_cursor,
                                        CONFIG_SYNC_BUCKET_LENGTH,
                                        &target_buffer,
                                        &result);

  if (err.code != AV_OK) {
    ESP_LOGW(TAG, "file serialize error %x (reason: %x) at file %s, going to next file", err.code, err.reason, me->current_filename);

    // do not try again, go to the next file
    me->last_error = err;

    close_current_file(me);
    transition(me, SYNC_FSM_PHASE_NEXT_FILE);
    return;
  }

  if (target_buffer.bytes_written > 0 || result.eof) {

    SendCallbackData data = {
      .filename = me->current_filename,
      .start_cursor = me->current_cursor,
      .end_cursor = result.cursor,
      .eof = result.eof,
      .buffer = target_buffer.buffer,
      .length = target_buffer.bytes_written
    };

    ErrorResult send_result = me->callbacks.send_callback(me->callbacks.arg,
                                                      data);
    if (send_result.code != AV_OK) {
      ESP_LOGW(TAG, "file send error %x (reason: %x) at file %s, going to next file",
               send_result.code, send_result.reason, me->current_filename);

      me->last_error = err;

      close_current_file(me);
      transition(me, SYNC_FSM_PHASE_NEXT_FILE);
      return;
    }
  }

  file_sync_commit(&me->file_sync, result);
  me->current_cursor = result.cursor;

  if (result.eof) {
    ESP_LOGI(TAG, "file successfully synchronized %s, cursor: %llu", me->current_filename, me->current_cursor);

    bool send_succeeded = me->callbacks.checkpoint_callback(me->callbacks.arg,
                                                            SYNC_FSM_CHECKPOINT_END_FILE, me->current_filename);
    if (!send_succeeded) {
      // try again
      transition(me, SYNC_FSM_PHASE_PROCESS_FILE);
      return;
    }

    close_current_file(me);
    transition(me, SYNC_FSM_PHASE_NEXT_FILE);
  }
}

static void phase_finished(DirectorySyncFSM *me, DirectorySyncFSMEvent event) {
  if (event != DIRECTORY_SYNC_EVENT_RESTART) {
    return;
  }
  
  transition(me, SYNC_FSM_PHASE_INIT);
}

static void transition(DirectorySyncFSM *me, DirectorySyncFSMPhase next_phase) {  
  me->requested_phase = next_phase;
}

static void close_current_directory(DirectorySyncFSM *me) {
  directory_walker_close(&me->directory_walker);  
}

static ErrorResult close_current_file(DirectorySyncFSM *me) {
  return file_sync_close(&me->file_sync);
}

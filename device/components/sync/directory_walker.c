#include "directory_walker.h"

#include <string.h>
#include <stdio.h>

#define MAX_EXTENSION_LENGTH 4

ErrorResult directory_walker_open(DirectoryWalker *me, char *dir_path, char *extension_to_find) {

  DIR *dir = opendir(dir_path);
  if (dir == NULL) {
    return AV_ERROR(DIRECTORY_WALKER_DIRECTORY_OPEN_ERROR);
  }

  me->dir = dir;
  me->dir_path = dir_path;
  me->extension = extension_to_find;
  return AV_OK_RESULT;
}

void directory_walker_close(DirectoryWalker *me) {
  closedir(me->dir);
}

bool director_walker_find_next_file(DirectoryWalker *me, char *result_filename, int result_filename_length) {
  struct dirent *entry;
  int len;
  while ((entry = readdir(me->dir)) != NULL) {

     len = strlen(entry->d_name);
    if (len < MAX_EXTENSION_LENGTH) {
      continue;
    }

    char *extension = &entry->d_name[len - MAX_EXTENSION_LENGTH];
    if (strcmp(me->extension, extension) == 0) {
      strncpy(result_filename, entry->d_name, result_filename_length);
      return true;
    }
  }

  return false;
}

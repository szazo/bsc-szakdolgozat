#include "sync_message_serializer.h"

#include "pb_common.h"
#include "pb_encode.h"
#include "sync_message.pb.h"

typedef struct {
  uint8_t *buffer;
  uint64_t length;
} MeasurementContent;

static bool serialize_measurement_content(pb_ostream_t *stream, const pb_field_t *field, void * const *arg);

bool sync_message_serialize_measurement(SyncMessageInput input, OutputBuffer *output_buffer) {
  MeasurementContent content = { .buffer = input.buffer, .length = input.length };
  
  ProtoSyncMessage message = ProtoSyncMessage_init_default;
  strncpy(message.client_id, input.client_id, 50);
  strncpy(message.filename, input.filename, 255);
  message.start_cursor = input.start_cursor;
  message.end_cursor = input.end_cursor;
  message.eof = input.eof;
  message.measurement.funcs.encode = serialize_measurement_content;
  message.measurement.arg = &content;

  pb_ostream_t out_stream = pb_ostream_from_buffer(output_buffer->buffer, output_buffer->size);
  bool success = pb_encode(&out_stream, ProtoSyncMessage_fields, &message);
  if (success) {
    output_buffer->bytes_written = out_stream.bytes_written;
  }

  return success;
}

static bool serialize_measurement_content(pb_ostream_t *stream, const pb_field_t *field, void * const *arg) {

  if (!pb_encode_tag_for_field(stream, field)) {
    return false;
  }

  MeasurementContent *content = *arg;
  return pb_encode_string(stream, content->buffer, content->length);
}


#ifndef SYNC_MESSAGE_SERIALIZER_H
#define SYNC_MESSAGE_SERIALIZER_H

#include <stdint.h>
#include <stdbool.h>
#include "directory_sync_fsm.h"

typedef struct {
  char *client_id;
  char *filename;
  int start_cursor;
  int end_cursor;
  bool eof;
  uint8_t *buffer;
  int length;
} SyncMessageInput;

typedef struct {
  // Pointer to the buffer
  uint8_t *buffer;
  // The size of the whole buffer
  int size;
  // The count of bytes written into the buffer
  int bytes_written;
} OutputBuffer;

bool sync_message_serialize_measurement(SyncMessageInput input, OutputBuffer *output_buffer);

#endif



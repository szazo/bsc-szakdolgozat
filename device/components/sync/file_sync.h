#ifndef FILE_SYNC_H
#define FILE_SYNC_H

#include "error.h"
#include "pb_common.h"
#include "pb_decode.h"
#include "sdkconfig.h"

#define MAX_PATH_LENGTH 256

AV_ERROR_DEFINE(FILE_SYNC_ALREADY_FINISHED, 0x0621)
AV_ERROR_DEFINE(FILE_SYNC_FILE_LOCKED, 0x0622)
AV_ERROR_DEFINE(FILE_SYNC_FILE_NOT_FOUND, 0x0623)
AV_ERROR_DEFINE(FILE_SYNC_STATE_DECODE_ERROR, 0x0624)
AV_ERROR_DEFINE(FILE_SYNC_STATE_ENCODE_ERROR, 0x0625)
AV_ERROR_DEFINE(FILE_SYNC_STATE_FILE_ERROR, 0x0626)
AV_ERROR_DEFINE(FILE_SYNC_STATE_FILE_RENAME_ERROR, 0x0627)
AV_ERROR_DEFINE(FILE_SYNC_FILE_ERROR, 0x0628)
AV_ERROR_DEFINE(FILE_SYNC_ENCODE_ERROR, 0x0629)
AV_ERROR_DEFINE(FILE_SYNC_INVALID_PHASE_ERROR, 0x062A)
AV_ERROR_DEFINE(FILE_SYNC_INVALID_CURSOR_ERROR, 0x062B)
AV_ERROR_DEFINE(FILE_SYNC_SEEK_ERROR, 0x062C)
AV_ERROR_DEFINE(FILE_SYNC_SMALL_CONTENT_BUFFER_ERROR, 0x062D)

typedef void* FileSynchronizerHandle;

typedef enum {
  FILE_SYNC_NONE,
  FILE_SYNC_OPENED,
  FILE_SYNC_CLOSED,
  FILE_SYNC_ERROR
} FileSyncPhase;

typedef struct {
  FileSyncPhase phase;
  char dir[MAX_PATH_LENGTH];
  char filename[MAX_PATH_LENGTH];
  // the number of processed measurement count
  uint64_t current_cursor;
  pb_istream_t in_stream;
  uint8_t content_buffer[CONFIG_SYNC_MAX_CONTENT_SIZE];
} FileSync;

typedef struct {
  // Pointer to the buffer
  uint8_t *buffer;
  // The size of the whole buffer
  int size;
  // The count of bytes written into the buffer
  int bytes_written;
} TargetBuffer;

typedef struct {
  uint64_t cursor;
  bool eof;
} SerializeResult;

void file_sync_create(FileSync *me);
ErrorResult file_sync_open(FileSync *me, char *dir, char *filename, uint64_t *cursor);
ErrorResult file_sync_serialize(FileSync *me,
                                int cursor, int count,
                                TargetBuffer *buffer, SerializeResult *result);
ErrorResult file_sync_commit(FileSync *me, SerializeResult state);
ErrorResult file_sync_close(FileSync *me);

#endif

COMPONENT_ADD_INCLUDEDIRS=.

file_sync.o: sync_state.pb.c sync_measurement.pb.c
sync.o: sync_message.pb.c

%.pb.c %.pb.h: $(COMPONENT_PATH)/%.proto
	$(PROTOC_WITH_OPTS) -I$(COMPONENT_PATH) --proto_path=$(COMPONENT_PATH) \
		--nanopb_out=-I$(COMPONENT_PATH):$(COMPONENT_PATH)  $<

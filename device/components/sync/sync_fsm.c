#include "sync_fsm.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_timer.h"

static inline void phase_syncing(SyncFSM *me, SyncEvent event);
static inline void phase_wait_for_restart(SyncFSM *me, SyncEvent event);
static void sync_fsm_execute(SyncFSM *me, SyncEvent event);
static bool finish_if_stopped(SyncFSM *me, SyncEvent event);
static void transition(SyncFSM *me, SyncPhase next_phase);

void sync_fsm_create(SyncFSM *me, int restart_wait_milliseconds, DirectorySyncAPI directory_sync_api) {
  me->restart_wait_milliseconds = restart_wait_milliseconds;
  me->directory_sync_api = directory_sync_api;
  sync_fsm_init(me);
}

void sync_fsm_init(SyncFSM *me) {
  me->phase = SYNC_PHASE_SYNCING;
  me->directory_sync_api.init(me->directory_sync_api.instance);
}

void sync_fsm_proceed(SyncFSM *me) {
  sync_fsm_execute(me, SYNC_EVENT_PROCEED);
}

void sync_fsm_stop(SyncFSM *me) {
  sync_fsm_execute(me, SYNC_EVENT_STOP);
}

static void sync_fsm_execute(SyncFSM *me, SyncEvent event) {
  me->requested_phase = SYNC_PHASE_NONE;
  
  switch (me->phase) {
  case SYNC_PHASE_NONE:
    // NOP
    break;
  case SYNC_PHASE_SYNCING:
    phase_syncing(me, event);
    break;
  case SYNC_PHASE_WAIT_FOR_RESTART:
    phase_wait_for_restart(me, event);
    break;
  case SYNC_PHASE_STOPPED:
    // NOP
    break;
  }

  if (me->requested_phase != SYNC_PHASE_NONE) {
    me->phase = me->requested_phase;
  }
}

SyncPhase sync_fsm_phase(SyncFSM *me) {
  return me->phase;
}

static inline void phase_syncing(SyncFSM *me, SyncEvent event) {

  if (finish_if_stopped(me, event)) {
    return;
  }

  me->directory_sync_api.proceed(me->directory_sync_api.instance);

  ErrorResult error = me->directory_sync_api.last_error(me->directory_sync_api.instance);
  if (error.code != AV_OK) {
    // we had an error
    av_error_publish_result(SEVERITY_WARNING, error);
  }

  if (me->directory_sync_api.is_finished(me->directory_sync_api.instance)) {
    me->restart_wait_start_time = esp_timer_get_time();
    transition(me, SYNC_PHASE_WAIT_FOR_RESTART);
  }
}

static inline void phase_wait_for_restart(SyncFSM *me, SyncEvent event) {

  if (finish_if_stopped(me, event)) {
    return;
  }
  
  uint64_t diff = esp_timer_get_time() - me->restart_wait_start_time;
  if (diff > me->restart_wait_milliseconds * 1000) {
    me->directory_sync_api.restart(me->directory_sync_api.instance);
    transition(me, SYNC_PHASE_SYNCING);
    return;
  }

  vTaskDelay(pdMS_TO_TICKS(200));
}

static bool finish_if_stopped(SyncFSM *me, SyncEvent event) {

  if (event == SYNC_EVENT_STOP) {

    me->directory_sync_api.stop(me->directory_sync_api.instance);
    
    transition(me, SYNC_PHASE_STOPPED);
    return true;
  }

  return false;
}

static void transition(SyncFSM *me, SyncPhase next_phase) {  
  me->requested_phase = next_phase;
}

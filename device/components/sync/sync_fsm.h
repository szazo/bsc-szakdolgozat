#ifndef SYNC_FSM_H
#define SYNC_FSM_H

#include "directory_sync_fsm.h"

typedef struct {
  void (*init) (DirectorySyncFSM *me);
  void (*proceed) (DirectorySyncFSM *me);
  void (*restart) (DirectorySyncFSM *me);
  void (*stop) (DirectorySyncFSM *me);
  bool (*is_finished) (DirectorySyncFSM *me);
  ErrorResult (*last_error) (DirectorySyncFSM *me);
  DirectorySyncFSM *instance;
} DirectorySyncAPI;

typedef enum {
  SYNC_PHASE_NONE,
  SYNC_PHASE_SYNCING,
  SYNC_PHASE_WAIT_FOR_RESTART,
  SYNC_PHASE_STOPPED
} SyncPhase;

typedef enum {
  SYNC_EVENT_PROCEED,
  SYNC_EVENT_STOP
} SyncEvent;

typedef struct {
  int restart_wait_milliseconds;
  DirectorySyncAPI directory_sync_api;
  SyncPhase phase;
  SyncPhase requested_phase;
  int64_t restart_wait_start_time;
} SyncFSM;

void sync_fsm_create(SyncFSM *me, int restart_wait_milliseconds, DirectorySyncAPI directory_sync_api);
void sync_fsm_proceed(SyncFSM *me);
SyncPhase sync_fsm_phase(SyncFSM *me);
void sync_fsm_init(SyncFSM *me);
void sync_fsm_stop(SyncFSM *me);

#endif

#ifndef SYNC_H
#define SYNC_H

#include <stdlib.h>
#include "error.h"
#include "mqtt_adapter.h"
#include "stoppable_task.h"
#include "sync_fsm.h"

AV_ERROR_DEFINE(SYNC_TASK_ERROR, 0x0601)
AV_ERROR_DEFINE(SYNC_SYNC_MESSAGE_SERIALIZE_ERROR, 0x0602)
AV_ERROR_DEFINE(SYNC_SYNC_MESSAGE_SEND_ERROR, 0x0603)

typedef struct {
  uint8_t sync_task_priority;
  TaskCPU sync_task_cpu;
  char *path;
  char *extension;
} SyncConfig;

typedef struct {
  MqttClientHandle mqtt_client;
  Task sync_task;
  SyncConfig config;
  DirectorySyncFSM directory_sync_fsm;
  SyncFSM sync_fsm;
  EventGroupHandle_t sync_enabled_event_group;
  uint8_t output_buffer[CONFIG_SYNC_BUFFER_LENGTH];
} Sync;

typedef Sync* SyncHandle;

ErrorResult sync_create(MqttClientHandle mqtt_client, SyncConfig config, SyncHandle *sync_handle);
void sync_delete(SyncHandle sync_handle);
ErrorResult sync_start(SyncHandle sync_handle);
void sync_stop_and_wait(SyncHandle sync_handle);

void sync_pause(SyncHandle sync_handle);
void sync_resume(SyncHandle sync_handle);

#endif

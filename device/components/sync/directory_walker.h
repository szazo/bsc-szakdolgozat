#ifndef DIRECTORY_WALKER_H
#define DIRECTORY_WALKER_H

#include "error.h"
#include <dirent.h>
#include "stdbool.h"

AV_ERROR_DEFINE(DIRECTORY_WALKER_DIRECTORY_OPEN_ERROR, 0x0611)

typedef struct {
  DIR *dir;
  char *extension;
  char *dir_path;
} DirectoryWalker;

ErrorResult directory_walker_open(DirectoryWalker *me, char *dir_path, char *extension_to_find);
bool director_walker_find_next_file(DirectoryWalker *me, char *result_filename, int result_filename_length);
void directory_walker_close(DirectoryWalker *me);

#endif

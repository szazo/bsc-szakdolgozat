#ifndef DIRECTORY_SYNC_FSM_H
#define DIRECTORY_SYNC_FSM_H

#include "sdkconfig.h"
#include "error.h"
#include "directory_walker.h"
#include "file_sync.h"

typedef enum {
  SYNC_FSM_CHECKPOINT_START_FILE,
  SYNC_FSM_CHECKPOINT_END_FILE
} DirectorySyncFSMcheckpoint;

typedef struct {
  char *filename;
  int start_cursor;
  int end_cursor;
  bool eof;
  uint8_t *buffer;
  int length;
} SendCallbackData;

typedef ErrorResult (SendCallback) (void *arg,
                             SendCallbackData data);
typedef bool (CheckpointCallback) (void *arg, DirectorySyncFSMcheckpoint checkpoint, char *filename);

typedef struct {
  SendCallback *send_callback;
  CheckpointCallback *checkpoint_callback;
  void *arg;
} DirectorySyncFSMCallbacks;

typedef enum {
  DIRECTORY_SYNC_EVENT_PROCEED,
  DIRECTORY_SYNC_EVENT_RESTART,
  DIRECTORY_SYNC_EVENT_STOP
} DirectorySyncFSMEvent;

typedef enum {
  SYNC_FSM_PHASE_NONE,
  SYNC_FSM_PHASE_INIT,
  SYNC_FSM_PHASE_NEXT_FILE,
  SYNC_FSM_PHASE_START_FILE,
  SYNC_FSM_PHASE_PROCESS_FILE,
  SYNC_FSM_PHASE_FINISHED
} DirectorySyncFSMPhase;

typedef struct {
  char *directory;
  char *extension;
  DirectorySyncFSMCallbacks callbacks;

  DirectorySyncFSMPhase phase;
  DirectorySyncFSMPhase requested_phase;
  DirectoryWalker directory_walker;
  FileSync file_sync;
  char current_filename[CONFIG_FATFS_MAX_LFN];
  uint64_t current_cursor;
  uint8_t buffer[CONFIG_SYNC_BUFFER_LENGTH];
  ErrorResult last_error;
} DirectorySyncFSM;

void directory_sync_fsm_create(DirectorySyncFSM *me, char *directory, char *extension, DirectorySyncFSMCallbacks callbacks);
void directory_sync_fsm_init(DirectorySyncFSM *me);
DirectorySyncFSMPhase directory_sync_fsm_phase(DirectorySyncFSM *me);
ErrorResult directory_sync_fsm_last_error(DirectorySyncFSM *me);
char *directory_sync_fsm_current_file(DirectorySyncFSM *me);
void directory_sync_fsm_proceed(DirectorySyncFSM *me);
void directory_sync_fsm_restart(DirectorySyncFSM *me);
void directory_sync_fsm_stop(DirectorySyncFSM *me);
bool directory_sync_fsm_is_finished(DirectorySyncFSM *me);

#endif

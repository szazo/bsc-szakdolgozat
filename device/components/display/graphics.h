#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "fonts.h"

typedef void* GraphicsHandle;

typedef enum {
  GRAPHICS_COLORED = 0,
  GRAPHICS_UNCOLORED = 1
} GraphicsColor;

#ifdef __cplusplus
extern "C" {
#endif

  GraphicsHandle graphics_create(unsigned char* image, int width, int height);
  void graphics_delete(GraphicsHandle graphics);
  void graphics_set_rotate(GraphicsHandle graphics, int rotate);
  int graphics_get_rotate(GraphicsHandle graphics);
  void graphics_drawstring_at(GraphicsHandle graphics, int x, int y,
                              const char* text, sFONT* font, GraphicsColor color);
  void graphics_draw_filled_rectangle(GraphicsHandle graphics, int x0, int y0, int x1, int y1, GraphicsColor color);
  void graphics_clear(GraphicsHandle graphics, GraphicsColor color);

#ifdef __cplusplus
}
#endif

#endif

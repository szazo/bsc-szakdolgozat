#include "epd1in54b.h"
#include "epaper.h"

extern "C" ErrorResult epaper_create(EPaperHandle *handle) {
  Epd *epd = new Epd();
  if (epd->Init() != 0) {
    return AV_ERROR(EPAPER_CREATE_ERROR);
  }

  *handle = epd;
  return AV_OK_RESULT;
}

extern "C" void epaper_delete(EPaperHandle epaper) {
  Epd *epd = (Epd *) epaper;
  delete epd;
}

extern "C" void epaper_display(EPaperHandle epaper, const unsigned char* framebuffer) {
  Epd *epd = (Epd *) epaper;
  epd->DisplayFrame(framebuffer, NULL);
}


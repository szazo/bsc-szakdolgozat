#include "graphics.h"
#include "epdpaint.h"

extern "C" GraphicsHandle graphics_create(unsigned char* image, int width, int height) {

  Paint *paint = new Paint(image, width, height);

  return paint;
}

extern "C" void graphics_delete(GraphicsHandle graphics) {
  Paint *paint = (Paint *) graphics;
  delete paint;
}

void graphics_set_rotate(GraphicsHandle graphics, int rotate) {
  Paint *paint = (Paint *) graphics;
  paint->SetRotate(rotate);
}

int graphics_get_rotate(GraphicsHandle graphics) {
  Paint *paint = (Paint *) graphics;
  return paint->GetRotate();
}

extern "C" void graphics_drawstring_at(GraphicsHandle graphics, int x, int y,
                                       const char* text, sFONT* font, GraphicsColor color) {
  Paint *paint = (Paint *) graphics;

  paint->DrawStringAt(x, y, text, font, color);
}

extern "C" void graphics_clear(GraphicsHandle graphics, GraphicsColor color) {
  Paint *paint = (Paint *) graphics;

  paint->Clear(color);
}

extern "C" void graphics_draw_filled_rectangle(GraphicsHandle graphics,
                                               int x0, int y0, int x1, int y1, GraphicsColor color) {
  Paint *paint = (Paint *) graphics;
  paint->DrawFilledRectangle(x0, y0, x1, y1, color);
}


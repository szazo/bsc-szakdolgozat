#ifndef epaper_H
#define epaper_H

#include "error.h"

AV_ERROR_DEFINE(EPAPER_CREATE_ERROR, 0x0E01)

typedef void* EPaperHandle;

#ifdef __cplusplus
extern "C" {
#endif

  ErrorResult epaper_create(EPaperHandle *handle);
  void epaper_delete(EPaperHandle epaper);
  void epaper_display(EPaperHandle epaper, const unsigned char* framebuffer);

#ifdef __cplusplus
}
#endif

#endif

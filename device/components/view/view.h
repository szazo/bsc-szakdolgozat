#ifndef VIEW_H
#define VIEW_H

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"

#include "error.h"
#include "stoppable_task.h"
#include "view_api.h"
#include "epaper.h"

#define MAX_OPEN_VIEW 5

typedef struct {
  int width;
  int height;
} ViewConfig;

typedef struct {
  ViewDraw *view_draw_function;
  void *view_draw_arg;
} ViewEntry;

typedef struct {
  ViewConfig config;

  int view_count;
  ViewEntry views[MAX_OPEN_VIEW];

  EPaperHandle epaper;
  unsigned char *framebuffer;
  GraphicsHandle graphics;
} View;

typedef View* ViewHandle;
typedef View* ViewApiHandle;
typedef int ViewId;

ErrorResult view_create(ViewConfig *config, EPaperHandle epaper, ViewHandle *view_handle);
ErrorResult view_delete(ViewHandle view);
ViewId view_open(ViewHandle view, ViewDraw *draw_function, void *draw_arg);
void view_close(ViewHandle view, ViewId view_id);

bool view_draw(ViewApiHandle view, ViewId view_id, bool redraw);

#endif

#ifndef VIEW_API_H
#define VIEW_API_H

#include "graphics.h"

typedef void (ViewDraw) (void *arg, GraphicsHandle graphics, bool redraw);

#endif

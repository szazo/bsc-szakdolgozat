#include "view.h"


typedef struct {
  int view_id;
  bool redraw;
} QueueItem;

ErrorResult view_create(ViewConfig *config, EPaperHandle epaper, ViewHandle *view_handle) {
  View *view = malloc(sizeof(View));
  view->config = *config;
  view->epaper = epaper;

  view->framebuffer = malloc(config->width * config->height / 8);
  view->graphics = graphics_create(view->framebuffer, config->width, config->height);
  graphics_set_rotate(view->graphics, 3);
  graphics_clear(view->graphics, GRAPHICS_UNCOLORED);

  view->view_count = 0;

  *view_handle = view;
  return AV_OK_RESULT;
}

ErrorResult view_delete(ViewHandle view) {
  View *me = view;

  graphics_delete(me->graphics);
  free(me->framebuffer);
  free(me);

  return AV_OK_RESULT;
}

bool view_draw(ViewHandle view, ViewId view_id, bool redraw) {
  View *me = view;

  if (view_id > me->view_count || view_id < 1) {
    return false;
  }

  ViewEntry *entry = &me->views[view_id - 1];
  
  entry->view_draw_function(entry->view_draw_arg, me->graphics, redraw);
  int64_t start = esp_timer_get_time();
  epaper_display(me->epaper, me->framebuffer);
  printf("Display refresh time: %lld\n", esp_timer_get_time() - start);

  return true;
}

ViewId view_open(ViewHandle view, ViewDraw *draw_function, void *draw_arg) {
  View *me = view;

  if (me->view_count >= MAX_OPEN_VIEW) {
    return -1;
  }

  me->views[me->view_count].view_draw_function = draw_function;
  me->views[me->view_count].view_draw_arg = draw_arg;
  me->view_count++;

  return me->view_count;
}

void view_close(ViewHandle view, ViewId view_id) {
  View *me = view;

  if (me->view_count > 0) {
    me->view_count--;
  }
}

#include "unity.h"
#include "stdio.h"
#include <sys/stat.h>
#include "sd_storage.h"

#define TEST_TAG "[sd_storage]"

TEST_CASE("sd_should_mount_write_unmount", TEST_TAG)
{
  char *filename = "/sd/test.txt";

  struct stat st;
  if (stat(filename, &st) == 0) {
    int err = remove(filename);
    TEST_ASSERT_EQUAL(0, err);
  }

  FILE *f = fopen(filename, "w");
  TEST_ASSERT_NOT_NULL(f);

  char text[]  = "The quick brown fox jumps over the lazy dog";
  int written = fwrite(&text, 1, sizeof(text), f);
  TEST_ASSERT_EQUAL(sizeof(text), written);
  fflush(f);

  fclose(f);

  // then
  f = fopen(filename, "r");
  TEST_ASSERT_NOT_NULL(f);

  char buffer[100];
  fread(buffer, sizeof(buffer), 1, f);

  TEST_ASSERT_EQUAL_STRING("The quick brown fox jumps over the lazy dog", buffer);
}

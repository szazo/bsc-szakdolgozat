#ifndef SD_STORAGE_H
#define SD_STORAGE_H

#include "error.h"

AV_ERROR_DEFINE(SD_MOUNT_ERR_FAILED_TO_MOUNT_FILESYSTEM, 0xA001)
AV_ERROR_DEFINE(SD_MOUNT_ERR_FAILED_TO_INITIALIZE_THE_CARD, 0xA002)

ErrorResult sd_storage_mount(char *base_path);
void sd_storage_unmount();

#endif

#include "esp_err.h"
#include "esp_spiffs.h"

#include "spiffs_storage.h"

ErrorResult spiffs_storage_mount(char *base_path) {

  esp_vfs_spiffs_conf_t conf = {
    .base_path = base_path,
    .partition_label = NULL,
    .max_files = 5,
    .format_if_mount_failed = true
  };

  esp_err_t ret = esp_vfs_spiffs_register(&conf);
  if (ret != ESP_OK) {
    if (ret == ESP_FAIL) {
      return AV_ERROR_WITH_REASON(SPIFFS_MOUNT_ERR_MOUNT_OR_FORMAT_ERROR, ret);
    } else if (ret == ESP_ERR_NOT_FOUND) {
      return AV_ERROR_WITH_REASON(SPIFFS_MOUNT_ERR_FAILED_TO_FIND_PARTITION, ret);
    } else {
      return AV_ERROR_WITH_REASON(SPIFFS_MOUNT_ERR_FAILED_TO_INITIALIZE_SPIFFS, ret);
    }
  }

  return AV_OK_RESULT;
}

void spiffs_storage_unmount() {
  esp_vfs_spiffs_unregister(NULL);
}


#include "error.h"
#include "driver/sdmmc_host.h"
#include "driver/sdspi_host.h"
#include "sdmmc_cmd.h"
#include "esp_vfs_fat.h"

#include "sd_storage.h"

#define PIN_NUM_MISO CONFIG_SPI_MISO_GPIO
#define PIN_NUM_MOSI CONFIG_SPI_MOSI_GPIO
#define PIN_NUM_CLK  CONFIG_SPI_CLK_GPIO
#define PIN_NUM_CS   CONFIG_SPI_CS_GPIO

ErrorResult sd_storage_mount(char *base_path) {

    sdmmc_host_t host = SDSPI_HOST_DEFAULT();
    sdspi_slot_config_t slot_config = SDSPI_SLOT_CONFIG_DEFAULT();
    slot_config.gpio_miso = PIN_NUM_MISO;
    slot_config.gpio_mosi = PIN_NUM_MOSI;
    slot_config.gpio_sck = PIN_NUM_CLK;
    slot_config.gpio_cs = PIN_NUM_CS;

    esp_vfs_fat_sdmmc_mount_config_t mount_config = {
        .format_if_mount_failed = false,
        .max_files = 5,
        .allocation_unit_size = 16 * 1024
    };

    // TODO: Note: esp_vfs_fat_sdmmc_mount is an all-in-one convenience function.
    // Please check its source code and implement error recovery when developing
    // production applications.
    sdmmc_card_t* card;
    esp_err_t ret = esp_vfs_fat_sdmmc_mount(base_path, &host, &slot_config, &mount_config, &card);

    if (ret != ESP_OK) {
      if (ret == ESP_FAIL) {
        return AV_ERROR_WITH_REASON(SD_MOUNT_ERR_FAILED_TO_MOUNT_FILESYSTEM, ret);
      } else {
        return AV_ERROR_WITH_REASON(SD_MOUNT_ERR_FAILED_TO_INITIALIZE_THE_CARD, ret);
      }
    }

    // Card has been initialized, print its properties
    sdmmc_card_print_info(stdout, card);

    return AV_OK_RESULT;
}

void sd_storage_unmount() {
  esp_vfs_fat_sdmmc_unmount();
}

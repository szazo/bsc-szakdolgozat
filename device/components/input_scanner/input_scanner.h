#ifndef INPUT_SCANNER_H
#define INPUT_SCANNER_H

#include "stoppable_task.h"
#include "push_button.h"
#include "message_queue_api.h"

MESSAGE_DECLARE_BASE(INPUT_SCANNER_EVENTS);
enum {
  ROTARY_PUSH_BUTTON_PRESSED
} InputScannerEvent;

typedef struct {
  uint8_t scan_task_priority;
  TaskCPU scan_task_cpu;
} InputScannerConfig;

typedef struct {
  InputScannerConfig config;
  PushButton push_button;
  Task scan_task;
  MessageQueueHandle output_queue;
} InputScanner;

ErrorResult input_scanner_create(InputScanner *me, InputScannerConfig config, MessageQueueHandle output_queue);
ErrorResult input_scanner_delete(InputScanner *me);
ErrorResult input_scanner_start(InputScanner *me);
ErrorResult input_scanner_stop_and_wait(InputScanner *me);

#endif

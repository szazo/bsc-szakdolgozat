#include "freertos/FreeRTOS.h"

#include "push_button.h"

#define PUSH_BUTTON_DOWN_THRESHOLD 50

static inline ErrorResult configure_push_gpio_pin(int gpio_pin, PushButton *me);

ErrorResult push_button_create(PushButton *me, int gpio_pin) {

  me->gpio_pin = gpio_pin;
  me->push_last_down = 0;
  me->state = PUSH_BUTTON_UP;
  ErrorResult result = configure_push_gpio_pin(gpio_pin, me);
  if (result.code != AV_OK) {
    free(me);
    return result;
  }

  return result;
}

ErrorResult push_button_delete(PushButton *me) {
  // NOP
  return AV_OK_RESULT;
}

static inline ErrorResult configure_push_gpio_pin(int gpio_pin, PushButton *me) {

  gpio_config_t conf;
  conf.intr_type = (gpio_int_type_t) GPIO_INTR_DISABLE;
  conf.pin_bit_mask = 1ULL << gpio_pin;
  conf.mode = GPIO_MODE_INPUT;
  conf.pull_up_en = 0;
  conf.pull_down_en = 1;

  esp_err_t esp_err = gpio_config(&conf);
  if (esp_err != ESP_OK) {
    return AV_ERROR_WITH_REASON(PUSH_BUTTON_CREATE_CONFIG_ERROR, esp_err);
  }

  return AV_OK_RESULT;
}

PushButtonEvent push_button_scan(PushButton *me) {

  bool level = gpio_get_level(me->gpio_pin);
  int64_t time = esp_timer_get_time();

  PushButtonEvent event = PUSH_BUTTON_EVENT_NONE;

  switch (me->state) {
  case PUSH_BUTTON_UP:
    if (level) {
      me->state = PUSH_BUTTON_DOWN;
      me->push_last_down = time;
    }
    break;
  case PUSH_BUTTON_DOWN:
    if (!level) {
      me->state = PUSH_BUTTON_UP;
    } else if (time - me->push_last_down > PUSH_BUTTON_DOWN_THRESHOLD * 1000) {
      me->state = PUSH_BUTTON_PRESSED;
      event = PUSH_BUTTON_EVENT_PRESSED;
    }
    break;
  case PUSH_BUTTON_PRESSED:
    if (!level) {
      me->state = PUSH_BUTTON_UP;
      event = PUSH_BUTTON_EVENT_RELEASED;
    }
    break;
  }

  return event;
}

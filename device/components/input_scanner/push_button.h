#ifndef PUSH_BUTTON_H
#define PUSH_BUTTON_H

#include "error.h"
#include "stdint.h"

AV_ERROR_DEFINE(PUSH_BUTTON_CREATE_CONFIG_ERROR, 0x0701)

typedef enum {
  PUSH_BUTTON_UP = 0,
  PUSH_BUTTON_DOWN = 1,
  PUSH_BUTTON_PRESSED = 2
} PushButtonState;

typedef enum {
  PUSH_BUTTON_EVENT_NONE = 0,
  PUSH_BUTTON_EVENT_PRESSED = 1,
  PUSH_BUTTON_EVENT_RELEASED = 2
} PushButtonEvent;

typedef struct {
  int gpio_pin;
  PushButtonState state;
  int64_t push_last_down;
} PushButton;

ErrorResult push_button_create(PushButton *me, int gpio_pin);
ErrorResult push_button_delete(PushButton *me);
PushButtonEvent push_button_scan(PushButton *me);

#endif


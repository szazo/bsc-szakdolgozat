#include "input_scanner.h"

#include "esp_log.h"

static void scan_task(void *arg, TaskHandle task);

MESSAGE_DEFINE_BASE(INPUT_SCANNER_EVENTS);

ErrorResult input_scanner_create(InputScanner *me, InputScannerConfig config, MessageQueueHandle output_queue) {
  me->config = config;
  me->output_queue = output_queue;
  push_button_create(&me->push_button, CONFIG_PUSH_BUTTON_GPIO);
  return task_create(&me->scan_task);
}

ErrorResult input_scanner_delete(InputScanner *me) {
  push_button_delete(&me->push_button);
  task_delete(&me->scan_task);

  return AV_OK_RESULT;
}

ErrorResult input_scanner_start(InputScanner *me) {
  TaskConfig task_config = {
    .name = "input_scan",
    .priority = me->config.scan_task_priority,
    .core_id = me->config.scan_task_cpu,
    .stack_depth = 2048,
    .task_function = scan_task,
    .task_param = me
  };

  return task_start(&me->scan_task, &task_config);
}

ErrorResult input_scanner_stop_and_wait(InputScanner *me) {
  task_stop_and_wait(&me->scan_task);

  return AV_OK_RESULT;
}

static void scan_task(void *arg, TaskHandle task) {

  InputScanner *me = arg;
  while (!task_is_stop_requested(task)) {

    PushButtonEvent push_event = push_button_scan(&me->push_button);

    if (push_event == PUSH_BUTTON_EVENT_PRESSED) {
      message_queue_post(me->output_queue, INPUT_SCANNER_EVENTS, ROTARY_PUSH_BUTTON_PRESSED);
    }
    
    /* if (push_event != PUSH_BUTTON_EVENT_NONE) { */
    /*   ESP_LOGI("PUSH", "Button event: %d", (int)push_event); */
    /* } */
    vTaskDelay(pdMS_TO_TICKS(10));
  }
}

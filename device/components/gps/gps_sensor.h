#ifndef GPS_SENSOR_H
#define GPS_SENSOR_H

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"

#include "sensor_api.h"

#include "gps_measurement.h"

#define GPS_SENSOR_BUFFER_SIZE 2048

#define GPS_MESSAGE_BASE 0x02

#define GPS_SENSOR() \
  { .init = gps_sensor_api_init, \
    .deinit = gps_sensor_api_deinit, \
    .start = gps_sensor_api_start, \
    .stop_and_wait = gps_sensor_api_stop, \
  }

#define GPS_SENSOR_DESCRIPTOR(ID, SENSOR) \
  { .id = ID, \
    .sensor = GPS_SENSOR(), \
    .arg = SENSOR }

typedef struct {

  /**
   * The identifier of the sensor, it should be sent in the measurement.
   */
  SensorId id;
  /**
   * Reference to the sensor api.
   */
  SensorApiHandle sensor_api_handle;
  /**
   * UART subsystem events will be propagated to this event queue.
   */
  QueueHandle_t uart_event_queue;
  /**
   * internal buffer for reading data from uart.
   */
  char buffer[GPS_SENSOR_BUFFER_SIZE];

  /**
   * The time of the last pps tick.
   */
  int64_t last_pps_time;

  /**
   * This task reads events from the UART event queue.
   */
  TaskHandle_t reader_task_handle;

  /**
   * Whether stop requested.
   */
  volatile bool reader_task_stop_request;

  /**
   * Whether the task successfully stopped.
   */
  EventGroupHandle_t stopped_event_group;

  /**
   * We don't allocate from the heap for each measurment, we use this buffer when
   * read requested by the governor.
   */
  char measurement_buffer[GPS_MEASUREMENT_SIZE];

} GpsSensor;

typedef void* GpsSensorHandle;

GpsSensorHandle gps_sensor_create();
void gps_sensor_delete(GpsSensorHandle sensor_handle);

bool gps_sensor_api_init(const SensorApiHandle sensor_api_handle, const SensorId id, void *arg);
bool gps_sensor_api_start(void *arg);
void gps_sensor_api_stop(void *arg);
void gps_sensor_api_deinit(void *arg);

#endif

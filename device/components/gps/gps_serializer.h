#ifndef GPS_SERIALIZER_H
#define GPS_SERIALIZER_H

#include "gps_measurement.h"
#include "measurement_serializer.h"
#include "gps_common.pb.h"

typedef struct minmea_date GpsDate;
typedef struct minmea_time GpsTime;

bool gps_serialize(const SerializerMessageType message_type, const void *data, pb_ostream_t *stream, void *arg);

void gps_serialize_float(const struct minmea_float *source, ProtoGpsFloat *target);
void gps_serialize_date(const GpsDate *source, ProtoGpsDate *target);
void gps_serialize_time(const GpsTime *source, ProtoGpsTime *target);

#endif

COMPONENT_ADD_INCLUDEDIRS=.

gps_serializer.o: gps_common.pb.c gps.pb.c

%.pb.c %.pb.h: $(COMPONENT_PATH)/%.proto
	$(PROTOC_WITH_OPTS) -I$(COMPONENT_PATH) --proto_path=$(COMPONENT_PATH) --nanopb_out=-I$(COMPONENT_PATH):$(COMPONENT_PATH) $<

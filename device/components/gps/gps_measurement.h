#ifndef GPS_MEASUREMENT_H
#define GPS_MEASUREMENT_H

#include "minmea.h"
#include "sensor_api.h"

typedef struct {
  union {
    // Recommended Minimum: position, velocity, time
    struct minmea_sentence_rmc rmc;
    // Fix Data
    struct minmea_sentence_gga gga;
    // DOP (Dilution and precision) and active satellites
    struct minmea_sentence_gsa gsa;
    // Geographic Position: Latitude/Longitude
    struct minmea_sentence_gll gll;
    // GPS Pseudorange Noise Statistics
    struct minmea_sentence_gst gst;
    // Satellites in view
    struct minmea_sentence_gsv gsv;
    // Track made good and Ground speed
    struct minmea_sentence_vtg vtg;
    // Time & Date - UTC, day, month, year and local time zone
    struct minmea_sentence_zda zda;
  } u;
} GpsData;

/**
 * Size of a GPS measurement. A fixed buffer will be allocated with this size.
 */
#define GPS_MEASUREMENT_SIZE sizeof(Measurement) + sizeof(GpsData)


#endif

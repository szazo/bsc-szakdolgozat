#include "gps_serializer.h"
#include "gps_measurement.h"
#include "gps.pb.h"

static void serialize_rmc(const struct minmea_sentence_rmc *source, ProtoGpsRmc *target);
static void serialize_gga(const struct minmea_sentence_gga *source, ProtoGpsGga *target);
static void serialize_gsa(const struct minmea_sentence_gsa *source, ProtoGpsGsa *target);
static void serialize_gll(const struct minmea_sentence_gll *source, ProtoGpsGll *target);
static void serialize_gst(const struct minmea_sentence_gst *source, ProtoGpsGst *target);
static void serialize_gsv(const struct minmea_sentence_gsv *source, ProtoGpsGsv *target);
static void serialize_vtg(const struct minmea_sentence_vtg *source, ProtoGpsVtg *target);
static void serialize_zda(const struct minmea_sentence_zda *source, ProtoGpsZda *target);
static void serialize_status(const char status, ProtoGpsStatus *target);
static void serialize_faa_mode(const char mode, ProtoGpsFaaMode *target);
static void serialize_mode(const char mode, ProtoGpsMode *target);
static void serialize_fix_type(const int fix_type, ProtoGpsFixType *target);
static void serialize_sat_info(const struct minmea_sat_info *source, ProtoGpsSatInfo *target);

bool gps_serialize(const SerializerMessageType message_type, const void *data, pb_ostream_t *stream, void *arg) {

  const GpsData *message = (const GpsData *) data;

  enum minmea_sentence_id type = DECODE_MEASUREMENT_TYPE(message_type);
  switch (type) {
  case MINMEA_SENTENCE_RMC: {
    ProtoGpsRmc target = ProtoGpsRmc_init_default;
    serialize_rmc(&message->u.rmc, &target);
    return pb_encode_submessage(stream, ProtoGpsRmc_fields, &target);
  }
  case MINMEA_SENTENCE_GGA: {
    ProtoGpsGga target = ProtoGpsGga_init_default;
    serialize_gga(&message->u.gga, &target);
    return pb_encode_submessage(stream, ProtoGpsGga_fields, &target);
  }
  case MINMEA_SENTENCE_GSA: {
    ProtoGpsGsa target = ProtoGpsGsa_init_default;
    serialize_gsa(&message->u.gsa, &target);
    return pb_encode_submessage(stream, ProtoGpsGsa_fields, &target);
  }
  case MINMEA_SENTENCE_GLL: {
    ProtoGpsGll target = ProtoGpsGll_init_default;
    serialize_gll(&message->u.gll, &target);
    return pb_encode_submessage(stream, ProtoGpsGll_fields, &target);
  }
  case MINMEA_SENTENCE_GST: {
    ProtoGpsGst target = ProtoGpsGst_init_default;
    serialize_gst(&message->u.gst, &target);
    return pb_encode_submessage(stream, ProtoGpsGst_fields, &target);
  }
  case MINMEA_SENTENCE_GSV: {
    ProtoGpsGsv target = ProtoGpsGsv_init_default;
    serialize_gsv(&message->u.gsv, &target);
    return pb_encode_submessage(stream, ProtoGpsGsv_fields, &target);
  }
  case MINMEA_SENTENCE_VTG: {
    ProtoGpsVtg target = ProtoGpsVtg_init_default;
    serialize_vtg(&message->u.vtg, &target);
    return pb_encode_submessage(stream, ProtoGpsVtg_fields, &target);
  }
  case MINMEA_SENTENCE_ZDA: {
    ProtoGpsZda target = ProtoGpsZda_init_default;
    serialize_zda(&message->u.zda, &target);
    return pb_encode_submessage(stream, ProtoGpsZda_fields, &target);
  }
  default:
    // NOP
    return true;
  }
}

static void serialize_rmc(const struct minmea_sentence_rmc *source, ProtoGpsRmc *target) {
  gps_serialize_time(&source->time, &target->time);
  target->valid = source->valid;
  gps_serialize_float(&source->latitude, &target->latitude);
  gps_serialize_float(&source->longitude, &target->longitude);
  gps_serialize_float(&source->speed, &target->speed);
  gps_serialize_float(&source->course, &target->course);
  gps_serialize_date(&source->date, &target->date);
  gps_serialize_float(&source->variation, &target->variation);
}

static void serialize_gga(const struct minmea_sentence_gga *source, ProtoGpsGga *target) {
  gps_serialize_time(&source->time, &target->time);
  gps_serialize_float(&source->latitude, &target->latitude);
  gps_serialize_float(&source->longitude, &target->longitude);
  target->fix_quality = source->fix_quality;
  target->satellites_tracked = source->satellites_tracked;
  gps_serialize_float(&source->hdop, &target->hdop);
  gps_serialize_float(&source->altitude, &target->altitude);
  target->altitude_units = source->altitude_units;
  gps_serialize_float(&source->height, &target->height);
  target->height_units = source->height_units;
  gps_serialize_float(&source->dgps_age, &target->dgps_age);
}

static void serialize_gsa(const struct minmea_sentence_gsa *source, ProtoGpsGsa *target) {
  serialize_mode(source->mode, &target->mode);
  serialize_fix_type(source->fix_type, &target->fix_type);
  for (int i = 0; i < 12; i++) {
    target->sats[i] = source->sats[i];
  }
  gps_serialize_float(&source->pdop, &target->pdop);
  gps_serialize_float(&source->hdop, &target->hdop);
  gps_serialize_float(&source->vdop, &target->vdop);
}

static void serialize_gll(const struct minmea_sentence_gll *source, ProtoGpsGll *target) {
  gps_serialize_float(&source->latitude, &target->latitude);
  gps_serialize_float(&source->longitude, &target->longitude);
  gps_serialize_time(&source->time, &target->time);
  serialize_status(source->status, &target->status);
  serialize_mode(source->mode, &target->mode);
}

static void serialize_gst(const struct minmea_sentence_gst *source, ProtoGpsGst *target) {
  gps_serialize_time(&source->time, &target->time);
  gps_serialize_float(&source->rms_deviation, &target->rms_deviation);
  gps_serialize_float(&source->semi_major_deviation, &target->semi_major_deviation);
  gps_serialize_float(&source->semi_minor_deviation, &target->semi_minor_deviation);
  gps_serialize_float(&source->semi_major_orientation, &target->semi_major_orientation);
  gps_serialize_float(&source->latitude_error_deviation, &target->latitude_error_deviation);
  gps_serialize_float(&source->longitude_error_deviation, &target->longitude_error_deviation);
  gps_serialize_float(&source->altitude_error_deviation, &target->altitude_error_deviation);
}

static void serialize_gsv(const struct minmea_sentence_gsv *source, ProtoGpsGsv *target) {
  target->total_msgs = source->total_msgs;
  target->msg_nr = source->msg_nr;
  target->total_sats = source->total_sats;
  for (int i = 0; i < 4; i++) {
    serialize_sat_info(&source->sats[i], &target->sats[i]);
  }
}

static void serialize_vtg(const struct minmea_sentence_vtg *source, ProtoGpsVtg *target) {
  gps_serialize_float(&source->true_track_degrees, &target->true_track_degrees);
  gps_serialize_float(&source->magnetic_track_degrees, &target->magnetic_track_degrees);
  gps_serialize_float(&source->speed_knots, &target->speed_knots);
  gps_serialize_float(&source->speed_kph, &target->speed_kph);
  serialize_faa_mode(source->faa_mode, &target->faa_mode);
}

static void serialize_zda(const struct minmea_sentence_zda *source, ProtoGpsZda *target) {
  gps_serialize_time(&source->time, &target->time);
  gps_serialize_date(&source->date, &target->date);
  target->hour_offset = source->hour_offset;
  target->minute_offset = source->minute_offset;
}

static void serialize_sat_info(const struct minmea_sat_info *source, ProtoGpsSatInfo *target) {
  target->nr = source->nr;
  target->elevation = source->elevation;
  target->azimuth = source->azimuth;
  target->snr = source->snr;
}

static void serialize_fix_type(const int fix_type, ProtoGpsFixType *target) {
  switch (fix_type) {
  case 1:
    *target = ProtoGpsFixType_FIX_NONE;
    break;
  case 2:
    *target = ProtoGpsFixType_FIX_2D;
    break;
  case 3:
    *target = ProtoGpsFixType_FIX_3D;
    break;
  default:
    *target = ProtoGpsFixType_FIX_UNKNOWN;
    break;
  }
}

static void serialize_status(const char status, ProtoGpsStatus *target) {
  switch (status) {
  case 'A':
    *target = ProtoGpsStatus_STATUS_DATA_VALID;
    break;
  case 'V':
    *target = ProtoGpsStatus_STATUS_DATA_NOT_VALID;
    break;
  default:
    *target = ProtoGpsStatus_STATUS_UNKNOWN;
    break;
  }
}

static void serialize_faa_mode(const char mode, ProtoGpsFaaMode *target) {
  switch (mode) {
  case 'A':
    *target = ProtoGpsFaaMode_FAA_MODE_AUTONOMOUS;
    break;
  case 'D':
    *target = ProtoGpsFaaMode_FAA_MODE_DIFFERENTIAL;
    break;
  case 'E':
    *target = ProtoGpsFaaMode_FAA_MODE_ESTIMATED;
    break;
  case 'M':
    *target = ProtoGpsFaaMode_FAA_MODE_MANUAL;
    break;
  case 'S':
    *target = ProtoGpsFaaMode_FAA_MODE_SIMULATED;
    break;
  case 'N':
    *target = ProtoGpsFaaMode_FAA_MODE_NOT_VALID;
    break;
  case 'P':
    *target = ProtoGpsFaaMode_FAA_MODE_PRECISE;
    break;
  default:
    *target = ProtoGpsFaaMode_FAA_MODE_UNKNOWN;
    break;
  }
}

static void serialize_mode(const char mode, ProtoGpsMode *target) {
  switch (mode) {
  case 'A':
    *target = ProtoGpsMode_MODE_AUTO;
    break;
  case 'M':
    *target = ProtoGpsMode_MODE_FORCED;
    break;
  default:
    *target = ProtoGpsMode_MODE_UNKNOWN;
    break;
  }
}

void gps_serialize_date(const GpsDate *source, ProtoGpsDate *target) {
  target->day = source->day;
  target->month = source->month;
  target->year = source->year;
}

void gps_serialize_time(const GpsTime *source, ProtoGpsTime *target) {
  target->hours = source->hours;
  target->minutes = source->minutes;
  target->seconds = source->seconds;
  target->microseconds = source->microseconds;
}

void gps_serialize_float(const struct minmea_float *source, ProtoGpsFloat *target) {
  target->value = source->value;
  target->scale = source->scale;
}

#ifndef MQTT_ADAPTER_H
#define MQTT_ADAPTER_H

#include "mqtt_client.h"
#include "error.h"

AV_ERROR_DEFINE(MQTT_CREATE_ERROR, 0x0F01)
AV_ERROR_DEFINE(MQTT_CREATE_INVALID_URI, 0x0F02)
AV_ERROR_DEFINE(MQTT_DELETE_ERROR, 0x0F03)
AV_ERROR_DEFINE(MQTT_START_ERROR, 0x0F04)
AV_ERROR_DEFINE(MQTT_STOP_NOT_STARTED_ERROR, 0x0F05)
AV_ERROR_DEFINE(MQTT_PUBLISH_ERROR, 0x0F06)
AV_ERROR_DEFINE(MQTT_NOT_CONNECTED_ERROR, 0x0F07)

typedef enum {
  MQTT_CLIENT_STATE_DISCONNECTED,
  MQTT_CLIENT_STATE_CONNECTED
} MqttClientState;

typedef void (*MqttStateChangedCallbackFunc)(void *arg,
                                             MqttClientState state);

typedef struct {
  void *arg;
  MqttStateChangedCallbackFunc state_changed;
} MqttClientCallback;

typedef struct {
  char *uri;
  MqttClientCallback callback;
} MqttClientSettings;

typedef struct {
  esp_mqtt_client_handle_t client;
  MqttClientCallback callback;
  volatile bool is_connected;
} MqttClient;

typedef MqttClient* MqttClientHandle;

typedef enum {
  QOS_AT_MOST_ONCE = 0,
  QOS_AT_LEAST_ONCE = 1,
  QOS_EXACTLY_ONCE = 2
} MqttQos;

ErrorResult mqtt_client_create(const MqttClientSettings settings, MqttClientHandle *handle);
ErrorResult mqtt_client_delete(MqttClientHandle handle);
void mqtt_client_register_callback(MqttClientHandle handle, MqttClientCallback callback);
ErrorResult mqtt_client_open(MqttClientHandle handle);
ErrorResult mqtt_client_close(MqttClientHandle handle);
ErrorResult mqtt_client_publish(MqttClientHandle handle,
                                const char *topic,
                                const char *data,
                                int size,
                                MqttQos qos,
                                bool retain);

#endif

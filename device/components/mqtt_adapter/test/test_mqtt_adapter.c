#include "unity.h"

#include "esp_wifi.h"
#include "mqtt_adapter.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#define TEST_TAG "[mqtt_adapter]"

static void setup() {
  tcpip_adapter_init();
}

static void tear_down() {
}

TEST_CASE("should_create_and_delete", TEST_TAG)
{
  // given
  setup();

  MqttClientSettings settings = {
    .uri = CONFIG_MQTT_SERVER_URL
  };

  MqttClientHandle mqtt_client;
  ErrorResult err = mqtt_client_create(settings, &mqtt_client);
  TEST_ASSERT_EQUAL(AV_OK, err.code);

  err = mqtt_client_open(mqtt_client);
  TEST_ASSERT_EQUAL(AV_OK, err.code);

  vTaskDelay(pdMS_TO_TICKS(10)); // wait for the mqtt task

  err = mqtt_client_close(mqtt_client);
  TEST_ASSERT_EQUAL(AV_OK, err.code);

  err = mqtt_client_delete(mqtt_client);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
  
  tear_down();
}

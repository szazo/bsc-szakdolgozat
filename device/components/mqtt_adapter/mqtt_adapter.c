#include "mqtt_adapter.h"

#include "esp_log.h"

static const char *TAG = "mqtt_adapter";

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event);
static void callback_state_change(MqttClient *me, MqttClientState state);

ErrorResult mqtt_client_create(const MqttClientSettings settings, MqttClientHandle *handle) {
  MqttClient *me = malloc(sizeof(MqttClient));
  esp_mqtt_client_config_t mqtt_cfg = {
    //    .uri = settings->uri,
    .event_handle = mqtt_event_handler,
    .user_context = me
  };
  esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);

  if (client == NULL) {
    free(me);
    return AV_ERROR(MQTT_CREATE_ERROR);
  }

  me->callback = settings.callback;
  me->client = client;
  me->is_connected = false;

  esp_err_t err = esp_mqtt_client_set_uri(me->client, settings.uri);
  if (err != ESP_OK) {
    esp_mqtt_client_destroy(me->client);
    free(me);

    return AV_ERROR_WITH_REASON(MQTT_CREATE_INVALID_URI, err);
  }

  *handle = me;

  return AV_OK_RESULT;
}

ErrorResult mqtt_client_delete(MqttClientHandle handle) {
  MqttClient *me = handle;
  
  esp_err_t err = esp_mqtt_client_destroy(me->client);
  if (err != ESP_OK) {
    return AV_ERROR_WITH_REASON(MQTT_DELETE_ERROR, err);
  }

  free(me);

  return AV_OK_RESULT;
}

void mqtt_client_register_callback(MqttClientHandle handle, MqttClientCallback callback) {
  MqttClient *me = handle;

  me->callback = callback;
}

ErrorResult mqtt_client_open(MqttClientHandle handle) {
  MqttClient *me = handle;
  
  esp_err_t err = esp_mqtt_client_start(me->client);
  if (err != ESP_OK) {
    return AV_ERROR_WITH_REASON(MQTT_START_ERROR, err);
  }

  return AV_OK_RESULT;
}

ErrorResult mqtt_client_close(MqttClientHandle handle) {
  MqttClient *me = handle;
  
  esp_err_t err = esp_mqtt_client_stop(me->client);
  if (err != ESP_OK) {
    return AV_ERROR_WITH_REASON(MQTT_STOP_NOT_STARTED_ERROR, err);
  }

  return AV_OK_RESULT;
}

ErrorResult mqtt_client_publish(MqttClientHandle handle,
                                const char *topic,
                                const char *data,
                                int size,
                                MqttQos qos,
                                bool retain) {
  MqttClient *me = handle;

  if (!me->is_connected) {
    return AV_ERROR(MQTT_NOT_CONNECTED_ERROR);
  }
  
  int msg_id = esp_mqtt_client_publish(me->client, topic, data, size, qos, retain);
  if (msg_id < 0) {
    return AV_ERROR(MQTT_PUBLISH_ERROR);
  }

  return AV_OK_RESULT;
}

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event) {

  MqttClient *mqtt = event->user_context;

  switch(event->event_id) {
  case MQTT_EVENT_CONNECTED:
    ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
    mqtt->is_connected = true;
    callback_state_change(mqtt, MQTT_CLIENT_STATE_CONNECTED);
    break;
  case MQTT_EVENT_DISCONNECTED:
    ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
    mqtt->is_connected = false;
    callback_state_change(mqtt, MQTT_CLIENT_STATE_DISCONNECTED);
    break;
  default:
    break;
  }

  return ESP_OK;
}

static void callback_state_change(MqttClient *me, MqttClientState state) {
  if (me->callback.state_changed == NULL) {
    return;
  }

  me->callback.state_changed(me->callback.arg, state);
}

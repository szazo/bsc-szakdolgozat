#ifndef LIVE_BROADCAST_MODULE_H
#define LIVE_BROADCAST_MODULE_H

#include "module_api.h"

ErrorResult live_broadcast_module_create(Container *container);
ErrorResult live_broadcast_module_delete(Container *container);
ErrorResult live_broadcast_module_start(Container *container);
ErrorResult live_broadcast_module_stop_and_wait(Container *container);

#define LIVE_BROADCAST_MODULE()                             \
  { .create = live_broadcast_module_create,                 \
      .delete = live_broadcast_module_delete,               \
      .start = live_broadcast_module_start,                 \
      .stop_and_wait = live_broadcast_module_stop_and_wait, \
      .name = "live_broadcast"                              \
      }

#endif

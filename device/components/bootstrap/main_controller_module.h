#ifndef MAIN_MODULE_H
#define MAIN_MODULE_H

#include "module_api.h"

ErrorResult main_controller_module_create(Container *container);
ErrorResult main_controller_module_delete(Container *container);
ErrorResult main_controller_module_start(Container *container);
ErrorResult main_controller_module_stop_and_wait(Container *container);

#define MAIN_CONTROLLER_MODULE()                              \
  { .create = main_controller_module_create,                  \
      .delete = main_controller_module_delete,                \
      .start = main_controller_module_start,                  \
      .stop_and_wait = main_controller_module_stop_and_wait,  \
      .name = "main_controller"                               \
      }

#endif

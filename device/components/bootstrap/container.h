#ifndef CONTAINER_H
#define CONTAINER_H

#include "message_queue.h"
#include "message_queue.h"
#include "epaper.h"
#include "view.h"
#include "dashboard_view.h"
#include "main_controller_fsm.h"
#include "input_scanner.h"
#include "sync.h"
#include "sensor_governor.h"
#include "measurement_loop.h"
#include "time_sync.h"
#include "live_broadcast.h"

#include "dummy_sensor.h"
#include "gyro_sensor.h"
#include "gps_sensor.h"
#include "airspeed_sensor.h"
#include "pressure_sensor.h"

typedef struct {
  GyroSensorHandle gyro;
  GpsSensorHandle gps;
  AirspeedSensorHandle airspeed_sensor;
  PressureSensorHandle pressure_sensor;
} SensorContainer;

typedef struct {
  MessageQueueHandle message_queue;
  EPaperHandle epaper;
  ViewHandle view;
  InputScanner input_scanner;
  MainFSM main_fsm;
  MqttClientHandle sync_mqtt_client;
  SyncHandle sync;
  GovernorHandle sensor_governor;
  MeasurementLoopHandle measurement_loop;
  SensorContainer sensors;
  SerializerHandle serializer;
  TelemetryWriterFSM telemetry_writer_fsm;
  TelemetryWriterHandle telemetry_writer;
  TimeSync time_sync;
  MqttClientHandle live_broadcast_mqtt_client;
  LiveBroadcast live_broadcast;
} Container;

#endif

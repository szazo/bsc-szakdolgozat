#ifndef I2C_H
#define I2C_H

#include <stdbool.h>

bool i2c_initialize();
void i2c_deinitialize();

#endif

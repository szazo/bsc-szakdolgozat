#ifndef VIEW_MODULE_H
#define VIEW_MODULE_H

#include "module_api.h"

ErrorResult view_module_create(Container *container);
ErrorResult view_module_delete(Container *container);
ErrorResult view_module_start(Container *container);
ErrorResult view_module_stop_and_wait(Container *container);

#define VIEW_MODULE()                             \
  { .create = view_module_create,                 \
      .delete = view_module_delete,               \
      .start = view_module_start,                 \
      .stop_and_wait = view_module_stop_and_wait, \
      .name = "view"                              \
      }

#endif

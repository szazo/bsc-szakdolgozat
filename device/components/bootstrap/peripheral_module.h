#ifndef PERIPHERAL_MODULE_H
#define PERIPHERAL_MODULE_H

#include "module_api.h"

AV_ERROR_DEFINE(PERIPHERAL_MODULE_CREATE_ERROR, 0xC101)

ErrorResult peripheral_module_create(Container *container);
ErrorResult peripheral_module_delete(Container *container);
ErrorResult peripheral_module_start(Container *container);
ErrorResult peripheral_module_stop_and_wait(Container *container);

#define PERIPHERAL_MODULE()                             \
  { .create = peripheral_module_create,                 \
      .delete = peripheral_module_delete,               \
      .start = peripheral_module_start,                 \
      .stop_and_wait = peripheral_module_stop_and_wait, \
      .name = "peripheral"                              \
      }

#endif

#ifndef MESSAGE_QUEUE_MODULE_H
#define MESSAGE_QUEUE_MODULE_H

#include "module_api.h"

ErrorResult message_queue_module_create(Container *container);
ErrorResult message_queue_module_delete(Container *container);
ErrorResult message_queue_module_start(Container *container);
ErrorResult message_queue_module_stop_and_wait(Container *container);

#define MESSAGE_QUEUE_MODULE()                              \
  { .create = message_queue_module_create,                  \
      .delete = message_queue_module_delete,                \
      .start = message_queue_module_start,                  \
      .stop_and_wait = message_queue_module_stop_and_wait,  \
      .name = "message_queue"                               \
      }


#endif

#ifndef TIME_SYNC_MODULE_H
#define TIME_SYNC_MODULE_H

#include "module_api.h"

ErrorResult time_sync_module_create(Container *container);
ErrorResult time_sync_module_delete(Container *container);
ErrorResult time_sync_module_start(Container *container);
ErrorResult time_sync_module_stop_and_wait(Container *container);

#define TIME_SYNC_MODULE()                              \
  { .create = time_sync_module_create,                  \
      .delete = time_sync_module_delete,                \
      .start = time_sync_module_start,                  \
      .stop_and_wait = time_sync_module_stop_and_wait,  \
      .name = "time_sync"                               \
      }

#endif

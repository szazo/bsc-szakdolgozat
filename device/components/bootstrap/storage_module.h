#ifndef STORAGE_MODULE_H
#define STORAGE_MODULE_H

#include "module_api.h"

ErrorResult storage_module_create(Container *container);
ErrorResult storage_module_delete(Container *container);
ErrorResult storage_module_start(Container *container);
ErrorResult storage_module_stop_and_wait(Container *container);

#define STORAGE_MODULE()                              \
  { .create = storage_module_create,                  \
      .delete = storage_module_delete,                \
      .start = storage_module_start,                  \
      .stop_and_wait = storage_module_stop_and_wait,  \
      .name = "storage"                               \
      }

#endif

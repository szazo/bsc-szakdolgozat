#include "view_module.h"
#include "epaper.h"
#include "view.h"

ErrorResult view_module_create(Container *container) {

  ErrorResult err = epaper_create(&container->epaper);
  if (err.code != AV_OK) {
    return err;
  }

  ViewConfig config = {
    .width = 200,
    .height = 200
  };
  err = view_create(&config, container->epaper, &container->view);
  if (err.code != AV_OK) {
    epaper_delete(container->epaper);
    return err;
  }

  return AV_OK_RESULT;
}

ErrorResult view_module_delete(Container *container) {
  ErrorResult err = view_delete(container->view);
  if (err.code != AV_OK) {
    return err;
  }
  epaper_delete(container->epaper);

  return AV_OK_RESULT;
}

ErrorResult view_module_start(Container *container) {
  return AV_OK_RESULT;
}

ErrorResult view_module_stop_and_wait(Container *container) {
  return AV_OK_RESULT;
}

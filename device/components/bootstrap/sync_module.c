#include "sync_module.h"

ErrorResult sync_module_create(Container *container) {
  MqttClientSettings settings = {
    .uri = CONFIG_MQTT_SERVER_URL,
    .callback = {
      .state_changed = NULL
    }
  };
  
  ErrorResult err = mqtt_client_create(settings, &container->sync_mqtt_client);
  if (err.code != AV_OK) {
    return err;
  }

  SyncConfig config = {
    .sync_task_priority = CONFIG_SYNC_TASK_PRIORITY,
    .sync_task_cpu = CONFIG_SYNC_TASK_CPU,
    .path = CONFIG_SD_MOUNT_POINT,
    .extension = ".avd"
  };
  err = sync_create(container->sync_mqtt_client, config, &container->sync);
  if (err.code != AV_OK) {
    mqtt_client_delete(container->sync_mqtt_client);
  }
  
  return err;
}

ErrorResult sync_module_delete(Container *container) {
  sync_delete(container->sync);
  mqtt_client_delete(container->sync_mqtt_client);

  return AV_OK_RESULT;
}

ErrorResult sync_module_start(Container *container) {
  return sync_start(container->sync);
}

ErrorResult sync_module_stop_and_wait(Container *container) {
  return AV_OK_RESULT;
}

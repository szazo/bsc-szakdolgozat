#ifndef INPUT_SCANNER_MODULE_H
#define INPUT_SCANNER_MODULE_H

#include "module_api.h"

ErrorResult input_scanner_module_create(Container *container);
ErrorResult input_scanner_module_delete(Container *container);
ErrorResult input_scanner_module_start(Container *container);
ErrorResult input_scanner_module_stop_and_wait(Container *container);

#define INPUT_SCANNER_MODULE()                              \
  { .create = input_scanner_module_create,                  \
      .delete = input_scanner_module_delete,                \
      .start = input_scanner_module_start,                  \
      .stop_and_wait = input_scanner_module_stop_and_wait,  \
      .name = "input_scanner"                               \
      }

#endif

#include "input_scanner_module.h"

ErrorResult input_scanner_module_create(Container *container) {
  InputScannerConfig config = (InputScannerConfig) {
    .scan_task_priority = CONFIG_INPUT_SCANNER_TASK_PRIORITY,
    .scan_task_cpu = CONFIG_INPUT_SCANNER_TASK_CPU
  };

  return input_scanner_create(&container->input_scanner, config, container->message_queue);
}

ErrorResult input_scanner_module_delete(Container *container) {
  return input_scanner_delete(&container->input_scanner);
}

ErrorResult input_scanner_module_start(Container *container) {
  return input_scanner_start(&container->input_scanner);
}

ErrorResult input_scanner_module_stop_and_wait(Container *container) {
  return input_scanner_stop_and_wait(&container->input_scanner);
}

#ifndef MEASUREMENT_LOOP_MODULE_H
#define MEASUREMENT_LOOP_MODULE_H

#include "module_api.h"

ErrorResult measurement_loop_module_create(Container *container);
ErrorResult measurement_loop_module_delete(Container *container);
ErrorResult measurement_loop_module_start(Container *container);
ErrorResult measurement_loop_module_stop_and_wait(Container *container);

#define MEASUREMENT_LOOP_MODULE()                             \
  { .create = measurement_loop_module_create,                 \
      .delete = measurement_loop_module_delete,               \
      .start = measurement_loop_module_start,                 \
      .stop_and_wait = measurement_loop_module_stop_and_wait, \
      .name = "measurement_loop"                              \
      }

#endif

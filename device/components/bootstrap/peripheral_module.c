#include "i2c.h"
#include "peripheral_module.h"

ErrorResult peripheral_module_create(Container *container) {
  // Configures isr using IRAM, because in this case it is not needed
  // to be disabled when flash operation occurs.
  esp_err_t err = gpio_install_isr_service(ESP_INTR_FLAG_IRAM);
  if (err != ESP_OK) {
    return AV_ERROR(PERIPHERAL_MODULE_CREATE_ERROR);
  }

  if (!i2c_initialize()) {
    return AV_ERROR(PERIPHERAL_MODULE_CREATE_ERROR);
  }

  return AV_OK_RESULT;
}

ErrorResult peripheral_module_delete(Container *container) {
  i2c_deinitialize();
  gpio_uninstall_isr_service();

  return AV_OK_RESULT;
}

ErrorResult peripheral_module_start(Container *container) {
  return AV_OK_RESULT;
}

ErrorResult peripheral_module_stop_and_wait(Container *container) {
  return AV_OK_RESULT;
}

#include "telemetry_writer_module.h"

#include "gyro_serializer.h"
#include "gps_serializer.h"
#include "airspeed_serializer.h"
#include "pressure_serializer.h"

enum { SERIALIZER_COUNT = 4 };
static SerializerDescriptor serializer_descriptors[SERIALIZER_COUNT] = {
  SERIALIZER_DESCRIPTOR(GYRO_MESSAGE_BASE, gyro_serialize, NULL),
  SERIALIZER_DESCRIPTOR(GPS_MESSAGE_BASE, gps_serialize, NULL),
  SERIALIZER_DESCRIPTOR(AIRSPEED_MESSAGE_BASE, airspeed_serialize, NULL),
  SERIALIZER_DESCRIPTOR(PRESSURE_MESSAGE_BASE, pressure_serialize, NULL)
};

ErrorResult telemetry_writer_module_create(Container *container) {
  container->serializer = serializer_create(serializer_descriptors, SERIALIZER_COUNT);

  ErrorResult err = telemetry_writer_fsm_create(&container->telemetry_writer_fsm,
                                                CONFIG_SD_MOUNT_POINT,
                                                CONFIG_TELEMETRY_FILE_EXTENSION,
                                                container->measurement_loop,
                                                container->serializer);
  if (err.code != AV_OK) {
    serializer_delete(container->serializer);
    return err;
  }

  TelemetryWriterConfig config = {
    .writer_task_priority = CONFIG_TELEMETRY_WRITER_TASK_PRIORITY,
    .writer_task_cpu = CONFIG_TELEMETRY_WRITER_TASK_CPU,
  };

  return telemetry_writer_create(config,
                                 &container->telemetry_writer_fsm,
                                 &container->telemetry_writer);  
}

ErrorResult telemetry_writer_module_delete(Container *container) {
  telemetry_writer_delete(container->telemetry_writer);
  telemetry_writer_fsm_delete(&container->telemetry_writer_fsm);
  serializer_delete(container->serializer);

  return AV_OK_RESULT;
}

ErrorResult telemetry_writer_module_start(Container *container) {
  return AV_OK_RESULT;
}

ErrorResult telemetry_writer_module_stop_and_wait(Container *container) {
  return AV_OK_RESULT;
}

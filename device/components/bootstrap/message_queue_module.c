#include "message_queue_module.h"

#include "message_queue.h"

ErrorResult message_queue_module_create(Container *container) {
  MessageQueueConfig loop_config = {
    .task_priority = CONFIG_MESSAGE_QUEUE_TASK_PRIORITY,
    .task_core_id = CONFIG_MESSAGE_QUEUE_TASK_CPU
  };
  ErrorResult err = message_queue_create(loop_config, &container->message_queue);
  if (err.code != AV_OK) {
    return err;
  }

  return AV_OK_RESULT;
}

ErrorResult message_queue_module_delete(Container *container) {
  return message_queue_delete(container->message_queue);
}

ErrorResult message_queue_module_start(Container *container) {
  return message_queue_start(container->message_queue);
}

ErrorResult message_queue_module_stop_and_wait(Container *container) {
  message_queue_stop_and_wait(container->message_queue);

  return AV_OK_RESULT;
}

#include "sensor_governor.h"
#include "measurement_loop_module.h"

ErrorResult measurement_loop_module_create(Container *container) {
  MeasurementSource measurement_source = {
    .read_measurement = governor_read_measurement,
    .return_measurement = governor_return_measurement,
    .arg = container->sensor_governor
  };
  ErrorResult err = measurement_loop_create(&measurement_source, &container->measurement_loop);
  if (err.code != AV_OK) {
    return err;
  }

  // we start immediately
  return measurement_loop_start(container->measurement_loop);
}

ErrorResult measurement_loop_module_delete(Container *container) {
  measurement_loop_delete(container->measurement_loop);

  return AV_OK_RESULT;
}

ErrorResult measurement_loop_module_start(Container *container) {
  return AV_OK_RESULT;
}

ErrorResult measurement_loop_module_stop_and_wait(Container *container) {
  measurement_loop_stop_and_wait(container->measurement_loop);

  return AV_OK_RESULT;
}

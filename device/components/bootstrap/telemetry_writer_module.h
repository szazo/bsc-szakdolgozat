#ifndef TELEMETRY_WRITER_MODULE_H
#define TELEMETRY_WRITER_MODULE_H

#include "module_api.h"

ErrorResult telemetry_writer_module_create(Container *container);
ErrorResult telemetry_writer_module_delete(Container *container);
ErrorResult telemetry_writer_module_start(Container *container);
ErrorResult telemetry_writer_module_stop_and_wait(Container *container);

#define TELEMETRY_WRITER_MODULE()                              \
  { .create = telemetry_writer_module_create,                  \
      .delete = telemetry_writer_module_delete,                \
      .start = telemetry_writer_module_start,                  \
      .stop_and_wait = telemetry_writer_module_stop_and_wait,  \
      .name = "telemetry_writer"                               \
      }

#endif

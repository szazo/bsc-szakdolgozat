#include "driver/i2c.h"

static const gpio_num_t SDA = CONFIG_I2C_SDA_GPIO;
static const gpio_num_t SCL = CONFIG_I2C_SCL_GPIO;
static const uint32_t CLOCK_SPEED = 400000;  // range from 100 KHz ~ 

bool i2c_initialize() {

  i2c_config_t config;
  config.mode = I2C_MODE_MASTER;
  config.sda_io_num = SDA;
  config.scl_io_num = SCL;
  config.sda_pullup_en = GPIO_PULLUP_ENABLE;
  config.scl_pullup_en = GPIO_PULLUP_ENABLE;
  config.master.clk_speed = CLOCK_SPEED;

  if (i2c_param_config(I2C_NUM_0, &config) != ESP_OK) {
    return false;
  }

  if (i2c_driver_install(I2C_NUM_0, config.mode, 0, 0, 0) != ESP_OK) {
    return false;
  }

  return true;
}

void i2c_deinitialize() {
  i2c_driver_delete(I2C_NUM_0);
}

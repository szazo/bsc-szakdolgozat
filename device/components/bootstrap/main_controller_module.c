#include "main_controller_module.h"

#include "dashboard_view.h"
#include "main_controller.h"

ErrorResult main_controller_module_create(Container *container) {

  DashboardView dashboard_view;
  dashboard_view_init(&dashboard_view);
  main_controller_fsm_create(&container->main_fsm, container->view,
                             dashboard_view, container->telemetry_writer, container->sync,
                             &container->live_broadcast);

  return AV_OK_RESULT;
}

ErrorResult main_controller_module_delete(Container *container) {
  return AV_OK_RESULT;
}

ErrorResult main_controller_module_start(Container *container) {

  ErrorResult err = main_controller_start(&container->main_fsm, container->message_queue);
  if (err.code != AV_OK) {
    return err;
  }

  return message_queue_post(container->message_queue, MAIN_CONTROLLER_EVENTS, MAIN_START_COMMAND);
}

ErrorResult main_controller_module_stop_and_wait(Container *container) {
  return main_controller_stop(&container->main_fsm, container->message_queue);
}

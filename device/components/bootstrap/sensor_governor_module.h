#ifndef SENSOR_GOVERNOR_MODULE_H
#define SENSOR_GOVERNOR_MODULE_H

#include "module_api.h"

ErrorResult sensor_governor_module_create(Container *container);
ErrorResult sensor_governor_module_delete(Container *container);
ErrorResult sensor_governor_module_start(Container *container);
ErrorResult sensor_governor_module_stop_and_wait(Container *container);

#define SENSOR_GOVERNOR_MODULE()                              \
  { .create = sensor_governor_module_create,                  \
      .delete = sensor_governor_module_delete,                \
      .start = sensor_governor_module_start,                  \
      .stop_and_wait = sensor_governor_module_stop_and_wait,  \
      .name = "sensor_governor"                               \
      }

#endif

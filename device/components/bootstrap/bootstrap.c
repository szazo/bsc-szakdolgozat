#include "esp_log.h"

#include "bootstrap.h"

static const char *TAG = "bootstrap";

ErrorResult bootstrap_create(Bootstrap *me, Container *container, Module *modules, int count) {

  if (count > BOOTSTRAP_MAX_MODULE_COUNT) {
    return AV_ERROR(BOOTSTRAP_MAX_MODULE_COUNT_EXHASTUED);
  }
  
  for (int i = 0; i < count; i++) {
    me->modules[i] = modules[i];
  }
  me->module_count = count;
  me->container = container;

  return AV_OK_RESULT;
}

ErrorResult bootstrap_create_modules(Bootstrap *me) {
  ErrorResult err = AV_OK_RESULT;
  Module *module;

  for (int i = 0; i < me->module_count; i++) {

    module = &me->modules[i];
    ESP_LOGI(TAG, "Creating module %s...", module->name);
    err = module->create(me->container);

    if (err.code != AV_OK) {
      for (int j = 0; j < i; j++) {
        me->modules[j].delete(me->container);
      }
    }
  }

  return err;
}

ErrorResult bootstrap_delete_modules(Bootstrap *me) {
  ErrorResult current_error;
  ErrorResult last_error = AV_OK_RESULT;
  Module *module;
  
  for (int i = 0; i < me->module_count; i++) {
    module = &me->modules[i];
    ESP_LOGI(TAG, "Deleting module %s...", module->name);
    current_error = module->delete(me->container);
    if (current_error.code != AV_OK) {
      last_error = current_error;
    }
  }

  return last_error;
}

ErrorResult bootstrap_start_modules(Bootstrap *me) {
  ErrorResult err = AV_OK_RESULT;
  Module *module;

  for (int i = 0; i < me->module_count; i++) {
    module = &me->modules[i];
    ESP_LOGI(TAG, "Starting module %s...", module->name);
    err = module->start(me->container);

    if (err.code != AV_OK) {
      for (int j = 0; j < i; j++) {
        me->modules[j].stop_and_wait(me->container);
      }
    }
  }

  return err;
}

ErrorResult bootstrap_stop_modules(Bootstrap *me) {
  ErrorResult current_error;
  ErrorResult last_error = AV_OK_RESULT;
  Module *module;
  
  for (int i = 0; i < me->module_count; i++) {
    module = &me->modules[i];
    ESP_LOGI(TAG, "Stopping module %s...", module->name);
    current_error = module->stop_and_wait(me->container);
    if (current_error.code != AV_OK) {
      last_error = current_error;
    }
  }

  return last_error;
}




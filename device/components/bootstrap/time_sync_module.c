/* #include "time_sync.h" */

#include "time_sync_module.h"

ErrorResult time_sync_module_create(Container *container) {
  return time_sync_create(&container->time_sync, container->measurement_loop);
}

ErrorResult time_sync_module_delete(Container *container) {
  return time_sync_delete(&container->time_sync);
}

ErrorResult time_sync_module_start(Container *container) {
  return time_sync_start(&container->time_sync);
}

ErrorResult time_sync_module_stop_and_wait(Container *container) {
  return time_sync_stop(&container->time_sync);
}

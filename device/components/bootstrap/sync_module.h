#ifndef SYNC_MODULE_H
#define SYNC_MODULE_H

#include "module_api.h"

ErrorResult sync_module_create(Container *container);
ErrorResult sync_module_delete(Container *container);
ErrorResult sync_module_start(Container *container);
ErrorResult sync_module_stop_and_wait(Container *container);

#define SYNC_MODULE()                             \
  { .create = sync_module_create,                 \
      .delete = sync_module_delete,               \
      .start = sync_module_start,                 \
      .stop_and_wait = sync_module_stop_and_wait, \
      .name = "sync"                              \
      }

#endif

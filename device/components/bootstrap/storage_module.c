#include "sd_storage.h"
#include "storage_module.h"

ErrorResult storage_module_create(Container *container) {
  return sd_storage_mount(CONFIG_SD_MOUNT_POINT);
}

ErrorResult storage_module_delete(Container *container) {
  sd_storage_unmount();
  return AV_OK_RESULT;
}

ErrorResult storage_module_start(Container *container) {
  return AV_OK_RESULT;
}

ErrorResult storage_module_stop_and_wait(Container *container) {
  return AV_OK_RESULT;
}


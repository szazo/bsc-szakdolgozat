#include "sensor_governor.h"
#include "sensor_governor_module.h"

typedef enum {
  GYRO1 = 1,
  GPS1 = 2,
  AIRSPEED1 = 3,
  PRESSURE1 = 4,
} SensorIds;

ErrorResult sensor_governor_module_create(Container *container) {
  ErrorResult err = airspeed_sensor_create(&container->sensors.airspeed_sensor);
  if (err.code != AV_OK) {
    return err;
  }

  
  err = pressure_sensor_create(&container->sensors.pressure_sensor);
  if (err.code != AV_OK) {
    airspeed_sensor_delete(container->sensors.airspeed_sensor);
    return err;
  }

  container->sensors.gyro = gyro_sensor_create();
  container->sensors.gps = gps_sensor_create();

  enum N { SENSOR_COUNT = 4 };
  SensorDescriptor sensor_descriptors[SENSOR_COUNT] = {
    GYRO_SENSOR_DESCRIPTOR(GYRO1, container->sensors.gyro),
    GPS_SENSOR_DESCRIPTOR(GPS1, container->sensors.gps),
    AIRSPEED_SENSOR_DESCRIPTOR(AIRSPEED1, container->sensors.airspeed_sensor),
    PRESSURE_SENSOR_DESCRIPTOR(PRESSURE1, container->sensors.pressure_sensor)
  };

  return governor_create(sensor_descriptors, SENSOR_COUNT, &container->sensor_governor);
}

ErrorResult sensor_governor_module_delete(Container *container) {
  governor_delete(container->sensor_governor);
  gyro_sensor_delete(container->sensors.gyro);
  gps_sensor_delete(container->sensors.gps);
  airspeed_sensor_delete(container->sensors.airspeed_sensor);
  pressure_sensor_delete(container->sensors.pressure_sensor);

  return AV_OK_RESULT;
}

ErrorResult sensor_governor_module_start(Container *container) {
  return governor_start(container->sensor_governor);
}

ErrorResult sensor_governor_module_stop_and_wait(Container *container) {
  governor_stop_and_wait(container->sensor_governor);

  return AV_OK_RESULT;
}

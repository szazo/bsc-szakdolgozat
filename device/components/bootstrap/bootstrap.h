#ifndef BOOTSTRAP_H
#define BOOTSTRAP_H

#include "error.h"
#include "container.h"
#include "module_api.h"

#define BOOTSTRAP_MAX_MODULE_COUNT 12

AV_ERROR_DEFINE(BOOTSTRAP_MAX_MODULE_COUNT_EXHASTUED, 0xA701)

typedef struct {
  Container *container;
  Module modules[BOOTSTRAP_MAX_MODULE_COUNT];
  int module_count;
} Bootstrap;

ErrorResult bootstrap_create(Bootstrap *me, Container *container, Module *modules, int count);
ErrorResult bootstrap_create_modules(Bootstrap *me);
ErrorResult bootstrap_delete_modules(Bootstrap *me);
ErrorResult bootstrap_start_modules(Bootstrap *me);
ErrorResult bootstrap_stop_modules(Bootstrap *me);

#endif

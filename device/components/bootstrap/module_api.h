#ifndef MODULE_API_H
#define MODULE_API_H

#include "container.h"

typedef ErrorResult (*ModuleFunction) (Container *container);

typedef struct {
  char *name;
  ModuleFunction create;
  ModuleFunction delete;
  ModuleFunction start;
  ModuleFunction stop_and_wait;
} Module;

#endif

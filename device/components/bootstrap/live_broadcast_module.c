#include "live_broadcast.h"

#include "live_broadcast_module.h"

ErrorResult live_broadcast_module_create(Container *container) {
  MqttClientSettings settings = {
    .uri = CONFIG_MQTT_SERVER_URL,
    .callback = {
      .state_changed = NULL
    }
  };
  
  ErrorResult err = mqtt_client_create(settings, &container->live_broadcast_mqtt_client);
  if (err.code != AV_OK) {
    return err;
  }

  LiveBroadcastConfig config = {
    .task_priority = CONFIG_LIVE_BROADCAST_TASK_PRIORITY,
    .task_cpu = CONFIG_LIVE_BROADCAST_TASK_CPU
  };
  err = live_broadcast_create(config,
                                 container->live_broadcast_mqtt_client,
                                 container->measurement_loop,
                                 &container->live_broadcast);
  if (err.code != AV_OK) {
    mqtt_client_delete(container->live_broadcast_mqtt_client);
  }

  return err;
}

ErrorResult live_broadcast_module_delete(Container *container) {
  live_broadcast_delete(&container->live_broadcast);
  mqtt_client_delete(container->live_broadcast_mqtt_client);

  return AV_OK_RESULT;
}

ErrorResult live_broadcast_module_start(Container *container) {
  return live_broadcast_start(&container->live_broadcast);
}

ErrorResult live_broadcast_module_stop_and_wait(Container *container) {
  live_broadcast_stop_and_wait(&container->live_broadcast);

  return AV_OK_RESULT;
}

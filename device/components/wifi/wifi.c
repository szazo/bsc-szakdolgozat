#include "esp_wifi.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "string.h"
#include "wifi.h"

static const char* TAG = "wifi";

ESP_EVENT_DEFINE_BASE(NETWORK_EVENTS);

static esp_err_t system_event_handler(void *ctx, system_event_t *event);

ErrorResult wifi_create(MessageQueueHandle message_queue, WifiHandle *handle) {

  nvs_flash_init();
  
  Wifi *me = malloc(sizeof(Wifi));
  me->message_queue = message_queue;
  me->phase = WIFI_PHASE_INITIALIZED;
  
  *handle = (WifiHandle) me;

  tcpip_adapter_init();
  esp_err_t err = esp_event_loop_init(system_event_handler, me);
  if (err != ESP_OK) {
    // already initialized, NOP
  }

  return AV_OK_RESULT;
}

void wifi_delete(WifiHandle wifi) {
  Wifi *me = wifi;
  free(me);
}

ErrorResult wifi_init_station(WifiStationSettings *station_settings) {

  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  esp_err_t err = esp_wifi_init(&cfg);
  if (err != ESP_OK) {
    return AV_ERROR_WITH_REASON(WIFI_INIT_STATION_ERROR, err);
  }

  err = esp_wifi_set_mode(WIFI_MODE_STA);
  if (err != ESP_OK) {
    return AV_ERROR_WITH_REASON(WIFI_INIT_STATION_MODE_ERROR, err);
  }

  //  wifi_config_t wifi_config;
  wifi_config_t wifi_config = {
    .sta = {
      .ssid = "",
      .password = ""
    }
  };
  strcpy((char *) wifi_config.sta.ssid, (char *) station_settings->ssid);
  strcpy((char *) wifi_config.sta.password, (char *) station_settings->password);

  err = esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config);
  if (err != ESP_OK) {
    if (err == ESP_ERR_WIFI_PASSWORD) {
      return AV_ERROR_WITH_REASON(WIFI_INIT_STATION_INVALID_PASSWORD, err);
    }

    return AV_ERROR_WITH_REASON(WIFI_INIT_STATION_CONFIG_ERROR, err);
  }

  return AV_OK_RESULT;
}

ErrorResult wifi_deinit(WifiHandle handle) {
  esp_err_t err = esp_wifi_deinit();
  if (err != ESP_OK) {
    return AV_ERROR_WITH_REASON(WIFI_DEINIT_ERROR, err);
  }

  return AV_OK_RESULT;
}

ErrorResult wifi_start(WifiHandle handle) {
  Wifi *me = handle;

  if (me->phase != WIFI_PHASE_INITIALIZED) {
    return AV_ERROR(WIFI_START_INVALID_PHASE);
  }
  
  me->phase = WIFI_PHASE_STARTING;
  esp_err_t err = esp_wifi_start();
  if (err != ESP_OK) {
    me->phase = WIFI_PHASE_INITIALIZED;
    return AV_ERROR_WITH_REASON(WIFI_START_ERROR, err);
  }

  return AV_OK_RESULT;
}

ErrorResult wifi_stop(WifiHandle handle) {
  Wifi *me = handle;  

  if (me->phase != WIFI_PHASE_CONNECTED && me->phase != WIFI_PHASE_STARTING) {
    return AV_ERROR(WIFI_STOP_INVALID_PHASE);
  }

  me->phase = WIFI_PHASE_STOPPING;
  
  esp_err_t err = esp_wifi_disconnect();
  if (err == ESP_FAIL) {
    return AV_ERROR_WITH_REASON(WIFI_STOP_INTERNAL_WIFI_ERROR, err);
  }

  esp_wifi_stop();

  return AV_OK_RESULT;
}

static esp_err_t system_event_handler(void *ctx, system_event_t *event) {

  Wifi *me = (Wifi*) ctx;

  switch (event -> event_id) {

  case SYSTEM_EVENT_STA_START: {
    esp_err_t err = esp_wifi_connect();
    if (err != ESP_OK) {
      av_error_publish_with_reason(SEVERITY_WARNING,
                                   WIFI_START_CONNECT_ERROR, err);
    }
    break;
  }
  case SYSTEM_EVENT_STA_GOT_IP:
    /* ESP_LOGI(TAG, "got ip: %s", */
    /*          ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip)); */

    if (me->phase == WIFI_PHASE_STARTING) {
      me->phase = WIFI_PHASE_CONNECTED;

      message_queue_post(me->message_queue,
                            NETWORK_EVENTS, NETWORK_ESTABLISHED_EVENT);
    }

    break;
  case SYSTEM_EVENT_STA_DISCONNECTED:
    {
      if (me->phase == WIFI_PHASE_STARTING) {
        av_error_publish_with_reason(SEVERITY_DEBUG,
                                     WIFI_DISCONNECTED_WHILE_STARTING, event->event_info.disconnected.reason);
        esp_wifi_connect();

      } else if (me->phase == WIFI_PHASE_CONNECTED) {
        av_error_publish_with_reason(SEVERITY_WARNING,
                                     WIFI_DISCONNECTED_WHILE_CONNECTED, event->event_info.disconnected.reason);

        message_queue_post(me->message_queue,
                              NETWORK_EVENTS, NETWORK_CLOSED_EVENT);

        esp_wifi_connect();
      }
      break;
    }
  case SYSTEM_EVENT_STA_STOP:
    {
      if (me->phase == WIFI_PHASE_STOPPING) {
        me->phase = WIFI_PHASE_INITIALIZED;
      } else {
        av_error_publish(SEVERITY_ERROR, WIFI_STOPPED_INVALID_PHASE);
      }
      break;
    }
  case SYSTEM_EVENT_STA_LOST_IP:
    {
      if (me->phase == WIFI_PHASE_CONNECTED) {
        av_error_publish_with_reason(SEVERITY_WARNING,
                                     WIFI_LOST_IP_WHILE_CONNECTED, event->event_info.disconnected.reason);
      }
      break;
    }
  default:
    break;
  }

  return ESP_OK;
}

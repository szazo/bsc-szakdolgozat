#include "unity.h"
#include "message_queue.h"
#include "message_queue_api.h"
#include "wifi.h"

#define TEST_TAG "[wifi]"

MessageQueueHandle app_loop;
WifiHandle wifi;

static void setup() {
  message_queue_create((MessageQueueConfig) { .task_priority = 8, .task_core_id = APP_CPU_NUM },
                          &app_loop);
  message_queue_start(app_loop);

  ErrorResult err = wifi_create(app_loop, &wifi);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
}

static void tear_down() {
  message_queue_stop_and_wait(app_loop);
  message_queue_delete(app_loop);

  wifi_delete(wifi);
}

TEST_CASE("sta_should_connect", TEST_TAG)
{
  // given
  setup();

  WifiStationSettings settings = {
    .ssid = CONFIG_WIFI_SSID,
    .password = CONFIG_WIFI_PASSWORD
  };
  ErrorResult err = wifi_init_station(&settings);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
  err = wifi_start(wifi);
  TEST_ASSERT_EQUAL(AV_OK, err.code);

  vTaskDelay(pdMS_TO_TICKS(4000));
  err = wifi_stop(wifi);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
  err = wifi_deinit(wifi);
  TEST_ASSERT_EQUAL(AV_OK, err.code);

  // then
  tear_down();
}

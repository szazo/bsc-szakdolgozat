#ifndef WIFI_H
#define WIFI_H

#include "error.h"
#include "message_queue_api.h"

AV_ERROR_DEFINE(WIFI_INIT_STATION_ERROR, 0xB001)
AV_ERROR_DEFINE(WIFI_INIT_STATION_MODE_ERROR, 0xB002)
AV_ERROR_DEFINE(WIFI_INIT_STATION_INVALID_PASSWORD, 0xB003)
AV_ERROR_DEFINE(WIFI_INIT_STATION_CONFIG_ERROR, 0xB004)
AV_ERROR_DEFINE(WIFI_START_ERROR, 0xB006)
AV_ERROR_DEFINE(WIFI_START_INVALID_PHASE, 0xB007)
AV_ERROR_DEFINE(WIFI_START_CONNECT_ERROR, 0xB008)
AV_ERROR_DEFINE(WIFI_STOP_INTERNAL_WIFI_ERROR, 0xB009)
AV_ERROR_DEFINE(WIFI_STOP_INVALID_PHASE, 0xB00A)
AV_ERROR_DEFINE(WIFI_STOPPED_INVALID_PHASE, 0xB00B)
AV_ERROR_DEFINE(WIFI_DEINIT_ERROR, 0xB00C)
AV_ERROR_DEFINE(WIFI_DISCONNECTED_WHILE_STARTING, 0xB00D)
AV_ERROR_DEFINE(WIFI_DISCONNECTED_WHILE_CONNECTED, 0xB00E)
AV_ERROR_DEFINE(WIFI_LOST_IP_WHILE_CONNECTED, 0xB00F)

ESP_EVENT_DECLARE_BASE(NETWORK_EVENTS);

enum {
  NETWORK_ESTABLISHED_EVENT,
  NETWORK_CLOSED_EVENT
};

typedef enum {
  WIFI_PHASE_NONE,
  WIFI_PHASE_INITIALIZED,
  WIFI_PHASE_STARTING,
  WIFI_PHASE_CONNECTED,
  WIFI_PHASE_STOPPING
} WifiPhase;

typedef struct {
  uint8_t ssid[32];
  uint8_t password[64];
} WifiStationSettings;

typedef struct {
  volatile WifiPhase phase;
  MessageQueueHandle message_queue;
} Wifi;

typedef Wifi* WifiHandle;

ErrorResult wifi_create(MessageQueueHandle message_queue, WifiHandle *handle);
void wifi_delete(WifiHandle wifi);
ErrorResult wifi_init_station(WifiStationSettings *station_settings);
ErrorResult wifi_deinit(WifiHandle handle);
ErrorResult wifi_start(WifiHandle handle);
ErrorResult wifi_stop(WifiHandle handle);

#endif

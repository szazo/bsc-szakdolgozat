#ifndef FILENAME_GENERATOR_H
#define FILENAME_GENERATOR_H

typedef struct {
  time_t time;
  uint8_t index;
  char *base_path;
  char *extension;
  int maximum_length;
} FilenameDescriptor;

void find_next_free_index(FilenameDescriptor *desc);
void generate_filename(FilenameDescriptor *desc, char *target);

#endif

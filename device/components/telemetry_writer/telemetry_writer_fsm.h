#ifndef TELEMETRY_WRITER_FSM_H
#define TELEMETRY_WRITER_FSM_H

#include "freertos/FreeRTOS.h"
#include "freertos/ringbuf.h"
#include "error.h"
#include "measurement_loop_api.h"
#include "serializer.h"
#include "filename_generator.h"

AV_ERROR_DEFINE(TELEMETRY_WRITER_SENSOR_RING_CREATE_ERROR, 0x0411)
AV_ERROR_DEFINE(TELEMETRY_WRITER_SENSOR_RING_SEND_ERROR, 0x0412)
AV_ERROR_DEFINE(TELEMETRY_WRITER_COULD_NOT_GENERATE_FILENAME, 0x0413)
AV_ERROR_DEFINE(TELEMETRY_WRITER_SERIALIZE_ERROR, 0x0414)
AV_ERROR_DEFINE(TELEMETRY_WRITER_SERIALIZE_FILE_ERROR, 0x0415)
AV_ERROR_DEFINE(TELEMETRY_WRITER_MEASUREMENT_REGISTER_ERROR, 0x0416)
AV_ERROR_DEFINE(TELEMETRY_WRITER_MEASUREMENT_UNREGISTER_ERROR, 0x0417)

typedef enum {
  TELEMETRY_EVENT_PROCEED,
  TELEMETRY_EVENT_STOP,
} TelemetryWriterFSMEvent;

typedef enum {
  TELEMETRY_PHASE_NONE,
  TELEMETRY_PHASE_INIT,
  TELEMETRY_PHASE_START,
  TELEMETRY_PHASE_ERROR_CREATING_FILE,
  TELEMETRY_PHASE_FILE_CREATED,
  TELEMETRY_PHASE_WRITING,
  TELEMETRY_PHASE_LOST_FILE,
  TELEMETRY_PHASE_CLOSED
} TelemetryWriterFSMPhase;

typedef struct {

  MeasurementLoopAPIHandle measurement_loop;
  SerializerHandle serializer;
  RingbufHandle_t sensor_ring;

  char *mount_point;
  char *extension;

  FilenameDescriptor filename_counter;
  FILE *current_file;
  pb_ostream_t out_stream;
  int file_error_count;

  TelemetryWriterFSMPhase phase;
  TelemetryWriterFSMPhase requested_phase;
} TelemetryWriterFSM;

ErrorResult telemetry_writer_fsm_create(TelemetryWriterFSM *me,
                                        char *mount_point,
                                        char *extension,
                                        MeasurementLoopAPIHandle measurement_loop,
                                        SerializerHandle serializer);
ErrorResult telemetry_writer_fsm_delete(TelemetryWriterFSM *me);
void telemetry_writer_fsm_init(TelemetryWriterFSM *me);
void telemetry_writer_fsm_proceed(TelemetryWriterFSM *me);
void telemetry_writer_fsm_stop(TelemetryWriterFSM *me);
TelemetryWriterFSMPhase telemetry_writer_fsm_phase(TelemetryWriterFSM *me);

#endif


#include "telemetry_writer.h"

static void writer_task(void *arg, TaskHandle task);

ErrorResult telemetry_writer_create(TelemetryWriterConfig config,
                                    TelemetryWriterFSM *fsm,
                                    TelemetryWriterHandle *handle) {
  
  TelemetryWriter *me = malloc(sizeof(TelemetryWriter));
  me->config = config;
  me->fsm = fsm;

  ErrorResult err = task_create(&me->writer_task);
  if (err.code != AV_OK) {
    free(me);
    return AV_ERROR_WITH_REASON(TELEMETRY_WRITER_TASK_CREATE_ERROR, err.code);
  }

  *handle = me;

  return AV_OK_RESULT;
}

ErrorResult telemetry_writer_delete(TelemetryWriterHandle handle) {
  TelemetryWriter *me = handle;

  task_delete(&me->writer_task);
  
  free(me);

  return AV_OK_RESULT;
}

ErrorResult telemetry_writer_start(TelemetryWriterHandle handle) {

  TelemetryWriter *me = handle;

  TaskConfig task_config = {
    .name = "writer_task",
    .priority = me->config.writer_task_priority,
    .core_id = me->config.writer_task_cpu,
    .stack_depth = 4096,
    .task_function = writer_task,
    .task_param = me
  };

  ErrorResult err = task_start(&me->writer_task, &task_config);
  if (err.code != AV_OK) {
    return AV_ERROR_WITH_REASON(TELEMETRY_WRITER_TASK_START_ERROR, err.code);
  }

  return AV_OK_RESULT;
}

void telemetry_writer_stop_and_wait(TelemetryWriterHandle handle) {
  TelemetryWriter *me = handle;

  task_stop_and_wait(&me->writer_task);
}

static void writer_task(void *arg, TaskHandle task) {

  TelemetryWriter *me = arg;

  telemetry_writer_fsm_init(me->fsm);

  while (true) {

    if (task_is_stop_requested(task)) {
      telemetry_writer_fsm_stop(me->fsm);
    }

    if (telemetry_writer_fsm_phase(me->fsm) == TELEMETRY_PHASE_CLOSED) {
      break;
    }

    telemetry_writer_fsm_proceed(me->fsm);
  }
}


#include "unity.h"
#include "telemetry_writer.h"
#include "measurement_loop.h"
#include "sd_storage.h"
#include "../../serializer/test/sample.pb.h"
#include "../../measurement_loop/test/mock_source.h"

#define TEST_TAG "[telemetry_writer]"

static MeasurementLoopHandle measurement_loop;
static SerializerHandle serializer;
static MockSource *mock_source;

typedef struct {
  int number1;
  float number2;
} SampleMessage;

static bool mock_measurement_serialize(const SerializerMessageType message_type,
                                       const void *data,
                                       pb_ostream_t *stream,
                                       void *arg) {

  const SampleMessage *message = (const SampleMessage *) data;
  
  ProtoSampleMessage proto_message = {
    .number1 = message->number1,
    .number2 = message->number2
  };

  bool success = pb_encode_submessage(stream, ProtoSampleMessage_fields, &proto_message);

  return success;
}

#define MESSAGE_BASE 0x1
#define MOCK_SERIALIZER_DESCRIPTOR(MESSAGE_TYPE_BASE, ARG ) { \
    .message_type_base = MESSAGE_TYPE_BASE, \
    .serialize = mock_measurement_serialize, \
    .arg = ARG }

static SerializerDescriptor descriptors[] = {
  MOCK_SERIALIZER_DESCRIPTOR(MESSAGE_BASE, NULL)
};

static void setup() {

  serializer = serializer_create(descriptors, 1);

  mock_source = mock_source_create();
  MeasurementSource descriptor = MOCK_SOURCE(mock_source);
  ErrorResult err = measurement_loop_create(&descriptor, &measurement_loop);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
  measurement_loop_start(measurement_loop);
}

static void teardown() {
  measurement_loop_stop_and_wait(measurement_loop);
  measurement_loop_delete(measurement_loop);
  
  serializer_delete(serializer);
}

TEST_CASE("should_create_and_delete", TEST_TAG)
{
  // given
  setup();

  TelemetryWriterConfig config = {
    .writer_task_priority = CONFIG_TELEMETRY_WRITER_TASK_PRIORITY,
    .writer_task_cpu = CONFIG_TELEMETRY_WRITER_TASK_CPU
  };

  TelemetryWriterFSM fsm;
  telemetry_writer_fsm_create(&fsm, "/sd", ".ttt", measurement_loop, serializer);
  TelemetryWriterHandle telemetry_writer;
  telemetry_writer_create(config, &fsm, &telemetry_writer);

  telemetry_writer_start(telemetry_writer);
  vTaskDelay(pdMS_TO_TICKS(100));

  Measurement *measurement = malloc(sizeof(Measurement) + sizeof(SampleMessage));
  for (int i = 0; i < 1000; i++) {

    set_measurement_header(measurement,
                           42,
                           sensor_get_time(),
                           (MESSAGE_BASE << 8) + 0x01,
                           sizeof(SampleMessage));
    SampleMessage *message = (SampleMessage *)measurement->data;
    message->number1 = i;
    message->number2 = i + 0.42;

    mock_source_send_measurement(mock_source, measurement);
        vTaskDelay(pdMS_TO_TICKS(10));
  }

  telemetry_writer_stop_and_wait(telemetry_writer);

  vTaskDelay(pdMS_TO_TICKS(10));
  
  // then
  telemetry_writer_delete(telemetry_writer);

  // teardown
  teardown();
}

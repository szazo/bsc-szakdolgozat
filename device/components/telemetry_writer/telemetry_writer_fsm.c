#include <time.h>
#include <stdio.h>
#include <errno.h>
#include "proto_callback.h"
#include "measurement_serializer.h"
#include "telemetry_writer_fsm.h"
#include "freertos/task.h"

static const int FILE_RETRY_DELAY_MS = 1000;
static const int RING_BUFFER_SIZE_BYTES = 32768;
static const int RING_BUFFER_SEND_TIMEOUT_TICKS = pdMS_TO_TICKS(100);

static void execute(TelemetryWriterFSM *me, TelemetryWriterFSMEvent event);
static void phase_init(TelemetryWriterFSM *me, TelemetryWriterFSMEvent event);
static void phase_start(TelemetryWriterFSM *me, TelemetryWriterFSMEvent event);
static void phase_error_creating_file(TelemetryWriterFSM *me, TelemetryWriterFSMEvent event);
static void phase_file_created(TelemetryWriterFSM *me, TelemetryWriterFSMEvent event);
static void phase_writing(TelemetryWriterFSM *me, TelemetryWriterFSMEvent event);
static void phase_lost_file(TelemetryWriterFSM *me, TelemetryWriterFSMEvent event);
static void phase_closed(TelemetryWriterFSM *me, TelemetryWriterFSMEvent event);
static ErrorResult start_new_file(FilenameDescriptor *descriptor, FILE **created_file);
static void close_file(TelemetryWriterFSM *me);
static ErrorResult write_measurement(TelemetryWriterFSM *me,
                                     const Measurement *measurement,
                                     pb_ostream_t *out_stream);
static ErrorResult register_measurement_loop(TelemetryWriterFSM *me);
static ErrorResult unregister_measurement_loop(TelemetryWriterFSM *me);
static void measurement_handler(const Measurement *measurement, void *arg);
static void transition(TelemetryWriterFSM *me, TelemetryWriterFSMPhase next_phase);

ErrorResult telemetry_writer_fsm_create(TelemetryWriterFSM *me,
                                        char *mount_point,
                                        char *extension,
                                        MeasurementLoopAPIHandle measurement_loop,
                                        SerializerHandle serializer) {

  me->measurement_loop = measurement_loop;
  me->serializer = serializer;
  me->mount_point = mount_point;
  me->extension = extension;

  me->sensor_ring = xRingbufferCreate(RING_BUFFER_SIZE_BYTES,
                                      RINGBUF_TYPE_NOSPLIT);
  if (me->sensor_ring == NULL) {
    return AV_ERROR(TELEMETRY_WRITER_SENSOR_RING_CREATE_ERROR);
  }

  telemetry_writer_fsm_init(me);

  return AV_OK_RESULT;
}

ErrorResult telemetry_writer_fsm_delete(TelemetryWriterFSM *me) {
  vRingbufferDelete(me->sensor_ring);  

  return AV_OK_RESULT;
}

void telemetry_writer_fsm_init(TelemetryWriterFSM *me) {

  // reset the buffer (it may container measurement from previous run)
  size_t item_size;
  void *item;

  while ((item = xRingbufferReceive(me->sensor_ring, &item_size, 0)) != NULL) {
      vRingbufferReturnItem(me->sensor_ring, item);
  }

  me->phase = TELEMETRY_PHASE_INIT;
}

void telemetry_writer_fsm_proceed(TelemetryWriterFSM *me) {
  execute(me, TELEMETRY_EVENT_PROCEED);
}

void telemetry_writer_fsm_stop(TelemetryWriterFSM *me) {
  execute(me, TELEMETRY_EVENT_STOP);
}

TelemetryWriterFSMPhase telemetry_writer_fsm_phase(TelemetryWriterFSM *me) {
  return me->phase;
}

static void execute(TelemetryWriterFSM *me, TelemetryWriterFSMEvent event) {

  me->requested_phase = me->phase;
  
  switch (me->phase) {
  case TELEMETRY_PHASE_NONE:
    // NOP
    break;
  case TELEMETRY_PHASE_INIT:
    phase_init(me, event);
    break;
  case TELEMETRY_PHASE_START:
    phase_start(me, event);
    break;
  case TELEMETRY_PHASE_ERROR_CREATING_FILE:
    phase_error_creating_file(me, event);
    break;
  case TELEMETRY_PHASE_FILE_CREATED:
    phase_file_created(me, event);
    break;
  case TELEMETRY_PHASE_WRITING:
    phase_writing(me, event);
    break;
  case TELEMETRY_PHASE_LOST_FILE:
    phase_lost_file(me, event);
    break;
  case TELEMETRY_PHASE_CLOSED:
    phase_closed(me, event);
    break;
  }

  if (me->requested_phase != me->phase) {
    me->phase = me->requested_phase;
    me->requested_phase = me->phase;
  }
}

static void phase_init(TelemetryWriterFSM *me, TelemetryWriterFSMEvent event) {
  if (event == TELEMETRY_EVENT_STOP) {
    transition(me, TELEMETRY_PHASE_CLOSED);
    return;
  }

  if (event != TELEMETRY_EVENT_PROCEED) { return; }

  time_t start_time = time(NULL);

  me->filename_counter = (FilenameDescriptor) {
    .time = start_time,
    .index = 0,
    .base_path = me->mount_point,
    .extension = me->extension,
    .maximum_length = CONFIG_MAXIMUM_FILENAME_LENGTH
  };

  transition(me, TELEMETRY_PHASE_START);
}

static void phase_start(TelemetryWriterFSM *me, TelemetryWriterFSMEvent event) {

  if (event == TELEMETRY_EVENT_STOP) {
    transition(me, TELEMETRY_PHASE_CLOSED);
    return;
  }

  if (event != TELEMETRY_EVENT_PROCEED) { return; }

  ErrorResult err = start_new_file(&me->filename_counter, &me->current_file);
  if (err.code != AV_OK) {
    av_error_publish_result(SEVERITY_WARNING, err);
    transition(me, TELEMETRY_PHASE_ERROR_CREATING_FILE);
  } else {
    transition(me, TELEMETRY_PHASE_FILE_CREATED);
  }
}

static void phase_error_creating_file(TelemetryWriterFSM *me, TelemetryWriterFSMEvent event) {

  if (event == TELEMETRY_EVENT_STOP) {
    transition(me, TELEMETRY_PHASE_CLOSED);
    return;
  }

  if (event != TELEMETRY_EVENT_PROCEED) { return; }

  vTaskDelay(pdMS_TO_TICKS(FILE_RETRY_DELAY_MS));
  ErrorResult err = start_new_file(&me->filename_counter, &me->current_file);
  if (err.code == AV_OK) {
    transition(me, TELEMETRY_PHASE_FILE_CREATED);
  }
}

static void phase_file_created(TelemetryWriterFSM *me, TelemetryWriterFSMEvent event) {
  if (event == TELEMETRY_EVENT_STOP) {
    close_file(me);
    
    transition(me, TELEMETRY_PHASE_CLOSED);
    return;
  }

  if (event != TELEMETRY_EVENT_PROCEED) { return; }

  // start the protobuf stream
  me->out_stream.callback = &proto_write_file_callback,
  me->out_stream.max_size = SIZE_MAX,
  me->out_stream.state = me->current_file;
  me->out_stream.bytes_written = 0;
  me->out_stream.errmsg = 0;

  me->file_error_count = 0;

  register_measurement_loop(me); // if not success, file will be empty
  transition(me, TELEMETRY_PHASE_WRITING);
}

static void phase_writing(TelemetryWriterFSM *me, TelemetryWriterFSMEvent event) {

  if (event == TELEMETRY_EVENT_STOP) {
    unregister_measurement_loop(me);
    close_file(me);

    transition(me, TELEMETRY_PHASE_CLOSED);
    return;
  }

  if (event != TELEMETRY_EVENT_PROCEED) { return; }

  size_t item_size;
  Measurement *item = xRingbufferReceive(me->sensor_ring, &item_size, pdMS_TO_TICKS(10));
  if (item == NULL) {
    return;
  }

  ErrorResult write_err = write_measurement(me, item, &me->out_stream);
               
  vRingbufferReturnItem(me->sensor_ring, item);

  if (write_err.code == AV_OK) {
    me->file_error_count = 0;
    return;
  }
        
  if (write_err.code == TELEMETRY_WRITER_SERIALIZE_FILE_ERROR) {
    me->file_error_count++;
    if (me->file_error_count > 10) {
      // we lost the file
      transition(me, TELEMETRY_PHASE_LOST_FILE);
      return;
    }
  } else {
    av_error_publish_with_reason(SEVERITY_ERROR, write_err.code, write_err.reason);
  }
}

static void phase_lost_file(TelemetryWriterFSM *me, TelemetryWriterFSMEvent event) {

  unregister_measurement_loop(me);
  close_file(me);

  if (event == TELEMETRY_EVENT_STOP) {
    transition(me, TELEMETRY_PHASE_CLOSED);
    return;
  }

  if (event != TELEMETRY_EVENT_PROCEED) { return; }

  // try again with new file
  transition(me, TELEMETRY_PHASE_START);
}

static void phase_closed(TelemetryWriterFSM *me, TelemetryWriterFSMEvent event) {
  // NOP
}

static ErrorResult register_measurement_loop(TelemetryWriterFSM *me) {
  ErrorResult err = measurement_loop_register(me->measurement_loop, measurement_handler, me);
  if (err.code != AV_OK) {
    av_error_publish_with_reason(SEVERITY_ERROR, TELEMETRY_WRITER_MEASUREMENT_REGISTER_ERROR, err.code);
  }

  return err;
}

static ErrorResult unregister_measurement_loop(TelemetryWriterFSM *me) {
  ErrorResult err = measurement_loop_unregister(me->measurement_loop, measurement_handler, me);
  if (err.code != AV_OK) {
    av_error_publish_with_reason(SEVERITY_ERROR, TELEMETRY_WRITER_MEASUREMENT_UNREGISTER_ERROR, err.code);
  }

  return err;
}

static void measurement_handler(const Measurement *measurement, void *arg) {

  TelemetryWriterFSM *me = arg;

  UBaseType_t result = xRingbufferSend(me->sensor_ring,
                                       measurement,
                                       sizeof(Measurement) + measurement->size,
                                       RING_BUFFER_SEND_TIMEOUT_TICKS);

  if (result != pdTRUE) {
    av_error_publish(SEVERITY_ERROR, TELEMETRY_WRITER_SENSOR_RING_SEND_ERROR);
  }
}

static void close_file(TelemetryWriterFSM *me) {
  funlockfile(me->current_file);
  fclose(me->current_file);
}

static ErrorResult start_new_file(FilenameDescriptor *descriptor, FILE **created_file) {
  find_next_free_index(descriptor);

  char filename[descriptor->maximum_length];
  generate_filename(descriptor, filename);

  FILE *f = fopen(filename, "wb");
  flockfile(f);
  printf("CREATE FILE: %s\n", filename);
  if (f == NULL) {
    return AV_ERROR_WITH_REASON(TELEMETRY_WRITER_COULD_NOT_GENERATE_FILENAME, errno);
  }

  *created_file = f;

  return AV_OK_RESULT;
}

static ErrorResult write_measurement(TelemetryWriterFSM *me,
                                     const Measurement *measurement,
                                     pb_ostream_t *out_stream) {

  ErrorResult result = measurement_serializer_serialize(me->serializer, measurement, out_stream);
  FILE *file = out_stream->state;
  int file_error = ferror(file);
  if (file_error != 0) {
    return AV_ERROR_WITH_REASON(TELEMETRY_WRITER_SERIALIZE_FILE_ERROR, file_error);
  }

  if (result.code != AV_OK) {
    return AV_ERROR_WITH_REASON(TELEMETRY_WRITER_SERIALIZE_ERROR, result.reason);
  }

  fflush(file);
  return AV_OK_RESULT;
}

static void transition(TelemetryWriterFSM *me, TelemetryWriterFSMPhase next_phase) {
  printf("switch phase to %d\n", next_phase);
  me->requested_phase = next_phase;
}

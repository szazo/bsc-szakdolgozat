#ifndef TELEMETRY_WRITER_H
#define TELEMETRY_WRITER_H

#include "error.h"
#include "stoppable_task.h"
#include "telemetry_writer_fsm.h"

AV_ERROR_DEFINE(TELEMETRY_WRITER_TASK_CREATE_ERROR, 0x0401)
AV_ERROR_DEFINE(TELEMETRY_WRITER_TASK_START_ERROR, 0x0402)

#define FILENAME_GENERATOR_COULD_NOT_CREATE_FILE_ERROR (FILENAME_GENERATOR_ERR_BASE + 0x0001)

typedef struct {
  uint8_t writer_task_priority;
  TaskCPU writer_task_cpu;
} TelemetryWriterConfig;

typedef struct {
  TelemetryWriterConfig config;
  Task writer_task;
  TelemetryWriterFSM *fsm;
} TelemetryWriter;

typedef TelemetryWriter* TelemetryWriterHandle;

ErrorResult telemetry_writer_create(TelemetryWriterConfig config,
                                    TelemetryWriterFSM *fsm,
                                    TelemetryWriterHandle *handle);
ErrorResult telemetry_writer_delete(TelemetryWriterHandle handle);

ErrorResult telemetry_writer_start(TelemetryWriterHandle handle);
void telemetry_writer_stop_and_wait(TelemetryWriterHandle handle);

#endif

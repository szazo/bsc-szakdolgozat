#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <sys/stat.h>

#include "filename_generator.h"

static const int FILENAME_TIME_LENGHT = (14 + 1);
static const char *FILENAME_TIME_FORMAT = "%Y%m%d%H%M%S";

void find_next_free_index(FilenameDescriptor *desc) {

  char filename[desc->maximum_length];
  do {
    generate_filename(desc, filename);

    struct stat st;
    if (stat(filename, &st) == 0) {
      desc->index++;
      continue;
    }

    break;
  } while (true);
}

void generate_filename(FilenameDescriptor *desc, char *target) {

  struct tm * local_time = localtime ( &desc->time );

  char time_text[FILENAME_TIME_LENGHT];
  strftime(time_text, FILENAME_TIME_LENGHT, FILENAME_TIME_FORMAT, local_time);

  char index_text[10] = "";
  if (desc->index > 0) {
    snprintf(index_text, 10, "_%d", desc->index);
  }

  snprintf(target, desc->maximum_length, "%s/av%s%s%s",
           desc->base_path, time_text, index_text, desc->extension);
}

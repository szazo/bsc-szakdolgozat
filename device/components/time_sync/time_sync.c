#include <sys/cdefs.h>
#include <time.h>
#include <sys/time.h>
#include "esp_log.h"
#include "gps_sensor.h"
#include "time_sync.h"

static const char *TAG = "time_sync";

static void update_time(struct minmea_date *date, struct minmea_time *time);
static void measurement_handler(const Measurement *measurement, void *arg);
static ErrorResult measurement_register(TimeSync *me);
static ErrorResult measurement_unregister(TimeSync *me);

ErrorResult time_sync_create(TimeSync *me, MeasurementLoopAPIHandle measurement_loop) {
  me->measurement_loop = measurement_loop;

  return AV_OK_RESULT;
}

ErrorResult time_sync_delete(TimeSync *me) {
  return AV_OK_RESULT;
}

ErrorResult time_sync_start(TimeSync *me) {
  return measurement_register(me);
}

ErrorResult time_sync_stop(TimeSync *me) {
  measurement_unregister(me);

  return AV_OK_RESULT;
}

static void measurement_handler(const Measurement *measurement, void *arg) {

  TimeSync *me = arg;

  int base_type = DECODE_MEASUREMENT_BASE_TYPE(measurement->message_type);
  if (base_type != GPS_MESSAGE_BASE) {
    return;
  }

  char type = DECODE_MEASUREMENT_TYPE(measurement->message_type);
  GpsData *data = (GpsData *) measurement->data;
  switch (type) {
  case MINMEA_SENTENCE_RMC:
    {
      struct minmea_sentence_rmc *rmc = &(data->u.rmc);
      if (!rmc->valid) {
        return;
      }

      update_time(&rmc->date, &rmc->time);
      measurement_unregister(me);

      break; 
    }
  }
}

static void update_time(struct minmea_date *input_date, struct minmea_time *input_time) {

  struct timeval systime;
  struct tm tm;

  tm.tm_sec = input_time->seconds;
  tm.tm_min = input_time->minutes;
  tm.tm_hour = input_time->hours;
  tm.tm_mday = input_date->day;
  tm.tm_mon = input_date->month - 1;
  tm.tm_year = input_date->year + 2000 - 1900;

  systime.tv_sec = mktime(&tm);
  systime.tv_usec = 0;

  settimeofday(&systime, NULL);

  time_t now = time(NULL);
  struct tm *localtm = localtime(&now);
  ESP_LOGI(TAG, "Time set using GPS time: %s", asctime(localtm));
}

static ErrorResult measurement_register(TimeSync *me) {
  return measurement_loop_register(me->measurement_loop, measurement_handler, me);
}

static ErrorResult measurement_unregister(TimeSync *me) {
  return measurement_loop_unregister(me->measurement_loop, measurement_handler, me);
}



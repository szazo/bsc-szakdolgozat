#ifndef TIME_SYNC_FSM_H
#define TIME_SYNC_FSM_H

#include "measurement_loop.h"

typedef struct {  
  MeasurementLoopAPIHandle measurement_loop;
} TimeSync;

ErrorResult time_sync_create(TimeSync *me, MeasurementLoopAPIHandle measurement_loop);
ErrorResult time_sync_delete(TimeSync *me);
ErrorResult time_sync_start(TimeSync *me);
ErrorResult time_sync_stop(TimeSync *me);

#endif


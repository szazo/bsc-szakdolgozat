#include "esp_log.h"
#include "freertos/FreeRTOS.h"

#include "sensor_governor.h"
#include "sensor_api.h"

/**
 * The size of the queue which will store measurement requests from the
 * sensors.
 */
static const int SENSOR_REQUEST_QUEUE_SIZE = 40;

/**
 * The stack size for the sensor task which will read actual measurements from the sensors.
 */
static const int SENSOR_TASK_STACK_SIZE = 3 * 1024;

/**
 * The size of the ring buffer which will store the actual measurements.
 */
static const int SENSOR_RING_BUFFER_SIZE_BYTES = 8192;

/**
 * Timeout for sending actual measurement to the ring buffer.
 */
static const int RING_BUFFER_SEND_TIMEOUT_TICKS = pdMS_TO_TICKS(10);

/**
 * Stop bit for the task.
 */
const static int STOPPED_BIT = BIT0;

/**
 * This sensor id will be sent to the sensor_request_queue and
 * used the release the lock and stop the task.
 */
const static SensorId STOP_SENSOR_ID = -1;

/**
 * The logging tag.
 */
static const char *TAG = "governor";

static bool create_request_queue(SensorGovernor *me);
static void delete_request_queue(SensorGovernor *me);
static bool create_sensor_ring(SensorGovernor *me);
static void delete_sensor_ring(SensorGovernor *me);
static void sensor_task(void *param);
static void place_measurement_on_the_ring(SensorGovernor *me, Measurement *measurement);
static void stop(GovernorHandle handle);
static void wait_stop(GovernorHandle handle);

void sensor_send_measurement(SensorApiHandle api, Measurement *measurement) {
  SensorGovernor *me = api;

  place_measurement_on_the_ring(me, measurement);
}

Measurement *governor_read_measurement(GovernorHandle governor, TickType_t ticks_to_wait) {

  SensorGovernor *me = governor;

  size_t item_size;
  Measurement *item = xRingbufferReceive(me->sensor_ring, &item_size, ticks_to_wait);
  if (item == NULL) {
    return NULL;
  }

  if (sizeof(Measurement) + item->size != item_size) {
    // TODO: error handling
    ESP_LOGE(TAG, "Message size mismatch, aborting");
    abort();
  }

  return item;
}

void governor_return_measurement(GovernorHandle governor, Measurement *measurement) {

  SensorGovernor *me = governor;
  vRingbufferReturnItem(me->sensor_ring, measurement);
}

inline IRAM_ATTR SensorTime sensor_get_time() {
  return esp_timer_get_time();
}

ErrorResult governor_create(SensorDescriptor *sensors, int count, GovernorHandle *handle) {

  // we don't copy the actual descriptor structs, only references to them
  SensorGovernor *me = malloc(sizeof(SensorGovernor));
  for (int i = 0; i < count; i++) {
    me->sensors[i] = sensors[i];
  }
  me->sensor_count = count;
  me->sensor_task_handle = NULL;
  me->stopped_event_group = xEventGroupCreate();
  
  if (!create_sensor_ring(me)) {
    free(me);
    return AV_ERROR(SENSOR_GOVERNOR_CREATE_RING_ERROR);
  }

  if (!create_request_queue(me)) {
    delete_sensor_ring(me);
    free(me);
    return AV_ERROR(SENSOR_GOVERNOR_CREATE_QUEUE_ERROR);
  }

  bool sensor_result;
  for (int i = 0; i < count; i++) {
    SensorDescriptor descriptor = sensors[i];

    assert(descriptor.sensor.init != NULL);
    assert(descriptor.sensor.deinit != NULL);

    sensor_result = descriptor.sensor.init(me, descriptor.id, descriptor.arg);
    if (!sensor_result) {
      // deinit already initialized sensors
      for (int j = 0; j < i; j++) {
        sensors[j].sensor.deinit(sensors[j].arg);
      }

      free(me);
      return AV_ERROR(SENSOR_GOVERNOR_CREATE_SENSOR_INIT_ERROR);
    }
  }

  *handle = me;
  return AV_OK_RESULT;
}

void governor_delete(GovernorHandle governor) {
  SensorGovernor *me = governor;

  for (int i = 0; i < me->sensor_count; i++) {
    me->sensors[i].sensor.deinit(me->sensors[i].arg);
  }

  delete_sensor_ring(me);
  delete_request_queue(me);
  vEventGroupDelete(me->stopped_event_group);
  free(governor);
}

ErrorResult governor_start(GovernorHandle handle) {

  SensorGovernor *me = handle;

  BaseType_t task_result = xTaskCreatePinnedToCore(sensor_task, "sensor_task", SENSOR_TASK_STACK_SIZE,
                                                   me, CONFIG_SENSOR_GOVERNOR_TASK_PRIORITY,
                                                   &me->sensor_task_handle, CONFIG_SENSOR_GOVERNOR_TASK_CPU);

  if (task_result != pdPASS) {
    return AV_ERROR(SENSOR_GOVERNOR_START_ERROR);
  }

  // start sensors
  for (int i = 0; i < me->sensor_count; i++) {
    SensorDescriptor descriptor = me->sensors[i];
    if (descriptor.sensor.start == NULL) {
      continue;
    }

    bool start_result = descriptor.sensor.start(descriptor.arg);
    if (!start_result) {
      // stop already started sensors
      for (int j = 0; j < i; j++) {
        me->sensors[j].sensor.stop_and_wait(me->sensors[j].arg);
      }

      return AV_ERROR(SENSOR_GOVERNOR_SENSOR_START_ERROR);
    }
  }

  return AV_OK_RESULT;
}

void governor_stop_and_wait(GovernorHandle handle) {

  SensorGovernor *me = handle;
  for (int i = 0; i < me->sensor_count; i++) {
    SensorDescriptor descriptor = me->sensors[i];
    if (descriptor.sensor.stop_and_wait == NULL) {
      continue;
    }

    descriptor.sensor.stop_and_wait(descriptor.arg);
  }
  
  stop(handle);
  wait_stop(handle);
}

static void stop(GovernorHandle handle) {
  SensorGovernor *me = handle;

  // post stop sensor id to the requires queue
  SensorId stop_id = STOP_SENSOR_ID;
  xQueueReset(me->sensor_request_queue);
  while (xQueueSendToFront(me->sensor_request_queue,
                           &stop_id,
                           pdMS_TO_TICKS(10)) != pdTRUE) {
    ESP_LOGW(TAG, "Could not send stop signal on the request queue, retrying...");
  }
}

static void wait_stop(GovernorHandle handle) {
  SensorGovernor *me = handle;

  xEventGroupWaitBits(me->stopped_event_group,
                      STOPPED_BIT,
                      false,
                      true, portMAX_DELAY);
}

static void sensor_task(void *param) {
  SensorGovernor *me = param;

  while (true) {

    SensorId id;
    if (xQueueReceive(me->sensor_request_queue, &id, portMAX_DELAY) == pdFALSE) {
      continue;
    }

    if (id == STOP_SENSOR_ID) {
      // stop requested
      break;
    }

    // read data from the sensor

    // TODO: quicker way to find the sensor???
    int i = 0;
    SensorDescriptor *descriptor;
    do {
      descriptor = &me->sensors[i];
      if (descriptor->id == id) {
        break;
      }
      i++;
    } while (i < me->sensor_count);

    if (i >= me->sensor_count) {
      ESP_LOGE(TAG, "Sensor not found with id: %d", id);
      abort();
    }

    if (descriptor->sensor.read == NULL) {
      av_error_publish(SEVERITY_ERROR, SENSOR_GOVERNOR_READ_MISSING);
      continue;
    }

    if (descriptor->sensor.read_return == NULL) {
      av_error_publish(SEVERITY_ERROR, SENSOR_GOVERNOR_READ_RETURN_MISSING);
      continue;
    }

    Measurement *measurement = descriptor->sensor.read(descriptor->arg);
    if (measurement != NULL) {
      place_measurement_on_the_ring(me, measurement);
      descriptor->sensor.read_return(measurement, descriptor->arg);
    }
  }

  xEventGroupSetBits(me->stopped_event_group, STOPPED_BIT);
  vTaskDelete(NULL);
}

void sensor_send_request(SensorApiHandle api, SensorId id) {
  SensorGovernor *me = api;

  
  BaseType_t result = xQueueSend(me->sensor_request_queue, &id, pdMS_TO_TICKS(10));
  if (result != pdTRUE) {
    av_error_publish_with_reason(SEVERITY_ERROR, SENSOR_GOVERNOR_SEND_REQUEST_QUEUE_ERROR, result);
  }
}

IRAM_ATTR void sensor_send_request_from_isr(SensorApiHandle api, SensorId id) {

  SensorGovernor *me = api;

  BaseType_t is_higher_priority_task_woken;
  BaseType_t result = xQueueSendFromISR(me->sensor_request_queue,
                                         &id, &is_higher_priority_task_woken);
  if (result != pdTRUE) {
    av_error_publish_with_reason(SEVERITY_ERROR, SENSOR_GOVERNOR_SEND_REQUEST_FROM_ISR_QUEUE_ERROR, result);
  }

  if (is_higher_priority_task_woken) {
    portYIELD_FROM_ISR();
  }
}

static void place_measurement_on_the_ring(SensorGovernor *me, Measurement *measurement) {

  UBaseType_t result = xRingbufferSend(me->sensor_ring,
                                       measurement,
                                       sizeof(Measurement) + measurement->size,
                                       RING_BUFFER_SEND_TIMEOUT_TICKS);

  if (result != pdTRUE) {
    av_error_publish(SEVERITY_ERROR, SENSOR_GOVERNOR_SEND_RING_ERROR);
    // TODO: remove log
    printf("Free size: %zu  size: %zu  max size: %zu \n", xRingbufferGetCurFreeSize(me->sensor_ring), measurement->size, xRingbufferGetMaxItemSize(me->sensor_ring));
  }
}

static inline bool create_sensor_ring(SensorGovernor *me) {

  me->sensor_ring = xRingbufferCreate(SENSOR_RING_BUFFER_SIZE_BYTES,
                                               RINGBUF_TYPE_NOSPLIT);
  if (me->sensor_ring == NULL) {
    ESP_LOGE(TAG, "Error creating sensor ring");
    return false;
  }

  return true;
}

static void delete_sensor_ring(SensorGovernor *me) {
  vRingbufferDelete(me->sensor_ring);  
}

static inline bool create_request_queue(SensorGovernor *me) {

  me->sensor_request_queue = xQueueCreate(SENSOR_REQUEST_QUEUE_SIZE, sizeof(SensorId));  

  if (me->sensor_request_queue == NULL) {
    ESP_LOGE(TAG, "Error creating sensor request queue.");
    return false;
  }

  return true;
}

static void delete_request_queue(SensorGovernor *me) {
  vQueueDelete(me->sensor_request_queue);
}

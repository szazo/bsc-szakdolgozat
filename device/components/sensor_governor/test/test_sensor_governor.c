#include "unity.h"
#include "esp_log.h"

#include "sensor_governor.h"

static const char *TAG = "test_sensor_governor";
#define TEST_TAG "[sensor_governor]"

typedef struct {
  union {
    int64_t type1;
    uint8_t type2;
  } u;
} MockSensorMeasurement;

#define MOCK_MESSAGE_SIZE sizeof(Measurement) + sizeof(MockSensorMeasurement)

typedef struct {
  void *init_arg;
  SensorApiHandle handle;
  SensorId id;
  bool init_should_return_false;
  bool init_called;
  bool deinit_called;
  bool start_called;
  bool stop_called;
  bool read_called;
  bool read_return_called;
  /**
   * We do not malloc, use preoccupied buffer for sending a measurement
   */
  char measurement_buffer[MOCK_MESSAGE_SIZE];
  Measurement *next_read_measurement;
} MockSensor;

typedef enum {
  MOCK_MSG1 = 1,
  MOCK_MSG2 = 2
} MockSensorMessage;

static void mock_set_measurement(MockSensor *me, MockSensorMessage type, MockSensorMeasurement mock_measurement,
                                 Measurement *measurement) {

  measurement->time = sensor_get_time();
  measurement->sensor_id = me->id;
  measurement->message_type = type;
  measurement->size = sizeof(MockSensorMeasurement);

  MockSensorMeasurement * data = (MockSensorMeasurement *) measurement->data;
  data->u = mock_measurement.u;
}

static Measurement *mock_create_measurement(MockSensor *me,
                                    MockSensorMessage type, MockSensorMeasurement mock_measurement) {

  Measurement *measurement = (Measurement *) me->measurement_buffer;
  mock_set_measurement(me, type, mock_measurement, measurement);
  return measurement;
}

/* static void mock_send_sensor_data(MockSensor *me, ) { */


/*   sensor_send_measurement(me->handle, measurement); */
/* } */

static bool mock_init(const SensorApiHandle handle, const SensorId id, void *arg) {
  MockSensor *me = arg;
  me->init_called = true;
  me->handle = handle;
  me->id = id;

  if (me->init_should_return_false) {
    return false;
  }

  return true;
}

static void mock_deinit(void *arg) {
  MockSensor *me = arg;
  me->deinit_called = true;
}

static bool mock_start(void *arg) {
  MockSensor *me = arg;
  me->start_called = true;

  return true;
}

static void mock_stop(void *arg) {
  MockSensor *me = arg;
  me->stop_called = true;
}

static Measurement *mock_read(void *arg) {

  MockSensor *me = arg;

  me->read_called = true;
  return me->next_read_measurement;
}

static void mock_read_return(Measurement *measurement, void *arg) {
  // do nothing, we use fix buffer
  MockSensor *me = arg;
  me->read_return_called = true;
}

/* static void mock_descriptor_create(SensorDescriptor *descriptor, SensorId id, MockSensor *sensor) { */
/*   descriptor->init = mock_init; */
/*   descriptor->id = id; */
/*   descriptor->arg = sensor; */
//}

#define MockSensor_zero { NULL, NULL, 0, false, false, false, false, false, false, false, {0}, NULL };
#define MOCK_SENSOR() { mock_init, mock_deinit, mock_start, mock_stop, mock_read, mock_read_return }
#define MockSensor_descriptor(ID, SENSOR) { ID, MOCK_SENSOR(), SENSOR }

/* TEST_SETUP() */
/* { */
/*   ESP_LOGI(TAG, "Test setup"); */
/* } */

/* TEST_TEAR_DOWN() */
/* { */
/*   ESP_LOGI(TAG, "TearDown"); */
/* } */

TEST_CASE("create should init sensors", TEST_TAG)
{
  // given
  MockSensor sensor1 = MockSensor_zero;
  MockSensor sensor2 = MockSensor_zero;
  
  /* SensorDescriptor sensor_descriptors[2]; */
  /* mock_descriptor_create(&sensor_descriptors[0], 1, &sensor1); */
  /* mock_descriptor_create(&sensor_descriptors[1], 2, &sensor2); */

  SensorDescriptor sensor_descriptors[2] = {
    MockSensor_descriptor(1, &sensor1),
    MockSensor_descriptor(2, &sensor2)
  };

  // when
  GovernorHandle governor;
  ErrorResult err  = governor_create(sensor_descriptors, 2, &governor);
  TEST_ASSERT_EQUAL(AV_OK, err.code);

  // then
  TEST_ASSERT_TRUE(sensor1.init_called);
  TEST_ASSERT_TRUE(sensor2.init_called);
  TEST_ASSERT_NOT_NULL(governor);

  governor_delete(governor);
}

TEST_CASE("create should return false and deinit others if sensor init failed", TEST_TAG) {

  // given
  MockSensor sensor1 = MockSensor_zero;
  MockSensor sensor2 = MockSensor_zero;
  sensor2.init_should_return_false = true;
  enum N { SENSOR_COUNT = 2 };
  SensorDescriptor sensor_descriptors[SENSOR_COUNT] = {
    MockSensor_descriptor(1, &sensor1),
    MockSensor_descriptor(2, &sensor2)
  };

  // when
  GovernorHandle governor;
  ErrorResult err  = governor_create(sensor_descriptors, 2, &governor);
  TEST_ASSERT_EQUAL(SENSOR_GOVERNOR_CREATE_SENSOR_INIT_ERROR, err.code);

  // then
  TEST_ASSERT_NULL(governor);
  TEST_ASSERT_TRUE(sensor1.deinit_called);
}

TEST_CASE("governor_delete should deinit sensors", TEST_TAG)
{
  // given
  MockSensor sensor = MockSensor_zero;
  enum N { SENSOR_COUNT = 1 };
  SensorDescriptor sensor_descriptors[SENSOR_COUNT] = {
    MockSensor_descriptor(1, &sensor)
  };

  GovernorHandle governor;
  ErrorResult err  = governor_create(sensor_descriptors, SENSOR_COUNT, &governor);
  TEST_ASSERT_EQUAL(AV_OK, err.code);

  // when
  governor_delete(governor);

  // then
  TEST_ASSERT_TRUE(sensor.deinit_called);
}

TEST_CASE("sensor_send_measurement should place on ring buffer", "[sensor_governor]") {

  // given
  MockSensor sensor = MockSensor_zero;
  enum N { SENSOR_COUNT = 1 };
  SensorDescriptor sensor_descriptors[SENSOR_COUNT] = {
    MockSensor_descriptor(1, &sensor)
  };

  GovernorHandle governor;
  ErrorResult err  = governor_create(sensor_descriptors, SENSOR_COUNT, &governor);
  TEST_ASSERT_EQUAL(AV_OK, err.code);

  // when
  MockSensorMeasurement measurement;
  measurement.u.type1 = 64;

  Measurement *to_send = mock_create_measurement(&sensor, MOCK_MSG1, measurement);
  
  sensor_send_measurement(governor, to_send);
  //mock_send_sensor_data(&sensor, MOCK_MSG1, measurement);
  
  // then
  Measurement *read_back = governor_read_measurement(governor, pdMS_TO_TICKS(1000));
  TEST_ASSERT_EQUAL_MEMORY(to_send, read_back, sizeof(Measurement) + sizeof(MockSensorMeasurement));

  governor_return_measurement(governor, read_back);
  governor_delete(governor);
}

TEST_CASE("start should start sensors", "[sensor_governor]")
{
  // given
  MockSensor sensor1 = MockSensor_zero;
  MockSensor sensor2 = MockSensor_zero;
  enum N { SENSOR_COUNT = 2 };
  SensorDescriptor sensor_descriptors[SENSOR_COUNT] = {
    MockSensor_descriptor(1, &sensor1),
    MockSensor_descriptor(2, &sensor2)
  };

  GovernorHandle governor;
  ErrorResult err  = governor_create(sensor_descriptors, SENSOR_COUNT, &governor);
  TEST_ASSERT_EQUAL(AV_OK, err.code);

  // when
  governor_start(governor);

  // then
  TEST_ASSERT_TRUE(sensor1.start_called);
  TEST_ASSERT_TRUE(sensor2.start_called);
  governor_delete(governor);

}

TEST_CASE("stop should stop sensors", "[sensor_governor]")
{
  // given
  MockSensor sensor1 = MockSensor_zero;
  MockSensor sensor2 = MockSensor_zero;
  enum N { SENSOR_COUNT = 2 };
  SensorDescriptor sensor_descriptors[SENSOR_COUNT] = {
    MockSensor_descriptor(1, &sensor1),
    MockSensor_descriptor(2, &sensor2)
  };

  GovernorHandle governor;
  ErrorResult err  = governor_create(sensor_descriptors, SENSOR_COUNT, &governor);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
  governor_start(governor);

  // when
  governor_stop_and_wait(governor);

  // then
  TEST_ASSERT_TRUE(sensor1.stop_called);
  TEST_ASSERT_TRUE(sensor2.stop_called);
  governor_delete(governor);
}

TEST_CASE("start and stop should not leak memory", "[sensor_governor]")
{
  // given
  MockSensor sensor = MockSensor_zero;
  enum N { SENSOR_COUNT = 1 };
  SensorDescriptor sensor_descriptors[SENSOR_COUNT] = {
    MockSensor_descriptor(1, &sensor)
  };

  GovernorHandle governor;
  ErrorResult err  = governor_create(sensor_descriptors, SENSOR_COUNT, &governor);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
  governor_start(governor);

  // when
  vTaskDelay(pdMS_TO_TICKS(150));
  governor_stop_and_wait(governor);
  governor_delete(governor);

  // then
  // TODO: how to check memory leak
}

TEST_CASE("sensor_send_request_from_isr should call read", TEST_TAG)
{
  // given
  MockSensor sensor = MockSensor_zero;
  enum N { SENSOR_COUNT = 1 };
  SensorDescriptor sensor_descriptors[SENSOR_COUNT] = {
    MockSensor_descriptor(1, &sensor)
  };

  GovernorHandle governor;
  ErrorResult err  = governor_create(sensor_descriptors, SENSOR_COUNT, &governor);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
  governor_start(governor);

  // when
  MockSensorMeasurement measurement;
  measurement.u.type1 = 64;

  Measurement *to_send = mock_create_measurement(&sensor, MOCK_MSG1, measurement);
  sensor.next_read_measurement = to_send;
  
  sensor_send_request_from_isr(sensor.handle, sensor.id);

  // then
  Measurement *read_back = governor_read_measurement(governor, pdMS_TO_TICKS(50));
  TEST_ASSERT_EQUAL_MEMORY(to_send, read_back, MOCK_MESSAGE_SIZE);
  
  TEST_ASSERT_TRUE(sensor.read_called);
  TEST_ASSERT_TRUE(sensor.read_return_called);

  governor_stop_and_wait(governor);
  governor_delete(governor);
}

TEST_CASE("sensor_send_request_from_isr should not call read_return if message is NULL", TEST_TAG)
{
  // given
  MockSensor sensor = MockSensor_zero;
  enum N { SENSOR_COUNT = 1 };
  SensorDescriptor sensor_descriptors[SENSOR_COUNT] = {
    MockSensor_descriptor(1, &sensor)
  };
  sensor.next_read_measurement = NULL;

  GovernorHandle governor;
  ErrorResult err  = governor_create(sensor_descriptors, SENSOR_COUNT, &governor);
  TEST_ASSERT_EQUAL(AV_OK, err.code);
  governor_start(governor);

  // when
  sensor_send_request_from_isr(sensor.handle, sensor.id);
  vTaskDelay(pdMS_TO_TICKS(1));

  // then
  TEST_ASSERT_TRUE(sensor.read_called);
  TEST_ASSERT_FALSE(sensor.read_return_called);

  governor_stop_and_wait(governor);
  governor_delete(governor);
}

#ifndef SENSOR_API_H
#define SENSOR_API_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#define MEASUREMENT_TYPE(base, type) ((base << 8) | (type))
#define DECODE_MEASUREMENT_BASE_TYPE(type) ((type) >> 8)
#define DECODE_MEASUREMENT_TYPE(type) ((type) & 0xFF)

typedef int64_t SensorTime;
typedef int8_t SensorId;
typedef int8_t MessageTypeBase;
typedef int16_t MessageType;

typedef struct {
  SensorTime time;
  SensorId sensor_id;
  MessageType message_type;
  size_t size;
  char data[];
} Measurement;

inline void set_measurement_header(Measurement *measurement,
                               SensorId sensor_id,
                               SensorTime time,
                               MessageType message_type,
                               size_t payload_size) {

  measurement->sensor_id = sensor_id;
  measurement->time = time;
  measurement->message_type = message_type;
  measurement->size = payload_size;
}

typedef void* SensorApiHandle;

typedef bool (SensorInit) (const SensorApiHandle sensor_api, const SensorId sensor_id, void *arg);
typedef void (SensorDeinit) (void *arg);
typedef bool (SensorStart) (void *arg);
typedef void (SensorStop) (void *arg);
typedef Measurement* (SensorRead) (void *arg);
typedef void (SensorReadReturn) (Measurement *returned, void *arg);

typedef struct {
  /**
   * The function which the governor calls to initialize the sensor.
   */
  SensorInit *init;

  /**
   * The function which the governor calls to deinitialize / disable the sensor.
   */
  SensorDeinit *deinit;
  /**
   * The function which the governor calls to start the sensor. After the sensor has been started,
   * it can send data.
   */
  SensorStart *start;
  /**
   * Stops the sensor and wait it to finish stopping.
   */
  SensorStop *stop_and_wait;
  /**
   * Read the next measurement from the sensor. The governor calls read_return after processed it.
   */
  SensorRead *read;
  /**
   * The governor calls when the sensor free to free resources for the measurement.
   */
  SensorReadReturn *read_return;
} Sensor;

typedef struct {
  SensorId id;
  Sensor sensor;
  void *arg;
} SensorDescriptor;

#ifdef __cplusplus
extern "C" {
#endif

void sensor_send_measurement(SensorApiHandle api, Measurement *measurement);
void sensor_send_request_from_isr(SensorApiHandle api, SensorId id);
void sensor_send_request(SensorApiHandle api, SensorId id);
SensorTime sensor_get_time();

#ifdef __cplusplus
}
#endif

#endif

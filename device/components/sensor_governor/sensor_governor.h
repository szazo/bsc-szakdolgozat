#ifndef SENSOR_GOVERNOR_H
#define SENSOR_GOVERNOR_H

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"
#include "freertos/ringbuf.h"

#include "error.h"
#include "sensor_api.h"

AV_ERROR_DEFINE(SENSOR_GOVERNOR_CREATE_RING_ERROR, 0x0201)
AV_ERROR_DEFINE(SENSOR_GOVERNOR_CREATE_QUEUE_ERROR, 0x0202)
AV_ERROR_DEFINE(SENSOR_GOVERNOR_CREATE_SENSOR_INIT_ERROR, 0x0203)
AV_ERROR_DEFINE(SENSOR_GOVERNOR_SEND_REQUEST_QUEUE_ERROR, 0x0204)
AV_ERROR_DEFINE(SENSOR_GOVERNOR_SEND_REQUEST_FROM_ISR_QUEUE_ERROR, 0x0205)
AV_ERROR_DEFINE(SENSOR_GOVERNOR_READ_MISSING, 0x0206)
AV_ERROR_DEFINE(SENSOR_GOVERNOR_READ_RETURN_MISSING, 0x0207)
AV_ERROR_DEFINE(SENSOR_GOVERNOR_SEND_RING_ERROR, 0x0208)
AV_ERROR_DEFINE(SENSOR_GOVERNOR_START_ERROR, 0x0209)
AV_ERROR_DEFINE(SENSOR_GOVERNOR_SENSOR_START_ERROR, 0x020A)

typedef void* GovernorHandle;

#define MAX_SENSOR_COUNT 10

typedef struct {
  /**
   * When a sensor requests a measurement read (from e.g. ISR), this will store the sensor ids.
   */
  QueueHandle_t sensor_request_queue;

  /**
   * This task reads the actual measurements from sensors which request it using sensor_request_queue
   */
  TaskHandle_t sensor_task_handle;

  /**
   * This ring buffer will contain the actual measurements.
   */
  RingbufHandle_t sensor_ring;

  /**
   * Array of sensor descriptors.
   */
  SensorDescriptor sensors[MAX_SENSOR_COUNT];

  /**
   * The total number of sensors.
   */
  int sensor_count;

  /**
   * Whether the sensor task successfully stopped.
   */
  EventGroupHandle_t stopped_event_group;
  
} SensorGovernor;

ErrorResult governor_create(SensorDescriptor *sensors, int count, GovernorHandle *handle);
void governor_delete(GovernorHandle governor);
ErrorResult governor_start(GovernorHandle handle);
void governor_stop_and_wait(GovernorHandle handle);

Measurement *governor_read_measurement(GovernorHandle governor, TickType_t ticks_to_wait);
void governor_return_measurement(GovernorHandle governor, Measurement *measurement);

#endif

#include "stoppable_task.h"

static void task_function(void* args);

ErrorResult task_create(Task *me) {
  ErrorResult stop_control_err = stop_control_create(&me->stop_control);
  if (stop_control_err.code != AV_OK) {
    return AV_ERROR_WITH_REASON(STOPPABLE_TASK_CREATE_STOP_CONTROL_ERROR, stop_control_err.code);
  }

  return AV_OK_RESULT;
}

ErrorResult task_delete(Task *me) {
  stop_control_delete(&me->stop_control);
  return AV_OK_RESULT;
}

ErrorResult task_start(Task *me, TaskConfig *config) {

  stop_control_reset(&me->stop_control);
  me->task_function = config->task_function;
  me->task_param = config->task_param;
  BaseType_t task_created = xTaskCreatePinnedToCore(task_function, config->name,
                                                    config->stack_depth, (void*) me,
                                                    config->priority, &(me->task_handle),
                                                    config->core_id);
  if (task_created != pdPASS) {
    return AV_ERROR(STOPPABLE_TASK_START_TASK_CREATION_ERROR);
  }
  
  return AV_OK_RESULT;
}

bool task_is_stop_requested(TaskHandle task_handle) {
  Task *me = (Task *) task_handle;
  return stop_control_is_stop_requested(&me->stop_control);
}

static void task_function(void *arg) {
  Task *me = arg;

  // call underlying task function
  me->task_function(me->task_param, (TaskHandle) me);

  stop_control_set_as_stopped(&me->stop_control);
  vTaskDelete(NULL);  
}

void task_stop(Task *me) {
  stop_control_stop(&me->stop_control);
}

void task_stop_and_wait(Task *me) {
  stop_control_stop_and_wait(&me->stop_control);
}

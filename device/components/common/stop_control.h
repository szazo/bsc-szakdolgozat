#ifndef STOP_CONTROL_H
#define STOP_CONTROL_H

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"

#include "error.h"

AV_ERROR_DEFINE(STOP_CONTROL_CREATE_OUT_OF_MEMORY, 0x0101)

typedef struct {
  volatile bool stopped;
  EventGroupHandle_t stopped_event_group;
} StopControl;

ErrorResult stop_control_create(StopControl *stop_control);
void stop_control_stop(StopControl *stop_control);
void stop_control_stop_and_wait(StopControl *stop_control);
bool stop_control_is_stop_requested(StopControl *stop_control);
void stop_control_set_as_stopped(StopControl *stop_control);
void stop_control_delete(StopControl *stop_control);
void stop_control_reset(StopControl *stop_control);

#endif

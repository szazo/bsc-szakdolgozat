#ifndef STOPPABLE_TASK_H
#define STOPPABLE_TASK_H

#include "stop_control.h"

#define STOPPABLE_TASK_ERR_BASE 0x0100
AV_ERROR_DEFINE(STOPPABLE_TASK_CREATE_STOP_CONTROL_ERROR, 0x0111)
AV_ERROR_DEFINE(STOPPABLE_TASK_START_TASK_CREATION_ERROR, 0x0112)

typedef enum {
  APPLICATION_CPU = APP_CPU_NUM,
  PROTOCOL_CPU = PRO_CPU_NUM
} TaskCPU;

struct Task;
typedef struct Task* TaskHandle;
typedef void (TaskFunction) (void *arg, TaskHandle task_handle);

typedef struct {
  const char* name;
  UBaseType_t priority;
  TaskCPU core_id;
  uint32_t stack_depth;

  TaskFunction *task_function;
  void *task_param;
  
} TaskConfig;

typedef struct {
  TaskHandle_t task_handle;
  StopControl stop_control;

  TaskFunction *task_function;
  void *task_param;
} Task;

bool task_is_stop_requested(TaskHandle task_handle);

ErrorResult task_create(Task *me);
ErrorResult task_delete(Task *me);
ErrorResult task_start(Task *me, TaskConfig *config);
void task_stop(Task *me);
void task_stop_and_wait(Task *me);

#endif

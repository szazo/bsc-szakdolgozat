#include "unity.h"
#include "stoppable_task.h"

#define TEST_TAG "[stoppable_task]"

static bool task_started = false;
static bool task_stopped = false;
static char *task_param_set = NULL;

static void test_task_function(void *arg, TaskHandle task) {
  task_started = true;
  task_param_set = arg;
  while (!task_is_stop_requested(task)) {
    vTaskDelay(pdMS_TO_TICKS(10));
  }
  task_stopped = true;
}

TEST_CASE("should_create_start_stop_and_delete", TEST_TAG)
{
  // given
  char *param = "param";
  
  TaskConfig config = {
    .name = "test_task",
    .priority = 5,
    .core_id = APPLICATION_CPU,
    .stack_depth = 2048,
    .task_function = test_task_function,
    .task_param = param
  };
  
  Task task;
  ErrorResult err = task_create(&task);
  TEST_ASSERT_EQUAL(AV_OK, err.code);

  // when
  err = task_start(&task, &config);
  TEST_ASSERT_EQUAL(AV_OK, err.code);

  // then
  vTaskDelay(pdMS_TO_TICKS(30));
  TEST_ASSERT_EQUAL_STRING("param", task_param_set);
  TEST_ASSERT_TRUE(task_started);
  TEST_ASSERT_FALSE(task_stopped);

  // when
  task_stop_and_wait(&task);

  // then
  vTaskDelay(pdMS_TO_TICKS(30));
  TEST_ASSERT_TRUE(task_started);
  TEST_ASSERT_TRUE(task_stopped);

  task_delete(&task);
}

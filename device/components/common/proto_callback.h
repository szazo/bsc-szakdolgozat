#ifndef PROTO_CALLBACK_H
#define PROTO_CALLBACK_H

#include "pb_common.h"
#include "pb_decode.h"
#include "pb_encode.h"


bool proto_write_file_callback(pb_ostream_t *stream, const uint8_t *buf, size_t count);
bool proto_read_file_callback(pb_istream_t *stream, uint8_t *buf, size_t count);
pb_istream_t proto_istream_from_file(FILE *file);
  
#endif

#include "esp_log.h"
#include "error.h"

static const char *TAG = "FAIL_OBSERVER";

static char *severity_to_string(ErrorSeverity severity) {
  switch (severity) {
  case SEVERITY_INFO:
    return "info";
  case SEVERITY_WARNING:
    return "warning";
  case SEVERITY_ERROR:
    return "error";
  case SEVERITY_FAILURE:
    return "failure";
  default:
    return "unknown";
  }
}

static esp_log_level_t log_level(ErrorSeverity severity) {
  switch (severity) {
  case SEVERITY_INFO:
    return ESP_LOG_INFO;
  case SEVERITY_WARNING:
    return ESP_LOG_WARN;
  case SEVERITY_ERROR:
    return ESP_LOG_ERROR;
  case SEVERITY_FAILURE:
    return ESP_LOG_ERROR;
  default:
    return ESP_LOG_ERROR;
  }
}

void av_error_publish_result(ErrorSeverity severity, ErrorResult result) {
  if (result.reason == 0) {
    av_error_publish(severity, result.code);
  } else {
    av_error_publish_with_reason(severity, result.code, result.reason);
  }
}

// TODO: create separate task for printing logging errors
void av_error_publish_internal(ErrorSeverity severity, int code, char *info) {
  ets_printf("FAIL_OBSERVER: %s: 0x%x (%s)\n", severity_to_string(severity), code, info);
  /* ESP_LOG_LEVEL_LOCAL(log_level(severity), TAG, "%s: 0x%x (%s)", severity_to_string(severity), code, info); */
}

void av_error_publish_with_reason_internal(ErrorSeverity severity, int code, int reason, char *info) {
  ets_printf("FAIL_OBSERVER: %s: 0x%x, reason: 0x%x (%s)\n", severity_to_string(severity), code, reason, info);
  /* ESP_LOG_LEVEL_LOCAL(log_level(severity), TAG, "%s: 0x%x, reason: %x (%s)", severity_to_string(severity), code, reason, info); */
}

#include "freertos/FreeRTOS.h"

#include "stop_control.h"

const static int STOPPED_BIT = BIT0;

ErrorResult stop_control_create(StopControl *stop_control) {

  stop_control->stopped = false;
  stop_control->stopped_event_group = xEventGroupCreate();
  if (stop_control->stopped_event_group == NULL) {
    return AV_ERROR(STOP_CONTROL_CREATE_OUT_OF_MEMORY);
  }

  return AV_OK_RESULT;
}

void stop_control_stop_and_wait(StopControl *stop_control) {
  stop_control_stop(stop_control);
  xEventGroupWaitBits(stop_control->stopped_event_group,
                      STOPPED_BIT,
                      false,
                      true,
                      portMAX_DELAY);
}

void stop_control_stop(StopControl *stop_control) {
  stop_control->stopped = true;
}

void stop_control_delete(StopControl *stop_control) {
  vEventGroupDelete(stop_control->stopped_event_group);
}

bool stop_control_is_stop_requested(StopControl *stop_control) {
  return stop_control->stopped;
}

void stop_control_set_as_stopped(StopControl *stop_control) {
  xEventGroupSetBits(stop_control->stopped_event_group, STOPPED_BIT);
}

void stop_control_reset(StopControl *stop_control) {
  stop_control->stopped = false;
  xEventGroupClearBits(stop_control->stopped_event_group, STOPPED_BIT);
}

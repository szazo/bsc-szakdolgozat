#include <stdio.h>
#include "proto_callback.h"
#include "pb_decode.h"

pb_istream_t proto_istream_from_file(FILE *file) {
  return (pb_istream_t){
    .callback = &proto_read_file_callback,
    .state = file,
    .bytes_left = SIZE_MAX,
    .errmsg = 0
  };
}

bool proto_write_file_callback(pb_ostream_t *stream, const uint8_t *buf, size_t count) {
 FILE *file = (FILE*) stream->state;
 int written = fwrite(buf, 1, count, file);
 return written == count;
}

bool proto_read_file_callback(pb_istream_t *stream, uint8_t *buf, size_t count)
{
  FILE *file = (FILE*)stream->state;
  bool status;

  if (buf == NULL)
    {
      while (count-- && fgetc(file) != EOF);
      return count == 0;
    }

  status = (fread(buf, 1, count, file) == count);

  if (feof(file)) {
    stream->bytes_left = 0;
  }

  return status;
}

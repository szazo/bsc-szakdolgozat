#ifndef ERROR_H
#define ERROR_H

typedef struct {
  int code;
  int reason;
} ErrorResult;

#define AV_ERROR_DEFINE(id, code) static const int id = code; static const char DUPL_TEST_##code = 0;

#define AV_OK (0)
#define AV_ERROR_WITH_REASON(__code, __reason) ((ErrorResult) { .code = __code, .reason = __reason })
#define AV_ERROR(__code) ((ErrorResult) { .code = __code, .reason = 0 })
#define AV_OK_RESULT ((ErrorResult) { .code = AV_OK, .reason = 0 })

#define av_error_publish(severity, code) do { av_error_publish_internal(severity, code, #code); } while (0)
#define av_error_publish_with_reason(severity, code, reason) do { av_error_publish_with_reason_internal(severity, code, reason, #code); } while (0)

//#define AV_ERROR_PUBLISH(__code) (ESP_LOGE("FAIL_OBSERVER", "Invalid phase while stopping");)

typedef enum {
  SEVERITY_DEBUG,
  SEVERITY_INFO,
  SEVERITY_WARNING,
  SEVERITY_ERROR,  
  SEVERITY_FAILURE
} ErrorSeverity;

void av_error_publish_result(ErrorSeverity severity, ErrorResult result);
void av_error_publish_internal(ErrorSeverity severity, int code, char *info);
void av_error_publish_with_reason_internal(ErrorSeverity severity, int code, int reason, char *info);

#endif

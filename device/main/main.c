#include "esp_log.h"

#include "bootstrap.h"
#include "wifi.h"

#include "peripheral_module.h"
#include "storage_module.h"
#include "container.h"
#include "view_module.h"
#include "input_scanner_module.h"
#include "message_queue_module.h"
#include "sync_module.h"
#include "main_controller_module.h"
#include "measurement_loop_module.h"
#include "sensor_governor_module.h"
#include "telemetry_writer_module.h"
#include "time_sync_module.h"
#include "live_broadcast_module.h"

static const char *TAG = "main";

static ErrorResult start_wifi();
static void stop_wifi();

static void handle_startup_error(ErrorResult err) {
  av_error_publish_with_reason(SEVERITY_FAILURE, err.code, err.reason);
  ESP_LOGE(TAG, "Unhandled error occurred at startup, restarting...");
  esp_restart();
}

enum MODULE_COUNT { MODULE_COUNT = 12 };
static Module modules[MODULE_COUNT] = {
    PERIPHERAL_MODULE(),
    STORAGE_MODULE(),
    MESSAGE_QUEUE_MODULE(),
    SYNC_MODULE(),
    INPUT_SCANNER_MODULE(),
    VIEW_MODULE(),
    SENSOR_GOVERNOR_MODULE(),
    MEASUREMENT_LOOP_MODULE(),
    TIME_SYNC_MODULE(),
    TELEMETRY_WRITER_MODULE(),
    LIVE_BROADCAST_MODULE(),
    MAIN_CONTROLLER_MODULE()
  };
  
static Container container;

void app_main() {

  Bootstrap bootstrap;
  ErrorResult err = bootstrap_create(&bootstrap, &container, modules, MODULE_COUNT);
  if (err.code != AV_OK) { handle_startup_error(err); return; }

  err = bootstrap_create_modules(&bootstrap);
  if (err.code != AV_OK) { handle_startup_error(err); return; }

  ESP_LOGI(TAG, "Starting wifi...");
  WifiHandle wifi;
  err = start_wifi(container.message_queue, &wifi);
  if (err.code != AV_OK) {
    handle_startup_error(err);
    return;
  }
  err = bootstrap_start_modules(&bootstrap);
  if (err.code != AV_OK) { handle_startup_error(err); return; }
  
  vTaskDelay(portMAX_DELAY);

  ESP_LOGI(TAG, "Stopping wifi...");
  stop_wifi(wifi);

  ESP_LOGI(TAG, "Done");
}

static ErrorResult start_wifi(MessageQueueHandle app_loop, WifiHandle *wifi) {
  ErrorResult err = wifi_create(app_loop, wifi);
  if (err.code != AV_OK) {
    return err;
  }

  WifiStationSettings settings = {
    .ssid = CONFIG_WIFI_SSID,
    .password = CONFIG_WIFI_PASSWORD
  };
  err = wifi_init_station(&settings);
  if (err.code != AV_OK) {
    wifi_delete(*wifi);
    return err;
  }

  err = wifi_start(*wifi);
  if (err.code != AV_OK) {
    wifi_deinit(*wifi);
    wifi_delete(*wifi);

    return err;
  }

  return AV_OK_RESULT;
}

static void stop_wifi(WifiHandle wifi) {
  ErrorResult err = wifi_stop(wifi);
  if (err.code != AV_OK) {
    av_error_publish_result(SEVERITY_WARNING, err);
  }

  err = wifi_deinit(wifi);
  if (err.code != AV_OK) {
    av_error_publish_result(SEVERITY_WARNING, err);
  }

  wifi_delete(wifi);
}
